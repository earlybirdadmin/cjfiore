<?php
require_once('functions.php');
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>About Us - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraHead(); ?>
</head>
<body>
<?php makeHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" style="padding: 10px 10px 0px 20px;">
<p class="firstLevel"><span style="color: #da771b; font-size: 20px; line-height: 1.3;">Fiore Nursery and Landscape Supply has been a trusted partner and supplier to the Green Industry for over 90 years,</span> <span>providing the Chicagoland area with the area’s largest selection of premium grade plant material with over 1,100 varieties. We now also offer natural stone and landscape supply products at both of our locations, sourced from some of the best suppliers in the Midwest and available for immediate pick up or delivery.</span></p>

<p class="firstLevel"><span>At both our Prairie View and Chicago sales yard, you will find the same extensive selection of premium plants, natural stone, and landscape materials, combined with the superior customer service you’ve come to expect from Fiore. At Fiore, we remain committed to offering our customers the highest quality products available, along with our top-notch expert advice and consultation.</span></p>

<p class="firstLevel"><span style="color: #da771b; font-size: 20px;">Come visit us!</span></p>
		</td>
		<td><img src="images/spacer.gif" width="1" height="520" border="0"></td>
	</tr>
	<tr>
		<td><img src="images/spacer.gif" width="480" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="500" height="1" border="0"></td>
	</tr>
</table>

<?php makeFooter(); ?>

</body>
</html>
