<?php
$staff = array (
	array (
    'name' => 'CJ Fiore',
    'title' => 'Co-Founder and Owner',
    'specialty' => 'Horticulturist',
    'email' => 'cj@cjfiore.com',
    'phone' => '',
  ),


  array (
    'name' => 'Mark Fiore',
    'title' => 'Co-founder and Owner',
    'specialty' => 'Horticulturist',
    'email' => 'mark@cjfiore.com',
    'phone' => '',
  ),
  array (
    'name' => 'Lisa A. Fiore',
    'title' => 'President',
    'specialty' => 'Herbaceous Plant Specialist',
    'email' => 'lisa@cjfiore.com',
    'phone' => '847-514-0892',
  ),
  array (
    'name' => 'David M. Fiore',
    'title' => 'Vice President and General Manager - Chicago Location',
    'specialty' => 'Horticulturist',
    'email' => 'dave@cjfiore.com',
    'phone' => '847-514-2453',
  ),
  array (
    'name' => 'Bill Dahlgren',
    'title' => '1954-2014',
  ),
  array (
    'name' => 'Tim Robbins',
    'title' => 'Sales Associate',
    'specialty' => 'Landscape Architect and Natural Stone Specialist',
    'email' => 'timrobbins@cjfiore.com',
    'phone' => '847-997-8752',
  ),

  array (
    'name' => 'Bill Hope',
    'title' => 'Business Development',
    'specialty' => '',
    'email' => 'billhope@cjfiore.com',
    'phone' => '708-600-2460',
  ),

  array (
    'name' => 'Vicky Brice',
    'title' => 'Controller',
    'specialty' => '',
    'email' => 'vicky@cjfiore.com',
    'phone' => '',
  ),

  array (
    'name' => 'Will Haverkamp',
    'title' => 'Sales Account Manager',
    'email' => 'willhaverkamp@cjfiore.com',
    'phone' => '847-346-7574',
  ),
  
  array (
    'name' => 'Alex Head',
    'title' => 'Product Manager - Green Goods',
    'specialty' => 'Horticulturist and Arborist',
    'email' => 'alex@cjfiore.com',
    'phone' => '847-514-6079',
  ),

  array (
    'name' => 'Cate Bertrand',
    'title' => 'Sales Associate',
    'specialty' => 'Horticulturist and Annual and Tropical Expert',
    'email' => 'catebertrand@cjfiore.com',
    'phone' => '847-514-6129',
  ),
  array (
    'name' => 'Amy Fiore',
    'title' => 'Sales Associate',
    'specialty' => 'Annual and Tropical Expert',
    'email' => 'amy@cjfiore.com',
    'phone' => '847-514-1968',
  ),
  array (
    'name' => 'Manuel Ochoa',
    'title' => 'Chicago Yard Manager',
    'specialty' => '',
    'email' => '',
    'phone' => '',
  ),
  array (
    'name' => 'David Flores',
    'title' => 'Inventory Leader',
    'specialty' => '',
    'email' => '',
    'phone' => '',
  ),
  array (
    'name' => 'Jorge Garcia',
    'title' => 'Inventory Manager',
    'specialty' => '',
    'email' => 'jorgegarcia@cjfiore.com',
    'phone' => '847-561-0509',
  ),

  array (
    'name' => 'Bulmaro Velazquez',
    'title' => 'Salem Lake Yard Manager',
    'specialty' => '',
    'email' => 'tagging@cjfiore.com',
    'phone' => '847-514-2455',
  ),

  array (
    'name' => 'Brian Henning',
    'title' => 'Sales Account Manager',
    'specialty' => '',
    'email' => 'brianhenning@cjfiore.com',
    'phone' => '847-732-0099',
  ),

  array (
    'name' => 'Michael Kwiatek',
    'title' => 'Herbaceous & Quality Control Supervisor',
    'specialty' => '',
    'email' => 'michaelkwiatek@cjfiore.com',
    'phone' => '847-370-7715',
  ),

  array (
    'name' => 'Mike Duttge',
    'title' => 'Branch Manager - Chicago',
    'specialty' => 'Annual, Perennial and Tropical Specialist',
    'email' => 'mikeduttge@cjfiore.com',
    'phone' => '847-514-6118',
  ),

  array (
    'name' => 'Gail Marik',
    'title' => 'Customer Service/ Inside Sales Associate',
    'email' => 'gailmarik@cjfiore.com',
    'phone' => '224-501-1866',
  ),

  array (
    'name' => 'Ed Rockhill',
    'title' => 'Territory Manager- Indiana',
    'email' => 'edrockhill@cjfiore.com',
    'phone' => '317-460-5763',
  ),

  array (
    'name' => 'Cara Fiore Furlong',
    'title' => 'Customer Service Associate',
    'email' => 'carafurlong@cjfiore.com',
  ),


  array (
    'name' => 'Emily Stuart',
    'title' => 'Horticulturalist and Sales Associate – Chicago',
    'phone' => '847-514-2458',
  ),



  array (
    'name' => 'David A. Fiore',
    'title' => 'Prairie View Yard Manager',
    'email' => 'davidanthonyfiore@cjfiore.com',
    'phone' => '847-812-4413',
  ),


	array (
    'name' => 'Alberto Valdes',
    'title' => 'Bookkeeper',
    'specialty' => '',
    'email' => 'orders@cjfiore.com',
    'phone' => '',
  ),

  array (
    'name' => 'Zack Sargent',
    'title' => 'Operations Associate',
    'specialty' => '',
    'email' => 'zachsargent@cjfiore.com',
    'phone' => '847-714-6099',
  ),

  array (
    'name' => 'Rob Yanney',
    'title' => 'Operations Manager – Indiana',
    'specialty' => '',
    'email' => 'robyanney@cjfiore.com',
    'phone' => '317-560-7657',
  ),

  array (
    'name' => 'Sarah Bottner',
    'title' => 'Sales Account Manager',
    'specialty' => '',
    'email' => 'sarahbottner@cjfiore.com',
    'phone' => '847-651-5834',
  ),

  array (
    'name' => 'Jake Bell',
    'title' => 'Customer Service Associate - Chicago',
    'specialty' => '',
    'email' => 'jakebell@cjfiore.com',
    'phone' => '847-514-2460',
  ),

  array (
    'name' => 'Brian Dulian',
    'title' => 'Dispatch Supervisor – Prairie View',
    'specialty' => '',
    'email' => 'briandulian@cjfiore.com',
    'phone' => '847-514-6453',
  ),

  array (
    'name' => 'Tyler Klivickis',
    'title' => 'Operations Associate - Chicago',
    'specialty' => '',
    'email' => 'tylerklivickis@cjfiore.com',
    'phone' => '847-513-2655',
  ),

  array (
    'name' => 'Ken Varner',
    'title' => 'Dispatch, Fleet and Facilities Manager',
    'specialty' => '',
    'email' => 'kenvarner@cjfiore.com',
    'phone' => '847-650-5854',
  ),
);