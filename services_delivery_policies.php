<?php
require_once('functions.php');
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Delivery Policies - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraHead(); ?>
</head>
<body>
<?php makeHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td><img src="images/spacer.gif" width="560" height="20" border="0"></td>
		<td><img src="images/spacer.gif" width="45" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="332" height="1" border="0"></td>
	</tr>
	<tr>
		<td valign="top" align="left" style="font-size: 13px;">
<h4><span>Delivery Policies</span></h4>
<br><span>Delivery charges are quoted for curbside drop off only. Delivery rates include services of truck and professional driver. Please request if additional labor or equipment is required. Additional charges may apply. If our truck is required to enter onto private property to unload plants, the customer will be responsible for any damage to the property. It is also the customer’s responsibility to provide adequate labor and equipment to unload the truck in a timely manner.</span>
<br>
<br><span>We will make every attempt to make deliveries as per customer’s request, however, all deliveries are subject to truck availability and prior schedules. All deliveries requiring a tractor are subject to truck and tractor availability and prior schedules. Fiore Nursery and Landscape Supply is not responsible for any damage done to property.</span>
<br>
<br><span><b>Delivery Cancellation Policy</b><br>All cancellations or changes to requested delivery date must be made at least 24 hours prior to scheduled time of delivery. A fee may be charged for orders cancelled or changed after a truck has been loaded. All deliveries depart as scheduled, rain or shine.</span>
<br>
<br><span><b>Pickups</b><br>No minimum order is necessary for material which is picked up at our yard. Same day pickup available</span>


<div class="leafItOnTop" style="top: 245px; left: 650px;"><img src="images/leafGreen.png" width="164" height="168" border="0"></div>

		</td>
		<td valign="top" align="center"><img src="images/spacer.gif" width="1" height="100%" border="0" style="width: 1px; height: 100%; background: #4e4244;"></td>
		<td valign="top" align="left" style="font-size: 13px; padding-bottom: 700px;">
<h4>&nbsp;</h4>
		</td>
	</tr>
	<tr>
		<td><img src="images/spacer.gif" width="560" height="20" border="0"></td>
		<td><img src="images/spacer.gif" width="45" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="332" height="1" border="0"></td>
	</tr>
</table>

<?php makeFooter(); ?>

</body>
</html>
