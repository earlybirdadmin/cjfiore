<?php
require_once('functions.php');

if (isset($_SESSION['user'])) {
	if (isWholesale()) {
		if (isset($_GET['t']) && $_GET['t'] == 'excel') {
			// output it as a file download
			$c = file_get_contents('pdf/2016_Fiore Nursery Estimated Price Book.xlsx');
			header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment; filename="Fiore Nursery Estimated Price Book.xlsx"');
			echo $c;
			die();
		} else {
			// output it as a file download
			$c = file_get_contents('pdf/2016_Fiore Nursery Estimated Price Book.pdf');
			header('Content-type: application/pdf');
			header('Content-Disposition: attachment; filename="Fiore Nursery Estimated Price Book.pdf"');
			echo $c;
			die();
		}
	}
}

header('Location: http://www.cjfiore.com/');
exit();