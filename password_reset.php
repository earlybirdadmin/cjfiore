<?php
require_once('functions.php');

$resetted = false;
// validate email address form
if (isset($_POST['filledOut']) && $_POST['filledOut'] == 'yup' && isset($_POST['e'])) {
	$atPOS = strpos($_POST['e'], '@');
	$dotPOS = strpos($_POST['e'], '.', $atPOS);
	if (!$atPOS || !$dotPOS || strpos($_POST['e'], ' ') !== false && strpos($_POST['e'], ' ') === false) {
		$err1 = 'Please enter a valid email address.';
		$_POST['e'] = '';
		$onload = ' onload="document.getElementById(\'e\').focus();"';
	}

	if (!isset($err1)) {
		$query = 'SELECT * FROM `UserLogin` WHERE email="'.mysql_real_escape_string($_POST['e']).'"';
		$result = mysql_query($query);
		if (mysql_num_rows($result) == 0) {
			$err1 = 'There is no user account set up with that email address.<br>Feel free to <a href="login.php">Create a new account</a> with that email address.';
		} else {
			// get the row
			$row = mysql_fetch_array($result);
			// create a new password
			$seed = str_split('abcdefghjkmnpqrstuvwxyz23456789'); // and any other characters
			shuffle($seed);
			$seed = array_slice($seed, 0, 6);
			$newPass = implode('', $seed);
			
			// update the database with the new password
			mysql_query('UPDATE `UserLogin` SET pass="'.mysql_real_escape_string(md5($newPass.'cjf')).'" WHERE user_id='.$row['user_id']);
			
			// email the password
			emailNewPasswordTo($newPass, $row);
			
			// set the resetted value
			$resetted = true;
			
		} // end if the email address exists
	} // end if isset($err1)

}


?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Password Reset - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraHead(); ?>
</head>
<body>
<?php makeHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" style="padding: 20px 10px 0px 20px;">
<h1>Password Reset</h1>
<br>
<?php
if (isset($err1)) {
	echo '<span style="color: #ff0000;">'.$err1.'</span>';
}

if (!$resetted) {
?>
<form method="POST" action="">
Please enter your email address below:
<br><input type="text" name="e" id="e" style="width: 200px;">
<br><script language="javascript">
var tmpVal = 'yup';
document.write('<input type="hidden" name="filledOut" value="'+tmpVal+'">');
</script>
<br><input type="submit" value="submit">
<?php
} else {
// password WAS reset
 	echo 'Your password was successfully reset!<br><br>A new password was sent to:<br>'.$_POST['e'];
}
?>
		</td>
	</tr>
</table>

<?php makeFooter(); ?>

</body>
</html>
<?php
function emailNewPasswordTo($newPass, $loginArr) {
     $html = "We have successfully reset your password for www.cjfiore.com.<br><br>For the username:<br><span style=\"font: 16px Monaco, Courier, sans-serif;\">".$loginArr['user']."</span><br>Your new password is:<br><span style=\"font: 16px Monaco, Courier, sans-serif;\">".$newPass."</span><br><br>You can access your account and change the password via<br><a href=\"http://www.cjfiore.com/user_account.php\">http://www.cjfiore.com/user_account.php</a><br><br>Enjoy!";
     
     sendPearMailToWithSubjectAndMessage($loginArr['email'], 'Your New Password for CJFiore.com', $html);
     

}