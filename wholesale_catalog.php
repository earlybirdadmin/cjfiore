<?php
require_once('functions.php');

// pdf location
$pdf = 'pdf/2013 Wholesale Catalog.pdf';

//pricelist for some stupid reason
/*
if (isset($_POST['pass']) && strtolower($_POST['pass']) == 'pricelist') {
	header('Content-Type: application/pdf');
	readfile($pdf);
	exit();
}
//pricelist for some stupid reason
if (isset($_POST['user']) && strtolower($_POST['user']) == 'pricelist') {
	header('Content-Type: application/pdf');
	readfile($pdf);
	exit();
}
*/

// if the user is logged in and has wholesale access
if (isset($_SESSION['user']['is_wholesale']) && $_SESSION['user']['is_wholesale'] == 1) {
	header('Content-Type: application/pdf');
	readfile($pdf);
	exit();
} else if (isset($_SESSION['user']['is_wholesale']) && $_SESSION['user']['is_wholesale'] == 0) {
	$err = 'Sorry, your account doesn\'t have access to wholesale prices.  Feel free to <a href="contact.php">Contact Us</a> if you feel this is an error.';
}

if (isset($_POST['pass']) && isset($_POST['user']) && !isset($err)) {
	$result = mysql_query('SELECT * FROM `UserLogin` WHERE pass="'.mysql_real_escape_string(md5($_POST['pass'].'cjf')).'"');
	if (mysql_num_rows($result) > 0) {
		for ($i=0;$i<mysql_num_rows($result);$i++) {
			$row = mysql_fetch_array($result);
			if (strtolower($_POST['user']) == strtolower($row['user']) || strtolower($_POST['user']) == strtolower($row['email'])) {
				logInUserWithID($row['user_id']);
				if ($_SESSION['user']['is_wholesale'] == 1) {
					header('Content-Type: application/pdf');
					readfile($pdf);
					exit();
				} else {
					$err = 'Sorry, that account doesn\'t have access to wholesale prices.  Feel free to <a href="contact.php">Contact Us</a> if you feel this is an error.<br><br>You can also <a href="password_reset.php">Reset your Password</a> in case you forgot it.';
				}
			}
		}
	}
	
	if (!isset($err))
		$err = 'Sorry, that is not a valid username and/or password.<br><br>Feel free to <a href="password_reset.php">Reset your Password</a> in case you forgot it.';
}

?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Wholesale Catalog - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraHead(); ?>
</head>
<body onload="document.getElementById('user').focus();">
<?php makeHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" style="padding: 20px 10px 0px 20px;">
<h1>Wholesale Catalog</h1>

<form method="POST" action="">
<p>Please log in
<br>to access our wholesale catalog:
<?php
if (isset($err)) {
	echo '<br><span style="color: #ff0000;">'.$err.'</span>';
}
?>
<br>
<br>Username or Email:
<br><input type="text" name="user" id="user">
<br>Password
<br><input type="password" name="pass" id="pass">
<br><input type="image" src="images/btnSubmit.png" width="75" height="22" border="0">
</p>
</form>
		</td>
	</tr>
</table>

<?php makeFooter(); ?>

</body>
</html>
