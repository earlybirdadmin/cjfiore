<?php
require_once('functions.php');

$onload = '';

// log in user 
if (isset($_POST['user']) && isset($_POST['pass']) && trim($_POST['user']) != '' && trim($_POST['pass']) != '') {
	$pass = md5($_POST['pass'].'cjf');
	$user = strtolower($_POST['user']);
	
	$result = mysql_query('SELECT * FROM `UserLogin` WHERE pass="'.mysql_real_escape_string($pass).'"');
	if (mysql_num_rows($result) == 0){
		$err0 = 'Incorrect username or password.  Please try again or <a href="password_reset.php">reset your password</a> to get access.';
	} else {
		for ($i=0;$i<mysql_num_rows($result);$i++) {
			$row = mysql_fetch_array($result);
			if ($user == strtolower($row['user']) || $user == strtolower($row['email'])) {
				// add user info to session
				$userArr = $row;
				
				logInUserWithID($userArr['user_id']);
				
			
				// bread the for loop
				break;
			} // end if user or email match
		} // end for $i
		
		if (!isset($_SESSION['user']))
			$err0 = 'Incorrect username or password.  Please try again or <a href="password_reset.php">reset your password</a> to get access.';
	} // end if mysql_num_rows > 0
} 
// end log in user



if (isset($_SESSION['user']) && PAGE != 'login.php') {
	header('Location: '.PAGE);
	exit();
}




// create account
if (isset($_POST['q0'])) {
	// set the index array
	$idxArr = array('User name', 'Password', 'Password Confirmation', 'Email address', 'Customer Type', 'First Name', 'Last Name', 'Business Name', 'Address', 'Telephone Number', 'Zip Code', 'Newsletter Preference');
	// set the required array
	$requireArr = array(0, 1, 2, 3, 4, 5, 6);
	
	// make sure all required fields are filled out
	for ($i=0;$i<count($requireArr);$i++) {
		$idx = $requireArr[$i];
		if (!isset($_POST['q'.$idx]) || trim($_POST['q'.$idx]) == '') {
			$err1 = 'You MUST fill out your '.$idxArr[$idx].' to create an account.';
			$onload = ' onload="document.getElementById(\'q'.$idx.'\').focus();"';
			break;
		}
	}
	
	// make sure passwords match
	if (!isset($err1)) {
		if ($_POST['q1'] != $_POST['q2']) {
			$err1 = 'Your passwords do not match.  Try typing them in again.';
			$_POST['q1'] = '';
			$_POST['q2'] = '';
			$onload = ' onload="document.getElementById(\'q1\').focus();"';
		}
	}
	
	// make sure it's a (somewhat) valid email address
	if (!isset($err1)) {
		$atPOS = strpos($_POST['q3'], '@');
		$dotPOS = strpos($_POST['q3'], '.', $atPOS);
		if (!$atPOS || !$dotPOS || strpos($_POST['q3'], ' ') !== false) {
			$err1 = 'Please enter a valid email address.';
			$_POST['q3'] = '';
			$onload = ' onload="document.getElementById(\'q3\').focus();"';
		}
	}
	
	
	// make sure they filled out the business name IF they checked business professional
	if (!isset($err1)) {
		if (trim($_POST['q7']) == '' && isset($_POST['q4']) && $_POST['q4'] == 'Green Industry Professional') {
			$err1 = 'You MUST fill out your Business Name to create a Business Account.';
			$onload = ' onload="document.getElementById(\'q7\').focus();"';
		}
	}

	
	
	// make sure the user name doesn't already exist
	if (!isset($err1)) {
		$result = mysql_query('SELECT * FROM `UserLogin` WHERE user="'.mysql_real_escape_string($_POST['q0']).'"');
		if (mysql_num_rows($result) > 0) {
				$err1 = 'That user name has already been used.  You can always <a href="password_reset.php">reset your password</a> if you can\'t remember it.';
				$_POST['q3'] = '';
				$onload = ' onload="document.getElementById(\'q3\').focus();"';
		}
	}
	
	// make sure the email address doesn't already exist
	if (!isset($err1)) {
		$result = mysql_query('SELECT * FROM `UserLogin` WHERE email="'.mysql_real_escape_string($_POST['q3']).'"');
		if (mysql_num_rows($result) > 0) {
				$err1 = 'That email address has already been used.  You can always <a href="password_reset.php">reset your password</a> if you can\'t remember it.';
				$_POST['q3'] = '';
				$onload = ' onload="document.getElementById(\'q3\').focus();"';
		}
	}
	
	$idxArr = array('User name', 'Password', 'Password Confirmation', 'Email address', 'Customer Type', 'First Name', 'Last Name', 'Business Name', 'Address', 'Telephone Number', 'Zip Code', 'Newsletter Preference');
	// if we're valid, add the user
	if (!isset($err1)) {
		// newsletter checkbox
		if (isset($_POST['q11']) && $_POST['q11'] == 'yup')
			$news = 1;
		else
			$news = 0;

		// weekly availability checkbox
		if (isset($_POST['q12']) && $_POST['q12'] == 'yup')
			$weeklyAvailability = 1;
		else
			$weeklyAvailability = 0;
		
		// customer type radio (business or home gardener)
		if (isset($_POST['q4']) && $_POST['q4'] == 'Green Industry Professional')
			$isBusiness = 1;
		else
			$isBusiness = 0;
			
		// city / state
		if (isset($_POST['q10']) && is_numeric(substr($_POST['q10'], 0, 5))) {
			$result = mysql_query('SELECT * FROM `zip_codes` WHERE zip_code='.substr($_POST['q10'], 0, 5));
			if ($result && mysql_num_rows($result) > 0) {
				$row = mysql_fetch_array($result);
				$city = ucfirst($row['city']);
				$state = $row['state'];
			} else {
				$city = '';
				$state = '';
			}
		} else {
			$city = '';
			$state = '';
		}
		
		// add to UserInfo
		$query = 'INSERT INTO `UserInfo` VALUES (NULL, "'.mysql_real_escape_string($_POST['q5']).'",' . // first name
														'"'.mysql_real_escape_string($_POST['q6']).'", ' . // last name
														'"'.mysql_real_escape_string($_POST['q7']).'", ' . // business name
														'"'.mysql_real_escape_string($_POST['q8']).'", ' . // address
														'"'.mysql_real_escape_string($city).'", ' . // city
														'"'.mysql_real_escape_string($state).'", ' . // state
														'"'.mysql_real_escape_string($_POST['q10']).'", ' . // zip
														'"'.mysql_real_escape_string($_POST['q3']).'", ' . // email
														'"'.mysql_real_escape_string($_POST['q9']).'", ' . // phone
														'0, '. // branch_id
														'0, '. // is_delivery
														$isBusiness.', '. // is_business
														$news.', '. // is_newsletter_subscriber
														$weeklyAvailability.', '. // is_weekly_availability_subscriber
														'0, '. // 
														date('U').', '
														.date('U').')';
		mysql_query($query);
		
		// add to UserLogin
		$user_id = mysql_insert_id();
		$query2 = 'INSERT INTO `UserLogin` VALUES ('.$user_id.', "'.mysql_real_escape_string($_POST['q0']).'",  "'.mysql_real_escape_string($_POST['q3']).'",  "'.mysql_real_escape_string(md5($_POST['q0'].'cjf')).'")';
		mysql_query($query2);
		
		// if it's a business, email the admin
		if ($isBusiness) {
			$html = '<b>New Business Account</b><br>For: '.$_POST['q7'].'.<br><br>To give them wholesale pricing, visit:<br><a href="http://www.cjfiore.com/update/wholesale_pricing.php?u='.$user_id.'">http://www.cjfiore.com/update/wholesale_pricing.php?u='.$user_id.'</a>.<br><br>Using this link will send an email to the business if you approve them for wholesale pricing.';
			
			sendPearMailToWithSubjectAndMessage(ADMIN_EMAIL, 'Business Account Created', $html);
		}
		
		// email whoever that they just created an account
		$html = '<h3>Thank your for creating an account with CJ Fiore!</h3><p>Your account information is:';
		$html .= '<br><span style="font: 16px Monaco, Courier, sans-serif">username:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$_POST['q0'].'</span>';
		$html .= '<br><span style="font: 16px Monaco, Courier, sans-serif">password:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$_POST['q1'].'</span>';
		if ($isBusiness) {
			// let them know about approval
			$html .= '<br><br>We will review your application and notify you about wholesale pricing.';
		}
		$html .= '<br><br>You can now sign in via:<br><a href="http://www.cjfiore.com/login.php">http://www.cjfiore.com/login.php</a></p>';
		
		sendPearMailToWithSubjectAndMessage($_POST['q3'], 'Your New CJ Fiore Account', $html);
		
		$newAccount = true;
		
		// finally, log in the user
		logInUserWithID($user_id);
	}
}
// end create account
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Sign In / Sign Up - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraHead(); ?>
</head>
<body<?php echo $onload; ?>>
<?php makeHeader(); ?>

<table width="940" cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td><img src="images/spacer.gif" width="200" height="10" border="0"></td>
		<td><img src="images/spacer.gif" width="180" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="125" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="15" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="420" height="1" border="0"></td>
	</tr>
	<tr>
<?php
if (isset($newAccount)) {
?>
	<td valign="top" align="left" colspan="2">
<h1>Thank you for registering. You will receive an email shortly confirming your account.</h1>
<div class="brownLinks">
<br>Feel free to <u><a href="products_plants.php">Browse Our Retail Catalog</a></u>.
</div>
	</td>
<?php
} else if (isset($_SESSION['user'])) {
?>
	<td valign="top" align="left" colspan="2">
<h1>You're Logged In!</h1>
<div class="brownLinks">
<br>Feel free to <u><a href="products_plants.php">Browse Our Catalog</a></u>, 
<br>
<br><u><a href="user_account.php">Change your Password</a></u>, or <u><a href="saved_orders.php">View your Order History</a></u>
</div>
	</td>
<?php
} else {
?>
		<td valign="top" align="left" style="padding-right: 30px;">
<h1>Sign In</h1>
<form method="POST" action="">
<div class="beigeBlock" style="font-size: 11px; padding: 10px; margin-top: 10px;">
<b>Returning Customer</b>
<!--
<br>We are currently updating the prices for our online catalog.
<br>
<br>Feel free to
<br> <b class="brownLinks"><a href="products_plants.php">browse without prices</a></b>
<br>or use download our
<br><b class="brownLinks"><a href="pdf/2015FioreWholesaleCatalog.pdf" target="_blank">PDF Catalog</a></b>
-->
<br><sup style="color: #da771b;">*</sup> User name or email address:
<br><input type="text" name="user" id="user">
<br><sup style="color: #da771b;">*</sup> Password:
<br><input type="password" name="pass" id="pass">
</div>
<input type="hidden" name="Submit" value="Enter">
<input type="image" src="images/sign_in.png" width="68" height="20" border="0" style="margin: 10px;">

</form>
<?php
if (isset($err0)) {
	echo '<span style="color: #ff0000; font-size: 13px;">'.$err0.'</span>';
}
?>
		</td>
		<td valign="top" align="left" style="padding-bottom: 20px;">
<h1>Sign Up</h1>
<?php
if (isset($err1)) {
	echo '<br><span style="color: #ff0000; font-size: 13px;">'.$err1.'</span>';
}
?>
<form method="POST" action="">
<input type="hidden" name="State" value="IL">
<div class="beigeBlock" style="font-size: 11px; padding: 10px; margin-top: 10px;">
<b>New Customers</b>
<br>Complete the following:
<br><sup style="color: #da771b;">*</sup> User name:
<br><input type="text" name="q0" id="q0" value="<?php echo getPostVal(0); ?>">

<br><sup style="color: #da771b;">*</sup> Password:
<br><input type="password" name="q1" id="q1" value="<?php echo getPostVal(1); ?>">
<br><sup style="color: #da771b;">*</sup> Confirm Password:
<br><input type="password" name="q2" id="q2" value="<?php echo getPostVal(2); ?>">

<br><sup style="color: #da771b;">*</sup> Email Address:
<br><input type="text" name="q3" id="q3" value="<?php echo getPostVal(3); ?>">
<br><sup style="color: #da771b;">*</sup> Check one:
<br><label><input type="radio" name="q4" id="q4" value="Home Gardener"<?php if (getPostVal(4) == 'Home Gardener') { echo ' CHECKED'; } ?>> Home Gardener</label>
<br><label><input type="radio" name="q4" id="q4" value="Green Industry Professional"<?php if (getPostVal(4) == 'Green Industry Professional') { echo ' CHECKED'; } ?>> Green Industry Professional</label>
<br><sup style="color: #da771b;">*</sup> First name:
<br><input type="text" name="q5" id="q5" value="<?php echo getPostVal(5); ?>">
<br><sup style="color: #da771b;">*</sup> Last name:
<br><input type="text" name="q6" id="q6" value="<?php echo getPostVal(6); ?>">
<br>Business name:
<br><input type="text" name="q7" id="q7" value="<?php echo getPostVal(7); ?>">
<br>Your address:
<br><input type="text" name="q8" id="q8" value="<?php echo getPostVal(8); ?>">
<br>Telephone Number:
<br><input type="text" name="q9" id="q9" value="<?php echo getPostVal(9); ?>">
<br>Zip Code:
<br><input type="text" name="q10" id="q10" value="<?php echo getPostVal(10); ?>">
<br><label><input type="checkbox" name="q11" id="q11" value="yup" CHECKED> <b>Sign up for Fiore News</b></label>
<br><label><input type="checkbox" name="q12" id="q12" value="yup" CHECKED> <b>Sign up for<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Weekly Availability</b></label>
</div>

<input type="image" src="images/sign_up.png" width="68" height="20" border="0" style="margin: 10px;">
</form>
		</td>
		<td valign="top" align="left" style="font-size: 10px; color: #da771b; padding: 133px 5px 0px 10px;">
<p>Passwords should be 6
<br>characters in length.</p>
<br>
<br>
<p>Email addresses are
<br>never shared</p>
		</td>
		<td valign="top" align="left" style="padding-top: 30px; padding-bottom: 20px;">
<img src="images/spacer.gif" width="1" height="100%" border="0" style="width: 1px; height: 100%; border: 0px; border-right: solid #4e4244 1px;">
		</td>
		<td valign="top" align="left" style="font-size: 13px; padding-top: 30px;">
<b>Fiore Account Benefits</b>
<br>Registering at <b class="brownLinks">cjfiore.com</b> allows you to take full advantage of all of our
<br>Web site’s features. Logged in customers have the ability to:
<br>• View online order history
<br>• Save your cart
<br>• Save your shipping & billing addresses
<br>• Stay up to date with industry trends and services
<br>
<br>If you forget your password, you can <a href="password_reset.php" style="color: #009dd0">have your password reset</a> and
<br>sent to your email address.</p>

<div class="leafItOnTop" style="top: 215px; left: -70px;"><img src="images/leafGreen.png" width="164" height="168" border="0">

		</td>
<?php
} // end if we're not logged in
?>
	</tr>
</table>

<?php makeFooter(); ?>

</body>
</html>
<?php
function getPostVal($id) {
	if (isset($_POST['q'.$id]))
		return $_POST['q'.$id];
	else
		return '';
}