<?php
require_once('functions.php');

if (!isWholesale()) {
	require('login.php');
	exit();
}
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Archived Newsletters - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraHead(); ?>
</head>
<body onload="document.getElementById('pass').focus();">
<?php makeHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" style="padding: 20px 10px 0px 20px;">
<h1>Seasonal</h1>

<form method="POST" action="">
<p>
	Download PDFs of our seasonal goods:
	<br>
	<br><a href="pdf/2015_FN_WLS008.pdf" target="_blank" class="rounded darkBG smallerText">Winter Landscape Supply</a>
	<br>
	<br><a href="pdf/2015_FN_HSG_20150908.pdf" target="_blank" class="rounded darkBG smallerText">Holiday Supply Guide</a></div>
</p>
</form>
		</td>
	</tr>
</table>

<?php makeFooter(); ?>

</body>
</html>
