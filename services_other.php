<?php
require_once('functions.php');
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Other Services We Provide - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraHead(); ?>
</head>
<body>
<?php makeHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td><img src="images/spacer.gif" width="590" height="20" border="0"></td>
		<td><img src="images/spacer.gif" width="25" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="355" height="1" border="0"></td>
	</tr>
	<tr>
		<td valign="top" align="left" style="font-size: 13px;">
<img src="images/services_other0.jpg" width="587" height="268" border="0">
<br>
<br><h4><span>Fabrication</span></h4>
<br><span><b>High end stone fabrication is an art and craft</b>. We partner with local, highly skilled, and trusted stone fabricators who know that detail and quality are as important as timeliness and accuracy. We offer the following fabrication services:</span>

<ul>
	<li><span>templating custom pieces for fabrication</span></li>
	<li><span>radius or straight cuts in bluestone, Indiana limestone, and quartzite materials</span></li>
	<li><span>edge finishing in thermal, rockface, and honed finishes with a bullnose or half bullnose edge</span></li>
	<li><span>thermal, polishing, brushing, and sandblasting of bluestone, limestone, and Eden product surfaces</span></li>
	<li><span>hole drilling in multiple diameters</span></li>
	<li><span>sourcing of material that we do not stock for fabrication such as granite or various imports.</span></li>
</ul>
<br>
<img src="images/services_other1.jpg" width="587" height="311" border="0">

<div style="clear: both; height: 50px;"></div>

<div class="leafItOnTop" style="top: 37px; left: 440px;"><img src="images/leafGreen2.png" width="164" height="168" border="0"></div>

		</td>
		<td></td>
		<td valign="top" align="left"  style="border-left: solid #4e4244 1px; padding: 0px 0px 183px 20px; font-size: 13px;">
<h4><span>Planting</span></h4>
<br><b><span>For over 90 years Fiore has supported and partnered with Chicago’s landscape trade professionals</span></b>. 
<span>We strongly recommend all homeowners or property owners, in need of a new landscape or a landscape renovation, to contact one of our industry trade associations for a local directory of accredited landscape architects, certified landscape contract professionals,  landscape design professionals, and stone masons as part of the selection process. It is also helpful to talk to neighbors and friends to see who they recommend. <br>Depending on the scope of your project, landscape design and construction can often require highly technical skills as well as highly creative talents. It is important that you find a professional who is qualified and whom you trust. The Chicagoland area is fortunate to have some of the most talented and skilled landscape professionals in the country. Please reference the following web sites to assist you in your selection process.</span>
<br><b class="brownLinks"><a href="http://www.ilca.net" target="_blank">www.ilca.net</a>
<br><a href="http://www.asla.org" target="_blank">www.asla.org</a>
<br><a href="http://www.melaweb.org" target="_blank">www.melaweb.org</a>
<br><a href="http://ldaonline.org" target="_blank">ldaonline.org</a>
<br><a href="http://www.apld.com" target="_blank">www.apld.com</a></b>

		</td>
	</tr>
</table>

<?php makeFooter(); ?>

</body>
</html>
