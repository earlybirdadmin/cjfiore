<?php


$staff_headers = array(
  'Prairie View'   => 'prairie_view',
  'Chicago'       => 'chicago',
  'Bolingbrook'  => 'bolingbrook',
  'Indiana'       => 'indiana'
);

$staff = array (
  array (
    'location' => 'prairie_view',
    'name' => 'Cate Migacz',
    'title' => 'Plant Buyer ',
    'specialty' => 'Horticulturist & ICN Pro',
    'email' => 'cate@cjfiore.com',
  ),
  array (
    'location' => 'prairie_view',
    'name' => 'Sarah Bottner',
    'title' => 'Sales Account Manager',
    'specialty' => '',
    'email' => 'sarahbottner@cjfiore.com',
  ),
  array (
    'location' => 'prairie_view',
    'name' => 'Vicky Brice',
    'title' => 'Controller',
    'specialty' => '',
    'email' => 'vicky@cjfiore.com',
  ),
  array (
    'location' => 'prairie_view',
    'name' => 'David A. Fiore',
    'title' => 'Prairie View Operations Manager',
    'specialty' => '',
    'email' => 'davidanthonyfiore@cjfiore.com',
  ),
  array (
    'location' => 'prairie_view',
    'name' => 'Amy Fiore',
    'title' => 'Customer Service Associate',
    'specialty' => '',
    'email' => 'amy@cjfiore.com',
  ),
  array (
    'location' => 'prairie_view',
    'name' => 'David M. Fiore',
    'title' => 'President',
    'specialty' => 'Horticulturist',
    'email' => 'dave@cjfiore.com',
  ),
  array (
    'location' => 'prairie_view',
    'name' => 'Lisa A. Fiore',
    'title' => 'Chairperson',
    'specialty' => '',
    'email' => 'lisa@cjfiore.com',
  ),
  array (
    'location' => 'prairie_view',
    'name' => 'Cara Fiore Furlong',
    'title' => 'Customer Service Associate',
    'email' => 'carafurlong@cjfiore.com',
  ),
  array (
    'location' => 'prairie_view',
    'name' => 'Alex Head',
    'title' => 'Plant Buyer',
    'specialty' => 'Horticulturist',
    'email' => 'alex@cjfiore.com',
  ),
  array (
    'location' => 'prairie_view',
    'name' => 'Gail Marik',
    'title' => 'Customer Service Associate - Inside Sales',
    'specialty' => 'Horticulturist & ICN Pro',
    'email' => 'gailmarik@cjfiore.com',
  ),
  array (
    'location' => 'prairie_view',
    'name' => 'Tim Robbins',
    'title' => 'Sales Account Manager-Hardscape Buyer',
    'specialty' => 'Landscape Architect and Natural Stone Specialist',
    'email' => 'timrobbins@cjfiore.com',
  ),
  array (
    'location' => 'prairie_view',
    'name' => 'Katherine Rohrer',
    'title' => 'HR Generalist/Accounting Support ',
    'email' => 'katherinerohrer@cjfiore.com',
  ),
  array (
	'location' => 'prairie_view',
	'name' => 'Jonathan Rosenthal',
	'title' => 'Accounting Assistant',
	'email' => 'jonathanrosenthal@cjfiore.com',
  ),
  array (
    'location' => 'prairie_view',
    'name' => 'Zack Sargent',
    'title' => 'Operations Associate',
    'specialty' => 'Horticulturist and CLT',
    'email' => 'zachsargent@cjfiore.com',
  ),
  array (
    'location' => 'prairie_view',
    'name' => 'Ken Varner',
    'title' => 'Facilities & Fleet Manager',
    'specialty' => '',
    'email' => 'kenvarner@cjfiore.com',
  ),
  array (
    'location' => 'prairie_view',
    'name' => 'Bulmaro Velazquez',
    'title' => 'SLF Operations Manager',
    'specialty' => '',
    'email' => 'tagging@cjfiore.com',
  ),











  // CHICAGO
/*  array (
    'location' => 'chicago',
    'name' => 'Jake Bell',
    'title' => 'Customer Service Associate',
    'specialty' => 'Horticulturist & ICN Pro',
    'email' => 'jakebell@cjfiore.com',
    'phone' => '847-514-2460',
  ), */
/*  array (
    'location' => 'chicago',
    'name' => 'Natalie Carreno',
    'title' => 'Customer Service Associate',
    'specialty' => 'Horticulturist',
    'email' => 'nataliecarreno@cjfiore.com',
    'phone' => '847-514-2460',
  ),*/
  array (
    'location' => 'chicago',
    'name' => 'Mike Duttge',
    'title' => 'Branch Manager',
    'specialty' => '',
    'email' => 'mikeduttge@cjfiore.com',
    'phone' => '847-514-6118',
  ),
  array (
    'location' => 'chicago',
    'name' => 'Robert Colin',
    'title' => 'Customer Service & Operations Associate',
    'specialty' => '',
    'email' => 'robertcolin@cjfiore.com',
    'phone' => '847-343-8757',
  ),

/*  array (
    'location' => 'chicago',
    'name' => 'Will Haverkamp',
    'title' => 'Sales Account Manager',
    'email' => 'willhaverkamp@cjfiore.com',
    'phone' => '847-346-7574',
  ), */
  array (
    'location' => 'chicago',
    'name' => 'Tyler Klivickis',
    'title' => 'Chicago Operations Manager',
    'specialty' => '',
    'email' => 'tylerklivickis@cjfiore.com',
    'phone' => '847-513-2655',
  ),
  array (
    'location' => 'chicago',
    'name' => 'Michael Kwiatek',
    'title' => 'Product Manager - Herbaceous & Quality Control Supervisor',
    'specialty' => 'Horticulturist & ICN Pro',
    'email' => 'michaelkwiatek@cjfiore.com',
    'phone' => '847-370-7715',
  ),
  array (
    'location' => 'chicago',
    'name' => 'Colleen Mulhern',
    'title' => 'Assistant Manager',
    'specialty' => '',
    'email' => 'colleenmulhern@cjfiore.com',
    'phone' => '847-514-2460',
  ),
/*  array (
    'location' => 'chicago',
    'name' => 'Emily Stuart',
    'title' => 'Sales Account Manager & Product Manager - Seasonals',
    'specialty' => 'ICN Pro',
    'phone' => '847-514-2458',
    'email' => 'emilystuart@cjfiore.com',
  ), */







  // bolingbrook
  array (
    'location' => 'bolingbrook',
    'name' => 'Jorge Garcia',
    'title' => 'Operations Manager',
    'specialty' => '',
    'email' => 'jorgegarcia@cjfiore.com',
    'phone' => '847-561-0509',
  ),

  array (
    'location' => 'bolingbrook',
    'name' => 'Brian Henning',
    'title' => 'Sales Account Manager',
    'specialty' => '',
    'email' => 'brianhenning@cjfiore.com',
    'phone' => '847-732-0099',
  ),

  array (
    'location' => 'bolingbrook',
    'name' => 'Bill Hope',
    'title' => 'Vice President & Bolingbrook Territory Manager',
    'specialty' => '',
    'email' => 'billhope@cjfiore.com',
    'phone' => '708-600-2460',
  ),











  // INDIANA
/*
  array (
    'location' => 'indiana',
    'name' => 'Ed Rockhill',
    'title' => 'Vice President & Indiana Territory Manager',
    'email' => 'edrockhill@cjfiore.com',
    'phone' => '317-460-5763',
  ),
  */
  /*
  array (
    'location' => 'indiana',
    'name' => 'Rob Yanney',
    'title' => 'Sales Account Manager',
    'specialty' => '',
    'email' => 'robyanney@cjfiore.com',
    'phone' => '317-560-7657',
  ),

  array (
    'location' => 'indiana',
    'name' => 'Eric McCoy',
    'title' => 'Operations Manager',
    'email' => 'ericmccoy@cjfiore.com',
    'phone' => '317-339-2747',
  ),
  */
  array (
    'location' => 'indiana',
    'name' => 'Michael McKernin',
    'title' => 'Branch Manager',
	'email' => 'michaelmckernin@cjfiore.com',
	'phone' => '317-471-7288',
  ),














	// array (
 //    'name' => 'CJ Fiore',
 //    'title' => 'Co-Founder and Owner',
 //    'specialty' => 'Horticulturist',
 //    'email' => 'cj@cjfiore.com',
 //    'phone' => '',
 //  ),
 //  array (
 //    'name' => 'Mark Fiore',
 //    'title' => 'Co-founder and Owner',
 //    'specialty' => 'Horticulturist',
 //    'email' => 'mark@cjfiore.com',
 //    'phone' => '',
 //  ),
 //  array (
 //    'name' => 'Bill Dahlgren',
 //    'title' => '1954-2014',
 //  ),
 //  array (
 //    'name' => 'Manuel Ochoa',
 //    'title' => 'Chicago Yard Manager',
 //    'specialty' => '',
 //    'email' => '',
 //    'phone' => '',
 //  ),
 //  array (
 //    'name' => 'David Flores',
 //    'title' => 'Inventory Leader',
 //    'specialty' => '',
 //    'email' => '',
 //    'phone' => '',
 //  ),
);
