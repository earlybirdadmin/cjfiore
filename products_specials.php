<?php
require_once('functions_catalog.php');

$_GET['type'] = 0; // specials

$query = 'SELECT * FROM `Items`, `Sizes` WHERE Items.discount != 0 AND Items.is_available=1 AND Items.item_id=Sizes.item_id';
$result = mysql_query($query);
$itemArr = array();
for ($i=0;$i<mysql_num_rows($result);$i++) {
	$itemArr[] = mysql_fetch_array($result);
}

// create the itemArr
$singleItem = array('name' => 'Item ', 'desc' =>'Size and weight here', 'price' => '000');

?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Specials - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraCatalogHead(); ?>
</head>
<body>
<?php makeCatalogHeader(); ?>

<h1>Specials</h1>
<br>

<?php
echo '<table cellspacing="0" cellpadding="0" border="0" style="margin-top: 20px;">';
for ($i=0;$i<count($itemArr);) {
	for ($j=0;$j<3;$j++,$i++) {
	if ($i % 3 == 0)
		echo '<tr>';

	echo '<td valign="top" align="left" style="padding: 0px 15px 15px 0px;">';

	if ($i < count($itemArr)) {
		echo '<a href="javascript:;"><img src="assets/catalog/'.$itemArr[$i]['image_file'].'" height="200" border="0" style="margin-bottom: 4px;"></a>';
		echo '<br><h1>'.$itemArr[$i]['item_name'].'</h1>';
		echo '<br><b>'.$itemArr[$i]['item_size'];
		echo '<br>$'.$itemArr[$i]['retail_price'].'</b>';
	}

	echo '</td>';

	if ($i % 3 == 2)
		echo '</tr>';
	}
}

if (count($itemArr) == 0) {
	echo '<td colspan="3" align="left" valign="top"><br>Sorry, no specials are available at this time.<br><br><b>Check back soon!</b></td>';
}
?>
	<tr>
		<td><img src="images/spacer.gif" width="266" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="266" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="266" height="1" border="0"></td>
	</tr>
</table>

<?php makeCatalogFooter(); ?>

</body>
</html>
