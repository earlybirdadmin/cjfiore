<?php
require_once('functions.php');
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>fiore</title>
<?php extraHead(); ?>
</head>
<body>
<?php makeHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td><img src="images/spacer.gif" width="600" height="20" border="0"></td>
		<td><img src="images/spacer.gif" width="345" height="1" border="0"></td>
	</tr>
	<tr>
		<td valign="top" align="left" style="font-size: 13px;">
<h4><span>The Fiore Team</span></h4>
<p style="width: 580px; margin-top: 0px;"><span><b>It is the people at Fiore, our greatest assets, that helps us stand apart</b>. Our highly regarded staff of horticulturists, certified arborists, landscape architects, and stone specialists average over 20 years of technical industry experience. These highly skilled and knowledgeable professionals are available as resources to help support you and your firm in achieving your goals. In order to provide the highest possible quality product, our buyers hand select approximately 85% of the plant material and stone products that are available in our sales yard.</span></p>

<!-- STAFF TABLE -->
<table width="597" cellspacing="0" cellpadding="8" border="0" id="staffTable">
<?php
include_once('about_staff_bios.php');

for ($i=0;$i<count($staff);) {
	echo '<tr>';
	for ($j=0;$j<4;$j++,$i++) { // 4 people per row
		// open TD
		if ($j % 4 < 3)
			echo '<td valign="top" align="left" style="border-right: solid #4e4244 1px;">';
		else
			echo '<td valign="top" align="left">';
			
			
		if ($i < count($staff)) {
			echo convertStaffArrayToHTML($staff[$i]);
		}
			
		// close TD
		echo '</td>';
	}
}


function convertStaffArrayToHTML($staffArr) {
	$out = '';
	/* start with the image */
	$filename = 'images/staff/'.strtolower(str_replace(' ', '_', $staffArr['name'])).'.jpg';
	if (!file_exists($filename))
		$out = '<img src="images/staff/missing.gif" width="90" height="110" border="0" style="background: #938990; margin-bottom: 6px;"><br>';
	else
		$out = '<img src="'.$filename.'" height="110" border="0" style="margin-bottom: 3px;"><br>';
		
	if (trim($staffArr['name']) != '')
		$out .= '<b>'.$staffArr['name'].'</b><br>';
		
	if (trim($staffArr['title']) != '')
		$out .= $staffArr['title'].'<br>';
		
	if (trim($staffArr['specialty']) != '')
		$out .= '<i>'.$staffArr['specialty'].'</i><br>';
		
	if (trim($staffArr['email']) != '')
		$out .= '<a href="mailto:'.$staffArr['email'].'">'.$staffArr['email'].'</a><br>';
		
	if (trim($staffArr['phone']) != '')
		$out .= $staffArr['phone'].'<br>';
		
	return $out;
}
?>
	<tr>
		<td><img src="images/spacer.gif" width="130" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="130" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="130" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="130" height="1" border="0"></td>
	</tr>
</table>
<!-- END STAFF TABLE -->
		</td>
		<td valign="top" align="left" style="font-size: 13px;">
<div class="darkGreyBlock" style="width: 290px;">
<span>Our team provides personal service from the beginning of your project to the end. Our goal is to get to know you and develop a deeper understanding of your unique project requirements. Feel free to contact any of our staff with questions about materials or process. We look forward to hearing from you.</span>
</div>
<br><a name="careers"></a>
<br>
<div class="beigeBlock" style="width: 290px;">
<span style="font-size: 16px;">Careers at Fiore</span>
<br><span>We are always on the lookout for great talent to add to our growing business. If you are interested in a career at Fiore, just send us an email at</span> <b class="brownLinks"><a href="mailto:management@cjfiore.com">management@cjfiore.com</a></b> <span>and tell us why. In the meantime, check out the openings below to see if you might be a good fit.</span>
<br>
<br><b class="brownLinks"><a href="assets/openings/Sales_BUSINESS DEVELOPMENT_POSITION revised KVD.doc.docx">Sales and Account Managment &gt;</a>
<br>
<br><a href="assets/openings/CJF - CUSTOMER SERVICE_PV.docx">Customer Service Associate &gt;</a>
</b>
</div>

<div class="leafItOnTop" style="top: -25px; left: 120px;"><img src="images/leafOrange.png" width="166" height="177" border="0"></div>

		</td>
</table>

<?php makeFooter(); ?>

</body>
</html>
