<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>CJ Fiore Blog</title>

<link rel=stylesheet type="text/css" href="/fiore.css">
<script type="text/javascript" src="/fiore.js"></script>
<script type="text/javascript" src="/fioretrans.js"></script>
<meta name="google-translate-customization" content="77e74ac91873d5c7-d2f996464c889d04-g290165d65e7c18f0-f"></meta>

<link rel=stylesheet type="text/css" href="/fiore_catalog.css">

<style type="text/css">
.submenu {
	margin-top: -200px;
}
</style>
</head>
<body>
<!-- header -->
<div id="topBG"></div>


<div id="topMenuHolder">
<div id="topMenu">
<div style="position: absolute; top: 15px; left: 15px;">
<a href="/."><img src="/images/logo1.png" width="579" height="85" border="0" alt="fiore nursery and landscape supply" id="topLogo"></a>
</div>



<div style="position: absolute; top: 15px; right: 15px; white-space: nowrap;">
<div class="rounded whiteBG smallerText" style="margin-right: 10px; padding: 6px 5px;"><a href="javascript:;" onclick="doTranslate();">¿Habla español?</a>&nbsp;&nbsp;</div>

<div class="rounded whiteBG smallerText nowrap"><a href="/cart.php">My Cart&nbsp;<img src="/images/cartIcon.gif" width="20" height="14" border="0" style="position: relative; top: 3px;"></a><span style="padding-left: 18px;"><a href="/login.php">Sign In</a> | <a href="/login.php">Sign Up</a></span></div>
</div>


<div style="position: absolute; top: 55px; right: 15px;">
<div class="rounded whiteBG smallerText nowrap"><a href="/pdf/2013 Retail Catalog.pdf" target="_blank">Retail Price List&nbsp;&nbsp;<img src="/images/lightRoundedArrow.gif" width="13" height="12" border="0" style="position: relative; top: 3px;"></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="/wholesale_catalog.php">Wholesale Price List&nbsp;&nbsp;<img src="/images/lightRoundedArrow.gif" width="13" height="12" border="0" style="position: relative; top: 3px;"></a>&nbsp;</div>
</div>


<div style="position: absolute; bottom: 15px; *bottom: 12px; right: 15px; white-space: nowrap;">
</div>


<div style="position: absolute; bottom: 15px; left: 15px; white-space: nowrap;">
<table id="menuTable" cellspacing="0" cellpadding="0" border="0" style="float: left; margin-right: 20px;">
	<tr>
		<td valign="middle" align="center" class="roundLeft4" id="menuTD0"><a href="/about.php" onmouseover="showSubmenu(0);" onmouseout="hideSubmenu(0);" id="menu0">ABOUT US</a></td>
		<td valign="middle" align="center" id="menuTD1"><a href="/services.php" onmouseover="showSubmenu(1);" onmouseout="hideSubmenu(1);" id="menu1">SERVICES</a></td>
		<td valign="middle" align="center" id="menuTD2"><a href="/products.php" onmouseover="showSubmenu(2);" onmouseout="hideSubmenu(2);" id="menu2">PRODUCTS</a></td>
		<td valign="middle" align="center"><a href="/partners.php">PARTNERS</a></td>
		<td valign="middle" align="center"><a href="/resources.php">RESOURCES</a></td>
		<td valign="middle" align="center" class="roundRight4"><a href="/contact.php">CONTACT</a></td>
	</tr>
</table>

<form method="GET" action="search.php" id="searchform" style="float: left;">
	<div class="rounded darkBG smallerText" style="*padding: 2px 0px 2px 10px;"><span>SEARCH ONLINE CATALOG</span>&nbsp;&nbsp;<input type="text" name="q" id="q" style="font-size: 9px;">&nbsp;&nbsp;<img src="/images/darkRoundedArrow3.gif" width="20" height="16" border="0" style="position: relative; top: 5px; cursor: pointer;" onclick="document.getElementById('searchform').submit();">
	</div>
</form>
	
</div><!-- absolute position -->

</div><!-- topMenu -->
</div><!-- topMenuHolder -->
<!-- end header -->
<div id="contentBackground">&nbsp;</div>
<div id="contentHolder">
<div id="content">

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" style="padding: 0px 10px 0px 20px; border-right: solid #d2ced0 1px;">
<!-- LH MENU -->
<div class="catalogLHMenu">
LH MENU
</div>

<!-- END LH MENU -->
		</td>
		<td valign="top" align="left" style="padding: 0px 10px 0px 20px;">

<table width="801" cellspacing="0" cellpadding="0" border="0" style="margin-bottom: 5px;">
	<tr>
		<td valign="top" align="left">

CONTENT

		</td>
		<td><img src="/images/spacer.gif" width="1" height="600" border="0"></td>
	</tr>
	<tr>
		<td><img src="/images/spacer.gif" width="800" height="1" border="0"></td>
		<td><img src="/images/spacer.gif" width="1" height="1" border="0"></td>
	</tr>
</table>
<!-- end content table -->


<!-- footer table -->
</div><!-- close content -->
<table width="970" cellspacing="0" cellpadding="0" border="0" id="footerTable" style="">
	<tr>
		<td valign="middle" align="left" style="height: 44px;"><img src="/images/socials.gif" width="73" height="23" border="0" usemap="#socials"></td>
		<td valign="middle" align="center">
Sign Up for Fiore Emails
		</td>
		<td valign="middle" align="left"><a href="/login.php"><img src="/images/btnSubmit.png" width="75" height="22" border="0" alt="submit"></a></td>
		<td valign="middle" align="left">
Prairie View: 847.913.1414 | Chicago: 773.533.1414 | <a href="/contact.php">Contact Us</a> | ©2013<span style="font-family: fiore-book;">&nbsp;</span>
		</td>
	</tr>
	<tr>
		<td><img src="/images/spacer.gif" width="100" height="1" border="0"></td>
		<td><img src="/images/spacer.gif" width="170" height="1" border="0"></td>
		<td><img src="/images/spacer.gif" width="120" height="1" border="0"></td>
		<td><img src="/images/spacer.gif" width="580" height="1" border="0"></td>
	</tr>
</table>

<script type="text/javascript">
 
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-39264121-1']);
  _gaq.push(['_trackPageview']);
 
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
 
</script>

</div><!-- close contentHolder -->


<map name="socials">
<area shape="rect" coords="0, 0, 23, 23" href="http://www.twitter.com/cjfnursery" target="_blank" style="border: solid #ff0000 1px;">
<area shape="rect" coords="25, 0, 47, 23" href="https://www.facebook.com/pages/Fiore-Nursery-and-Landscape-Supply/181704598536855" target="_blank">
<area shape="rect" coords="48, 0, 73, 23" href="http://www.linkedin.com/company/fiore-nursery-and-landscape-supply?trk=hb_tab_compy_id_1731374" target="_blank">
</map>

<div class="submenu" id="submenu0" onmouseover="showSubmenu(0);" onmouseout="hideSubmenu(0);" >
<div class="submenuSpacer"></div>
<a href="/about_history.php" style="margin-top: 0px;">History</a>
<a href="/about_staff.php">Staff</a>
<a href="/about_locations.php">Hours / Locations</a>
<a href="/about_staff.php#careers">Careers</a>
</div>

<div class="submenu" id="submenu1" onmouseover="showSubmenu(1);" onmouseout="hideSubmenu(1);">
<div class="submenuSpacer"></div>
<a href="/services_sourcing.php" style="margin-top: 0px;">Sourcing</a>
<a href="/services_sourcing.php#purchasing_solutions">Purchasing Solutions</a>
<a href="/services_delivery.php">Delivery</a>
<a href="/services_other.php">Planting / Fabrication</a>
</div>

<div class="submenu" id="submenu2" onmouseover="showSubmenu(2);" onmouseout="hideSubmenu(2);" >
<div class="submenuSpacer"></div>
<a href="/products_plants.php" style="margin-top: 0px;">Plants</a>
<a href="/products_landscape_supply.php">Landscape Supply</a>
<a href="/products_natural_stone.php">Natural Stone</a>
<a href="/products_concrete_clay_pavers.php">Concrete &amp; Clay Pavers</a>
<a href="/products_green_roof.php">Green Roof</a>
<a href="/products_planters_accessories.php">Planters and Accessories</a>
<a href="/products_specials.php">Specials</a>
</div>


</body>
</html>
