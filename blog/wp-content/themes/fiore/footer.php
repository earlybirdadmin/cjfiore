

<!-- footer table -->
		</td>
	</tr>
</table>
</div><!-- close content -->
<table width="970" cellspacing="0" cellpadding="0" border="0" id="footerTable" style="">
	<tr>
		<td valign="middle" align="left" style="height: 44px;"><img src="/images/socials.gif" width="73" height="23" border="0" usemap="#socials"></td>
		<td valign="middle" align="center">
Sign Up for Fiore Emails
		</td>
		<td valign="middle" align="left"><a href="/login.php"><img src="/images/btnSubmit.png" width="75" height="22" border="0" alt="submit"></a></td>
		<td valign="middle" align="left">
Prairie View: 847.913.1414 | Chicago: 773.533.1414 | <a href="/contact.php">Contact Us</a> | ©2013<span style="font-family: fiore-book;">&nbsp;</span>
		</td>
	</tr>
	<tr>
		<td><img src="/images/spacer.gif" width="100" height="1" border="0"></td>
		<td><img src="/images/spacer.gif" width="170" height="1" border="0"></td>
		<td><img src="/images/spacer.gif" width="120" height="1" border="0"></td>
		<td><img src="/images/spacer.gif" width="580" height="1" border="0"></td>
	</tr>
</table>

<script type="text/javascript">
 
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-39264121-1']);
  _gaq.push(['_trackPageview']);
 
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
 
</script>

</div><!-- close contentHolder -->


<map name="socials">
<area shape="rect" coords="0, 0, 23, 23" href="http://www.twitter.com/fiorenursery" target="_blank" style="border: solid #ff0000 1px;">
<area shape="rect" coords="25, 0, 47, 23" href="https://www.facebook.com/pages/Fiore-Nursery-and-Landscape-Supply/181704598536855" target="_blank">
<area shape="rect" coords="48, 0, 73, 23" href="http://www.linkedin.com/company/fiore-nursery-and-landscape-supply?trk=hb_tab_compy_id_1731374" target="_blank">
</map>

<div class="submenu" id="submenu0" onmouseover="showSubmenu(0);" onmouseout="hideSubmenu(0);" >
<div class="submenuSpacer"></div>
<a href="/about_history.php" style="margin-top: 0px;">History</a>
<a href="/about_staff.php">Staff</a>
<a href="/about_locations.php">Hours / Locations</a>
<a href="/about_staff.php#careers">Careers</a>
</div>

<div class="submenu" id="submenu1" onmouseover="showSubmenu(1);" onmouseout="hideSubmenu(1);">
<div class="submenuSpacer"></div>
<a href="/services_sourcing.php" style="margin-top: 0px;">Sourcing</a>
<a href="/services_sourcing.php#purchasing_solutions">Purchasing Solutions</a>
<a href="/services_delivery.php">Delivery</a>
<a href="/services_other.php">Planting / Fabrication</a>
</div>

<div class="submenu" id="submenu2" onmouseover="showSubmenu(2);" onmouseout="hideSubmenu(2);" >
<div class="submenuSpacer"></div>
<a href="/products_plants.php" style="margin-top: 0px;">Plants</a>
<a href="/products_landscape_supply.php">Landscape Supply</a>
<a href="/products_natural_stone.php">Natural Stone</a>
<a href="/products_concrete_clay_pavers.php">Concrete &amp; Clay Pavers</a>
<a href="/products_green_roof.php">Green Roof</a>
<a href="/products_planters_accessories.php">Planters and Accessories</a>
<a href="/products_specials.php">Specials</a>
</div>


</body>
</html>
