<?php get_header(); ?>  

<!-- SIDEBAR BEGIN -->
<?php get_sidebar(); ?>
<!-- SIDEBAR END -->

		</td>
		<td valign="top" align="left" style="padding: 0px 10px 0px 20px;">

<table width="801" cellspacing="0" cellpadding="0" border="0" style="margin-bottom: 5px;">
	<tr>
		<td valign="top" align="left">

<!-- CONTENT BEGIN -->
<?php
if ( have_posts() ) {
 while( have_posts() ) { 
  the_post(); 
?>

<div style="clear: both;">
	<h3><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h3>
	<small><?php the_time('l, F jS, Y') ?> at <?php the_time('g:i a'); ?></small>

	<?php the_content(); ?>  

</div>
<?php
 }
}
?>
<!-- CONTENT END -->

		</td>
		<td><img src="/images/spacer.gif" width="1" height="600" border="0"></td>
	</tr>
	<tr>
		<td><img src="/images/spacer.gif" width="800" height="1" border="0"></td>
		<td><img src="/images/spacer.gif" width="1" height="1" border="0"></td>
	</tr>
</table>
<!-- end content table -->

<?php get_footer(); ?>  

