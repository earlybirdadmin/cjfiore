<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>CJ Fiore Blog - <?php bloginfo('name'); ?> <?php wp_title(); ?></title>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />  

<link rel=stylesheet type="text/css" href="/fiore.css">
<script type="text/javascript" src="/fiore.js"></script>
<script type="text/javascript" src="/fioretrans.js"></script>
<meta name="google-translate-customization" content="77e74ac91873d5c7-d2f996464c889d04-g290165d65e7c18f0-f"></meta>

<link rel=stylesheet type="text/css" href="/fiore_catalog.css">
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen,projection" />  

</head>
<body>
<!-- header -->
<div id="topBG"></div>


<div id="topMenuHolder">
<div id="topMenu">
<div style="position: absolute; top: 15px; left: 15px;">
<a href="/."><img src="/images/logo1.png" width="579" height="85" border="0" alt="fiore nursery and landscape supply" id="topLogo"></a>
</div>



<div style="position: absolute; top: 15px; right: 15px; white-space: nowrap;">
<div class="rounded whiteBG smallerText" style="margin-right: 10px; padding: 6px 5px;"><a href="javascript:;" onclick="doTranslate();">¿Habla español?</a>&nbsp;&nbsp;</div>

<div class="rounded whiteBG smallerText nowrap"><a href="/cart.php">My Cart&nbsp;<img src="/images/cartIcon.gif" width="20" height="14" border="0" style="position: relative; top: 3px;"></a><span style="padding-left: 18px;"><a href="/login.php">Sign In</a> | <a href="/login.php">Sign Up</a></span></div>
</div>


<div style="position: absolute; top: 55px; right: 15px;">
<div class="rounded whiteBG smallerText nowrap"><a href="/pdf/2013 Retail Catalog.pdf" target="_blank">Retail Price List&nbsp;&nbsp;<img src="/images/lightRoundedArrow.gif" width="13" height="12" border="0" style="position: relative; top: 3px;"></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="/wholesale_catalog.php">Wholesale Price List&nbsp;&nbsp;<img src="/images/lightRoundedArrow.gif" width="13" height="12" border="0" style="position: relative; top: 3px;"></a>&nbsp;</div>
</div>


<div style="position: absolute; bottom: 15px; *bottom: 12px; right: 15px; white-space: nowrap;">
</div>


<div style="position: absolute; bottom: 15px; left: 15px; white-space: nowrap;">
<table id="menuTable" cellspacing="0" cellpadding="0" border="0" style="float: left; margin-right: 20px;">
	<tr>
		<td valign="middle" align="center" class="roundLeft4" id="menuTD0"><a href="/about.php" onmouseover="showSubmenu(0);" onmouseout="hideSubmenu(0);" id="menu0">ABOUT US</a></td>
		<td valign="middle" align="center" id="menuTD1"><a href="/services.php" onmouseover="showSubmenu(1);" onmouseout="hideSubmenu(1);" id="menu1">SERVICES</a></td>
		<td valign="middle" align="center" id="menuTD2"><a href="/products.php" onmouseover="showSubmenu(2);" onmouseout="hideSubmenu(2);" id="menu2">PRODUCTS</a></td>
		<td valign="middle" align="center"><a href="/partners.php">PARTNERS</a></td>
		<td valign="middle" align="center"><a href="/resources.php">RESOURCES</a></td>
		<td valign="middle" align="center" class="roundRight4"><a href="/contact.php">CONTACT</a></td>
	</tr>
</table>

<form method="GET" action="search.php" id="searchform" style="float: left;">
	<div class="rounded darkBG smallerText" style="*padding: 2px 0px 2px 10px;"><span>SEARCH ONLINE CATALOG</span>&nbsp;&nbsp;<input type="text" name="q" id="q" style="font-size: 9px;">&nbsp;&nbsp;<img src="/images/darkRoundedArrow3.gif" width="20" height="16" border="0" style="position: relative; top: 5px; cursor: pointer;" onclick="document.getElementById('searchform').submit();">
	</div>
</form>
	
</div><!-- absolute position -->

</div><!-- topMenu -->
</div><!-- topMenuHolder -->
<!-- end header -->
<div id="contentBackground">&nbsp;</div>
<div id="contentHolder">
<div id="content">

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" style="padding: 0px 10px 0px 20px; border-right: solid #d2ced0 1px;">