<?php
require_once('functions.php');
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Resources - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraHead(); ?>
</head>
<body>
<?php makeHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" style="padding: 10px 10px 0px 20px;">
<h1><span>Fiore Resources</span></h1>
<p style="width: 450px; margin-top: 10px;"><span>At Fiore we continually strive to keep you up to date on the latest products and industry trends that can help you be more successful. Join us for an event or browse our resources to <br>help with research on your latest project.</span></p>

<table cellspacing="0" cellpadding="0" border="0" style="margin-bottom: 100px;">
	<tr>
		<td valign="top" align="left">
<div class="greyBlock">
<h4><span>News and Events</span></h4>
<br><span>Fiore is Now Servicing the Indianapolis Market
<br><a href="mailto:MichaelMcKernin@cjfiore.com">Contact our Indiana Branch Manager &gt;</a>
</span>
</div>
		</td>
		<td valign="top" align="left" style="border-right: solid #4e4244 1px; padding: 0px 20px; font-size: 13px;">
<h4><span>Helpful Information</span></h4>
<br><span>We have accumulated helpful and useful information over the years which is available here for your use.</span>
<br>
<br><b class="brownLinks"><a href="pdf/Fertilizing Evergreens.pdf" target="_blank">Fertilizing Evergreens &gt;</a>
<br>
<br><a href="pdf/Rhododendron Care.pdf" target="_blank">Rhododendron Care &gt;</a>
<br>
<br><a href="pdf/Approximate Dimensions for Standard Containers.pdf" target="_blank">Standard Container Dimensions &gt;</a>
</b>

		</td>
		<td valign="top" align="left" style="padding: 0px 20px; font-size: 13px;">
<h4><span>Fiore E-News Archive</span></h4>
<br><span>Stay up-to-date with the latest events and news at Fiore by singing up for our E-newsletter. And if you’ve missed something you can always find it in our archive.</span><br><b class="brownLinks"><a href="archived_newsletters.php" target="_blank">Archived Fiore Newsletters &gt;</a></b>
<br>
<br><h4><span>Fiore Blog</span></h4>
<br><span>Day-to-day thoughts and ideas from Fiore can help keep the ideas flowing. Follow us here.</span>
<br>
<br><a href="/blog/" class="rounded darkBG smallerText">FIORE BLOG</a>

		</td>
		<td valign="top" align="left"><div class="leafItOnTop" style="top: 300px; left: -300px;"><img src="images/leafOrange.png" width="166" height="171" border="0"></div>
</td>
	</tr>
	<tr>
		<td><img src="images/spacer.gif" width="225" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="295" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="300" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="100" height="1" border="0"></td>
	</tr>
</table>

		</td>
	</tr>
</table>

<?php makeFooter(); ?>

</body>
</html>
