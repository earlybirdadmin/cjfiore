<?php
require_once('functions.php');
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Our Products - CJ Fiore, Nursery and Landscape Supply</title>
<style type="text/css">
.servicesOverviewBlock a {
	letter-spacing: 1px;
}
</style>
<?php extraHead(); ?>
</head>
<body>
<?php makeHeader(); ?>

<table  width="400" cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" style="padding: 0px 10px 0px 20px;">

<div class="leafItOnTop" style="top: 433px; left: 580px;"><img src="images/leafOrange.png" width="166" height="171" border="0"></div>


<!-- BROWN TABLE -->
<table width="820" cellspacing="0" cellpadding="0" border="0" style="margin-bottom: 50px;">
	<tr>
		<td valign="top" align="left">
<!-- PRODUCTS TABLES -->
<table width="535" cellspacing="0" cellpadding="0" border="0" style="margin-bottom: 5px;">
	<tr>
		<td valign="top" align="left">
			<div class="servicesOverviewBlock roundTopLeft10">
			<a href="products_plants.php"><img src="images/products0.jpg" width="293" height="143" border="0" class="roundTopLeft10"><br>&nbsp;&nbsp;<span>Plants</span> &gt;</a>
			</div>
		</td>
		<td><img src="images/spacer.gif" width="5" height="1" border="0"></td>
		<td valign="top" align="left">
			<div class="servicesOverviewBlock">
			<a href="products_landscape_supply.php"><img src="images/products1.jpg" width="237" height="143" border="0"><br>&nbsp;&nbsp;<span>Landscape Supply</span> &gt;</a>
			</div>
		</td>
	</tr>
</table>

<table width="535" cellspacing="0" cellpadding="0" border="0" style="margin-bottom: 5px;">
	<tr>
		<td valign="top" align="left">
			<div class="servicesOverviewBlock">
			<a href="products_natural_stone.php"><img src="images/products2.jpg" width="243" height="163" border="0"><br>&nbsp;&nbsp;<span>Natural Stone</span> &gt;</a>
			</div>
		</td>
		<td><img src="images/spacer.gif" width="5" height="1" border="0"></td>
		<td valign="top" align="left">
			<div class="servicesOverviewBlock">
			<a href="products_concrete_clay_pavers.php"><img src="images/products3.jpg" width="287" height="163" border="0"><br>&nbsp;&nbsp;<span>Concrete &amp; Clay Pavers</span> &gt;</a>
			</div>
		</td>
	</tr>
</table>

<table width="535" cellspacing="0" cellpadding="0" border="0" style="">
	<tr>
		<td valign="top" align="left">
			<div class="servicesOverviewBlock roundLeft10">
			<a href="products_green_roof.php"><img src="images/products4.jpg" width="201" height="163" border="0" class="roundBottomLeft10"><br>&nbsp;&nbsp;<span>Green Roof</span> &gt;</a>
			</div>
		</td>
		<td><img src="images/spacer.gif" width="5" height="1" border="0"></td>
		<td valign="top" align="left">
			<div class="servicesOverviewBlock">
			<a href="products_planters_accessories.php"><img src="images/products5.jpg" width="165" height="163" border="0"><br>&nbsp;&nbsp;<span style="letter-spacing: 0px;">Planters/Accessories</span> &gt;</a>
			</div>
		</td>
		<td><img src="images/spacer.gif" width="5" height="1" border="0"></td>
		<td valign="top" align="left">
			<div class="servicesOverviewBlock">
			<a href="products_specials.php"><img src="images/products6.jpg" width="159" height="163" border="0"><br>&nbsp;&nbsp;<span>Specials</span> &gt;</a>
			</div>
		</td>	
	</tr>
</table>
<!-- END PRODUCTS TABLES -->

		</td>
		<td><img src="images/spacer.gif" width="5" height="1" border="0"></td>
		<td valign="top" align="left" class="servicesOverviewBlock roundRight10">
			<div class="servicesOverviewBlock roundRight10" style="padding: 20px; font-size: 16px; line-height: 1.5;">
			<span style="font-size: 24px;">Fiore Products</span>
			<br><span>Fiore inventories the largest selection of above-ground, premium grade plant material in the Chicagoland area with over 1,100 varieties, including many that are unique or hard to find, selected from the finest nurseries in the country. We also offer a generous selection of natural stone products from the nation’s most trusted quarries, and a full range of landscape supply products including soil mixes, mulches and composts, assorted bulk materials, and drainage products.</span>
			</div>
		</td>
	</tr>
	<tr>
		<td><img src="images/spacer.gif" width="535" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="5" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="280" height="1" border="0"></td>
	</tr>
</table>
<!-- END BROWN TABLE -->

		</td>
	</tr>
</table>

<?php makeFooter(); ?>

</body>
</html>
