var activeSubmenu = -1;
function toggleSubmenu(nm) {
	if (activeSubmenu != -1) {
		hideSubmenu(activeSubmenu);
	}
	
	if (activeSubmenu == nm) {
		activeSubmenu = -1;
		return;
	}
	
	showSubmenu(nm);
	activeSubmenu = nm;
}

var submenuTO = false;
function showSubmenu(nm) {
	clearTimeout(submenuTO);
	
	if (activeSubmenu != -1) {
		reallyHideSubmenu(activeSubmenu);
	}
	activeSubmenu = nm;

	POSArr = getPOSArr(document.getElementById('menu'+nm));
	var o = document.getElementById('submenu'+nm);
	o.style.top = POSArr[0]+'px';
	o.style.left = POSArr[1]+'px';
	o.style.display = 'block';

	document.getElementById('menuTD'+nm).style.background = '#f6f2df';
	if (nm == 0) {
		document.getElementById('menuTD'+nm).style.borderBottomLeftRadius = '0px';
		document.getElementById('menuTable').style.borderBottomLeftRadius = '0px';
	}
}
function hideSubmenu(nm) {
	clearTimeout(submenuTO);
	
	submenuTO = setTimeout('reallyHideSubmenu('+nm+');', 300);
}
function reallyHideSubmenu(nm) {
	document.getElementById('submenu'+nm).style.display = 'none';
	document.getElementById('menuTD'+nm).style.background = '';
	if (nm == 0) {
		document.getElementById('menuTD'+nm).style.borderBottomLeftRadius = '4px';
		document.getElementById('menuTable').style.borderBottomLeftRadius = '4px';
	}
}

function getPOSArr(o) {
	var top = o.offsetTop;
	var left = o.offsetLeft;
	while (o.offsetParent) {
		o = o.offsetParent;
		top += o.offsetTop;
		left += o.offsetLeft;
	}
	
	top += 18;
	left -= 12;
	
	return new Array(top, left);
}


var INDEX_SCROLL_WAIT = 3500;
var TOTAL_INDEX_SCROLL_IMAGES = 3;
var curIndexScroll = 0;
var indexScrollTO = false;
var ieScrollTO = new Array(false, false, false);

function nextIndexScroll() {
	clearTimeout(indexScrollTO);
	indexScrollTO = setTimeout('animateIndexScroll()', INDEX_SCROLL_WAIT);
}

function animateIndexScroll() {
	clearTimeout(indexScrollTO);
	
	/* get ids */
	var prevIndexScroll = curIndexScroll;
	curIndexScroll = (curIndexScroll + 1) % TOTAL_INDEX_SCROLL_IMAGES;
	var oldIndexScroll = (curIndexScroll + 1) % TOTAL_INDEX_SCROLL_IMAGES;
	
	/* handle IE first (stupid IE) */
	if (navigator.userAgent.indexOf('MSIE') > -1) {
		/* remove transition just in case IE gets their shit together one day */
		document.getElementById('indexBG'+curIndexScroll).style.transition = 'none';
		
		/* cur scrolls into view */
		document.getElementById('indexBG'+curIndexScroll).style.zIndex = 13;
		doManualScroll(curIndexScroll, -100, 0, 0);
		/* prev scrolls to the right out of view */
		document.getElementById('indexBG'+prevIndexScroll).style.zIndex = 13;
		doManualScroll(prevIndexScroll, 0, 100, 0);
		/* old scrolls back to the beginning */
		document.getElementById('indexBG'+oldIndexScroll).style.zIndex = 12;
		document.getElementById('indexBG'+oldIndexScroll).style.left = '-100%';
		
		indexScrollTO = setTimeout('nextIndexScroll()', INDEX_SCROLL_WAIT);
		
		return;
	}
	
	
	/* cur scrolls into view */
	document.getElementById('indexBG'+curIndexScroll).style.zIndex = 13;
	document.getElementById('indexBG'+curIndexScroll).style.left = '0px';
	/* prev scrolls to the right out of view */
	document.getElementById('indexBG'+prevIndexScroll).style.left = '100%';
	/* old scrolls back to the beginning */
	document.getElementById('indexBG'+oldIndexScroll).style.zIndex = 12;
	document.getElementById('indexBG'+oldIndexScroll).style.left = '-100%';
	
	indexScrollTO = setTimeout('nextIndexScroll()', INDEX_SCROLL_WAIT);
}

var MIN_SPEED = 0.4;
function doManualScroll(id, curX, endX, speed) {
	clearTimeout(ieScrollTO[id]);
	
	/* get our direction multiplier */
	var dir = (endX > curX) ? 1 : -1;
	
	/* accelerate if we're < halfway, otherwise decelerate */
	if (Math.abs(endX - curX) > 50)
		speed += MIN_SPEED; /* ease in (giddayup!)
	else
		speed -= MIN_SPEED; /* ease out (whoa, boy!)
		
	/* make sure speed is at least 1 */
	speed = Math.max(MIN_SPEED, speed);
	
	/* add speed * direction multiplier to curX */
	curX += speed * dir;
	
	/* if curX is past endX, just make it = endX */
	var newDir = (endX > curX) ? 1 : -1;
	if (dir != newDir)
		curX = endX;
	
	/* move the image */
	document.getElementById('indexBG'+id).style.left = curX+'%';
	
	if (curX != endX)
		ieScrollTO[id] = setTimeout('doManualScroll('+id+', '+curX+', '+endX+', '+speed+')', 50);
}

var hasAddedIEBGImages = false;
var bgImgSizes = new Array(new Array(1200, 600), new Array(1200, 600), new Array(1200, 600));
function resizeIndexBG() {
	/* we have to use REAL images for IE instead of background images */
	if (!hasAddedIEBGImages && navigator.userAgent.indexOf('MSIE') > -1)
		addIEBGImages();
	
	/* get dims */
	var screenH = 500;
	var screenW = 1000;
	if (window.innerHeight) {
		screenW = window.innerWidth;
	} else if (window.document.documentElement.clientWidth) {
		screenW = window.document.documentElement.clientWidth;
	} else if (document.body && document.body.offsetHeight) {
		screenW = document.body.offsetWidth;
	}
	
	/* make sure we're at least this tall and wide */
	if (screenW < 800)
		screenW = 800;
	
	/* set the outer dimensions */
	document.getElementById('indexBG').style.width = screenW + 'px';
	
	/* resize the actual images */
	if (navigator.userAgent.indexOf('MSIE') > -1) {
		for (var i=0;i<TOTAL_INDEX_SCROLL_IMAGES;i++) {
			var tmpScale = screenW / bgImgSizes[i][0];
			var newW = tmpScale * bgImgSizes[i][0];
			var newH = tmpScale * bgImgSizes[i][1];
			document.getElementById('StupidIEImg'+i).width = newW;
			document.getElementById('StupidIEImg'+i).height = newH;
		}
	}
}

function addIEBGImages() {
	for (var i=0;i<TOTAL_INDEX_SCROLL_IMAGES;i++) {
		document.getElementById('indexBG'+i).innerHTML = '<img src="images/index'+i+'.jpg" border="0" id="StupidIEImg'+i+'" style="position: relative; top: 0px; left: 0px;">';
		document.getElementById('indexBG'+i).style.overflow = 'hidden';
		document.getElementById('indexBG'+i).style.backgroundImage = 'none';
	}
	hasAddedIEBGImages = true;
}




/* catalog functions */
function toggleLHSubmenu(id) {
	if (document.getElementById('lhSubmenu'+id).style.display == 'block') {
		/* hide the submenu */
		document.getElementById('lhSubmenu'+id).style.display = 'none';
		/* replace the border and color */
		document.getElementById('lhMenu'+id).style.borderBottom = 'solid #d2ced0 1px';
		document.getElementById('lhMenu'+id).style.color = '';
	} else {
		/* show the submenu */
		document.getElementById('lhSubmenu'+id).style.display = 'block';
		/* set the border and color */
		if (document.getElementById('lhMenu'+id)) {
			document.getElementById('lhMenu'+id).style.borderBottom = 'none';
			document.getElementById('lhMenu'+id).style.color = '#da771b';
		}
	}
}

function setBotanicalView(viewBotanical) {
	if (viewBotanical) {
		setCookie('primary_name_type', 'botanical', 365);
	} else {
		setCookie('primary_name_type', 'common', 365);
	}
	location.reload(true);
}

function setCookie(c_name,value,exdays) {
	var exdate=new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
	document.cookie=c_name + "=" + c_value;
}

function updateItemTotal(id) {
	/* submit it if it's a return */
	evt = window.event;
	if (evt && evt.keyCode == 13) {
		setTimeout('addItemsToCart()', 100);
		return true;
	}

	var tmpQuantity = parseInt(document.getElementById('item'+id+'Quantity').value);
	if (isNaN(tmpQuantity)) {
		document.getElementById('item'+id+'Quantity').value = '';
		tmpQuantity = 0;
	}
	
	if (tmpQuantity == 0) {
		if (document.getElementById('item'+id+'Total'))
			document.getElementById('item'+id+'Total').innerHTML = '';
	} else {
		newVal = tmpQuantity * parseFloat(document.getElementById('item'+id+'Price').innerHTML.replace(/,/g, ''));
		document.getElementById('item'+id+'Total').innerHTML = '$'+addCommas(newVal.toFixed(2));
	}
	
	return true;
}

function addCommas(n){
    var rx=  /(\d+)(\d{3})/;
    return String(n).replace(/^\d+/, function(w){
        while(rx.test(w)){
            w= w.replace(rx, '$1,$2');
        }
        return w;
    });
}

/* catalog */
function addItemsToCart() {
	var params = '';
	var j = 0;
	for (var i=0;document.getElementById('size'+i);i++) {
		if (document.getElementById('item'+i+'Quantity').value != '' && document.getElementById('item'+i+'Quantity').value != '0') {
			params += 'size'+j+ '=' +document.getElementById('size'+i).value+'&';
			params += 'quantity'+j+'=' +document.getElementById('item'+i+'Quantity').value+'&';
			j++;
			
			/* reset the quantity just in case someone hits the back button */
			document.getElementById('item'+i+'Quantity').value = '';
			updateItemTotal(i);
		}
	}
	
	/* submit it via js */
	var newJS = document.createElement('script');
	newJS.type = 'text/javascript';
	newJS.src = 'addToCart.php?'+params;
	document.getElementsByTagName('head')[0].appendChild(newJS);
}

function addSingleItemToCart(id) {
	document.getElementById('item'+id+'Quantity').value = 1;
	addItemsToCart();
}

function deleteCartItem(nm) {
	document.getElementById('item'+nm+'Quantity').value = 0;
	
	updateCart();
}

function updateCart() {
	document.getElementById('checkoutForm').action = 'cart.php';
	document.getElementById('checkoutForm').submit();
}

setTimeout(function() {
    document.getElementById('downloadAvailabilityList').addEventListener('click', function(e) {
        e.preventDefault();

        var code = prompt('Please provide the appropriate code in order to download the availability list');

        if (code != null && code == 'Fiore17') {
            window.location.href = 'availability_prices.php';
        } else if (code == null) {
            /* do nothing */
        } else {
            alert('Sorry, that code is invalid.  Please try again.');
        }
    });
}, 100);