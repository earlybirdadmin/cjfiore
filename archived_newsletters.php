<?php
if (isset($_POST['pass']) && strtolower($_POST['pass']) == 'pricelist') {
	header('Location: http://archive.constantcontact.com/fs020/1101928436273/archive/1102200719057.html');
	exit();
}

require_once('functions.php');
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Archived Newsletters - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraHead(); ?>
</head>
<body onload="document.getElementById('pass').focus();">
<?php makeHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" style="padding: 20px 10px 0px 20px;">
<h1>Archived Newsletters</h1>
<?php
if (isset($_POST['pass'])) {
	echo '<p style="color: #ff0000;">The password is incorrect.  Please try again.</p>';
}
?>

<form method="POST" action="">
<p>Please enter your password
<br>to access our archived newsletters:
<br><input type="password" name="pass" id="pass">
<br><input type="image" src="images/btnSubmit.png" width="75" height="22" border="0">
</p>
</form>
		</td>
	</tr>
</table>

<?php makeFooter(); ?>

</body>
</html>
