<?php
require_once('functions.php');
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Hours and Locations - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraHead(); ?>
</head>
<body>
<?php makeHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td><img src="images/spacer.gif" width="380" height="20" border="0"></td>
		<td><img src="images/spacer.gif" width="290" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="280" height="1" border="0"></td>
	</tr>
	<tr>
		<td valign="top" align="left" colspan="3"><h4><span>Hours and Locations</span></h4></td>
	</tr>
	<tr>
		<td valign="top" align="left" style="font-size: 13px; padding-bottom: 40px;">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2955.8327762462372!2d-87.95962208454887!3d42.19664287919819!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880fbdbe7001787d%3A0xa713c164b694b900!2s16606+IL-22%2C+Prairie+View%2C+IL+60069!5e0!3m2!1sen!2sus!4v1484152446932" width="360" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
		</td>
		<td valign="top" align="left" style="font-size: 13px;">
			<b>Fiore Prairie View</b>
			<br><span>16606 West Highway 22,
			<br>4 miles west of I-94,
			<br>1½ miles west of RT-21 and
			<br>2 miles east of RT-83.</span>
			<br>
			<br><b class="brownLinks"><a href="https://maps.google.com/maps?q=16606+Highway+22,+Prairie+View,+IL&hl=en&sll=44.53941,-73.506833&sspn=0.011241,0.019355&oq=16606+West+Highway+22+prai&hnear=16606+Illinois+22,+Prairie+View,+Illinois+60069&t=m&z=16" target="_blank">Driving Directions &gt;</a></b>

			<!--<div class="leafItOnTop" style="top: 60px; left: 80px;"><img src="images/leafOrange2.png" width="166" height="177" border="0"></div>-->

		</td>
		<td valign="top" align="left" rowspan="2" style="border-left: solid #4e4244 1px; padding: 0px 20px; font-size: 13px;">
			<h4><span>Hours</span></h4>

			<br><span style="font-size: 1.1em;">SPRING/SUMMER/FALL</span>
			<br><b>April to Thanksgiving</b>
			<br>Monday thru Friday 7:00am - 4:00pm
      <br>
			<br><b>Mid-April to July 1</b>
			<br>Saturday – 7:00am – 12 Noon
			<br>
			<br><span style="font-size: 1.1em;">WINTER</span>
			<br><b>Thanksgiving thru March</b>
			<br>Monday thru Friday 8:00am - 4:00pm
      <br>Saturday CLOSED
      <br /><i>Loading by Appointment Only</i>
      <br />
      <br /><span style="font-size: 1.1em;">CLOSED HOLIDAYS</span>
      <br />Martin Luther King Day, Presidents’ Day,
      <br />Memorial Day, Independence Day,
      <br />Labor Day, Thanksgiving Day &amp; the day after,
      <br />Christmas Day thru New Year’s Day
		</td>
	</tr>
	<tr>
		<td valign="top" align="left" style="font-size: 13px; padding-bottom: 40px;">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2970.134163077955!2d-87.71854284835946!3d41.88997167911937!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880e32b631e6a003%3A0xea004fd5ad618f93!2s2901+W+Ferdinand+St%2C+Chicago%2C+IL+60612!5e0!3m2!1sen!2sus!4v1484152598802" width="360" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
		</td>
		<td valign="top" align="left" style="font-size: 13px;">
			<b>Fiore Chicago</b>
			<br><span>2901 West Ferdinand Street,
			<br>1 mile north of I-290
			<br>3 miles west of I-90
			<br>Just off Sacramento Blvd.
			<br>Next to the Center for Green Technology.</span>
			<br>
			<br><b class="brownLinks"><a href="https://maps.google.com/maps?q=2901+West+Ferdinand+Street,+Chicago,+IL&hl=en&sll=41.117935,-77.604698&sspn=6.081999,9.909668&oq=2901+West+Ferdinand+Street+chi&hnear=2901+W+Ferdinand+St,+Chicago,+Illinois+60612&t=m&z=16" target="_blank">Driving Directions &gt;</a></b>

		</td>
	</tr>
	<tr>
		<td valign="top" align="left" style="font-size: 13px; padding-bottom: 40px;">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2977.7756960175457!2d-88.06647514836307!3d41.72535877913272!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880e5a69dd7c1aab%3A0x93ec0ee3f40c40!2s801+N+Bolingbrook+Dr%2C+Bolingbrook%2C+IL+60440!5e0!3m2!1sen!2sus!4v1484152919721" width="360" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
		</td>
		<td valign="top" align="left" style="font-size: 13px;">
			<b>Fiore Bolingbrook</b>
			<br><span>801 N Bolingbrook Dr
			<br>Bolingbrook, IL 60440</span>
			<br>
			<br><b class="brownLinks"><a href="https://www.google.com/maps/place/801+N+Bolingbrook+Dr,+Bolingbrook,+IL+60440/@41.7253588,-88.0664751,17z/data=!3m1!4b1!4m5!3m4!1s0x880e5a69dd7c1aab:0x93ec0ee3f40c40!8m2!3d41.7253588!4d-88.0642811" target="_blank">Driving Directions &gt;</a></b>

		</td>
	</tr>
	<tr>
		<td valign="top" align="left" style="font-size: 13px; padding-bottom: 40px;">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3055.8609794052545!2d-85.96506038393717!3d40.011552779414345!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8814b667b6792089%3A0xfface1ba21d4e54c!2s11460+Greenfield+Ave%2C+Noblesville%2C+IN+46060!5e0!3m2!1sen!2sus!4v1517950540375" width="360" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
		</td>
		<td valign="top" align="left" style="font-size: 13px;">
			<b>Fiore Indiana</b>
			<br><span>11460 Greenfield Ave
			<br>Noblesville, IN 46060</span>
			<br>
			<br><b class="brownLinks"><a href="https://www.google.com/maps/place/11460+Greenfield+Ave,+Noblesville,+IN+46060/@40.0115528,-85.9650604,17z/data=!3m1!4b1!4m5!3m4!1s0x8814b667b6792089:0xfface1ba21d4e54c!8m2!3d40.0115528!4d-85.9628717" target="_blank">Driving Directions &gt;</a></b>

		</td>
	</tr>
	<tr>
		<td><img src="images/spacer.gif" width="380" height="20" border="0"></td>
		<td><img src="images/spacer.gif" width="290" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="280" height="1" border="0"></td>
	</tr>
</table>
<?php makeFooter(); ?>

</body>
</html>
