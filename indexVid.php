<?php
require_once('functions.php');
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Charles J. Fiore Company, Inc- wholesale premium tree & shrub nursery and garden center near Chicago Illinois</title>

<?php extraHead(); ?>

<style type="text/css">
body {
	background: #4e4244;
}
#topMenu {
	background-image: URL('images/menuBG.png');
	background-repeat: repeat;
	*box-shadow: none;
}
#topBG {
	height: 120px;
}
#footerTable {
	top: 5px;
	border-top: none;
}
#contentBackground {
	height: 480px;
}
</style>
</head>
<body onload="nextIndexScroll();resizeIndexBG();" onresize="resizeIndexBG();">

<?php
/*
LEAVES OVERLAY
<table width="100%" height="500" cellspacing="0" cellpadding="0" border="0" style="position: absolute; top: 120px; left: 0px; z-index: 14;" id="indexLeafTable">
	<tr>
		<td valign="top" align="left"><img src="images/indexLeaf0.png" width="334" height="500" border="0"></td>
		<td valign="bottom" align="center"><img src="images/indexLeaf1.png" width="314" height="500" border="0"></td>
		<td valign="top" align="right"><img src="images/indexLeaf2.png" width="314" height="500" border="0"></td>
	</tr>
</table>
*/
?>

<?php makeHeader(); ?>


<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" style="padding: 0px;">
<img src="images/indexBox3.png" width="255" height="188" border="0" style="position: relative; top: -30px; left: 88px;" id="indexBox2">
		</td>
		<td valign="top" align="left" style="padding: 0px 0px 0px 290px;">
<!-- EMAIL SIGNUP -->
<div style="font: 28px fiore-bold, sans-serif; color: #ffffff; margin: 130px 0px 20px 0px; text-shadow: 1px 1px 3px #000000; margin-top: 20px; ">
Subscribe to Fiore<br>News for special offers<br>and tips!
</div>

<div id="emailSignup">Sign Up for Fiore Emails&nbsp;&nbsp;<a href="login.php"><img src="images/btnSubmit.png" width="75" height="22" border="0" alt="submit" style="position: relative; top: 6px;"></a></div>
<!-- END EMAIL SIGNUP -->

<br><br>

<!-- NEWS -->
<div style="position: absolute; bottom: 80px; left: 0px; width: 100%;">
<div id="newsHolder">
<table width="950" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td valign="top" align="left" style="border-right: dotted #4e4244 1px;">
			<div class="newsItem" style="">
				<b>2016 Pricing Available!</b>
				<br>We are happy to report that after months of working with our vast network of growers & suppliers our 2016 Estimated Pricing Guide is ready! You are welcome to stop in to any Fiore location and pick up a printed copy. Electronic versions are also available by signing into the wholesale section of our website. Here’s to a successful Spring!
 
				<br>
				<br><span class="blueLinks"><a href="products_seasonal.php">Click here to view these product guides &gt;</a></span>
			</div>
		</td>
		<td valign="top" align="left" style="border-right: dotted #4e4244 1px;">
			<div class="newsItem" style="border-bottom: none; width: 280px;">
			<b>New Sales Yard in the South Suburbs – 2016</b>
			<br>We are excited to announce the addition of our new, full service nursery and landscape supply center set to open in Spring 2016. Our new location at 801 N. Bolingbrook Drive, Bolingbrook, Il. is conveniently located 3 miles off both I-355
and I-55 and will feature a complete line of plant material, natural stone and landscape supplies.
			<br><span class="blueLinks"><a href="pdf/New Sales Yard in the South Suburbs.pdf">Please click here for more &gt;</a></span>
			</div>
		</td>
		<td valign="top" align="left">
			<div class="newsItem" style="border-bottom: none; width: 240px; font-size: 0.7em;">
			<b>Business Hours SUMMER/FALL: 
			<br>July 5 thru November 23, 2016 </b>
			<br>
			<br>Monday thru Friday 7:00am - 4:00pm
			<br>Saturday – Prairie View 7:00am – 12 Noon
			<br>Saturday – Chicago Appointment Only 
			<br>
			<BR>Sept 5, Nov 24, 25, 26 CLOSED
			</div>
		</td>
	</tr>
</table>
</div>
</div>
<!-- END NEWS -->
		</td>
	</tr>
</table>

</div><!-- close content -->

<?php makeFooter(); ?>


<!-- footer textrured strip -->
<div style="position: absolute; top: 620px; width: 100%; height: 100px; background: #ddd278 url('images/topBG.gif') repeat;">
&nbsp;
</div>
<!-- end footer textrured strip -->


<!-- index footer -->
<div style="position: absolute; top: 740px; left: 0px; width: 100%; height: 23px; z-index: 50;">
	<div class="footerIndex" style="white-space: nowrap;">
		<img src="images/socialsIndex2.gif" width="97" height="23" border="0" usemap="#socials" style="vertical-align: middle; margin-right: 15px; position: relative; top: -3px;">
PRAIRIE VIEW: 16606 W HIGHWAY 22 | PRAIRIE VIEW, IL 60069 | 847.913.1414 | CHICAGO: 2901 W FERDINAND ST | CHICAGO, IL 60612 | 773.533.1414 | INDIANAPOLIS, IN 317.774.5266 | <a href="contact.php">CONTACT US</a> | ©<?php echo date('Y'); ?>
	</div>
</div>
<!-- end index footer -->
<?php



	if ($_SERVER['REMOTE_ADDR'] == '::1' || $_SERVER['SERVER_ADDR'] == '10.0.1.2' || $_SERVER['SERVER_ADDR'] == '68.168.98.213') { } else {
?>
<script type="text/javascript">
 
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-39264121-1']);
  _gaq.push(['_trackPageview']);
 
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
 
</script>
<?php
	} // end if we're not on a staging site
?>





<style>
#virtualTourCover {
	display: none;
	position: absolute;
	top: 0px;
	left: 0px;
	z-index: 100;
	width: 100%;
	min-width: 500px;
	height: 100%;
	
	background-color: #000000;
	background-color: rgba(0, 0, 0, 0.8);
}
#virtualTourTableCell {
	display: table-cell;
	vertical-align: middle;
	text-align: center;
	padding: 40px;
}
#virtualTourVidHolder {
	display: inline-block;
	position: relative;
	border: solid #ffffff 20px;
	border-radius: 10px;
}
#virtualTourVid {
	position: relative;
	margin: 0px auto;
	max-width: 100%;
	
	background-color: #ffffff;
	box-shadow: 3px 3px 10px rgba(0, 0, 0, 0.5);
}
#virtualTourVid:before {
	content: "";
	display: block;
	width: 100%;
	padding-bottom: 56.25%; /* 720:1280 aspect ratio */
}

#virtualTourClose {
	position: absolute;
	top: -60px;
	right: -30px;
	width: 40px;
	height: 40px;
	
	cursor: pointer;
}
#virtualTourClose svg {
	stroke: #ffffff;
	fill: transparent;
	stroke-linecap: round;
	stroke-width: 2;
}
#virtualTourClose:hover svg {
	stroke: #db781b;
}


</style>


<script language="javascript">
function showVirtualTourVid() {
	document.getElementById('virtualTourCover').style.display = 'table';
	document.getElementById('virtualTourVid').play();
}
function hideVirtualTourVid() {
	document.getElementById('virtualTourVid').pause();
	document.getElementById('virtualTourCover').style.display = 'none';
}
function clickVirtualTourCover(e) {
	e = e || window.event;
	if (e.srcElement.id == 'virtualTourTableCell') {
		hideVirtualTourVid();
	}
}
</script>

<div id="virtualTourCover"><div id="virtualTourTableCell" onclick="clickVirtualTourCover(event);">
	<div id="virtualTourVidHolder">
		<video controls id="virtualTourVid">
			<source src="vid/virtual_nursey_tour.mp4" type="video/mp4">
			<source src="vid/virtual_nursey_tour.webm" type="video/webm">
			Your browser does not support the video tag. Time to UPGRADE, SON!
		</video>
		<div onclick="hideVirtualTourVid();" id="virtualTourClose">
			<svg viewbox="0 0 40 40">
				<path class="close-x" d="M 10,10 L 30,30 M 30,10 L 10,30"></svg>
		</div>
	</div>
</div></div><!-- close cover & table-cell -->


</body>
</html>
