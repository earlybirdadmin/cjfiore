<?php
require_once('functions_catalog.php');

if (isWholesale())
	$priceIdx = 'wholesale_price';
else
	$priceIdx = 'retail_price';


$idArr = array();
$q = mysql_real_escape_string($_GET['q']);
if ($q == '*' || $q == '')
	$q = 'fldksjflkdsjlkfjds';

// match the common name EXACTLY
$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Items` WHERE item_name="'.$q.'"');

// handle multiple words
$wordArr = explode(' ', $q);
if (count($wordArr) > 1) {
	// match the genus and species
	$tmpArr = $wordArr;
	$genus = array_shift($tmpArr);
	$species = implode(' ', $tmpArr);
	$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE genus="'.$genus.'" AND species="'.$species.'"');
	
	// match the species
	$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE species="'.$species.'"');
		
	// match start genus and species
	$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE genus="'.$genus.'" AND species LIKE "'.$species.'%"');

	// match start genus and any species
	$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE genus="'.$genus.'" AND species LIKE "%'.$species.'%"');

	// match the genus
	$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE genus="'.$genus.'"');
	
	// match the genus common name
	$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE genus_common_name="'.$genus.'"');


} else {
	$genus = $q;
	$species = $q;
}
// match the species
$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE species="'.$q.'"');
// match the genus
$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE genus="'.$q.'"');
$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE genus_common_name="'.$q.'"');


// match the partial common name start
$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Items` WHERE item_name LIKE "'.$q.'%"');
// match the partial species start
$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE species LIKE "'.$q.'%"');
// match the partial genus start
$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE genus LIKE "'.$q.'%"');
$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE genus_common_name LIKE "'.$q.'%"');


// match the partial common name word
$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Items` WHERE item_name LIKE "%'.$q.' %" OR  item_name LIKE "% '.$q.'%"');
// match the partial species
$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE species LIKE "%'.$q.' %" OR species LIKE "% '.$q.'%"');
// match the partial genus
$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE genus LIKE "%'.$q.' %" OR genus LIKE "% '.$q.'%"');


// match the partial common name
$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Items` WHERE item_name LIKE "%'.$q.'%"');
// match the partial species
$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE species LIKE "%'.$q.'%"');
// match the partial genus
$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE genus LIKE "%'.$q.'%"');

// match words
if (count($idArr) < 20 && count($wordArr) > 1) {
	for ($i=0;$i<count($wordArr);$i++) {
		// match words in common name
		$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Items` WHERE item_name LIKE "%'.$wordArr[$i].'%"');
	}
	
	for ($i=0;$i<count($wordArr);$i++) {
		// match words in species
		$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE species LIKE "%'.$wordArr[$i].'%"');
	}

	for ($i=0;$i<count($wordArr);$i++) {
		// match words in genus
		$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE genus LIKE "%'.$wordArr[$i].'%"');
	}
}


// only use uniques
$idArr = array_unique($idArr);
$idArr = array_values($idArr);

// finally, we create the regular query
$itemsArr = array();
for ($i=0;$i<count($idArr);$i++) {
	$id = $idArr[$i];
	$result = mysql_query('SELECT * from `Items` WHERE is_available=1 AND item_id='.$id);
	//$result = mysql_query('SELECT * from `Items` WHERE item_id='.$id);
	if ($result && mysql_num_rows($result) > 0) {
		$tmpArr = mysql_fetch_array($result);
		if (categoryIsPlant($tmpArr['category_id'])) {
			$result = mysql_query('SELECT * from `Plants` WHERE item_id='.$id);
			$row = mysql_fetch_array($result);
			$tmpArr['botanical_name'] = $row['genus'].' '.$row['species'];
			$tmpArr['common_name'] = $tmpArr['item_name'];
			$tmpArr = array_merge($tmpArr, $row);
		}
	
		if (hasSizesAvailable($tmpArr['item_id'])) {
			$itemsArr[] = $tmpArr;
		}
	} // end if mysql_num_rows == 1
}
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Item Search - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraCatalogHead(); ?>
</head>
<body>
<?php makeCatalogHeader(); ?>

<?php
for ($i=0;$i<count($itemsArr);$i++) {
?>
<div style="padding: 0px 0px 15px 0px; border-bottom: solid #d2ced0 1px;">
<table width="800" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td valign="top" align="left" style="border-right: solid #d2ced0 1px; padding: 0px 15px 20px 0px;">
<!-- PLANT NAME -->
<?php
if (categoryIsPlant($itemsArr[$i]['category_id'])) {
	echo '<div class="brownLinks" style="font-size: 11px; margin-top: 4px;">'.getBreadCrumbsForItemWithID($itemsArr[$i]['item_id']).'</div>';
	echo '<h1>'.$itemsArr[$i][PRIMARY_NAME_TYPE].'</h1>';
	echo '<br><b style="font-size: 12px;"><i>'.$itemsArr[$i][SECONDARY_NAME_TYPE].'</i></b>';
} else {
	echo '<div class="brownLinks" style="font-size: 11px; margin-top: 4px;">'.getBreadCrumbsForItemWithID($itemsArr[$i]['item_id']).'</div>';
	echo '<h1>'.$itemsArr[$i]['item_name'].'</h1>';
}
?>
<br>
<br>
<!-- PLANT IMAGE -->
<?php
$imgSrc = 'assets/catalog/'.$itemsArr[$i]['image_file'];
// get width and height
if (!file_exists($imgSrc)) {
	$imgHTML = '<img src="images/nophoto.jpg" width="153" height="173" border="0">';
} else {
	list($ow, $oh, $type, $attr) = getimagesize($imgSrc, $info);
	
	$scale = max(153/$ow, 173/$oh); // 153 is the width (+2 for the border), 155 is the height (+2 for border)
	$w = $scale * $ow;
	$h = $scale * $oh;
	
	$x = (153 - $w)/2;
	$y = (173 - $h)/2;
	
	$imgHTML = '<img src="'.$imgSrc.'" width="'.$w.'" height="'.$h.'" border="0" style="position: relative; top: '.$y.'px; left: '.$x.'px;">';
}
?>
<div class="catalogFloatImage">
<?php echo $imgHTML; ?>
</div>
<!-- PLANT SPECS -->
<?php
if (categoryIsPlant($itemsArr[$i]['category_id'])) {
	$infoArr = array('mature_height', 'mature_spread', 'growth_habit', 'growth_rate', 'light_preference', 'flower_color', 'bears_fruit_in', 'fruit', 'foliage', 'flowers_in', 'fall_color', 'growth_zone');

	for ($j=0;$j<count($infoArr);$j++) {
		$idx = $infoArr[$j];
		if (isset($itemsArr[$i][$idx]) && $itemsArr[$i][$idx] != '')
			echo convertFromGetVar($idx).': <b>'.$itemsArr[$i][$idx].'</b><br>';
	}
}
?>
<!-- PLANT DESCRIPTION -->
<div style="clear: both; margin-top: 8px;">
<?php echo $itemsArr[$i]['description']; ?>
</div>
		</td>
		<td valign="top" align="left" style="padding: 0px 15px;">
<!-- PLANT PRICES/SIZES -->
<?php
// get pricing table
$result = mysql_query('SELECT * FROM `Sizes` WHERE item_id='.$itemsArr[$i]['item_id'].' AND is_available ORDER BY sort_order ASC, id ASC');
if (mysql_num_rows($result) > 0) {
	
/* headers */
echo '<table width="350" cellspacing="0" cellpadding="0" border="0" class="catalogPricingTable">';
echo '<tr>';
	echo '<td valign="middle" align="left"><b>Size</b></td>';	
	if (isWholesale()) {
		echo '<td valign="middle" align="left"><b>Price</b></td>';	
		echo '<td valign="middle" align="left"><b>Quantity</b></td>';	
		echo '<td valign="middle" align="left"><b>Total</b></td>';	
	}
echo '</tr>';	

for ($j=0;$j<mysql_num_rows($result);$j++) {
	$row = mysql_fetch_array($result);
	echo '<tr>';
	echo '<td valign="middle" align="left" style="padding-right: 10px;">'.$row['item_size'].'</td>';
	if (isWholesale()) {
		echo '<td valign="middle" align="left">$<span id="item'.$row['item_id'].$j.'Price">'.number_format($row[$priceIdx], 2).'</span></td>';
		echo '<td valign="middle" align="left"><input type="text" name="item'.$row['item_id'].$j.'Quantity" id="item'.$row['item_id'].$j.'Quantity" size="2" onkeyup="return updateItemTotal('.$row['item_id'].$j.');" onchange="updateItemTotal('.$row['item_id'].$j.');" maxlength="3"></td>';
		echo '<td valign="middle" align="left"><span id="item'.$row['item_id'].$j.'Total"></span></td>';
	}
	echo '</tr>';
}

/* footer/add to cart */
if (isWholesale()) {
	echo '<tr>';
		echo '<td valign="middle" align="right" colspan="4" style="border: none;">';	
			echo '<a href="javascript:;" onclick="addItemsToCart();"><img src="images/btnAddToCart.gif" width="103" height="37" border="0" alt="Add to Cart">';	
		echo '</td>';	
	echo '</tr>';	
	
	echo '<tr>';
		echo '<td style="border: none;"><img src="images/spacer.gif" width="120" height="1" border="0"></td>';	
		echo '<td style="border: none;"><img src="images/spacer.gif" width="90" height="1" border="0"></td>';	
		echo '<td style="border: none;"><img src="images/spacer.gif" width="80" height="1" border="0"></td>';	
		echo '<td style="border: none;"><img src="images/spacer.gif" width="60" height="1" border="0"></td>';	
	echo '</tr>';	
}
?>
</table>
<?php
}
?>
<!-- END PLANT PRICES/SIZES -->

		</td>
	</tr>
	<tr>
		<td><img src="images/spacer.gif" width="420" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="380" height="1" border="0"></td>
	</tr>
</table>
</div>
<?php
} // end for $i

if (count($itemsArr) == 0) {
	echo '<b>No Results!</b><br><br>Try searching again or search for partial words.';
}
?>


<?php makeCatalogFooter(); ?>

</body>
</html>
<?php

function mergeIDArrayWithQuery($inArr, $query) {
	$idArr = array();
	$result = mysql_query($query);
	if ($result) {
		for ($i=0;$i<mysql_num_rows($result);$i++) {
			$row = mysql_fetch_array($result);
			$idArr[] = $row['item_id'];
		}
	}
	
	if (count($idArr) == 0)
		return $inArr;
	else
		return array_merge($inArr, $idArr);
}
