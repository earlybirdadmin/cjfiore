<?php
require_once('functions_catalog.php');

// init cart if there's nothing... um... in it
if (!isset($_SESSION['cart']))
	$_SESSION['cart'] = array();
	
// check for cart post
if (isset($_POST['doCartPost']) && $_POST['doCartPost'] == 'yup') {
	$newCartArr = array();
	for ($i=0;isset($_POST['cartItem'.$i]);$i++) {
		if (is_numeric($_POST['cartItem'.$i]) && is_numeric($_POST['item'.$i.'Quantity']) && $_POST['item'.$i.'Quantity'] > 0) {
			$newCartArr[] = array('item_id' => $_POST['cartItem'.$i], 'quantity' => round($_POST['item'.$i.'Quantity']));
		}
	}
	
	$_SESSION['cart'] = $newCartArr;
}

// combine cart items that match
$cartQuantity = 0;
for ($i=0;$i<count($_SESSION['cart']) - 1;$i++) {
	for ($j=$i+1;$j<count($_SESSION['cart']);$j++) {
		if ($_SESSION['cart'][$i]['item_id'] == $_SESSION['cart'][$j]['item_id']) {
			$_SESSION['cart'][$i]['quantity'] += $_SESSION['cart'][$j]['quantity'];
			unset($_SESSION['cart'][$j]);
			$_SESSION['cart'] = array_values($_SESSION['cart']);
		}
	}
	$cartQuantity += $_SESSION['cart'][$i]['quantity'];
}
$cartQuantity += $_SESSION['cart'][$i]['quantity'];

// if there are no items in the cart, we redirect to the cart page...
if ($cartQuantity == 0) {
	header('Location: cart.php');
	die();
}

// make sure we have a user even if they're blank
if (!isset($_SESSION['user'])) {
	$_SESSION['user'] = array('name' => '', 'company' => '', 'address' => '', 'email' => '', 'phone' => '', 'fax' => '', 'is_pickup' => '1', 'fiore_location' => '0');
}

// set wholesale
if (isWholesale())
	$priceIdx = 'wholesale_price';
else
	$priceIdx = 'retail_price';

// handle post changes to the cart

// finally, save the cart if we're a logged in user
saveCartToDB();

?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Checkout - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraHead(); ?>

<style type="text/css">
#contentTable td, input, .checkoutInput {
	font-size: 12px;
}
.repositionedRadio {
	position: relative;
	top: -4px;
}
</style>
</head>
<body<?php
if (isset($GLOBALS['highlight'])) {
	echo ' onload="document.getElementById(\'q'.$GLOBALS['highlight'].'\').focus();"';
}?>>
<?php makeHeader(); ?>

<form method="POST" action="confirm.php">
<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" colspan="3" style="padding: 0px 20px 10px 20px; border-bottom: solid #d9d7d7 1px;">
			<h1>Summary</h1>
		</td>
	</tr>
	<tr>
		<td valign="top" align="left" style="padding: 20px; border-right: solid #d9d7d7 1px;">
<?php
// cart items
$totalCost = 0;
for ($i=0;$i<count($_SESSION['cart']);$i++) {
	// validate data
	if (!is_numeric($_SESSION['cart'][$i]['item_id']) || !is_numeric($_SESSION['cart'][$i]['quantity']))
		continue;
		
	$item = getItemArrFromSizeID($_SESSION['cart'][$i]['item_id']);
	if (count($item) > 0) {
		echo '<b>'.$item['item_name'].'</b>';
		if (isset($item['item_name2']))
			echo '<br>'.$item['item_name2'];
			
		// size & price
		echo '<br><span style="font-size: 12px; margin-right: 25px;">Size</span><span style="font: 12px fiore-book, sans-serif;">'.$item['item_size'].'</span>';
		echo '<br><span style="font-size: 12px;  margin-right: 20px;">Price</span><span style="font: 12px fiore-book, sans-serif;">$'.$item[$priceIdx].'</span>';
		echo '<br><span style="font-size: 12px;  margin-right: 30px;">Qty</span><span style="font: 12px fiore-book, sans-serif;">'.$_SESSION['cart'][$i]['quantity'].'</span><br><br>';
		
		// add to total cost
		$totalCost += $_SESSION['cart'][$i]['quantity'] * $item[$priceIdx];
	}
}
?>
		</td>
		<td valign="top" align="left" style="padding: 20px; border-right: solid #d9d7d7 1px;">
<?php if (isset($GLOBALS['errorMsg'])) { echo '<b style="color: #ff0000;">'.$GLOBALS['errorMsg'].'</b><br>'; } ?>
<b>Customer Account Information:</b>
<table cellspacing="5" cellpadding="0" border="0">
	<tr>
		<td valign="middle" align="right">Name:</td>
		<td valign="middle" align="left"><input type="text" name="q0" id="q0" value="<?php echo $_SESSION['user']['first_name'].' '.$_SESSION['user']['last_name']; ?>" class="checkoutInput"></td>
	</tr>
	<tr>
		<td valign="middle" align="right">Company:</td>
		<td valign="middle" align="left"><input type="text" name="q1" id="q1" value="<?php echo $_SESSION['user']['company']; ?>" class="checkoutInput"></td>
	</tr>
	<tr>
		<td valign="top" align="right">Address:</td>
		<td valign="middle" align="left"><textarea name="q2" id="q2" rows="3" class="checkoutInput"><?php if (isset($_SESSION['user']['city']) && $_SESSION['user']['city'] != '') { echo $_SESSION['user']['address']."\n".$_SESSION['user']['city'].", ".$_SESSION['user']['state']." ".$_SESSION['user']['zip']; } ?></textarea></td>
	</tr>
	<tr>
		<td valign="middle" align="right">Email:</td>
		<td valign="middle" align="left"><input type="text" name="q3" id="q3" value="<?php echo $_SESSION['user']['email']; ?>" class="checkoutInput"></td>
	</tr>
	<tr>
		<td valign="middle" align="right">Phone:</td>
		<td valign="middle" align="left"><input type="text" name="q4" id="q4" value="<?php echo $_SESSION['user']['phone']; ?>" class="checkoutInput"></td>
	</tr>
<?php
/*	<tr>
		<td valign="middle" align="right">Fax:</td>
		<td valign="middle" align="left"><input type="text" name="q5" id="q5" value="<?php echo $_SESSION['user']['fax']; ?>" class="checkoutInput"></td>
*/
?>
	</tr>
</table>

<br><b>Optional:</b>
<br>
<input type="text" name="q6" id="q6" value="JOB NAME" onfocus="if (this.value == 'JOB NAME') { this.value = ''; }" onblur="if (this.value == '') { this.value = 'JOB NAME';}" class="checkoutInput">
<br>
<input type="text" name="q7" id="q7" value="PURCHASE ORDER #" onfocus="if (this.value == 'PURCHASE ORDER #') { this.value = ''; }" onblur="if (this.value == '') { this.value = 'PURCHASE ORDER #';}" class="checkoutInput">
<br>
<br><b>Please indicate your pickup/delivery options:</b>
<br><input type="radio" name="q8" value="Pickup"<?php if ($_SESSION['user']['is_pickup'] == 1) { echo ' CHECKED'; } ?> class="repositionedRadio">Pickup&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="q8" value="Delivery"<?php if ($_SESSION['user']['is_pickup'] != 1) { echo ' CHECKED'; } ?> class="repositionedRadio">Delivery
<br>
<br><b>Location:</b>
<br><input type="radio" name="q9" value="Prairie View"<?php if (!isset($_SESSION['user']['branch_id']) || $_SESSION['user']['branch_id'] == 0) { echo ' CHECKED'; } ?> class="repositionedRadio">Prairie View&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="radio" name="q9" value="Chicago"<?php if (isset($_SESSION['user']['branch_id']) && $_SESSION['user']['branch_id'] == 1) { echo ' CHECKED'; } ?> class="repositionedRadio">Chicago
<br>
<input type="text" name="q10" id="q10" value="DATE WANTED" onfocus="if (this.value == 'DATE WANTED') { this.value = ''; }" onblur="if (this.value == '') { this.value = 'DATE WANTED';}" class="checkoutInput">
		</td>
		<td valign="top" align="left" style="padding: 20px;">
Order Subtotal
<br><b>$<?php echo number_format($totalCost, 2, '.', ','); ?>
<br>
<br><input type="hidden" name="checkMeOut" value="rightOnRightOn">
<input type="image" src="images/btnPlaceOrder.gif" width="97" height="36" border="0" alt="Place Order">
<br>
<br><span style="font-size: 11px; line-height: 1.4; font-weight: normal;">Taxes and delivery fees (if applicable) will be calculated at the time of availability confirmation from Fiore.</span>
		</td>
	</tr>
	<tr>
		<td><img src="images/spacer.gif" width="320" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="350" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="225" height="1" border="0"></td>
	</tr>
</table>
</form>

<?php makeFooter(); ?>

</body>
</html>
