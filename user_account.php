<?php
require_once('functions.php');

// handle updates
if (isset($_SESSION['user']) && isset($_POST['q3'])) {
	// fill in blank post vals with the CURRENT values in case somebody's being shady
	if (trim($_POST['q3']) == '')
		$_POST['q3'] = $_SESSION['user']['email'];
	if (trim($_POST['q4']) == '')
		$_POST['q4'] = $_SESSION['user']['is_business'];
	if (trim($_POST['q5']) == '')
		$_POST['q5'] = $_SESSION['user']['first_name'];
	if (trim($_POST['q6']) == '')
		$_POST['q6'] = $_SESSION['user']['last_name'];
	if (trim($_POST['q7']) == '')
		$_POST['q7'] = $_SESSION['user']['company'];
	if (trim($_POST['q8']) == '')
		$_POST['q8'] = $_SESSION['user']['address'];
	if (trim($_POST['q9']) == '')
		$_POST['q9'] = $_SESSION['user']['phone'];
	if (trim($_POST['q10']) == '')
		$_POST['q10'] = $_SESSION['user']['zip'];
		
	// make sure it's a (somewhat) valid email address
	if (!isset($err1)) {
		$atPOS = strpos($_POST['q3'], '@');
		$dotPOS = strpos($_POST['q3'], '.', $atPOS);
		if (!$atPOS || !$dotPOS || strpos($_POST['q3'], ' ') !== false) {
			$_POST['q3'] = $_SESSION['user']['email'];
		}
	}

	// build userinfo query
	if (isset($_POST['q11']) && $_POST['q11'] == 'yup')
		$isNews = 1;
	else
		$isNews = 0;

	if (isset($_POST['q12']) && $_POST['q12'] == 'yup')
		$weeklyAvailability = 1;
	else
		$weeklyAvailability = 0;


	$query = 'UPDATE `UserInfo` SET email="'.mysql_real_escape_string($_POST['q3']).'", is_business="'.mysql_real_escape_string($_POST['q4']).'", first_name="'.mysql_real_escape_string($_POST['q5']).'", last_name="'.mysql_real_escape_string($_POST['q6']).'", company="'.mysql_real_escape_string($_POST['q7']).'", address="'.mysql_real_escape_string($_POST['q8']).'", phone="'.mysql_real_escape_string($_POST['q9']).'", zip="'.mysql_real_escape_string($_POST['q10']).'", is_newsletter_subscriber='.$isNews.', is_weekly_availability_subscriber='.$weeklyAvailability.' WHERE user_id='.$_SESSION['user']['user_id'];
	mysql_query($query);
	
	// handle password update (if they did it)
	if (trim($_POST['q1']) != '') {
		if ($_POST['q1'] == $_POST['q2']) {
			// update the password
			mysql_query('UPDATE `UserLogin` SET pass="'.mysql_real_escape_string(md5($_POST['q1'].'cjf')).'" WHERE user_id='.$_SESSION['user']['user_id']);
		} else {
			// alert the user that their passwords don't match
			$err = 'Your passwords do not match.  Please try typing them in again.';
		}
	}
	
	// handle the email LOGIN update
	mysql_query('UPDATE `UserLogin` SET email="'.mysql_real_escape_string($_POST['q3']).'" WHERE user_id='.$_SESSION['user']['user_id']);

	
	// now we update the session information by just logging the user in again
	logInUserWithID($_SESSION['user']['user_id']);
	
	// finally, we alert the user about the update if there were no errors
	if (!isset($err))
		$err = 'Success!  We have changed your account information.';
}
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Your Account - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraHead(); ?>
</head>
<body>
<?php makeHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" style="padding: 20px 10px 0px 20px;">
<h1>Edit Your Account</h1>
<br>
<?php
if (!isset($_SESSION['user'])) {
	echo 'You must be logged in to edit your account.<br><br><a href="login.php">Click Here</a> to sign in.';
} else {
	$result = mysql_query('SELECT * FROM `UserLogin` WHERE user_id='.$_SESSION['user']['user_id']);
	$row = mysql_fetch_array($result);
	$username = $row['user'];
?>

Edit the items below and click "Submit" to make any changes to your account.
<br><?php
if (isset($err))
	echo '<span style="color: #ff0000;">'.$err.'</span>';
?>

<!-- end edit/bluePay table -->
<table cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td valign="top" align="left">
			<form method="POST" action="">
			<div class="beigeBlock" style="font-size: 11px; padding: 10px; margin-top: 10px;">
			<table cellspacing="0" cellpadding="0" border="0">
				<tr>
					<td valign="top" align="left" style="font-size: 11px;">
			User name:
			<br><input type="text" name="q0" id="q0" value="<?php echo $username; ?>" style="width: 150px;" disabled="true">
			<br>
			<br>Change Password:
			<br><input type="password" name="q1" id="q1" value="" style="width: 150px;">
			<br>Confirm Password:
			<br><input type="password" name="q2" id="q2" value="" style="width: 150px;">
			<br>
			<br>Email Address:
			<br><input type="text" name="q3" id="q3" value="<?php echo getUserVal(3); ?>" style="width: 150px;">
			<br>
			<br>Check one:
			<br><label><input type="radio" name="q4" id="q4" value="0"<?php if (getUserVal(4) == '0') { echo ' CHECKED'; } ?> style="position: relative; top: -4px;"> Home Gardener</label>
			<br><label><input type="radio" name="q4" id="q4" value="1"<?php if (getUserVal(4) == '1') { echo ' CHECKED'; } ?> style="position: relative; top: -4px;"> Green Industry Professional</label>
					</td>
					<td><img src="images/spacer.gif" width="50" height="1"></td>
					<td valign="top" align="left" style="font-size: 11px;">
			First name:
			<br><input type="text" name="q5" id="q5" value="<?php echo getUserVal(5); ?>" style="width: 150px;">
			<br>Last name:
			<br><input type="text" name="q6" id="q6" value="<?php echo getUserVal(6); ?>" style="width: 150px;">
			<br>Business name:
			<br><input type="text" name="q7" id="q7" value="<?php echo getUserVal(7); ?>" style="width: 150px;">
			<br>Your address:
			<br><input type="text" name="q8" id="q8" value="<?php echo getUserVal(8); ?>" style="width: 150px;">
			<br>Telephone Number:
			<br><input type="text" name="q9" id="q9" value="<?php echo getUserVal(9); ?>" style="width: 150px;">
			<br>Zip Code:
			<br><input type="text" name="q10" id="q10" value="<?php echo getUserVal(10); ?>" style="width: 150px;">
			<br>
			<br><label><input type="checkbox" name="q11" id="q11" value="yup"<?php if ($_SESSION['user']['is_newsletter_subscriber']) { echo ' CHECKED'; } ?>> <b>Sign up for Fiore News</b></label>
				<br>
			<label><input type="checkbox" name="q12" id="q12" value="yup"<?php if ($_SESSION['user']['is_weekly_availability_subscriber']) { echo ' CHECKED'; } ?>> <b>Sign up for Weekly Availability</b></label>
				</td>
					<td><img src="images/spacer.gif" width="50" height="1"></td>
					<td valign="bottom" align="left"><input type="image" src="images/btnSubmit.png" value="submit"></td>
				</tr>
			</table>
			</div>
			</form>
		</td>
		<td><div style="width: 20px; height: 1px;">&nbsp;</div></td>
		<td valign="top" align="left">
			&nbsp;
			<br>
			<small><b>Make Payment:</b></small>
			<br>
			<!-- bluePay Form -->
			<form action="https://secure.bluepay.com/interfaces/shpf" method="POST" target="_blank">
				<input type="hidden" name="SHPF_FORM_ID" value="CJF01">
				<input type="hidden" name="SHPF_ACCOUNT_ID" value="100033809953">
				<input type="hidden" name="SHPF_TPS_DEF" value="SHPF_FORM_ID SHPF_ACCOUNT_ID DBA TAMPER_PROOF_SEAL AMEX_IMAGE DISCOVER_IMAGE TPS_DEF SHPF_TPS_DEF CUSTOM_HTML REBILLING REB_CYCLES REB_AMOUNT REB_EXPR REB_FIRST_DATE">
				<input type="hidden" name="SHPF_TPS" value="20082bd28ccc0adc94b0c5389040b21b">
				<input type="hidden" name="MODE" value="LIVE">
				<input type="hidden" name="TRANSACTION_TYPE" value="SALE">
				<input type="hidden" name="DBA" value="Charles J. Fiore Company Inc. ">
				<input type="hidden" name="TAMPER_PROOF_SEAL" value="f219b89be6cf7b846d360215f8a5a591">
				<input type="hidden" name="REBILLING" value="0">
				<input type="hidden" name="REB_CYCLES" value="">
				<input type="hidden" name="REB_AMOUNT" value="">
				<input type="hidden" name="REB_EXPR" value="">
				<input type="hidden" name="REB_FIRST_DATE" value="">
				<input type="hidden" name="AMEX_IMAGE" value="amex.gif">
				<input type="hidden" name="DISCOVER_IMAGE" value="discvr.gif">
				<input type="hidden" name="REDIRECT_URL" value="https://secure.bluepay.com/interfaces/shpf?SHPF_FORM_ID=CJF02&amp;SHPF_ACCOUNT_ID=100033809953&amp;SHPF_TPS_DEF=SHPF_ACCOUNT_ID SHPF_FORM_ID RETURN_URL DBA AMEX_IMAGE DISCOVER_IMAGE SHPF_TPS_DEF&amp;SHPF_TPS=4bce39f14011411bd7838a0f6d19823e&amp;RETURN_URL=javascript%3Ahistory%2Ego%28%2D2%29&amp;DBA=Charles%20J%2E%20Fiore%20Company%20Inc%2E%20&amp;AMEX_IMAGE=amex%2Egif&amp;DISCOVER_IMAGE=discvr%2Egif">
				<input type="hidden" name="TPS_DEF" value="MERCHANT APPROVED_URL DECLINED_URL MISSING_URL MODE TRANSACTION_TYPE TPS_DEF REBILLING REB_CYCLES REB_AMOUNT REB_EXPR REB_FIRST_DATE">
				<input type="hidden" name="CUSTOM_HTML" value="">

				<!-- USD Amount -->
				<input type="hidden" name="AMOUNT" value="">
				<!-- Customer ID -->
				<input type="hidden" name="CUSTOM_ID" value="<?php echo getUserVal(13); ?>">
				<!-- Invoice ID -->
				<input type="hidden" name="INVOICE_ID" value="N/A">

				<!-- Name -->
				<input type="hidden" name="NAME" value="<?php echo getUserVal(5) . ' ' . getUserVal(6); ?>">
				<!-- Company Name -->
				<input type="hidden" name="COMPANY_NAME" value="<?php echo getUserVal(7); ?>">
				<!-- Address -->
				<input type="hidden" name="ADDR1" value="<?php echo getUserVal(8); ?>">
				<!-- City -->
				<input type="hidden" name="CITY" value="<?php echo getUserVal(11); ?>">
				<!-- State -->
				<input type="hidden" name="STATE" value="<?php echo getUserVal(12); ?>">
				<!-- Zip -->
				<input type="hidden" name="ZIPCODE" value="<?php echo getUserVal(10); ?>">
				<!-- Email -->
				<input type="hidden" name="EMAIL" value="<?php echo getUserVal(3); ?>">
				<!-- Phone -->
				<input type="hidden" name="PHONE" value="<?php echo getUserVal(9); ?>">


				<input type="image" src="images/bluePayLogo.gif" width="140" height="45" border="0" value="Pay Now">
			</form>
			<!-- end bluePay form -->

		</td>
	</tr>
</table>
<!-- end edit/bluePay table -->

<br>
<br>
<div class="brownLinks">
<a href="saved_orders.php">View Previous Orders</a>
</div>
<?php
} // end if we're logged in
?>
		</td>
	</tr>
</table>

<?php makeFooter(); ?>

</body>
</html>
<?php
function getUserVal($id) {
	$idxArr = array('user', 'password0', 'password1', 'email', 'is_business', 'first_name', 'last_name', 'company', 'address', 'phone', 'zip', 'city', 'state', 'user_id');
	
	$idx = $idxArr[$id];
	if (isset($_SESSION['user'][$idx]))
		return $_SESSION['user'][$idx];
	else
		return '';
}