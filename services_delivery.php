<?php
require_once('functions.php');
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Delivery - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraHead(); ?>
</head>
<body>
<?php makeHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td colspan="3" valign="top" align="left"><img src="images/services_delivery0.jpg" width="950" height="229" border="0"></td>
	</tr>
	<tr>
		<td><img src="images/spacer.gif" width="560" height="20" border="0"></td>
		<td><img src="images/spacer.gif" width="45" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="332" height="1" border="0"></td>
	</tr>
	<tr>
		<td valign="top" align="left" style="font-size: 13px;">
<h4><span>Delivery</span></h4>
<br><span>Fiore‘s convenient and cost saving delivery fleet includes large and small trucks, trailers, and enclosed trailers available daily to deliver material directly to your job site or yard. All trucks come with a lift gate to help in the unloading process.
<br>
<br>Please call for delivery rates.</span>
<br>

<br><h4><span>Delivery from Prairie View</span></h4>
<br><span>Delivery from our Prairie View Nursery by a Fiore truck is available in Illinois with no minimum order required. Delivery charges are based on mileage from our Prairie View Nursery to the delivery site. Please call for a quote for all out of state deliveries.</span>
<br>
<br><h4><span>Delivery from Chicago</span></h4>
<br><span>Delivery from our Chicago Nursery by a Fiore truck is available in Illinois with no minimum order required. Delivery charges are based on mileage from our Chicago Nursery to the delivery site.</span>
<br>
<br><b class="brownLinks"><a href="services_delivery_policies.php"><span>Delivery Policies</span> &gt;</a>
<br>
<br>&nbsp;
<?php
/*
<br><b class="orangey"><span>Rates from Prairie View Nursery Center</span></b>
<!-- PRAIRIE VIEW RATES TABLE -->
<?php include('services_delivery_table_prairie_view.php'); ?>
<!-- END PRARIE VIEW TABLE -->

<br>
<br><b class="orangey"><span>Rates from Chicago Nursery Center</span></b>
<!-- CHICAGO DELIVERY RATES TABLE -->
<?php include('services_delivery_table_chicago.php'); ?>
<!-- END CHICAGO DELIVER RATES TABLE -->

<div class="leafItOnTop" style="top: -180px; left: 550px;"><img src="images/leafGreen2.png" width="164" height="168" border="0"></div>
*/
?>
		</td>
		<td valign="top" align="center"><img src="images/spacer.gif" width="1" height="100%" border="0" style="width: 1px; height: 100%; background: #4e4244;"></td>
		<td valign="top" align="left" style="font-size: 13px; padding-right: 50px;">
<h4>&nbsp;</h4>
<br><span><b class="orangey">Did you know?</b> We can send additional labor and/or equipment (e.g. ball cart or tractor) to help you unload or move material at your job site. Please specify when you place your order. Additional fees may apply. <br><b>Forklift assistance</b> is available for stone or palletized orders for an additional $50.00. <br><b>On-site tractor service</b> (material placement) is $150 per hour; portal to portal.</span>
<br>
<br>



		</td>
	</tr>
</table>

<?php makeFooter(); ?>

</body>
</html>
