<?php
/* set the page var */
$tmpArr = explode('/', $_SERVER['SCRIPT_NAME']);
define('PAGE',  end($tmpArr));

/* timezone */
date_default_timezone_set('America/Chicago');


// connect to database
if ($_SERVER['REMOTE_ADDR'] == '::1' || strpos($_SERVER['SERVER_ADDR'], '10.0.1') !== false) {
	// localhost
	@ $db = mysql_pconnect('localhost:/tmp/mysql.sock', 'root', 'Smashing');
	mysql_select_db('Fiore');
} else if ($_SERVER['SERVER_ADDR'] == '64.150.176.44') {
	// new-sites.net
	@ $db = mysql_pconnect('localhost', 'FioreUser', 'FiorePass');
	mysql_select_db('Fiore');
} else {
	@ $db = mysql_pconnect('127.0.0.1', 'cjfiore', 'cjftp60069');
	mysql_select_db('cjfioredb');
}

mysql_query("SET NAMES utf8");


/* set the bg var */
$bgFilename = 'images/bg_'.basename(PAGE, '.php').'.png';
/* if no bgfile exists, use the default */
if (file_exists($bgFilename)) {
	define('CONTENT_BACKGROUND_STYLE', ' style="background: URL('.$bgFilename.') top center no-repeat;"');
} else {
	define('CONTENT_BACKGROUND_STYLE', '');
}

/* start the session */
session_start();



/* include PEAR mail */
@include('Mail.php');
//define('ADMIN_EMAIL', 'meighan@depkedesign.com');
define('ADMIN_EMAIL', 'mikeduttge@cjfiore.com');
//define('ADMIN_EMAIL', 'soundstylus@gmail.com');

//define('ADMIN_EMAIL', 'matt@mattcourtright.com');

/* defaults css/javascript stuff */
function extraHead() {
?>

<link rel=stylesheet type="text/css" href="fiore.css">
<script type="text/javascript" src="fiore.js"></script>
<script type="text/javascript" src="fioretrans.js"></script>
<meta name="google-translate-customization" content="77e74ac91873d5c7-d2f996464c889d04-g290165d65e7c18f0-f"></meta>

<?php
}

/* stuff right after the <body> tag */
function makeHeader() {
?>
<!-- header -->
<div id="topBG"></div>

<?php if (PAGE == 'index.php') { makeIndexBG(); } ?>

<div id="topMenuHolder">
<div id="topMenu">
<div style="position: absolute; top: 15px; left: 15px;">
<a href="."><img src="images/logo1.png" width="579" height="85" border="0" alt="fiore nursery and landscape supply" id="topLogo"></a>
</div>



<div style="position: absolute; top: 15px; right: 15px; white-space: nowrap;">
	<div class="rounded whiteBG smallerText" style="margin-right: 10px; padding: 6px 5px;"><a href="javascript:;" onclick="doTranslate();">¿Habla español?</a>&nbsp;&nbsp;</div>

	<div class="rounded whiteBG smallerText" style="margin-right: 10px; padding: 6px 5px;"><a href="http://pdf.ac/aLVlDa" target="_blank">Credit Application</a>&nbsp;</div>

	<div class="rounded whiteBG smallerText nowrap">
		<?php
		if (isWholesale()) {
			echo '<a href="cart.php">My Cart&nbsp;<img src="images/cartIcon.gif" width="20" height="14" border="0" style="position: relative; top: 3px;"></a>';
			echo '<span style="padding-left: 18px;">';
		} else {
			echo '<span>';
		}

		if (isset($_SESSION['user'])) {
			echo '<a href="user_account.php">Settings</a> | <a href="logout.php">Sign Out</a>';
			if (isWholesale()) {
				echo '<div style="position: absolute; top: 40px; right: 15px;"><a href="/availability.php">Download availability list&nbsp;&nbsp;<img src="images/lightRoundedArrow.gif" width="13" height="12" border="0" style="position: relative; top: 3px;"></a></div>';
				
				echo '<div style="position: absolute; top: 65px; right: 15px;">Download 2016 Catalog (<a href="catalog_download.php?t=pdf" style="text-decoration: underline;">PDF</a> | <a href="catalog_download.php?t=excel" style="text-decoration: underline;">Excel</a>)&nbsp;&nbsp;</div>';
			}
		} else {
			echo '<a href="login.php">Wholesale Sign In</a> | <a href="login.php">Sign Up</a>';
			//echo '<div style="position: absolute; top: 55px; right: 15px;"><div class="smallerText nowrap" style="font-size: 1.2em; color: #009dd0;">2016 Pricing Coming Soon</div></div>';

		}
	
		echo '</span>';
		?>
	</div>
</div>



<?php
/*
<div style="position: absolute; top: 55px; right: 15px;">
<div class="rounded whiteBG smallerText nowrap"><a href="pdf/2013 Retail Catalog.pdf" target="_blank">Retail Price List&nbsp;&nbsp;<img src="images/lightRoundedArrow.gif" width="13" height="12" border="0" style="position: relative; top: 3px;"></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="wholesale_catalog.php">Wholesale Price List&nbsp;&nbsp;<img src="images/lightRoundedArrow.gif" width="13" height="12" border="0" style="position: relative; top: 3px;"></a>&nbsp;</div>
</div>
*/
?>

<div style="position: absolute; bottom: 15px; *bottom: 12px; right: 15px; white-space: nowrap;">
</div>


<div style="position: absolute; bottom: 15px; left: 15px; white-space: nowrap;">
<?php makeMenu(); ?>

<form method="GET" action="search.php" id="searchform" style="float: left;">
	<div class="rounded darkBG smallerText" style="*padding: 2px 0px 2px 10px;"><span>SEARCH ONLINE CATALOG</span>&nbsp;&nbsp;<input type="text" name="q" id="q" style="font-size: 9px;"<?php if (isset($_GET['q'])) { echo ' value="'.str_replace('"', '&quot;', $_GET['q']).'"'; } ?>>&nbsp;&nbsp;<img src="images/darkRoundedArrow3.gif" width="20" height="16" border="0" style="position: relative; top: 5px; cursor: pointer;" onclick="document.getElementById('searchform').submit();">
	</div>
</form>

<div class="rounded darkBG smallerText nowrap" style="width: 80px; text-align: center; position: absolute; bottom: -2px; left: 840px;"><a href="https://secure.bluepay.com/interfaces/shpf?SHPF_FORM_ID=CJF01&SHPF_ACCOUNT_ID=100033809953&SHPF_TPS_DEF=SHPF%5FFORM%5FID%20SHPF%5FACCOUNT%5FID%20DBA%20TAMPER%5FPROOF%5FSEAL%20AMEX%5FIMAGE%20DISCOVER%5FIMAGE%20TPS%5FDEF%20SHPF%5FTPS%5FDEF%20CUSTOM%5FHTML%20REBILLING%20REB%5FCYCLES%20REB%5FAMOUNT%20REB%5FEXPR%20REB%5FFIRST%5FDATE&SHPF_TPS=20082bd28ccc0adc94b0c5389040b21b&MODE=LIVE&TRANSACTION_TYPE=SALE&DBA=Charles%20J%2E%20Fiore%20Company%20Inc%2E%20&AMOUNT=&TAMPER_PROOF_SEAL=f219b89be6cf7b846d360215f8a5a591&CUSTOM_ID=&CUSTOM_ID2=&REBILLING=0&REB_CYCLES=&REB_AMOUNT=&REB_EXPR=&REB_FIRST_DATE=&AMEX_IMAGE=amex%2Egif&DISCOVER_IMAGE=discvr%2Egif&REDIRECT_URL=https%3A%2F%2Fsecure%2Ebluepay%2Ecom%2Finterfaces%2Fshpf%3FSHPF%5FFORM%5FID%3DCJF02%26SHPF%5FACCOUNT%5FID%3D100033809953%26SHPF%5FTPS%5FDEF%3DSHPF%5FACCOUNT%5FID%20SHPF%5FFORM%5FID%20RETURN%5FURL%20DBA%20AMEX%5FIMAGE%20DISCOVER%5FIMAGE%20SHPF%5FTPS%5FDEF%26SHPF%5FTPS%3D4bce39f14011411bd7838a0f6d19823e%26RETURN%5FURL%3Djavascript%253Ahistory%252Ego%2528%252D2%2529%26DBA%3DCharles%2520J%252E%2520Fiore%2520Company%2520Inc%252E%2520%26AMEX%5FIMAGE%3Damex%252Egif%26DISCOVER%5FIMAGE%3Ddiscvr%252Egif&TPS_DEF=MERCHANT%20APPROVED%5FURL%20DECLINED%5FURL%20MISSING%5FURL%20MODE%20TRANSACTION%5FTYPE%20TPS%5FDEF%20REBILLING%20REB%5FCYCLES%20REB%5FAMOUNT%20REB%5FEXPR%20REB%5FFIRST%5FDATE&CUSTOM_HTML=
" style="color: #ffffff;">Make Payment</a></div>
	
</div><!-- absolute position -->

</div><!-- topMenu -->
</div><!-- topMenuHolder -->
<!-- end header -->
<div id="contentBackground"<?php echo CONTENT_BACKGROUND_STYLE; ?>>&nbsp;</div>
<div id="contentHolder">
<div id="content">
<?php
}

/* top menu */
function makeMenu() {
?>
<table id="menuTable" cellspacing="0" cellpadding="0" border="0" style="float: left; margin-right: 20px;">
	<tr>
		<td valign="middle" align="center" class="roundLeft4" id="menuTD0"><a href="about.php" onmouseover="showSubmenu(0);" onmouseout="hideSubmenu(0);" id="menu0"<?php if (strpos(PAGE, 'about') !== false) { echo ' style="color: #da771b;"'; } ?>>ABOUT US</a></td>
		<td valign="middle" align="center" id="menuTD1"><a href="services.php" onmouseover="showSubmenu(1);" onmouseout="hideSubmenu(1);" id="menu1"<?php if (strpos(PAGE, 'services') !== false) { echo ' style="color: #da771b;"'; } ?>>SERVICES</a></td>
		<td valign="middle" align="center" id="menuTD2"><a href="products.php" onmouseover="showSubmenu(2);" onmouseout="hideSubmenu(2);" id="menu2"<?php if (PAGE == 'products.php') { echo ' style="color: #da771b;"'; } ?>>PRODUCTS</a></td>
		<td valign="middle" align="center"><a href="partners.php"<?php if (strpos(PAGE, 'partners') !== false) { echo ' style="color: #da771b;"'; } ?>>PARTNERS</a></td>
		<td valign="middle" align="center"><a href="resources.php"<?php if (PAGE == 'resources.php') { echo ' style="color: #da771b;"'; } ?>>RESOURCES</a></td>
		<td valign="middle" align="center" class="roundRight4"><a href="contact.php"<?php if (PAGE == 'contact.php') { echo ' style="color: #da771b;"'; } ?>>CONTACT</a></td>
	</tr>
</table>
<?php
}



/* bottom footer */
function makeFooter() {
?>

<!-- footer table -->
<?php
if (PAGE != 'index.php') {
?>
</div><!-- close content -->
<table width="970" cellspacing="0" cellpadding="0" border="0" id="footerTable" style="">
	<tr>
		<td valign="middle" align="left" style="height: 44px;"><img src="images/socials2.gif" width="98" height="23" border="0" usemap="#socials"></td>
		<td valign="middle" align="right" style="white-space: nowrap;">
			PRAIRIE VIEW: 847.913.1414 · CHICAGO: 773.533.1414 · BOLINGBROOK: 630.739.1414 · INDIANAPOLIS: 317.471.7288 · <a href="contact.php">Contact Us</a> · ©<?php echo date('Y'); ?>
		</td>
	</tr>
	<tr>
		<td><img src="images/spacer.gif" width="100" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="870" height="1" border="0"></td>
	</tr>
</table>

<?php
	if ($_SERVER['REMOTE_ADDR'] == '::1' || $_SERVER['SERVER_ADDR'] == '10.0.1.2' || $_SERVER['SERVER_ADDR'] == '68.168.98.213') { } else {
?>
<script type="text/javascript">
 
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-39264121-1']);
  _gaq.push(['_trackPageview']);
 
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
 
</script>
<?php
	} // end if we're not on a staging site
}
?>

</div><!-- close contentHolder -->


<map name="socials">
<area shape="rect" coords="0, 0, 23, 23" href="http://www.twitter.com/fiorenursery" target="_blank" style="border: solid #ff0000 1px;">
<area shape="rect" coords="25, 0, 47, 23" href="https://www.facebook.com/pages/Fiore-Nursery-and-Landscape-Supply/181704598536855" target="_blank">
<area shape="rect" coords="48, 0, 73, 23" href="https://instagram.com/fiorenursery/" target="_blank">
<area shape="rect" coords="73, 0, 97, 23" href="http://www.linkedin.com/company/fiore-nursery-and-landscape-supply?trk=hb_tab_compy_id_1731374" target="_blank">
</map>

<div class="submenu" id="submenu0" onmouseover="showSubmenu(0);" onmouseout="hideSubmenu(0);" >
<div class="submenuSpacer"></div>
<a href="about_history.php" style="margin-top: 0px;<?php if (PAGE == 'about_history.php') { echo ' color: #da771b;'; } ?>">History</a>
<a href="about_team.php"<?php if (PAGE == 'about_team.php') { echo ' style="color: #da771b;"'; } ?>>Team</a>
<a href="about_locations.php"<?php if (PAGE == 'about_locations.php') { echo ' style="color: #da771b;"'; } ?>>Hours / Locations</a>
<a href="about_staff.php#careers"<?php if (PAGE == 'about_careers.php') { echo ' style="color: #da771b;"'; } ?>>Careers</a>
</div>

<div class="submenu" id="submenu1" onmouseover="showSubmenu(1);" onmouseout="hideSubmenu(1);">
<div class="submenuSpacer"></div>
<a href="services_sourcing.php" style="margin-top: 0px;<?php if (PAGE == 'services_sourcing.php') { echo ' color: #da771b;'; } ?>">Sourcing</a>
<a href="services_sourcing.php#purchasing_solutions"<?php if (PAGE == 'services_purchasing.php') { echo ' style="color: #da771b;"'; } ?>>Purchasing Solutions</a>
<a href="services_delivery.php"<?php if (PAGE == 'services_delivery.php') { echo ' style="color: #da771b;"'; } ?>>Delivery</a>
<a href="services_other.php"<?php if (PAGE == 'services_other.php') { echo ' style="color: #da771b;"'; } ?>>Planting / Fabrication</a>
</div>

<div class="submenu" id="submenu2" onmouseover="showSubmenu(2);" onmouseout="hideSubmenu(2);" >
<div class="submenuSpacer"></div>
<a href="products_plants.php" style="margin-top: 0px;">Plants</a>
<a href="products_landscape_supply.php">Landscape Supply</a>
<a href="products_natural_stone.php">Natural Stone</a>
<a href="products_concrete_clay_pavers.php">Concrete &amp; Clay Pavers</a>
<a href="products_green_roof.php">Green Roof</a>
<a href="products_planters_accessories.php">Planters and Accessories</a>
<a href="products_specials.php">Specials</a>
<?php
if (isWholesale()) {
	echo '<a href="products_seasonal.php">Seasonal</a>';
}
?>
</div>

<?php
	if (isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'es') {
		echo '<script language="javascript">doTranslate();</script>';
	}
}




function makeSubmenuLink($link) {
	if ($link == PAGE) {
		echo '<a href="'.$link.'" style="color: #db781b;">';
	} else {
		echo '<a href="'.$link.'">';
	}
}

function makeIndexBG() {
	$tmpDate = date('nd');
	if (true) {
		// always on
		$img0 = 'images/index18.jpg';
		$img1 = 'images/index17.jpg';
		$img2 = 'images/index16.jpg';
	} else if ($tmpDate >= 310 && $tmpDate < 621) {
		// spring
		$img0 = 'images/index3.jpg';
		$img1 = 'images/index4.jpg';
		$img2 = 'images/index15.jpg';
	} else if ($tmpDate >= 621 && $tmpDate < 922) {
		// summer
		$img0 = 'images/index6.jpg';
		$img1 = 'images/index7.jpg';
		$img2 = 'images/index8.jpg';
	} else if ($tmpDate >= 922 && $tmpDate < 1221) {
		// fall
		$img0 = 'images/index9.jpg';
		$img1 = 'images/index10.jpg';
		$img2 = 'images/index11.jpg';
	} else {
		// winter
		$img0 = 'images/index0.jpg';
		$img1 = 'images/index1.jpg';
		$img2 = 'images/index2.jpg';
	}
	
	if (isset($_GET['t']))
		$img2 = 'images/index11b.jpg';
?>
<div id="indexBG">
<div style="background-image: URL('<?php echo $img0; ?>'); background-position: top center; left: 0px;" id="indexBG0"></div>
<div style="background-image: URL('<?php echo $img1; ?>'); background-position: top center;" id="indexBG1"></div>
<div style="background-image: URL('<?php echo $img2; ?>'); background-position: top center;" id="indexBG2"></div>
</div>

<script language="javascript">
resizeIndexBG();
</script>
<?php
}











function emaiHTMLWithTitleTo($html, $title = 'Online Form', $to = 'matt@mattcourtright.com', $from = 'Admin <noreply@cjfiore.com>') {
	$txt = str_replace('<tr>', '<tr><br>', $html);
	$txt = str_replace("<br>", "\n", $txt);
	$txt = str_replace("</h3>", "\n\n", $txt);
	$txt = str_replace("&nbsp;", " ", $txt);
	$txt = strip_tags($txt);
	
	$txt = wordwrap($txt);
	$html = wordwrap($html);

	 $semi_rand = md5( time() ); 
	 $mime_boundary = "==MattMail-1-x{$semi_rand}x"; 
	 $mime_boundary2 = "==MattMail-2-x{$semi_rand}x"; 
	 
	 $headers = "From: ".$from."\n";
	 $headers .= "MIME-Version: 1.0\n" . 
		"Content-Type: multipart/mixed; \n" .
		" boundary=\"{$mime_boundary}\"";
	
	 $message = "This is a multi-part message in MIME format.\n\n" . 
		"--{$mime_boundary}\n" .
		"Content-Type: multipart/alternative; boundary=\"{$mime_boundary2}\"\n\n";
	
	 $message .= "--{$mime_boundary2}" .
		"\nContent-Type: text/plain; charset=\"us-ascii\"\n" . 
		"Content-Transfer-Encoding: 7bit\n\n";
	
	 $message .= $txt."\n\n";
	
	 $message .= "--{$mime_boundary2}\n" . 
					"Content-Type: text/html; charset=\"us-ascii\"\n" .
					"Content-Transfer-Encoding: 7bit\n\n" . 
			$html."\n\n";
	
	 $message .= "--{$mime_boundary2}--\n\n";
	 
			 $message .= "--{$mime_boundary}--\n\n";
	
	
	
	//mail($to, $title, $message, $headers);
	sendPearMailToWithSubjectAndMessage($to, $title, $html);
}

function emailAssociativeArrayWithTitleTo($arr, $title = 'Online Form', $to = 'matt@mattcourtright.com', $from = 'Admin <noreply@cjfiore.com>') {
	$txt = '';
	$html = '<table cellspacing="5" cellpadding="0" border="0"><tr><td valign="top" align="left" colspan="2"><h3>' . $title . '</h3> </td></tr>';
	
	/* convert to txt and html */
	foreach($arr as $key => $val) {
		if ($val == $key)
			$val = '';
			
		$txt .= "\n".$key.":\t".$val;
		$html .= '<tr><td valign="top" align="right"><b>'.$key.'</b> </td><td valign="top" align="left">'.$val.' </td></tr>';
	}
	
	/* close the html table */
	$html .= '</table><br><br>';
	
	 $semi_rand = md5( time() ); 
	 $mime_boundary = "==MattMail-1-x{$semi_rand}x"; 
	 $mime_boundary2 = "==MattMail-2-x{$semi_rand}x"; 
	 
	 $headers = "From: ".$from."\n";
	 $headers .= "MIME-Version: 1.0\n" . 
		"Content-Type: multipart/mixed; \n" .
		" boundary=\"{$mime_boundary}\"";
	
	 $message = "This is a multi-part message in MIME format.\n\n" . 
		"--{$mime_boundary}\n" .
		"Content-Type: multipart/alternative; boundary=\"{$mime_boundary2}\"\n\n";
	
	 $message .= "--{$mime_boundary2}" .
		"\nContent-Type: text/plain; charset=\"us-ascii\"\n" . 
		"Content-Transfer-Encoding: 7bit\n\n";
	
	 $message .= $txt."\n\n";
	
	 $message .= "--{$mime_boundary2}\n" . 
					"Content-Type: text/html; charset=\"us-ascii\"\n" .
					"Content-Transfer-Encoding: 7bit\n\n" . 
			$html."\n\n";
	
	 $message .= "--{$mime_boundary2}--\n\n";
	 
			 $message .= "--{$mime_boundary}--\n\n";
	//mail($to, $title, $message, $headers);
	sendPearMailToWithSubjectAndMessage($to, $title, $html);
}

function sendPearMailToWithSubjectAndMessage($to, $subject, $html) {
	// get random boundary
	$semi_rand = md5( time() );
	$mime_boundary = "==MattMail-1-x{$semi_rand}x";
	$mime_boundary2 = "==MattMail-2-x{$semi_rand}x"; 
	
	// build headers array 
	$headers = array(	'From' => 'CJ Fiore <noreply@cjfiore.com>',
						'To' => $to,
						'Subject' => $subject,
						'MIME-Version' => '1.0',
						'Content-Type' => 'multipart/mixed; boundary="'.$mime_boundary.'"',
						'Date'      => date('r', time()),
						'Message-Id' => '<'.  microtime(true).'@cjfiore.com>');
	
	
	// send from cjfiore.com
	$params["host"] = "mail1.nicsys.net";
	$params["port"] = "8889";
	$params["auth"] = true;
	$params["username"] = "noreply@cjfiore.com";
	$params["username"] = "cjfiore@nicsys.net";
	$params["password"] = "CjFiore42213!!!";
	
	//localhost fix
	if ($_SERVER['REMOTE_ADDR'] == '::1' || $_SERVER['SERVER_ADDR'] == '10.0.1.2') {
		// localhost
		$params["host"] = "mail.ivocabulary.com";
		$params["port"] = "2525";
		$params["auth"] = true;
		$params["username"] = "admin@ivocabulary.com";
		$params["password"] = "Smashing17";
	}

	
	// convert html to text
	$txt = strip_tags(br2nl($html));
	
	// add word-wraps		
	$txt = wordwrap($txt);
	$html = wordwrap($html);

	
	// build the message body
	$message = "This is a multi-part message in MIME format.\n\n" . 
	"--{$mime_boundary}\n" .
	"Content-Type: multipart/alternative; boundary=\"{$mime_boundary2}\"\n\n";
	$message .= "--{$mime_boundary2}" .
	"\nContent-Type: text/plain; charset=\"us-ascii\"\n" . 
	"Content-Transfer-Encoding: 7bit\n\n";
	$message .= $txt."\n\n";
	$message .= "--{$mime_boundary2}\n" . 
				"Content-Type: text/html; charset=\"us-ascii\"\n" .
				"Content-Transfer-Encoding: 7bit\n\n" . 
		$html."\n\n";
	$message .= "--{$mime_boundary2}--\n\n";
	$message .= "--{$mime_boundary}--\n\n";
	
	
	$mail_object =& Mail::factory('smtp', $params);
	$mail = $mail_object->send($to, $headers, $message);
	
	/* check for error
	$params["verp"] = FALSE; // comment out this line if you see an XVERP error
	$params["debug"] = TRUE;

	if (PEAR::isError($mail)) {
        echo($mail->getMessage());
    }
    else {
        echo("Message successfully sent!");
    }
    */
}


function br2nl($string){
	$out = preg_replace('#<br\s*?/?>#i', " \r\n", $string);
	$out = str_replace("<tr>", " \r\n", $out);
	$out = str_replace("</td>", " \t", $out);

	return $out;
}








// getItemArray functions all take some argument and return an array of associative arrays
function getProductArrayFromCatID($cat) {
	// validate $cat
	if (!is_numeric($cat))
		$cat = -1;
	
	// select all items with $cat = category_id
	$query = 'SELECT * FROM `Items` WHERE category_id='.$cat.' ORDER BY is_available DESC, sort_order ASC';
		
	$result = mysql_query($query);
	$outArr = array();
	for ($i=0;$i<mysql_num_rows($result);$i++) {
		// create a new array
		$tmpArr = array();
		// add item array
		$tmpArr['itemArr'] = mysql_fetch_array($result);
		// add plants array 
		$tmpArr['plantsArr'] = getPlantsArrayFromID($tmpArr['itemArr']['item_id']);
		// add sizes array 
		$tmpArr['sizesArr'] = getSizesArrayFromID($tmpArr['itemArr']['item_id']);
		
		// finally, we add the $tmpArr to the $outArr
		$outArr[] = $tmpArr;
	}
	
	return $outArr;
}

// returns the plants row if it exists or false
function getPlantsArrayFromID($id) {
	// validate $id
	if (!is_numeric($id))
		$id = -1;
	
	$query = 'SELECT * FROM `Plants` WHERE item_id='.$id;
	$result = mysql_query($query);
	if (mysql_num_rows($result) > 0)
		return mysql_fetch_array($result);
		
	return false;
}

// returns an array of sizes if they exist or false
function getSizesArrayFromID($id) {
	// validate $id
	if (!is_numeric($id))
		$id = -1;
	
	$query = 'SELECT * FROM `Sizes` WHERE item_id='.$id.' AND is_available=1 ORDER BY sort_order';
	$result = mysql_query($query);
	$outArr = array();
	for ($i=0;$i<mysql_num_rows($result);$i++) {
		$outArr[] = mysql_fetch_array($result);
	}
		
	return $outArr;
}


function logInUserWithID($id) {
	if (!is_numeric($id)) {
		return;
	}
	
	$result = mysql_query('SELECT * FROM `UserInfo` WHERE user_id='.$id);
	$_SESSION['user'] = mysql_fetch_array($result);
	
	// add wholesale if it's set
	if ($_SESSION['user']['is_wholesale'] == 1) {
		$_SESSION['wholesale'] = true;
	}
	
	// update the last_login
	mysql_query('UPDATE `UserInfo` SET last_login='.date('U').' WHERE user_id='.$id);
	
	// update cart
	restoreCartFromDB();
}




function saveCartToDB() {
	if (isset($_SESSION['user']['user_id']) && is_numeric($_SESSION['user']['user_id'])) {
		// delete any carts
		mysql_query('DELETE FROM `SavedCarts` WHERE user_id='.$_SESSION['user']['user_id']);
		// re-add the cart
		mysql_query('INSERT INTO `SavedCarts` VALUES ('.$_SESSION['user']['user_id'].', "'.mysql_real_escape_string(serialize($_SESSION['cart'])).'")');
	}
}

function restoreCartFromDB() {
	if (isset($_SESSION['user']['user_id']) && is_numeric($_SESSION['user']['user_id'])) {
		$result = mysql_query('SELECT * FROM `SavedCarts` WHERE user_id='.$_SESSION['user']['user_id']);
		if (mysql_num_rows($result) > 0) {
			$row = mysql_fetch_array($result);
			
			if (!isset($_SESSION['cart'])) {
				// if they don't have a SESSION cart, we just set it.
				$_SESSION['cart'] = unserialize($row['cart']);
			} else {
				// they DO have a SESSOIN cart, so we must MERGE the two
				$_SESSION['cart'] = array_merge($_SESSION['cart'], unserialize($row['cart']));
			}
			
		} // end if there's a cart in the database
	} // end if the user is signed in
}



function isWholesale() {
	if (isset($_SESSION['wholesale']) && $_SESSION['wholesale'] == true)
		return true;
	else
		return false;
}
