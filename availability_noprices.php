<?php
// output it as a file download
$filename = '_avail/_fiorenurseryavailability_noprices.xlsx';
$c = file_get_contents($filename);
header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment; filename="Fiore Nursery Availability ' . date('Ymd', filemtime($filename)) . '.xlsx"');
echo $c;
die();
