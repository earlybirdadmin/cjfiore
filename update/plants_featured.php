<?php
require_once('../functions.php');
require_once('../functions_catalog.php');

// handle $_GET remove
if (isset($_GET['i']) && is_numeric($_GET['i'])) {
	mysql_query('UPDATE `Items` SET is_featured=0 WHERE item_id='.$_GET['i']);
}

// handle $_POST featuredDescription
if (isset($_POST['featuredDescription'])) {
	mysql_query('UPDATE `Categories` SET category_text="'.mysql_real_escape_string($_POST['featuredDescription']).'" WHERE category_id=100');
}

// get featuredDescription
$result = mysql_query('SELECT * FROM `Categories` WHERE category_id=100');
$row = mysql_fetch_array($result);
$featuredDescription = $row['category_text'];

// set up productArr
$productArr = array();
$result = mysql_query('SELECT * FROM `Items`, `Plants` WHERE Items.is_featured=1 AND Plants.item_id=Items.item_id');
for ($i=0;$i<mysql_num_rows($result);$i++) {
	$productArr[] = mysql_fetch_array($result);
}
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>fiore</title>
<?php extraCatalogHead(); ?>
<script type="text/javascript" src="update.js"></script>
<style type="text/css">
a {
	text-decoration: none;
	color: #4e4244;
}
a:hover {
	text-decoration: underline;
}
</style>
</head>
<body>
<?php makeCatalogHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">

	<tr>
		<td valign="top" align="left" style="padding: 0px 10px 0px 20px;">

<form method="POST" action="plants_featured.php">
<textarea name="featuredDescription" id="featuredDescription" rows="4" style="width: 800px;"><?php echo $featuredDescription; ?></textarea>
<br><input type="submit" value="submit">
</form>

<br>
<br>
<table width="100%" cellspacing="0" cellpadding="5" border="0" id="productTable">
	<tr>
		<td valign="middle" align="left" style="border-bottom: solid #878787 1px;"><b>Item Name</b></td>
		<td valign="middle" align="left" style="border-bottom: solid #878787 1px; width: 200px;">&nbsp;</td>
		<td valign="middle" align="right" style="border-bottom: solid #878787 1px; width: 300px;"><b>Remove From Featured</b></td>
	</tr>

<?php
for ($i=0;$i<count($productArr);$i++) {
	echo '<tr>';
	
	// item name
		echo '<td valign="top" align="left" style="border-bottom: solid #878787 1px;"><span id="product'.$i.'" onclick="convertToInput(this, '.$i.');" style="font-family: fiore-bold; font-weight: bold;">'.$productArr[$i]['item_name'].'</span></td>';
	// details
	echo '<td valign="top" align="center" style="border-bottom: solid #878787 1px;"><a href="item.php?i='.$productArr[$i]['item_id'].'">Details...</a></td>';
	
	// remove link
	echo '<td valign="top" align="right" style="border-bottom: solid #878787 1px;"><a href="plants_featured.php?i='.$productArr[$i]['item_id'].'">[x] Remove from featured</a></td>';

	echo '</tr>';
}
?>
</table>

		</td>
	</tr>
</table>

<?php makeCatalogFooter(); ?>



</body>
</html>
