<?php
// set category = planters and accessories
$_GET['c'] = 5;


require_once('../functions_catalog.php');
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Plants</title>
<?php extraCatalogHead(); ?>
</head>
<body>
<?php makeCatalogHeader(false); ?>

<table width="801" cellspacing="0" cellpadding="0" border="0" style="margin-bottom: 5px;">
	<tr>
		<td valign="top" align="left">
<div class="productPlantBlock roundTopLeft10">
<a href="browse.php?c=23&category=planters"><img src="../images/products_planters_accessories0.jpg" width="375" height="261" border="0" class="roundTopLeft10"><div>Planters &gt;</div></a>
</div>
		</td>
		<td valign="top" align="left">
<div class="productPlantBlock">
<a href="browse.php?c=15&category=artificial_boulders"><img src="../images/products_planters_accessories1.jpg" width="420" height="261" border="0"><div>Artificial Boulders &gt;</div></a>
</div>
		</td>
	</tr>
</table>


<table width="801" cellspacing="0" cellpadding="0" border="0" style="margin-bottom: 5px;">
	<tr>
		<td valign="top" align="left">
<div class="productPlantBlock">
<a href="browse.php?c=25&category=accessories"><img src="../images/products_planters_accessories2.jpg" width="312" height="292" border="0"><div>Accessories &gt;</div></a>
</div>
		</td>
		<td valign="top" align="left">
<div class="productPlantBlock">
<a href="products_grow_modular.php"><img src="../images/products_planters_accessories3.jpg" width="483" height="292" border="0"><div>Grow Modular &gt;</div></a>
</div>
		</td>
	</tr>
</table>





<?php makeCatalogFooter(); ?>

</body>
</html>
