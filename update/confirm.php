<?php
require_once('functions_catalog.php');

// init cart if there's nothing... um... in it
if (!isset($_SESSION['cart']))
	$_SESSION['cart'] = array();
	

// combine cart items that match
$cartQuantity = 0;
for ($i=0;$i<count($_SESSION['cart']) - 1;$i++) {
	for ($j=$i+1;$j<count($_SESSION['cart']);$j++) {
		if ($_SESSION['cart'][$i]['item_id'] == $_SESSION['cart'][$j]['item_id']) {
			$_SESSION['cart'][$i]['quantity'] += $_SESSION['cart'][$j]['quantity'];
			unset($_SESSION['cart'][$j]);
			$_SESSION['cart'] = array_values($_SESSION['cart']);
		}
	}
	$cartQuantity += $_SESSION['cart'][$i]['quantity'];
}
$cartQuantity += $_SESSION['cart'][$i]['quantity'];

// if there are no items in the cart, we redirect to the cart page...
if ($cartQuantity == 0) {
	header('Location: cart.php');
	die();
}


// add post data to user session information
$qVals = array('Name', 'Company', 'Address', 'Email', 'Phone', 'Fax', 'Job Name', 'Purchase Order #', 'Pickup or Delivery', 'Location', 'Date Wanted');
$sessionVals = array('name', 'company', 'address', 'email', 'phone', 'fax', '', '', 'is_pickup', 'fiore_location');

// set all session vals from post data (used on the checkout page)
for ($i=0;$i<count($sessionVals);$i++) {
	$idx = $sessionVals[$i];
	if ($idx != '' && isset($_POST['q'.$i])) {
		$_SESSION['user'][$idx] = trim($_POST['q'.$i]);
	}
}

$reqArr = array(0, 2, 3, 4);
// make sure all required fields are filled out
for ($i=0;$i<count($reqArr);$i++) {
	$tmpIdx = $reqArr[$i];
	if (!isset($_POST['q'.$tmpIdx]) || trim($_POST['q'.$tmpIdx]) == '') {
		$GLOBALS['errorMsg'] = 'Please fill out your '.$qVals[$tmpIdx].'.';
		$GLOBALS['highlight'] = $reqArr[$i];
		require('checkout.php');
		die();
	}
}


// set wholesale
if (isWholesale())
	$priceIdx = 'wholesale_price';
else
	$priceIdx = 'retail_price';
	
// email the order
	$html = '<table width="735" cellspacing="10" cellpadding="0">';
	for ($i=0;$i<count($qVals);$i++) {
		if (isset($_POST['q'.$i]) && trim($_POST['q'.$i]) != '' && strtolower($qVals[$i]) != strtolower($_POST['q'.$i])) {
			$html .= '<tr><td valign="top" align="left"><b>'.$qVals[$i].'</b></td>';
			$html .= '<td valign="top" align="left">'.$_POST['q'.$i].'</td></tr>';
		}
	}	

$html .= getCartTable();

//send mail to the admin
if (isset($_POST['q9']) && $_POST['q9'] == 'Chicago')
	sendPearMailToWithSubjectAndMessage(ADMIN_EMAIL, 'CJ Fiore Order', $html);
else
	sendPearMailToWithSubjectAndMessage('catebertrand@cjfiore.com', 'CJ Fiore Order', $html);

// send mail to person who ordered
$newHTML = '&nbsp;<br><h1>Thank you for your Order!</h1><p>Below is a summary of your order for your records.</p>'.$html;
sendPearMailToWithSubjectAndMessage($_POST['q3'], 'CJ Fiore Order', $newHTML);



// if we're logged in, run some maintenance
if (isset($_SESSION['user']['user_id']) && is_numeric($_SESSION['user']['user_id'])) {
	// save the order
	mysql_query('INSERT INTO `SavedOrders` VALUES (NULL, '.$_SESSION['user']['user_id'].', '.date('U').', "'.mysql_real_escape_string($html).'")');
	
	// get the order ID
	$orderID = mysql_insert_id();
	
	// delete the cart from the db
	mysql_query('DELETE FROM `SavedCarts` WHERE user_id='.$_SESSION['user']['user_id']);
	// empty the cart
	$_SESSION['cart'] = array();
} else {
	$orderID = 'N/A';
}

// add bluePay
$html = addBluePayTable($html, $orderID);


?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Thank you for your Order! CJ Fiore, Nursery and Landscape Supply</title>
<?php extraHead(); ?>
</head>
<body>
<?php makeHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" colspan="3" style="padding: 0px 20px 10px 20px;">
			<h1>Thank you for your Purchase Order!</h1>
			<br>We will contact you about filling your order soon.
			<br><?php echo $html; ?>
		</td>
	</tr>

	<tr>
		<td><img src="images/spacer.gif" width="320" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="350" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="225" height="1" border="0"></td>
	</tr>
</table>

<?php makeFooter(); ?>


</body>
</html>
<?php
// finally, re-load the user if we can
if (isset($_SESSION['user']['user_id']) && is_numeric($_SESSION['user']['user_id'])) {
	logInUserWithID($_SESSION['user']['user_id']);
}


function getCartTable() {
	$o = '<link rel=stylesheet type="text/css" href="http://www.cjfiore.com/fiore.css">';
	// order table
	$o .= '<table width="735" cellspacing="0" cellpadding="0" border="0">';
	$o .= '	<tr>		<td><img src="http://www.cjfiore.com/images/spacer.gif" width="15" height="20" border="0"></td>		<td><img src="http://www.cjfiore.com/images/spacer.gif" width="330" height="1" border="0"></td>		<td><img src="http://www.cjfiore.com/images/spacer.gif" width="180" height="1" border="0"></td>		<td><img src="http://www.cjfiore.com/images/spacer.gif" width="100" height="1" border="0"></td>		<td><img src="http://www.cjfiore.com/images/spacer.gif" width="80" height="1" border="0"></td>			</tr>	<tr>		<td class="roundLeft4 darkBG">&nbsp;</td>		<td class="darkBG"><b style="font: 11px fiore-bold, Arial, sans-serif; font-weight: bold;">ITEM</b></td>		<td class="darkBG"><b style="font: 11px fiore-bold, Arial, sans-serif; font-weight: bold; padding-left: 50px;">DETAILS</b></td>		<td class="darkBG"><b style="font: 11px fiore-bold, Arial, sans-serif; font-weight: bold;">QUANTITY</b></td>		<td class="roundRight4 darkBG"><b style="font: 11px fiore-bold, Arial, sans-serif; font-weight: bold;">COST</b></td>	</tr>';

	if (isWholesale())
		$priceIdx = 'wholesale_price';
	else
		$priceIdx = 'retail_price';
	
	$subtotal = 0;

	for ($i=0;$i<count($_SESSION['cart']);$i++) {
		// validate data
		if (!is_numeric($_SESSION['cart'][$i]['item_id']) || !is_numeric($_SESSION['cart'][$i]['quantity']))
			continue;

		$item = getItemArrFromSizeID($_SESSION['cart'][$i]['item_id']);
		if (count($item) > 0) {
			$o .= '<tr>';
				$o .= '<td></td>';
				$o .= '<td valign="middle" align="left" style="border-bottom: solid #d9d7d7 1px; padding: 15px 0px 20px 0px;"><div class="brownLinks" style="font-size: 11px;">'.getAbsoluteBreadCrumbsForItemWithID($item['item_id']).'</div><h4>'.$item['item_name'].'</h4>';
				if (isset($item['item_name2']))
					$o .= '<br>'.$item['item_name2'];
				$o .= '</td>';
		
		
				$o .= '<td valign="middle" align="left" style="border-bottom: solid #d9d7d7 1px;">';
					$o .= '<table cellspacing="0" cellpadding="3" border="0"><tr><td><img src="images/spacer.gif" width="45" height="1" border="0"></td><td><img src="images/spacer.gif" width="135" height="1" border="0"></td></tr><tr>';
					$o .= '<td valign="middle" align="right" style="padding-right: 10px;"><span style="font-size: 12px;">Size</span></td>';
					$o .= '<td valign="middle" align="left" style="padding-bottom: 8px;"><span style="font: 12px fiore-book, sans-serif;">'.$item['item_size'].'</span></td></tr>';
					$o .= '<tr><td valign="middle" align="right" style="padding-right: 10px;"><span style="font-size: 12px;">Price</span></td>';
					$o .= '<td valign="middle" align="left"><span style="font: 12px fiore-book, sans-serif;">$'.$item[$priceIdx].'</span></td></tr></table>';
				$o .= '</td>'; 
		
				$o .= '<td valign="middle" align="left" style="border-bottom: solid #d9d7d7 1px; padding-left: 10px;">'.$_SESSION['cart'][$i]['quantity'].'</td>';
		
				$o .= '<td valign="middle" align="left" style="border-bottom: solid #d9d7d7 1px;"><span style="font: 12px fiore-book, Arial;">$'.number_format($item[$priceIdx] * $_SESSION['cart'][$i]['quantity'],2, '.', ',').'</span></td>';
		
			$o .= '</tr>';
		
			$subtotal += $item[$priceIdx] * $_SESSION['cart'][$i]['quantity'];
		}
	}
	$o .= '<tr>';
	$o .= '<td colspan="5" valign="top" align="right" padding-right: 20px;">&nbsp;<br>Subtotal:<br><b style="font: 18px fiore-bold, Arial, sans-serif; font-weight: bold;">$'.number_format($subtotal, 2, '.', ',');

	$o .= '<br><br><span style="font: 11px Arial, sans-serif; line-height: 1.4; font-weight: normal; width: 100%; text-align: left;">Taxes and delivery fees (if applicable) <br>will be calculated at the time of <br>availability confirmation from Fiore.</span>';

	$o .= '</td>';

	$o .= '	</tr></table>';
	// end order table
	
	// set global var for price
	$GLOBALS['subtotal'] = number_format($subtotal, 2, '.', ',');
	
	return $o;
}



function addBluePayTable($html = '', $invoiceID = 'N/A') {
	// init subtotal
	if (isset($GLOBALS['subtotal']))
		$subtotal = $GLOBALS['subtotal'];
	else
		$subtotal = '';

	// outer-table (for bluePay link)
	$o = '<table width="970" cellspacing="0" cellpadding="0" border="0"><tr><td valign="top" align="left">';
	// add original html
	$o .= $html;
	$o .= '<td><div style="width: 20px;">&nbsp;</div></td><td valign="bottom" align="left">';
		$o .= '<small><b>Make Payment:</b></small><br>';
		// begin bluePay link
		$o .= '<form action="https://secure.bluepay.com/interfaces/shpf" method="POST" target="_blank">';
		$o .= '<input type="hidden" name="SHPF_FORM_ID" value="CJF01"><input type="hidden" name="SHPF_ACCOUNT_ID" value="100033809953"><input type="hidden" name="SHPF_TPS_DEF" value="SHPF_FORM_ID SHPF_ACCOUNT_ID DBA TAMPER_PROOF_SEAL AMEX_IMAGE DISCOVER_IMAGE TPS_DEF SHPF_TPS_DEF CUSTOM_HTML REBILLING REB_CYCLES REB_AMOUNT REB_EXPR REB_FIRST_DATE"><input type="hidden" name="SHPF_TPS" value="20082bd28ccc0adc94b0c5389040b21b"><input type="hidden" name="MODE" value="LIVE"><input type="hidden" name="TRANSACTION_TYPE" value="SALE"><input type="hidden" name="DBA" value="Charles J. Fiore Company Inc. "><input type="hidden" name="TAMPER_PROOF_SEAL" value="f219b89be6cf7b846d360215f8a5a591"><input type="hidden" name="REBILLING" value="0"><input type="hidden" name="REB_CYCLES" value=""><input type="hidden" name="REB_AMOUNT" value=""><input type="hidden" name="REB_EXPR" value=""><input type="hidden" name="REB_FIRST_DATE" value=""><input type="hidden" name="AMEX_IMAGE" value="amex.gif"><input type="hidden" name="DISCOVER_IMAGE" value="discvr.gif"><input type="hidden" name="REDIRECT_URL" value="https://secure.bluepay.com/interfaces/shpf?SHPF_FORM_ID=CJF02&amp;SHPF_ACCOUNT_ID=100033809953&amp;SHPF_TPS_DEF=SHPF_ACCOUNT_ID SHPF_FORM_ID RETURN_URL DBA AMEX_IMAGE DISCOVER_IMAGE SHPF_TPS_DEF&amp;SHPF_TPS=4bce39f14011411bd7838a0f6d19823e&amp;RETURN_URL=javascript%3Ahistory%2Ego%28%2D2%29&amp;DBA=Charles%20J%2E%20Fiore%20Company%20Inc%2E%20&amp;AMEX_IMAGE=amex%2Egif&amp;DISCOVER_IMAGE=discvr%2Egif"><input type="hidden" name="TPS_DEF" value="MERCHANT APPROVED_URL DECLINED_URL MISSING_URL MODE TRANSACTION_TYPE TPS_DEF REBILLING REB_CYCLES REB_AMOUNT REB_EXPR REB_FIRST_DATE"><input type="hidden" name="CUSTOM_HTML" value="">';
		$o .= '<input type="hidden" name="AMOUNT" value="'.$subtotal.'">';
		$o .= '<input type="hidden" name="CUSTOM_ID" value="'.getUserVal(13).'">';
		$o .= '<input type="hidden" name="INVOICE_ID" value="'.$invoiceID.'">';
		$o .= '<input type="hidden" name="NAME" value="'.getUserVal(5).'">';
		$o .= '<input type="hidden" name="COMPANY_NAME" value="'.getUserVal(7).'">';
		$o .= '<input type="hidden" name="ADDR1" value="'.getUserVal(8).'">';
		$o .= '<input type="hidden" name="CITY" value="'.getUserVal(11).'">';
		$o .= '<input type="hidden" name="STATE" value="'.getUserVal(12).'">';
		$o .= '<input type="hidden" name="ZIPCODE" value="'.getUserVal(10).'">';
		$o .= '<input type="hidden" name="EMAIL" value="'.getUserVal(3).'">';
		$o .= '<input type="hidden" name="PHONE" value="'.getUserVal(9).'">';
		
		$o .= '<input type="image" src="images/bluePayLogo.gif" width="140" height="45" border="0" value="Pay Now">';
		$o .= '</form><br>&nbsp;';
		// end bluePay link
		
	// close outer-table
	$o .= '</td></tr></table>';
	
	return $o;
}

function getUserVal($id) {
	$idxArr = array('user', 'password0', 'password1', 'email', 'is_business', 'first_name', 'last_name', 'company', 'address', 'phone', 'zip', 'city', 'state', 'user_id');
	
	$idx = $idxArr[$id];
	if (isset($_SESSION['user'][$idx]))
		return $_SESSION['user'][$idx];
	else
		return '';
}