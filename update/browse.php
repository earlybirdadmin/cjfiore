<?php
require_once('../functions.php');
require_once('../functions_catalog.php');

// handle POST data
// it's a tab-delimited set of:
//itemID, itemOrder, itemName, itemDescription, sizesArr, available
if (isset($_POST['itemTable']) && isset($_POST['catID']) && is_numeric($_POST['catID'])) {
	// handle delete
	if (isset($_POST['deleteID']) && is_numeric($_POST['deleteID'])) {
		mysql_query('DELETE FROM `Items` WHERE item_id='.$_POST['deleteID']);
		mysql_query('DELETE FROM `Sizes` WHERE item_id='.$_POST['deleteID']);
		mysql_query('DELETE FROM `Plants` WHERE item_id='.$_POST['deleteID']);
	}
	
	// update category description
	mysql_query('UPDATE `Categories` SET category_text="'.mysql_real_escape_string($_POST['categoryDescription']).'" WHERE category_id='.$_POST['catID']);

	$rows = explode("****----****", $_POST['itemTable']);
	for ($i=0;$i<count($rows);$i++) {
		$cols = explode("-*-*-*-*", $rows[$i]);
		
	
		// validate
		if (!is_numeric($cols[0]) || !is_numeric($cols[1]) && !is_numeric($cols[5])) {
			continue;
		}

		// update ItemsArr
		if ($cols[0] == -1) {
			// insert row
			$query = 'INSERT INTO `Items` VALUES (NULL, '.$_POST['catID'].', "'.mysql_real_escape_string(strip_tags($cols[2])).'", "'.mysql_real_escape_string($cols[3]).'", "nophoto.jpg", '.$cols[1].', '.$cols[5].', 0, 0.00)';
			$result = mysql_query($query);
			// set the item id
			$cols[0] = mysql_insert_id();
		} else {
			// update row
			$query = 'UPDATE `Items` SET sort_order='.$cols[1].', item_name="'.mysql_real_escape_string(strip_tags($cols[2])).'", description="'.mysql_real_escape_string($cols[3]).'", is_available='.$cols[5].' WHERE item_id='.$cols[0];
			mysql_query($query);
		}
		
		
		// update sizesArr
		// first we delete all the sizes
		mysql_query('DELETE FROM `Sizes` WHERE item_id='.$cols[0]);
		// then we re-add them
		$sizes = explode('::', $cols[4]);
		for ($j=0;$j<count($sizes);$j++) {
			$pArr = explode(':', $sizes[$j]);
			
			// validate & run insert query
			if (count($pArr) > 2 && is_numeric($pArr[1]) && is_numeric($pArr[2])) {
				// remove commas
				$pArr[1] = str_replace(',', '', $pArr[1]);
				$pArr[2] = str_replace(',', '', $pArr[2]);
			
				$query = 'INSERT INTO `Sizes` VALUES (NULL, '.$cols[0].', '.$j.', "'.mysql_real_escape_string($pArr[0]).'", '.$pArr[1].', '.$pArr[2].', 1)';
				$result = mysql_query($query);
			} // end if valid
			
		} // end for all sizesArr

	} // end for all rows
}

// redirect if we're looking at some item's deets
if (isset($_POST['detailsID']) && is_numeric($_POST['detailsID'])) {
	header('Location: item.php?i='.$_POST['detailsID']);
	die();
}

// get the category_id
if (isset($_POST['catID']) && is_numeric($_POST['catID'])) {
	$_GET['c'] = $_POST['catID'];
}
if (!isset($_GET['c']) || !is_numeric($_GET['c'])) {
	$_GET['c'] = 7;
} 

$cat = $_GET['c'];
// get category description
$result = mysql_query('SELECT * FROM `Categories` WHERE category_id='.$cat);
$row = mysql_fetch_array($result);
$categoryDescription = $row['category_text'];


// get the list of products
$productArr = getProductArrayFromCatID($cat);
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>fiore</title>
<?php extraCatalogHead(); ?>
<script type="text/javascript" src="update.js"></script>
<style type="text/css">
a {
	text-decoration: none;
	color: #4e4244;
}
a:hover {
	text-decoration: underline;
}
</style>
</head>
<body>
<?php makeCatalogHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">

	<tr>
		<td valign="top" align="left" style="padding: 20px 10px 0px 20px;">
<input type="hidden" name="totalItems" id="totalItems" value="<?php echo count($productArr); ?>">

<textarea name="categoryDescription" id="categoryDescription" rows="4" style="width: 800px;"><?php echo $categoryDescription; ?></textarea>
<br><input type="submit" value="submit" onclick="doSubmit();document.getElementById('updateForm').submit();">
<table width="100%" cellspacing="0" cellpadding="5" border="0" id="productTable">
	<tr>
		<td valign="middle" align="left" style="border-bottom: solid #878787 1px;"><b>Item Name</b></td>
		<td valign="middle" align="left" style="border-bottom: solid #878787 1px;"><b>Size</b></td>
		<td valign="middle" align="center" style="border-bottom: solid #878787 1px;"><b>Wholesale</b></td>
		<td valign="middle" align="center" style="border-bottom: solid #878787 1px;"><b>Retail</b></td>
		<td style="border-bottom: solid #878787 1px;"> </td>
		<td valign="middle" align="left" style="border-bottom: solid #878787 1px;"><b>Available</b></td>
		<td valign="middle" align="center" style="border-bottom: solid #878787 1px;"><b>Move</b></td>
		<td valign="middle" align="left" style="border-bottom: solid #878787 1px;"> </td>
		<td valign="middle" align="left" style="border-bottom: solid #878787 1px;"><b>Delete</b></td>
	</tr>
<?php
for ($i=0;$i<count($productArr);$i++) {
	echo '<tr>';
	
	// item id
	echo '<input type="hidden" name="itemID'.$i.'" id="itemID'.$i.'" value="'.$productArr[$i]['itemArr']['item_id'].'">';
	// sort order
	echo '<input type="hidden" id="itemOrder'.$i.'" value="'.$i.'">';
	if (!$productArr[$i]['sizesArr']) {
		// it's a label
		echo '<td valign="top" align="left" style="border-bottom: solid #878787 1px;" colspan="5"><span id="product'.$i.'" onclick="convertToInput(this, '.$i.');" style="font-family: fiore-bold; font-weight: bold;">'.$productArr[$i]['itemArr']['item_name'].'</span></td>';
	} else {
		// it's a product
		// product name
		echo '<td valign="top" align="left" style="border-bottom: solid #878787 1px; width: 250px;">';
		echo '<span id="product'.$i.'" onclick="convertToInput(this, '.$i.');">'.$productArr[$i]['itemArr']['item_name'].'</span>';
		if ($productArr[$i]['itemArr']['description'] != '')
			echo '<br><span id="description'.$i.'" onclick="convertToInput(this, '.$i.');" style="font-style: italic;">'.$productArr[$i]['itemArr']['description'].'</span></td>';
	
		// product sizes
		echo '<td valign="top" align="left" style="border-bottom: solid #878787 1px;">';
		for ($j=0;$j<count($productArr[$i]['sizesArr']);$j++) {
			echo '<div id="size'.$i.'-'.$j.'" onclick="convertToInput(this, '.$i.');">'.$productArr[$i]['sizesArr'][$j]['item_size'].'</div>';
		}
		echo '</td>';
		
		// wholesale prices
		echo '<td valign="top" align="center" style="border-bottom: solid #878787 1px;">';
		for ($j=0;$j<count($productArr[$i]['sizesArr']);$j++) {
			echo '<div id="wholesale'.$i.'-'.$j.'" onclick="convertToInput(this, '.$i.');">'.$productArr[$i]['sizesArr'][$j]['wholesale_price'].'</div>';
		}
		echo '</td>';

		// retail prices
		echo '<td valign="top" align="center" style="border-bottom: solid #878787 1px;">';
		for ($j=0;$j<count($productArr[$i]['sizesArr']);$j++) {
			echo '<div id="retail'.$i.'-'.$j.'" onclick="convertToInput(this, '.$i.');">'.$productArr[$i]['sizesArr'][$j]['retail_price'].'</div>';
		}
		echo '</td>';
		
		// add new size
		echo '<td valign="bottom" align="center" style="border-bottom: solid #878787 1px;"><a href="javascript:;" onclick="addNewSize('.$i.');">[+]</a></td>';
	} // end if it HAS sizes (IS a real product)
	
	// available
	echo '<td valign="top" align="center" style="border-bottom: solid #878787 1px;"><input type="checkbox" id="available'.$i.'" name="available'.$i.'"';
	if ($productArr[$i]['itemArr']['is_available']) {
		echo ' CHECKED';
	}
	echo '></td>';
	
	// move
	echo '<td valign="top" align="center" style="border-bottom: solid #878787 1px;"><a href="javascript:;" onclick="moveItem('.$i.', -1);">▲</a> <a href="javascript:;" onclick="moveItem('.$i.', 1);">▼</a></td>';
	
	// details
	echo '<td valign="top" align="center" style="border-bottom: solid #878787 1px;"><a href="javascript:;" onclick="showItemDetails('.$i.');">Details...</a></td>';
	
	// delete
	echo '<td valign="top" align="center" style="border-bottom: solid #878787 1px;"><a href="javascript:;" onclick="deleteItem('.$i.');">[x]</a></td>';


	echo '</tr>';
}
?>
	<tr>
		<td><img src="../images/spacer.gif" width="260" height="1" border="0"></td>
		<td><img src="../images/spacer.gif" width="110" height="1" border="0"></td>
		<td><img src="../images/spacer.gif" width="60" height="1" border="0"></td>
		<td><img src="../images/spacer.gif" width="60" height="1" border="0"></td>
		<td><img src="../images/spacer.gif" width="10" height="1" border="0"></td>
		<td><img src="../images/spacer.gif" width="50" height="1" border="0"></td>
		<td><img src="../images/spacer.gif" width="50" height="1" border="0"></td>
		<td><img src="../images/spacer.gif" width="50" height="1" border="0"></td>
		<td><img src="../images/spacer.gif" width="50" height="1" border="0" id="lastSpacer"></td>
	</tr>

</table>

&nbsp;&nbsp;&nbsp;<a href="javascript:;" onclick="addNewItem();">[+] Add New Item</a><a href="javascript:;" onclick="addNewLabel();" style="padding-left: 100px;">[+] Add New Label</a>
<br><form method="POST" action="" onsubmit="return doSubmit();" id="updateForm"><input type="hidden" name="catID" value="<?php echo $_GET['c']; ?>"><br><input type="submit" value="submit"></form>
		</td>
	</tr>
</table>

<?php makeCatalogFooter(); ?>

<script language="javascript">
function convertToInput(o, id) {
	var tmpVal = o.innerHTML;
	var newWidth = '100px';
	// if it's a title or description, the width is 250
	if (o.id.indexOf('product') > -1 || o.id.indexOf('description') > -1)
		newWidth = '250px';
	// if it's a price, the width is 50
	if (o.id.indexOf('wholesale') > -1 || o.id.indexOf('retail') > -1)
		newWidth = '50px';
	
	var newObj = document.createElement('input');
	newObj.type = 'text';
	newObj.name = o.id;
	newObj.id = o.id;
	newObj.value = tmpVal;
	newObj.style.width = newWidth;
	
	o.parentNode.replaceChild(newObj, o);
	
	// add hidden vars names
	document.getElementById('itemID'+id).name = 'itemID'+id;
}


function addNewSize(id) {
	// get the new j value (tmp id)
	for (j=0;document.getElementById('size'+id+'-'+j);j++) {
	
	}
	
	var newJ = j;
	var oldJ = j - 1;
	
	// new size input
	var tmpHTML = '<input type="text" id="size'+id+'-'+newJ+'" name="size'+id+'-'+newJ+'" style="width: 100px;">';
	appendHTMLBelowElementWithID(tmpHTML, 'size'+id+'-'+oldJ);
	
	// new wholesale price input
	var tmpHTML = '<input type="text" id="wholesale'+id+'-'+newJ+'" name="wholesale'+id+'-'+newJ+'" style="width: 50px;">';
	appendHTMLBelowElementWithID(tmpHTML, 'wholesale'+id+'-'+oldJ);

	// new ertail price input
	var tmpHTML = '<input type="text" id="retail'+id+'-'+newJ+'" name="retail'+id+'-'+newJ+'" style="width: 50px;">';
	appendHTMLBelowElementWithID(tmpHTML, 'retail'+id+'-'+oldJ);
}

function appendHTMLBelowElementWithID(html, id) {
	var newObj = document.createElement('div');
	newObj.innerHTML = html;
	o = document.getElementById(id);
	insertAfter(o, newObj);
}

function insertAfter(referenceNode, newNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

function moveItem(i, direction) {
	// get the original product
	var o = document.getElementById('product'+i);
	var j = parseInt(document.getElementById('itemOrder'+i).value) + direction;
	
	// validate it
	if (j < 0 || j >= parseInt(document.getElementById('totalItems').value)) {
		return;
	}
	

	o = getParentTR(o);
	
	// get the new product (that will swap out)
	if (direction < 0) {
		n = o.previousElementSibling;
		p = o.parentNode;
		p.insertBefore(o, n);
	} else {
		n = o.nextElementSibling.nextElementSibling;
		p = o.parentNode;
		p.insertBefore(o, n);
	}
	
	// switch the itemOrder vals
	o = getChildElementWithIDContainingText(o, 'itemOrder');
	n = getChildElementWithIDContainingText(n, 'itemOrder');
	
	var tmpOrder = o.value;
	o.value = n.value;
	n.value = tmpOrder;
	
	
	// add the name so it gets posted to both
	n.name = n.id;
	o.name = o.id;
}

function getParentTR(tmpObj) {
	while (tmpObj.tagName.toLowerCase() != 'tr' && tmpObj.parentNode) {
		tmpObj = tmpObj.parentNode;
	}
	
	return tmpObj;
}

function getChildElementWithIDContainingText(o, txt) {
	for (var i=0;i<o.childNodes.length;i++) {
		if (o.childNodes[i].id.indexOf(txt) > -1) {
			return o.childNodes[i];
		}
	}
}

function showItemDetails(i) {
	var itemID = document.getElementById('itemID'+i).value;
	
	// create the hidden var
	var o = document.createElement('input');
	o.type = 'hidden';
	o.name = 'detailsID';
	o.value = itemID;
	
	document.getElementById('updateForm').appendChild(o);
	
	if (doSubmit())
		document.getElementById('updateForm').submit();
}

function deleteItem(i) {
	var itemID = document.getElementById('itemID'+i).value;
	if (document.getElementById('product'+i).value)
		var productName = document.getElementById('product'+i).value;
	else
		var productName = document.getElementById('product'+i).innerHTML;
	
	if (confirm('Are you SURE it is OK to delete '+productName+'?')) {
			// create the hidden var
			var o = document.createElement('input');
			o.type = 'hidden';
			o.name = 'deleteID';
			o.value = itemID;
	
			document.getElementById('updateForm').appendChild(o);
	
			if (doSubmit())
				document.getElementById('updateForm').submit();
	}
}

function addNewItem() {
	// get next NEW id
	var i = parseInt(document.getElementById('totalItems').value);
		
	// create the element and add it
	var tr = document.createElement('tr');
	
	// item name, id & itemOrder
	tr.appendChild(createNewTDWithInnerHTML('<input type="hidden" name="itemID'+i+'" id="itemID'+i+'" value="-1" colspan="5"><input type="hidden" name="itemOrder'+i+'" id="itemOrder'+i+'" value="'+i+'"><input type="text" name="product'+i+'" id="product'+i+'" style="width: 250px;">'));
	
	// product size
	tr.appendChild(createNewTDWithInnerHTML('<input name="size'+i+'-0" id="size'+i+'-0" style="width: 100px;">'));
	// wholesale price
	tr.appendChild(createNewTDWithInnerHTML('<input name="wholesale'+i+'-0" id="wholesale'+i+'-0" style="width: 50px;">'));
	// retail price
	tr.appendChild(createNewTDWithInnerHTML('<input name="retail'+i+'-0" id="retail'+i+'-0" style="width: 50px;">'));
	// add new size
	tr.appendChild(createNewTDWithInnerHTML('<a href="javascript:;" onclick="addNewSize('+i+');">[+]</a>'));
	// available
	tr.appendChild(createNewTDWithInnerHTML('<input type="checkbox" id="available'+i+'" name="available'+i+'" CHECKED>'));
	// move
	tr.appendChild(createNewTDWithInnerHTML('<!--<a href="javascript:;" onclick="moveItem('+i+', -1);">▲</a> <a href="javascript:;" onclick="moveItem('+i+', 1);">▼</a>-->'));
	// details
	tr.appendChild(createNewTDWithInnerHTML('<!--<a href="javascript:;" onclick="showItemDetails('+i+');">Details...</a>-->'));
	// delete
	tr.appendChild(createNewTDWithInnerHTML('<!--<a href="javascript:;" onclick="deleteItem('+i+');">[x]</a>-->'));
	
	// get the second-to-last row
	n = document.getElementById('lastSpacer').parentNode.parentNode;
	p = n.parentNode;
	p.insertBefore(tr, n);
	
	// finally, we update the totalItems
	i++;
	document.getElementById('totalItems').value = i;
}

function addNewLabel() {
	// get next NEW id
	var i = parseInt(document.getElementById('totalItems').value);
	
	// create the element and add it
	var tr = document.createElement('tr');
	
	// create a 5-colspan td with the correct styles
	var td = document.createElement('td');
	td.colSpan = '5';
	td.vAlign = 'top';
	td.align = 'left';
	td.style.borderBottom = 'solid #878787 1px';
	// item name, id & itemOrder
	td.innerHTML = '<input type="hidden" name="itemID'+i+'" id="itemID'+i+'" value="-1" colspan="5"><input type="hidden" name="itemOrder'+i+'" id="itemOrder'+i+'" value="'+i+'"><input type="text" name="product'+i+'" id="product'+i+'" style="width: 500px;">';
	tr.appendChild(td);
	
	// available
	tr.appendChild(createNewTDWithInnerHTML('<input type="checkbox" id="available'+i+'" name="available'+i+'" CHECKED>'));
	// move
	tr.appendChild(createNewTDWithInnerHTML('<!--<a href="javascript:;" onclick="moveItem('+i+', -1);">▲</a> <a href="javascript:;" onclick="moveItem('+i+', 1);">▼</a>-->'));
	// details
	tr.appendChild(createNewTDWithInnerHTML('<!--<a href="javascript:;" onclick="showItemDetails('+i+');">Details...</a>-->'));
	// delete
	tr.appendChild(createNewTDWithInnerHTML('<!--<a href="javascript:;" onclick="deleteItem('+i+');">[x]</a>-->'));
	
	// get the second-to-last row
	n = document.getElementById('lastSpacer').parentNode.parentNode;
	p = n.parentNode;
	p.insertBefore(tr, n);
	
	// finally, we update the totalItems
	i++;
	document.getElementById('totalItems').value = i;
}

function createNewTDWithInnerHTML(html) {
	var td = document.createElement('td');
	td.vAlign = 'top'
	td.align = 'left';
	td.style.borderBottom = 'solid #878787 1px';
	
	td.innerHTML = html;
	return td;
}

function isValidPost($idx) {
	if (isset($_POST[$idx]) && trim($_POST[$idx]) != '')
		return true;
	
	return false;
}

// nab all items & move them into a tab-delimited table:
//itemID, itemOrder, itemName, itemDescription, sizesArr, available
function doSubmit() {
	var totalItems = parseInt(document.getElementById('totalItems').value);
	var table = '';
	for (var i=0;i<totalItems;i++) {
		table += getItemValue('itemID'+i) + "-*-*-*-*";
		table += getItemValue('itemOrder'+i) + "-*-*-*-*";
		table += getItemValue('product'+i) + "-*-*-*-*";
		table += getItemValue('description'+i) + "-*-*-*-*";
		table += getSizesFromID(i) + "-*-*-*-*";
		table += getItemValue('available'+i);
		table += "****----****";
	}
	
	// create the hidden var
	var o = document.createElement('input');
	o.type = 'hidden';
	o.name = 'itemTable';
	o.value = table;
	document.getElementById('updateForm').appendChild(o);
	
	// add the category description
	var categoryDescription = document.getElementById('categoryDescription').value;
	o = document.createElement('input');
	o.type = 'hidden';
	o.name = 'categoryDescription';
	o.value = categoryDescription;
	document.getElementById('updateForm').appendChild(o);
	
	return true;
}

function getItemValue(id) {
	if (!document.getElementById(id))
		return '';
	else if (document.getElementById(id).type == 'checkbox' && document.getElementById(id).checked)
		return '1';
	else if (document.getElementById(id).type == 'checkbox' && !document.getElementById(id).checked)
		return '0';
	else if (document.getElementById(id).value)
		return document.getElementById(id).value;
	else if (document.getElementById(id).innerHTML)
		return document.getElementById(id).innerHTML;
	else
		return '';
}

function getSizesFromID(i) {
	var outSizes = '';
	for (var j=0;document.getElementById('size'+i+'-'+j);j++) {
		if (getItemValue('wholesale'+i+'-'+j) != '' && getItemValue('retail'+i+'-'+j) != '') {
			outSizes += getItemValue('size'+i+'-'+j) + ':' + getItemValue('wholesale'+i+'-'+j) + ':' + getItemValue('retail'+i+'-'+j) + '::';
		}
	}
	
	return outSizes;
}
</script>

</body>
</html>
