<?php
/* set the page var */
define('PAGE',  end(explode('/', $_SERVER['SCRIPT_NAME'])));

// connect to database
if ($_SERVER['REMOTE_ADDR'] == '::1' || $_SERVER['SERVER_ADDR'] == '10.0.1.2') {
	// localhost
	@ $db = mysql_pconnect('localhost:/tmp/mysql.sock', 'root', 'Smashing');
	mysql_select_db('Fiore');
} else if ($_SERVER['SERVER_ADDR'] == '68.168.98.213') {
	// new-sites.net
	@ $db = mysql_pconnect('localhost', 'FioreUser', 'FiorePass');
	mysql_select_db('Fiore');
} else {
	@ $db = mysql_pconnect('127.0.0.1', 'cjfiore', 'cjftp60069');
	mysql_select_db('cjfioredb');
}

mysql_query("SET NAMES utf8");


/* set the bg var */
$bgFilename = 'images/bg_'.basename(PAGE, '.php').'.png';
/* if no bgfile exists, use the default */
if (file_exists($bgFilename)) {
	define('CONTENT_BACKGROUND_STYLE', ' style="background: URL('.$bgFilename.') top center no-repeat;"');
} else {
	define('CONTENT_BACKGROUND_STYLE', '');
}

/* start the session */
session_start();

/* defaults css/javascript stuff */
function extraHead() {
?>

<link rel=stylesheet type="text/css" href="fiore.css">
<script type="text/javascript" src="fiore.js"></script>
<script type="text/javascript" src="fioretrans.js"></script>
<meta name="google-translate-customization" content="77e74ac91873d5c7-d2f996464c889d04-g290165d65e7c18f0-f"></meta>

<?php
}

/* stuff right after the <body> tag */
function makeHeader() {
?>
<!-- header -->
<div id="topBG"></div>

<?php if (PAGE == 'index.php') { makeIndexBG(); } ?>

<div id="topMenuHolder">
<div id="topMenu">
<div style="position: absolute; top: 15px; left: 15px;">
<a href="."><img src="images/logo1.png" width="579" height="85" border="0" alt="fiore nursery and landscape supply" id="topLogo"></a>
</div>



<div style="position: absolute; top: 15px; right: 15px; white-space: nowrap;">
<div class="rounded whiteBG smallerText" style="margin-right: 10px; padding: 6px 5px;"><a href="javascript:;" onclick="doTranslate();">¿Habla español?</a>&nbsp;&nbsp;</div>

<div class="rounded whiteBG smallerText nowrap"><a href="cart.php">My Cart&nbsp;<img src="images/cartIcon.gif" width="20" height="14" border="0" style="position: relative; top: 3px;"></a><span style="padding-left: 18px;"><?php
if (isset($_SESSION['user'])) {
	echo '<a href="logout.php">Sign Out</a>';
} else {
	echo '<a href="login.php">Sign In</a> | <a href="login.php">Sign Up</a>';
}
?></span></div>
</div>


<div style="position: absolute; top: 55px; right: 15px;">
<div class="rounded whiteBG smallerText nowrap"><a href="pdf/2012_Retail_Prices.pdf" target="_blank">Retail Price List&nbsp;&nbsp;<img src="images/lightRoundedArrow.gif" width="13" height="12" border="0" style="position: relative; top: 3px;"></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="wholesale_catalog.php">Wholesale Price List&nbsp;&nbsp;<img src="images/lightRoundedArrow.gif" width="13" height="12" border="0" style="position: relative; top: 3px;"></a>&nbsp;</div>
</div>


<div style="position: absolute; bottom: 15px; *bottom: 12px; right: 15px; white-space: nowrap;">
</div>


<div style="position: absolute; bottom: 15px; left: 15px; white-space: nowrap;">
<?php makeMenu(); ?>

<form method="GET" action="search.php" id="searchform" style="float: left;">
	<div class="rounded darkBG smallerText" style="*padding: 2px 0px 2px 10px;"><span>SEARCH ONLINE CATALOG</span>&nbsp;&nbsp;<input type="text" name="q" id="q" style="font-size: 9px;"<?php if (isset($_GET['q'])) { echo ' value="'.str_replace('"', '&quot;', $_GET['q']).'"'; } ?>>&nbsp;&nbsp;<img src="images/darkRoundedArrow3.gif" width="20" height="16" border="0" style="position: relative; top: 5px; cursor: pointer;" onclick="document.getElementById('searchform').submit();">
	</div>
</form>
	
</div><!-- absolute position -->

</div><!-- topMenu -->
</div><!-- topMenuHolder -->
<!-- end header -->
<div id="contentBackground"<?php echo CONTENT_BACKGROUND_STYLE; ?>>&nbsp;</div>
<div id="contentHolder">
<div id="content">
<?php
}

/* top menu */
function makeMenu() {
?>
<table id="menuTable" cellspacing="0" cellpadding="0" border="0" style="float: left; margin-right: 20px;">
	<tr>
		<td valign="middle" align="center" class="roundLeft4" id="menuTD0"><a href="about.php" onmouseover="showSubmenu(0);" onmouseout="hideSubmenu(0);" id="menu0"<?php if (strpos(PAGE, 'about') !== false) { echo ' style="color: #da771b;"'; } ?>>ABOUT US</a></td>
		<td valign="middle" align="center" id="menuTD1"><a href="services.php" onmouseover="showSubmenu(1);" onmouseout="hideSubmenu(1);" id="menu1"<?php if (strpos(PAGE, 'services') !== false) { echo ' style="color: #da771b;"'; } ?>>SERVICES</a></td>
		<td valign="middle" align="center" id="menuTD2"><a href="products.php" onmouseover="showSubmenu(2);" onmouseout="hideSubmenu(2);" id="menu2"<?php if (PAGE == 'products.php') { echo ' style="color: #da771b;"'; } ?>>PRODUCTS</a></td>
		<td valign="middle" align="center"><a href="partners.php"<?php if (strpos(PAGE, 'partners') !== false) { echo ' style="color: #da771b;"'; } ?>>PARTNERS</a></td>
		<td valign="middle" align="center"><a href="resources.php"<?php if (PAGE == 'resources.php') { echo ' style="color: #da771b;"'; } ?>>RESOURCES</a></td>
		<td valign="middle" align="center" class="roundRight4"><a href="contact.php"<?php if (PAGE == 'contact.php') { echo ' style="color: #da771b;"'; } ?>>CONTACT</a></td>
	</tr>
</table>
<?php
}



/* bottom footer */
function makeFooter() {
?>

<!-- footer table -->
<?php
if (PAGE != 'index.php') {
?>
</div><!-- close content -->
<table width="970" cellspacing="0" cellpadding="0" border="0" id="footerTable" style="">
	<tr>
		<td valign="middle" align="left" style="height: 44px;"><img src="images/socials.gif" width="73" height="23" border="0" usemap="#socials"></td>
		<td valign="middle" align="center">
Sign Up for Fiore Emails
		</td>
		<td valign="middle" align="left"><a href="login.php"><img src="images/btnSubmit.png" width="75" height="22" border="0" alt="submit"></a></td>
		<td valign="middle" align="left">
Prairie View: 847.913.1414 | Chicago: 773.533.1414 | <a href="contact.php">Contact Us</a> | ©<?php echo date('Y'); ?><span style="font-family: fiore-book;">&nbsp;</span>
		</td>
	</tr>
	<tr>
		<td><img src="images/spacer.gif" width="100" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="170" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="120" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="580" height="1" border="0"></td>
	</tr>
</table>

<?php
}
?>

</div><!-- close contentHolder -->


<map name="socials">
<area shape="rect" coords="0, 0, 23, 23" href="http://www.twitter.com/cjfnursery" target="_blank" style="border: solid #ff0000 1px;">
<area shape="rect" coords="25, 0, 47, 23" href="https://www.facebook.com/pages/Fiore-Nursery-and-Landscape-Supply/181704598536855" target="_blank">
<area shape="rect" coords="48, 0, 73, 23" href="http://www.linkedin.com/company/fiore-nursery-and-landscape-supply?trk=hb_tab_compy_id_1731374" target="_blank">
</map>

<div class="submenu" id="submenu0" onmouseover="showSubmenu(0);" onmouseout="hideSubmenu(0);" >
<div class="submenuSpacer"></div>
<a href="about_history.php" style="margin-top: 0px;<?php if (PAGE == 'about_history.php') { echo ' color: #da771b;'; } ?>">History</a>
<a href="about_staff.php"<?php if (PAGE == 'about_staff.php') { echo ' style="color: #da771b;"'; } ?>>Staff</a>
<a href="about_locations.php"<?php if (PAGE == 'about_locations.php') { echo ' style="color: #da771b;"'; } ?>>Hours / Locations</a>
<a href="about_staff.php#careers"<?php if (PAGE == 'about_careers.php') { echo ' style="color: #da771b;"'; } ?>>Careers</a>
</div>

<div class="submenu" id="submenu1" onmouseover="showSubmenu(1);" onmouseout="hideSubmenu(1);">
<div class="submenuSpacer"></div>
<a href="services_sourcing.php" style="margin-top: 0px;<?php if (PAGE == 'services_sourcing.php') { echo ' color: #da771b;'; } ?>">Sourcing</a>
<a href="services_sourcing.php#purchasing_solutions"<?php if (PAGE == 'services_purchasing.php') { echo ' style="color: #da771b;"'; } ?>>Purchasing Solutions</a>
<a href="services_delivery.php"<?php if (PAGE == 'services_delivery.php') { echo ' style="color: #da771b;"'; } ?>>Delivery</a>
<a href="services_other.php"<?php if (PAGE == 'services_other.php') { echo ' style="color: #da771b;"'; } ?>>Planting / Fabrication</a>
</div>

<div class="submenu" id="submenu2" onmouseover="showSubmenu(2);" onmouseout="hideSubmenu(2);" >
<div class="submenuSpacer"></div>
<a href="products_plants.php" style="margin-top: 0px;">Plants</a>
<a href="products_landscape_supply.php">Landscape Supply</a>
<a href="products_natural_stone.php">Natural Stone</a>
<a href="products_concrete_clay_pavers.php">Concrete &amp; Clay Pavers</a>
<a href="products_green_roof.php">Green Roof</a>
<a href="products_planters_accessories.php">Planters and Accessories</a>
<a href="products_specials.php">Specials</a>
</div>

<?php
	if (isset($_COOKIE['lang']) && $_COOKIE['lang'] == 'es') {
		echo '<script language="javascript">doTranslate();</script>';
	}
}




function makeSubmenuLink($link) {
	if ($link == PAGE) {
		echo '<a href="'.$link.'" style="color: #db781b;">';
	} else {
		echo '<a href="'.$link.'">';
	}
}

function makeIndexBG() {
	$tmpDate = date('nd');
	if ($tmpDate >= 320 && $tmpDate < 621) {
		// spring
		$img0 = 'images/index3.jpg';
		$img1 = 'images/index4.jpg';
		$img2 = 'images/index5.jpg';
	} else if ($tmpDate >= 621 && $tmpDate < 922) {
		// summer
		$img0 = 'images/index6.jpg';
		$img1 = 'images/index7.jpg';
		$img2 = 'images/index8.jpg';
	} else if ($tmpDate >= 922 && $tmpDate < 1221) {
		// fall
		$img0 = 'images/index9.jpg';
		$img1 = 'images/index10.jpg';
		$img2 = 'images/index11.jpg';
	} else {
		// winter
		$img0 = 'images/index0.jpg';
		$img1 = 'images/index1.jpg';
		$img2 = 'images/index2.jpg';
	}
?>
<div id="indexBG">
<div style="background-image: URL('<?php echo $img0; ?>'); left: 0px;" id="indexBG0"></div>
<div style="background-image: URL('<?php echo $img1; ?>');" id="indexBG1"></div>
<div style="background-image: URL('<?php echo $img2; ?>');" id="indexBG2"></div>
</div>

<script language="javascript">
resizeIndexBG();
</script>
<?php
}













function emailAssociativeArrayWithTitleTo($arr, $title = 'Online Form', $to = 'matt@mattcourtright.com', $from = 'Admin <noreply@admin.com>') {
	$txt = '';
	$html = '<table cellspacing="5" cellpadding="0" border="0"><tr><td valign="top" align="left" colspan="2"><h3>' . $title . '</h3></td></tr>';
	
	/* convert to txt and html */
	foreach($arr as $key => $val) {
		if ($val == $key)
			$val = '';
			
		$txt .= "\n".$key.":\t".$val;
		$html .= '<tr><td valign="top" align="right"><b>'.$key.'</b></td><td valign="top" align="left">'.$val.'</td></tr>';
	}
	
	/* close the html table */
	$html .= '</table><br><br>';
	
	$txt = wordwrap($txt);
	$html = wordwrap($html);
	
	 $semi_rand = md5( time() ); 
	 $mime_boundary = "==MattMail-1-x{$semi_rand}x"; 
	 $mime_boundary2 = "==MattMail-2-x{$semi_rand}x"; 
	 
	 $headers = "From: ".$from."\n";
	 $headers .= "MIME-Version: 1.0\n" . 
		"Content-Type: multipart/mixed; \n" .
		" boundary=\"{$mime_boundary}\"";
	
	 $message = "This is a multi-part message in MIME format.\n\n" . 
		"--{$mime_boundary}\n" .
		"Content-Type: multipart/alternative; boundary=\"{$mime_boundary2}\"\n\n";
	
	 $message .= "--{$mime_boundary2}" .
		"\nContent-Type: text/plain; charset=\"us-ascii\"\n" . 
		"Content-Transfer-Encoding: 7bit\n\n";
	
	 $message .= $txt."\n\n";
	
	 $message .= "--{$mime_boundary2}\n" . 
					"Content-Type: text/html; charset=\"us-ascii\"\n" .
					"Content-Transfer-Encoding: 7bit\n\n" . 
			$html."\n\n";
	
	 $message .= "--{$mime_boundary2}--\n\n";
	 
			 $message .= "--{$mime_boundary}--\n\n";
	mail($to, $title, $message, $headers);
}








// getItemArray functions all take some argument and return an array of associative arrays
function getProductArrayFromCatID($cat) {
	// validate $cat
	if (!is_numeric($cat))
		$cat = -1;
	
	// select all items with $cat = category_id
	$query = 'SELECT * FROM `Items` WHERE category_id='.$cat.' ORDER BY is_available DESC, sort_order ASC';
		
	$result = mysql_query($query);
	$outArr = array();
	for ($i=0;$i<mysql_num_rows($result);$i++) {
		// create a new array
		$tmpArr = array();
		// add item array
		$tmpArr['itemArr'] = mysql_fetch_array($result);
		// add plants array 
		$tmpArr['plantsArr'] = getPlantsArrayFromID($tmpArr['itemArr']['item_id']);
		// add sizes array 
		$tmpArr['sizesArr'] = getSizesArrayFromID($tmpArr['itemArr']['item_id']);
		
		// finally, we add the $tmpArr to the $outArr
		$outArr[] = $tmpArr;
	}
	
	return $outArr;
}

// returns the plants row if it exists or false
function getPlantsArrayFromID($id) {
	// validate $id
	if (!is_numeric($id))
		$id = -1;
	
	$query = 'SELECT * FROM `Plants` WHERE item_id='.$id;
	$result = mysql_query($query);
	if (mysql_num_rows($result) > 0)
		return mysql_fetch_array($result);
		
	return false;
}

// returns an array of sizes if they exist or false
function getSizesArrayFromID($id) {
	// validate $id
	if (!is_numeric($id))
		$id = -1;
	
	$query = 'SELECT * FROM `Sizes` WHERE item_id='.$id.' ORDER BY sort_order';
	$result = mysql_query($query);
	$outArr = array();
	for ($i=0;$i<mysql_num_rows($result);$i++) {
		$outArr[] = mysql_fetch_array($result);
	}
		
	return $outArr;
}