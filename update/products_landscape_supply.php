<?php
// set category = natural stone
$_GET['c'] = 2;


require_once('../functions_catalog.php');
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Plants</title>
<?php extraCatalogHead(); ?>
</head>
<body>
<?php makeCatalogHeader(false); ?>

<table width="801" cellspacing="0" cellpadding="0" border="0" style="margin-bottom: 5px;">
	<tr>
		<td valign="top" align="left">
<div class="productPlantBlock roundTopLeft10">
<a href="browse.php?c=16&category=bagged_goods"><img src="../images/products_landscape_supply0.jpg" width="338" height="265" border="0" class="roundTopLeft10"><div>Bagged Goods &gt;</div></a>
</div>
		</td>
		<td valign="top" align="left">
<div class="productPlantBlock">
<a href="browse.php?c=17&category=bulk_items"><img src="../images/products_landscape_supply1.jpg" width="457" height="265" border="0"><div>Bulk Items &gt;</div></a>
</div>
		</td>
	</tr>
</table>



<table width="801" cellspacing="0" cellpadding="0" border="0" style="margin-bottom: 5px;">
	<tr>
		<td valign="top" align="left">
<div class="productPlantBlock">
<a href="browse.php?c=19&category=drainage"><img src="../images/products_landscape_supply2.jpg" width="224" height="235" border="0"><div>Drainage &gt;</div></a>
</div>
		</td>
		<td valign="top" align="left">
<div class="productPlantBlock">
<a href="browse.php?c=18&category=grass_seed"><img src="../images/products_landscape_supply3.jpg" width="343" height="235" border="0"><div>Grass Seed &gt;</div></a>
</div>
		</td>
		<td valign="top" align="left">
<div class="productPlantBlock">
<a href="browse.php?c=20&category=miscellaneous"><img src="../images/products_landscape_supply4.jpg" width="223" height="235" border="0"><div>Miscellaneous &gt;</div></a>
</div>
		</td>
	</tr>
</table>





<?php makeCatalogFooter(); ?>

</body>
</html>
