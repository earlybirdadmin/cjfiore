<?php
require_once('functions.php');

$result = mysql_query('SELECT * FROM `Categories`');
for ($i=0;$i<mysql_num_rows($result);$i++) {
	$row = mysql_fetch_array($result);
	$theCount = getItemCountForCategoryId($row['category_id']);
	mysql_query('UPDATE `Categories` SET item_count='.$theCount.' WHERE category_id='.$row['category_id']);
}


function getItemCountForCategoryId($id) {
	// init $theCount
	$theCount = 0;
	
	// get all items
	$result = mysql_query('SELECT * FROM `Items` WHERE is_available=1 AND category_id='.$id);
	for ($i=0;$i<mysql_num_rows($result);$i++) {
		$row = mysql_fetch_array($result);
		// now get the sizes to make sure we have a valid size
		$result2 = mysql_query('SELECT * FROM `Sizes` WHERE item_id='.$row['item_id'].' AND is_available=1 LIMIT 1');
		if (mysql_num_rows($result2) > 0) {
			$theCount++;
		}
	}
	
	
	// get the children
	$result = mysql_query('SELECT * FROM `Categories` WHERE parent_id='.$id);
	for ($i=0;$i<mysql_num_rows($result);$i++) {
		$row = mysql_fetch_array($result);
		$theCount += getItemCountForCategoryId($row['category_id']);
	}
	return $theCount;
}
