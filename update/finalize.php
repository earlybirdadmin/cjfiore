<?php
ini_set("memory_limit","500M");
ini_set("max_execution_time","14400");

require_once('functions.php');
require_once('../functions_catalog.php');

/* 
	This is run after importing everything.
	
	It basically just updates prices and marks items as 'available' 
	based on their navID
	
*/







$c = file_get_contents('NEWCatalog.txt');
if (!$c)
	die('No dice.');
	
// break into array
$items = explode("\n", $c);
echo number_format(count($items)).' Items in list.<br><br>';

if (count($items) < 500)
	die('No dice 2.');


// import data into table
for ($i=0;$i<count($items);$i++) {
	$itemArr = explode("\t", $items[$i]);

	if (count($itemArr) < 10) {
		continue;
	}
	
	$itemArr = array('navID' => trim($itemArr[0]),
					'item_name' => trim($itemArr[2]),
					'full_name' => trim($itemArr[1]),
					'item_size' => trim($itemArr[11]),
					'price' => round(trim($itemArr[18]), 2)
					);
	
	// no name? WTF?!
	if (!is_numeric($itemArr['price']) || $itemArr['price'] == 0) {
		continue;
	}
	
	// check for navID
	$result = mysql_query('SELECT * FROM `Items` WHERE navID='.$itemArr['navID']);
	if (!$result)
		echo '<br>NAV ID: '.$itemArr['navID'];
	if (mysql_num_rows($result) > 0) {
		$row = mysql_fetch_array($result);
		$itemArr['item_id'] = $row['item_id'];
		
		updateSizeFor($itemArr);
	} else {
		// insert into NavImport table so we can add it later
		mysql_query('INSERT INTO `NavImport` VALUES (NULL, '.$itemArr['navID'].', "'.mysql_real_escape_string($itemArr['item_name']).'", "'.mysql_real_escape_string($itemArr['full_name']).'", "'.mysql_real_escape_string($itemArr['item_size']).'", '.$itemArr['price'].')');
		echo '<br>'.'INSERT INTO `NavImport` VALUES (NULL, '.$itemArr['navID'].', "'.mysql_real_escape_string($itemArr['item_name']).'", "'.mysql_real_escape_string($itemArr['full_name']).'", "'.mysql_real_escape_string($itemArr['item_size']).'", '.$itemArr['price'].')';
	}
}





echo '<br><br>done!';
//print_r($groupArr);








function updateSizeFor($itemArr) {
	// get item_id if we don't, but DO have a navID
	if (!isset($itemArr['item_id']) && isset($itemArr['navID']) && is_numeric($itemArr['navID'])) {
		$result = mysql_query('SELECT * FROM `Items` WHERE navID='.$itemArr['navID']);
		if (mysql_num_rows($result) > 0) {
			$row = mysql_fetch_array($result);
			$itemArr['item_id'] = $row['item_id'];
		}
	}
	
	// make sure we have an item_id
	if (!isset($itemArr['item_id'])) {
		echo "\n<br>No item ID!";
		return;
	}
	
	// mark item as "available"
	mysql_query('UPDATE `Items` SET is_available=1 WHERE item_id='.$itemArr['item_id']);

	// see if size already exists
	$result = mysql_query('SELECT * FROM `Sizes` WHERE item_id='.$itemArr['item_id'].' AND item_size="'.mysql_real_escape_string($itemArr['item_size']).'"');
	if (mysql_num_rows($result) > 0) {
		// found it! update price 
		for ($j=0;$j<mysql_num_rows($result);$j++) {
			$row = mysql_fetch_array($result);
			if ($j == 0) {
				mysql_query('UPDATE `Sizes` SET wholesale_price='.$itemArr['price'].', retail_price='.getRetailPrice($itemArr['price']).', is_available=1 WHERE id='.$row['id']);
			} else {
				// and delete extras
				//echo '<br>DELETE FROM `Sizes` WHERE id='.$row['id'];
				mysql_query('DELETE FROM `Sizes` WHERE id='.$row['id']);
			}
		}
	} else {
		// didn't find it, add the size
		$sortOrder = 0;
		$isAvailable = 1;
		mysql_query('INSERT INTO `Sizes` (id, item_id, sort_order, item_size, wholesale_price, retail_price, is_available) VALUES (NULL, '.$itemArr['item_id'].', '.$sortOrder.', "'.mysql_real_escape_string($itemArr['item_size']).'", '.$itemArr['price'].', '.getRetailPrice($itemArr['price']).', '.$isAvailable.')');
	}
}




function getRetailPrice($price) {
	$price *= 30;// 1.5 X 20 to round to the nearest nickel
	ceil($price);
	$price /= 20;// divide back out that 20
	return $price;
}

