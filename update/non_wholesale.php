<?php
require_once('../functions.php');
require_once('../functions_catalog.php');

// update POST vals
for ($i=0;isset($_POST['id'.$i]) && is_numeric($_POST['id'.$i]);$i++) {
	$sendEmail = false;
	
	// set is wholesale
	if (isset($_POST['wholesale'.$i]) && $_POST['wholesale'.$i] == 'yup') {
		$is_wholesale = 1;
		
		// see if we need to send email
		$res = mysql_query('SELECT * FROM `UserInfo` WHERE is_wholesale=0 AND user_id='.$_POST['id'.$i]);
		if (mysql_num_rows($res) > 0)
			$sendEmail = true;
		
		$wholesaleID  = $_POST['id'.$i];
	} else
		$is_wholesale = 0;
	
	// wholesale	
	$query = 'UPDATE `UserInfo` SET is_wholesale='.$is_wholesale;
	// branch id
	if (isset($_POST['branchID'.$i]) && is_numeric($_POST['branchID'.$i]))
		$query .= ', branch_id='.$_POST['branchID'.$i];
	
	
	$query .= ' WHERE user_id='.$_POST['id'.$i];
	$result = mysql_query($query);
	
	// shoot an email to whoever letting them know that they've been approved for wholesale pricing
	if ($sendEmail) {
		//echo '<br>Sending email to: '.$row['email'].'<br>';
		$result = mysql_query('SELECT * FROM `UserInfo` WHERE user_id='.$_POST['id'.$i]);
		$row = mysql_fetch_array($result);
		$html = '<h3>You have been approved!</h3>';
		$html .= '<p>You can sign in via:<br><a href="http://www.cjfiore.com/login.php">http://www.cjfiore.com/login.php</a> to access our wholesale catalog and wholesale pricing.';
		$html .= '<br><br>Fiore Nursery and Landscape Supply has been a trusted partner and supplier to the Green Industry and home gardener for over 90 years, providing the Chicagoland area with the area’s largest selection of premium grade plant material with over 1,100 varities. We now also offer natural stone and landscape supply products at both of our locations, sourced from some of the best suppliers in the midwest and available for immediate pick up or delivery.<br><br>At both our Prairie View and Chicago sales yard, you will find the same extensive selection of premium plants, natural stone, and landscape materials, combined with the superior customer service you’ve come to expect from Fiore. At Fiore, we remain committed to offering our customers the highest quality products available, along with our top-notch expert advice and consultation.</p>';
		sendPearMailToWithSubjectAndMessage($row['email'], 'CJ Fiore Wholesale', $html);
	}

}

// set the page info
$usersPerPage = 100;

$result = mysql_query('SELECT * FROM `UserInfo`');
$totalUsers = mysql_num_rows($result);
$totalPages = ceil($totalUsers / $usersPerPage);

if (isset($_GET['p']) && is_numeric($_GET['p']) && $_GET['p'] > 0 && $_GET['p'] < $totalPages) {
	$curPage = $_GET['p'];
} else {
	$curPage = 0;
}


?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>fiore</title>
<?php extraCatalogHead();?>
<script type="text/javascript" src="update.js"></script>
</head>
<body onload="addSize();">
<?php makeCatalogHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" style="padding: 20px 10px 0px 20px;">
<h1>User Accounts</h1>
<br><form method="POST" action="">
<table cellspacing="0" cellpadding="2" border="0">
	<tr>
		<td valign="middle" align="left"><b>Name</b></td>
		<td valign="middle" align="left"><b>Company</b></td>
		<td valign="middle" align="left"><b>Created On</b></td>
		<td valign="middle" align="left"><b>Email</b></td>
		<td valign="middle" align="left"><b>Branch</b></td>
		<td valign="middle" align="left"><b>Wholesale</b></td>
		<td></td>
	</tr>
<?php
$startLimit = $curPage * $usersPerPage;
$endLimit = $startLimit + $usersPerPage;

$query = 'SELECT * FROM `UserInfo` ORDER BY date_created DESC LIMIT '.$startLimit.', '.$endLimit;
$query = 'SELECT * FROM `UserInfo` ORDER BY date_created DESC';
$query = 'SELECT * FROM `UserInfo` ORDER BY company ASC';
$query = 'SELECT * FROM `UserInfo` WHERE date_created >= 1389916800 ORDER BY date_created ASC';
$result = mysql_query($query);
for ($i=0;$i<mysql_num_rows($result);$i++) {
	$row = mysql_fetch_array($result);
	echo '<tr>';
		echo '<td valign="middle" align="left"><input type="hidden" name="id'.$i.'" value="'.$row['user_id'].'">'.$row['first_name'].' '.$row['last_name'].'</td>';
		echo '<td valign="middle" align="left">'.$row['company'].'</td>';
		echo '<td valign="middle" align="left">'.date('Y/m/d', $row['date_created']).'</td>';
		echo '<td valign="middle" align="left"><a href="mailto:'.$row['email'].'">'.$row['email'].'</a></td>';
		echo '<td valign="middle" align="center">'.getBranchSelectFromBranchIDWithID($row['branch_id'], $i).'</td>';
		echo '<td valign="middle" align="center"><input type="checkbox" name="wholesale'.$i.'"';
		if ($row['is_wholesale'] == 1)
			echo ' CHECKED';
		echo ' value="yup"></td>';
		echo '<td valign="middle" align="center"><a href="user_account.php?u='.$row['user_id'].'">edit</a></td>';
	echo '</tr>';
}
?>
</table>
&nbsp;
<br>
<input type="submit" value="submit">
<br>&nbsp;
</form>

		</td>
	</tr>
</table>

<?php makeCatalogFooter(); ?>

</body>
</html>
<?php
function getBranchSelectFromBranchIDWithID($branchID, $i) {
	$out = '<select name="branchID'.$i.'">';
	
	$out .= '<option value="0"';
	if ($branchID == 0)
		$out .= ' SELECTED';
	$out .= '>Prairie View</option>';
	
	$out .= '<option value="1"';
	if ($branchID == 1)
		$out .= ' SELECTED';
	$out .= '>Chicago</option>';
	
	$out .= '</select>';
	
	return $out;
}