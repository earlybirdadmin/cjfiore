<?php
error_reporting(E_ALL);
if (isset($_FILES['f0']) && $_FILES['f0']['error'] == UPLOAD_ERR_OK) {
	if (move_uploaded_file($_FILES['f0']['tmp_name'], 'NEWCatalog.txt') !== false) {
		header('Location: finalize.php');
		exit();
	}
}

require_once('../functions.php');
require_once('../functions_catalog.php');
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>fiore</title>
<?php extraCatalogHead(); ?>
<script type="text/javascript" src="update.js"></script>
</head>
<body>
<?php makeCatalogHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" style="padding: 20px 10px 0px 20px;">
<h1>NAV Import</h1>
<form method="POST" enctype="multipart/form-data">
	Upload TAB DELIMITED file:
	<br><input type="file" name="f0">
	<br>
	<br><input type="submit" value="submit">
</form>
		</td>
	</tr>
</table>

<?php makeCatalogFooter(); ?>

</body>
</html>
