<?php
// make sure we have a SINGLE user
if (!isset($_GET['u']) || !is_numeric($_GET['u'])) {
	header('Location: user_accounts.php');
	exit();
}


require_once('functions.php');

// make sure the user EXISTS
$result = mysql_query('SELECT * FROM `UserInfo`, `UserLogin` WHERE UserInfo.user_id='.$_GET['u'].' AND UserInfo.user_id = UserLogin.user_id');
if (mysql_num_rows($result) == 0) {
	header('Location: user_accounts.php');
	exit();
}

$GLOBALS['userArr'] = mysql_fetch_array($result);


// handle updates
if (isset($GLOBALS['userArr']) && isset($_POST['q3'])) {
	// build userinfo query
	if (isset($_POST['q11']) && $_POST['q11'] == 'yup')
		$isNews = 1;
	else
		$isNews = 0;

	if (isset($_POST['q12']) && $_POST['q12'] == 'yup')
		$weeklyAvailability = 1;
	else
		$weeklyAvailability = 0;
		
	if (isset($_POST['is_wholesale']) && $_POST['is_wholesale'] == 'yup')
		$isWholesale = 1;
	else
		$isWholesale = 0;
		
	$query = 'UPDATE `UserInfo` SET email="'.mysql_real_escape_string($_POST['q3']).'", is_business="'.mysql_real_escape_string($_POST['q4']).'", first_name="'.mysql_real_escape_string($_POST['q5']).'", last_name="'.mysql_real_escape_string($_POST['q6']).'", company="'.mysql_real_escape_string($_POST['q7']).'", address="'.mysql_real_escape_string($_POST['q8']).'", phone="'.mysql_real_escape_string($_POST['q9']).'", zip="'.mysql_real_escape_string($_POST['q10']).'", is_newsletter_subscriber='.$isNews.', is_weekly_availability_subscriber='.$weeklyAvailability.' WHERE user_id='.$_SESSION['user']['user_id'];
	mysql_query($query);
	
	// handle password update (if they did it)
	if (trim($_POST['q1']) != '') {
		if ($_POST['q1'] == $_POST['q2']) {
			// update the password
			mysql_query('UPDATE `UserLogin` SET pass="'.mysql_real_escape_string(md5($_POST['q1'].'cjf')).'" WHERE user_id='.$GLOBALS['userArr']['user_id']);
		} else {
			// alert the user that their passwords don't match
			$err = 'Your passwords do not match.  Please try typing them in again.';
		}
	}
	
	// handle the username/email update
	mysql_query('UPDATE `UserLogin` SET user="'.mysql_real_escape_string($_POST['q0']).'", email="'.mysql_real_escape_string($_POST['q3']).'" WHERE user_id='.$GLOBALS['userArr']['user_id']);
	
	// now we update the session information by just logging the user in again
	$result = mysql_query('SELECT * FROM `UserInfo`, `UserLogin` WHERE UserInfo.user_id='.$GLOBALS['userArr']['user_id'].' AND UserInfo.user_id = UserLogin.user_id');
	$GLOBALS['userArr'] = mysql_fetch_array($result);
	
	// finally, we alert the user about the update if there were no errors
	if (!isset($err))
		$err = 'Success!  We have changed the account information.';
}
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>fiore</title>
<?php extraHead(); ?>
</head>
<body>
<?php makeHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" style="padding: 20px 10px 0px 20px;">
<h1>Edit Your Account</h1>
<br>
<?php
$result = mysql_query('SELECT * FROM `UserLogin` WHERE user_id='.$GLOBALS['userArr']['user_id']);
$row = mysql_fetch_array($result);
$username = $row['user'];
?>

Edit the items below and click "Submit" to make any changes to your account.
<br><?php
if (isset($err))
	echo '<span style="color: #ff0000;">'.$err.'</span>';
?>
<form method="POST" action="">
<div class="beigeBlock" style="font-size: 11px; padding: 10px; margin-top: 10px;">
<table cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td valign="top" align="left" style="font-size: 11px;">
User name:
<br><input type="text" name="q0" id="q0" value="<?php echo $GLOBALS['userArr']['user']; ?>" style="width: 150px;">
<br>
<br>Change Password:
<br><input type="password" name="q1" id="q1" value="" style="width: 150px;">
<br>Confirm Password:
<br><input type="password" name="q2" id="q2" value="" style="width: 150px;">
<br>
<br>Email Address:
<br><input type="text" name="q3" id="q3" value="<?php echo getUserVal(3); ?>" style="width: 150px;">
<br>
<br>Check one:
<br><label><input type="radio" name="q4" id="q4" value="0"<?php if (getUserVal(4) == '0') { echo ' CHECKED'; } ?> style="position: relative; top: -4px;"> Home Gardener</label>
<br><label><input type="radio" name="q4" id="q4" value="1"<?php if (getUserVal(4) == '1') { echo ' CHECKED'; } ?> style="position: relative; top: -4px;"> Green Industry Professional</label>
		</td>
		<td><img src="images/spacer.gif" width="50" height="1"></td>
		<td valign="top" align="left" style="font-size: 11px;">
First name:
<br><input type="text" name="q5" id="q5" value="<?php echo getUserVal(5); ?>" style="width: 150px;">
<br>Last name:
<br><input type="text" name="q6" id="q6" value="<?php echo getUserVal(6); ?>" style="width: 150px;">
<br>Business name:
<br><input type="text" name="q7" id="q7" value="<?php echo getUserVal(7); ?>" style="width: 150px;">
<br>Your address:
<br><input type="text" name="q8" id="q8" value="<?php echo getUserVal(8); ?>" style="width: 150px;">
<br>Telephone Number:
<br><input type="text" name="q9" id="q9" value="<?php echo getUserVal(9); ?>" style="width: 150px;">
<br>Zip Code:
<br><input type="text" name="q10" id="q10" value="<?php echo getUserVal(10); ?>" style="width: 150px;">
<br>
<br><label><input type="checkbox" name="q11" id="q11" value="yup"<?php if ($GLOBALS['userArr']['is_newsletter_subscriber']) { echo ' CHECKED'; } ?>> <b>Sign up for Fiore News</b></label>
<br><label><input type="checkbox" name="q12" id="q12" value="yup"<?php if ($GLOBALS['userArr']['is_weekly_availability_subscriber']) { echo ' CHECKED'; } ?>> <b>Sign up for Weekly Availability</b></label>
		</td>
		<td><img src="images/spacer.gif" width="50" height="1"></td>
		<td valign="bottom" align="left" style="font-size: 11px; padding-right: 20px;">
			<label><input type="checkbox" name="is_wholesale" value="yup"<?php if ($GLOBALS['userArr']['is_wholesale'] == 1) { echo ' CHECKED'; } ?>> <b>Wholesale Prices</b></label>
			<br>
			<input type="image" src="images/btnSubmit.png" value="submit" style="margin-top: 220px;">
		</td>
	</tr>
</table>
</div>
</form>


		</td>
	</tr>
</table>

<?php makeFooter(); ?>

</body>
</html>
<?php
function getUserVal($id) {
	$idxArr = array('user', 'password0', 'password1', 'email', 'is_business', 'first_name', 'last_name', 'company', 'address', 'phone', 'zip');
	
	$idx = $idxArr[$id];
	if (isset($GLOBALS['userArr'][$idx]))
		return $GLOBALS['userArr'][$idx];
	else
		return '';
}