<?php
require_once('../functions.php');
require_once('../functions_catalog.php');

if (!isset($_GET['u']) || !is_numeric($_GET['u'])) {
	header('Location: user_accounts.php');
	die();
}

$userID = $_GET['u'];
$result = mysql_query('SELECT * FROM `UserInfo` WHERE user_id='.$userID);
if (mysql_num_rows($result) == 0) {
	header('Location: user_accounts.php');
	die();
}

$row = mysql_fetch_array($result);
$businessName = $row['company'];
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>fiore</title>
<?php extraCatalogHead(); ?>
<script type="text/javascript" src="update.js"></script>
</head>
<body onload="addSize();">
<?php makeCatalogHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" style="padding: 20px 10px 0px 20px;">
<h1>Enable Wholesale Pricing</h1>
<br><form method="POST" action="user_accounts.php">
<input type="hidden" name="id0" value="<?php echo $userID; ?>">
<input type="hidden" name="emailWholesale" value="yup">
<input type="checkbox" name="wholesale0" value="yup"> Enable Wholesale Pricing for the business: <b><?php echo $businessName; ?></b>
<br>
<br><input type="submit" value="submit">
</form>
		</td>
	</tr>
</table>

<?php makeCatalogFooter(); ?>

</body>
</html>
