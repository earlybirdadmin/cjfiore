<?php
header('Location: plants.php');
exit();

// set category = plants
$_GET['c'] = 0;


require_once('../functions_catalog.php');
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Plants</title>
<?php extraCatalogHead(); ?>
</head>
<body>
<?php makeCatalogHeader(false); ?>

<table width="801" cellspacing="0" cellpadding="0" border="0" style="margin-bottom: 5px;">
	<tr>
		<td valign="top" align="left">
<div class="productPlantBlock roundTopLeft10">
<a href="browse.php?c=7&category=annuals_and_tropicals"><img src="../images/plants0.jpg" width="334" height="210" border="0" class="roundTopLeft10"><div>Annuals and Tropicals &gt;</div></a>
</div>
		</td>
		<td valign="top" align="left">
<div class="productPlantBlock">
<a href="browse.php?c=8&category=broadleaf_evergreens"><img src="../images/plants1.jpg" width="454" height="210" border="0"><div>Broadleaf Evergreens  &gt;</div></a>
</div>
		</td>
	</tr>
</table>

<table width="801" cellspacing="0" cellpadding="0" border="0" style="margin-bottom: 5px;">
	<tr>
		<td valign="top" align="left">
<div class="productPlantBlock">
<a href="browse.php?c=9&category=evergreen_trees_and_shrubs"><img src="../images/plants2.jpg" width="357" height="210" border="0"><div>Evergreen Trees and Shrubs  &gt;</div></a>
</div>
		</td>
		<td valign="top" align="left">
<div class="productPlantBlock">
<a href="browse.php?c=10&category=fruiting_trees_and_vines"><img src="../images/plants3.jpg" width="431" height="210" border="0"><div>Fruiting Trees and Shrubs  &gt;</div></a>
</div>
		</td>
	</tr>
</table>


<!-- stupid float -->
<table width="801" cellspacing="0" cellpadding="0" border="0" style="margin-bottom: 5px;">
	<tr>
		<td valign="top" align="left" style="white-space: nowrap;">

<table cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td valign="top" align="left">
<div class="productPlantBlock">
<a href="browse.php?c=11&category=perennials_ground_cover_and_grasses"><img src="../images/plants4.jpg" width="293" height="185" border="0"><div>Per<span style="letter-spacing: 0px;">ennials</span>, Gr<span style="letter-spacing: 0px;">ound Cover and </span>Gr<span style="letter-spacing: 0px;">asses</span> &gt;</div></a>
</div>
		</td>
		<td valign="top" align="left">
<div class="productPlantBlock">
<a href="browse.php?c=12&category=shade_trees_and_ornamentals"><img src="../images/plants5.jpg" width="289" height="185" border="0"><div>Shade Trees and Ornamentals  &gt;</div></a>
</div>
		</td>
	</tr>
</table>
		</td>
		<td valign="top" align="left" rowspan="2">
<div class="productPlantBlock">
<a href="plants_featured.php"><img src="../images/plants6.jpg" width="201" height="370" border="0"><div>Featured  &gt;</div></a>
</div>
		</td>
	</tr>
	<tr>
		<td valign="top" align="left" style="white-space: nowrap;">


<table cellspacing="0" cellpadding="0" border="0" style="margin-top: 5px;">
	<tr>
		<td valign="top" align="left">
<div class="productPlantBlock">
<a href="browse.php?c=13&category=shrubs"><img src="../images/plants7.jpg" width="358" height="180" border="0"><div>Shrubs  &gt;</div></a>
</div>
		</td>
		<td valign="top" align="left">
<div class="productPlantBlock">
<a href="browse.php?c=14&category=topiaries_and_espaliers"><img src="../images/plants8.jpg" width="224" height="180" border="0"><div>Topiaries and Espailers  &gt;</div></a>
</div>
		</td>
	</tr>
</table>

		</td>
	</tr>
</table>
<!-- end stupid float -->

<?php makeCatalogFooter(); ?>

</body>
</html>
