<?php
// connect to database
if ($_SERVER['REMOTE_ADDR'] == '::1' || strpos($_SERVER['SERVER_ADDR'], '10.0.1') !== false) {
	// localhost
	@ $db = mysql_pconnect('localhost:/tmp/mysql.sock', 'root', 'Smashing');
	mysql_select_db('Fiore');
} else if ($_SERVER['SERVER_ADDR'] == '64.150.176.44') {
	// new-sites.net
	@ $db = mysql_pconnect('localhost', 'FioreUser', 'FiorePass');
	mysql_select_db('Fiore');
} else {
	@ $db = mysql_pconnect('127.0.0.1', 'cjfiore', 'cjftp60069');
	mysql_select_db('cjfioredb');
}

/*
error_reporting(E_ALL & ~E_WARNING & ~E_NOTICE & ~E_DEPRECATED);

@ $db = mysql_connect('sjjt.local', 'homestead', 'password');
mysql_select_db('cjfiore');
mysql_query("SET NAMES utf8");

 */



/* parse data file */

//				0				6					10					14
$keys = array('Item No.' => -1, 'Item UOM' => -1, 'Item Description' => -1, 'Variant Code' => -1, 'Item Variant GP Desc' => -1, 'Sales Price' => -1);

$data = array();
$c = file_get_contents('./FiorePrices052617.txt', 'r');
$lineArr = explode("\n", $c);

echo '<pre>';
$foundKeys = false;
for ($i=0;$i<count($lineArr);$i++) {
    $row = explode("\t", $lineArr[$i]);

    if ($foundKeys) {
    	 //$data[] = $row;
    	 $data[] = filterArrayToOnlyInclude($row, $keys);
    } else {
    	foreach($keys as $key => $val) {
    		for ($j=0;$j<count($row);$j++) {
    			if (str_replace(array('"', '*'), "", $row[$j]) == $key) {
    				$keys[$key] = $j;
    			}
    		}
    	}
    	// now see if all keys are > -1
    	$foundKeys = true;
    	foreach($keys as $key => $val) {
    		if ($val == -1)
    			$foundKeys = false;
    	}
    }
}

/* import data file to NavImport Table */
mysql_query('TRUNCATE `NavImport`');
for ($i=0;$i<count($data);$i++) {
	$row = $data[$i];

	// if we're not using valid numbers, ignore this row
	if (!is_numeric($row['Item No.']) || !is_numeric($row['Sales Price'])) {
		continue;
	}

	// replace missing Item Variant GP Desc with Item UOM
	if ($row['Item Variant GP Desc'] == '')
		$row['Item Variant GP Desc'] = $row['Item UOM'];

	$query = 'INSERT INTO `NavImport` (id, navID, item_description, variant_code, variant_desc, price) VALUES  (NULL, '.$row['Item No.'].', "'.mysql_real_escape_string($row['Item Description']).'", "'.mysql_real_escape_string($row['Variant Code']).'", "'.mysql_real_escape_string($row['Item Variant GP Desc']).'", '.round($row['Sales Price'], 2).')';
	mysql_query($query);
}

$sizesCount = 0;
$itemsCount = 0;
$insertedCount = 0;
$missingItemsCount = 0;
$matchingPriceCount = 0;

/* update pricing tables */
$query = 'SELECT * FROM `NavImport` ORDER BY navID ASC';
$result = mysql_query($query);
$lastNavID = -1;
for ($i=0;$i<mysql_num_rows($result);$i++) {
	$row = mysql_fetch_array($result);

	// blanks? ignore 'em.
	if (!is_numeric($row['navID']))
		continue;

	if ($row['navID'] != $lastNavID) {

		// get item_id (website ID)
		$result2 = mysql_query('SELECT * FROM `Items` WHERE navID='.$row['navID']);
		if (mysql_num_rows($result2) == 0) {
			// try the `Sizes` table
			$result2 = mysql_query('SELECT * FROM `Sizes` WHERE navID='.$row['navID']);
			if (mysql_num_rows($result2) == 0) {
				$itemID = -1;
			} else {
				$row2 = mysql_fetch_array($result2);
				$itemID = $row2['item_id'];
			}
		} else {
			$row2 = mysql_fetch_array($result2);
			$itemID = $row2['item_id'];
		}

		if ($itemID == -1) {
			$missingItemsCount++;
			continue;
		}

		$itemsCount++;

		// update `Items` table (set is_available)
		mysql_query('UPDATE `Items` SET is_available=1 WHERE item_id='.$itemID);

		// clear `Sizes` table for item_id (so we can start fresh)
		mysql_query('UPDATE `Sizes` SET is_available=0 WHERE (item_id='.$itemID.' OR navID='.$row['navID'].')');
		mysql_query('DELETE FROM `Sizes` WHERE (item_id='.$itemID.' OR navID='.$row['navID'].') AND variantCode=""');

		// set lastNavID
		$lastNavID = $row['navID'];
	}

	// see if item AND size exist in `Sizes` table
	$query = 'SELECT * FROM `Sizes` WHERE (item_id='.$itemID.' OR navID='.$row['navID'].') AND item_size="'.mysql_real_escape_string($row['variant_desc']).'"';
	$result2 = mysql_query($query);

	if (mysql_num_rows($result2) == 0) {
		// INSERT into `Sizes` table
		$query = 'INSERT INTO `Sizes` (id, item_id, navID, variantCode, sort_order, item_size, wholesale_price, retail_price, is_available) VALUES (NULL, '.$itemID.', '.$row['navID'].', "'.mysql_real_escape_string($row['variant_code']).'", 0, "'.mysql_real_escape_string($row['variant_desc']).'", '.$row['price'].', '.$row['price'].', 1)';
		$result3 = mysql_query($query);
	} else {
		// see how many prices match
		$row2 = mysql_fetch_array($result2);
		if ($row2['wholesale_price'] == $row['price'])
			$matchingPriceCount++;

		$query = 'UPDATE `Sizes` SET is_available=1, wholesale_price='.$row['price'].', retail_price='.$row['price'].', variantCode="'.mysql_real_escape_string($row['variant_code']).'", item_size="'.mysql_real_escape_string($row['variant_desc']).'" WHERE id='.$row2['id'];
		$result3 = mysql_query($query);
	}
	$sizesCount++;

	// remove from `NavImport` table
	if ($result3) {
		mysql_query('DELETE FROM `NavImport` WHERE id='.$row['id']);
		$insertedCount++;
	}
}



 echo '<pre>';

echo $i . ' records found.';
echo '<br>'.$sizesCount.' sizes TRIED to add/update';
echo '<br>'.$insertedCount.' sizes GOT added/updated';
echo '<br>'.$itemsCount.' items found';
echo '<br>'.$missingItemsCount.' items NOT found';
echo '<br>'.$matchingPriceCount.' prices match';




function filterArrayToOnlyInclude($inArr, $keys) {
	$outArr = array();
	foreach($keys as $key => $val) {
		if (isset($inArr[$val])) {
			$outArr[$key] = trim($inArr[$val]);
		} else {
			$outArr[$key] = '';
		}
	}
	return $outArr;
}
