<?php
require_once('../functions_catalog.php');

if (isWholesale())
	$priceIdx = 'wholesale_price';
else
	$priceIdx = 'retail_price';


$idArr = array();
$q = mysql_real_escape_string($_GET['q']);
if ($q == '*' || $q == '')
	$q = 'fldksjflkdsjlkfjds';

// match the common name EXACTLY
$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Items` WHERE item_name="'.$q.'"');

// handle multiple words
$wordArr = explode(' ', $q);
if (count($wordArr) > 1) {
	// match the genus and species
	$tmpArr = $wordArr;
	$genus = array_shift($tmpArr);
	$species = implode(' ', $tmpArr);
	$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE genus="'.$genus.'" AND species="'.$species.'"');
	
	// match the species
	$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE species="'.$species.'"');
		
	// match start genus and species
	$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE genus="'.$genus.'" AND species LIKE "'.$species.'%"');

	// match start genus and any species
	$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE genus="'.$genus.'" AND species LIKE "%'.$species.'%"');

	// match the genus
	$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE genus="'.$genus.'"');
	
	// match the genus common name
	$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE genus_common_name="'.$genus.'"');


} else {
	$genus = $q;
	$species = $q;
}
// match the species
$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE species="'.$q.'"');
// match the genus
$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE genus="'.$q.'"');
$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE genus_common_name="'.$q.'"');


// match the partial common name start
$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Items` WHERE item_name LIKE "'.$q.'%"');
// match the partial species start
$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE species LIKE "'.$q.'%"');
// match the partial genus start
$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE genus LIKE "'.$q.'%"');
$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE genus_common_name LIKE "'.$q.'%"');


// match the partial common name word
$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Items` WHERE item_name LIKE "%'.$q.' %" OR  item_name LIKE "% '.$q.'%"');
// match the partial species
$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE species LIKE "%'.$q.' %" OR species LIKE "% '.$q.'%"');
// match the partial genus
$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE genus LIKE "%'.$q.' %" OR genus LIKE "% '.$q.'%"');


// match the partial common name
$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Items` WHERE item_name LIKE "%'.$q.'%"');
// match the partial species
$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE species LIKE "%'.$q.'%"');
// match the partial genus
$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE genus LIKE "%'.$q.'%"');

// match words
if (count($idArr) < 20 && count($wordArr) > 1) {
	for ($i=0;$i<count($wordArr);$i++) {
		// match words in common name
		$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Items` WHERE item_name LIKE "%'.$wordArr[$i].'%"');
	}
	
	for ($i=0;$i<count($wordArr);$i++) {
		// match words in species
		$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE species LIKE "%'.$wordArr[$i].'%"');
	}

	for ($i=0;$i<count($wordArr);$i++) {
		// match words in genus
		$idArr = mergeIDArrayWithQuery($idArr, 'SELECT * FROM `Plants` WHERE genus LIKE "%'.$wordArr[$i].'%"');
	}
}


// only use uniques
$idArr = array_unique($idArr);
$idArr = array_values($idArr);

// finally, we create the regular query
$itemsArr = array();
for ($i=0;$i<count($idArr);$i++) {
	$id = $idArr[$i];
	$result = mysql_query('SELECT * from `Items` WHERE is_available=1 AND item_id='.$id);
	//$result = mysql_query('SELECT * from `Items` WHERE item_id='.$id);
	if ($result && mysql_num_rows($result) > 0) {
		$tmpArr = mysql_fetch_array($result);
		if (categoryIsPlant($tmpArr['category_id'])) {
			$result = mysql_query('SELECT * from `Plants` WHERE item_id='.$id);
			$row = mysql_fetch_array($result);
			$tmpArr['botanical_name'] = $row['genus'].' '.$row['species'];
			$tmpArr['common_name'] = $tmpArr['item_name'];
			$tmpArr = array_merge($tmpArr, $row);
		}
	
		if (hasSizesAvailable($tmpArr['item_id'])) {
			$itemsArr[] = $tmpArr;
		}
	} // end if mysql_num_rows == 1
}

?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Item Search - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraCatalogHead(); ?>
</head>
<body>
<?php makeCatalogHeader(); ?>

<table cellspacing="0" cellpadding="5" border="0">
<?php
for ($i=0;$i<count($itemsArr);$i++) {
	echo '<tr>';
	
	// plant name
	echo '<td valign="middle" align="left" style="border-bottom: solid #878787 1px;">'.$itemsArr[$i]['item_name'].'</td>';
	
	// edit
	echo '<td valign="middle" align="right" style="border-bottom: solid #878787 1px; width: 200px;"><a href="item.php?i='.$itemsArr[$i]['item_id'].'">Edit</a></td>';
	
	echo '</tr>';
} // end for $i

?>

</table>

<?php

if (count($itemsArr) == 0) {
	echo '<b>No Results!</b><br><br>Try searching again or search for partial words.';
}
?>


<?php makeCatalogFooter(); ?>

</body>
</html>
<?php

function mergeIDArrayWithQuery($inArr, $query) {
	$idArr = array();
	$result = mysql_query($query);
	if ($result) {
		for ($i=0;$i<mysql_num_rows($result);$i++) {
			$row = mysql_fetch_array($result);
			$idArr[] = $row['item_id'];
		}
	}
	
	if (count($idArr) == 0)
		return $inArr;
	else
		return array_merge($inArr, $idArr);
}
