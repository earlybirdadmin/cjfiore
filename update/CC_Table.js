function searchForText(txt) {
	var tmpArr = removeRowsThatDontContain(rowsArr, txt);
	var html = getTableHTMLWithRowsArr(tmpArr);
	document.getElementById('dataTable').innerHTML = html;
}
function removeRowsThatDontContain(rows, txt) {
	if (txt == '')
		return rows;
		
	txt = txt.toLowerCase();
	var outArr = [];
	for (var i=0;i<rows.length;i++) {
		if (rows[i].toLowerCase().indexOf(txt) > -1) {
			outArr.push(rows[i]);
		}
	}
	return outArr;
}


var curSort = -1;
var sortDir = 1;
function sortBy(idx) {
	// set sort "direction"
	if (idx == curSort)
		sortDir *= -1;
	else
		sortDir = 1;
		
	// set curSort
	curSort = idx;
	
	// get index we're sorting BY
	var idx = d.cols[idx];
	
	// set isString flag so we can use toLowerCase()
	if (typeof d.table[0][idx] == 'string')
		var isString = true;
	else
		var isString = false;
		
	d.table.sort(function(x, y) {
		if (isString) {
			// here we use regex to strip the HTML tags and then go all "toLowerCase" on this string's ass
			if (x[idx].replace(/(<([^>]+)>)/ig,"").toLowerCase() > y[idx].replace(/(<([^>]+)>)/ig,"").toLowerCase())
				return sortDir;
			else if (x[idx].replace(/(<([^>]+)>)/ig,"").toLowerCase() < y[idx].replace(/(<([^>]+)>)/ig,"").toLowerCase())
				return -1 * sortDir;
			else
				return 0;
		} else {
			// normal sort
			if (x[idx] > y[idx])
				return sortDir;
			else if (x[idx] < y[idx])
				return -1 * sortDir;
			else
				return 0;
		}
	});
	
	rowsArr = getTableRowsArr(d);
	searchForText(document.getElementById('searchBox').value);
}


var MAX_ROWS = 50;
var lastTableArr = [];
function setMaxRows(nm) {
	MAX_ROWS = nm;
	var html = getTableHTMLWithRowsArr(lastTableArr);
	document.getElementById('dataTable').innerHTML = html;
}
function getTableHTMLWithRowsArr(rowsArr) {
	lastTableArr = rowsArr;
	
	var outHTML = '<table cellspacing="0" cellpadding="0" border="1" class="adminTable">';
	// get header cols
	for (var i=0;i<d.cols.length;i++) {
		outHTML += '<td valign="middle" align="left"><b><a href="javascript:;" onclick="sortBy('+i+');">' + d.cols[i] + '</a></b></td>';
	}
	outHTML += '</tr>';
	
	for (var i=0;i<rowsArr.length && i < MAX_ROWS;i++) {
		outHTML += rowsArr[i];
	}
	outHTML += '</table>';
	return outHTML;
}
function getTableRowsArr(d) {
	var outArr = [];
	for (var key in d.table) {
		outArr.push(d.table[key].rowHTML);
	}
	return outArr;
}


/* init by assigning the html (so we don't have to recreate it every time */
function addTableRowsToData(data) {
	var d = JSON.parse(atob(data));
	for (var idx in d.table) {
		d.table[idx].rowHTML = getSingleRowHTML(d.table[idx], d.cols, idx);
	}
	return d;
}
function getSingleRowHTML(o, colsArr, idx) {
	var outHTML = '<tr id="CC_Row_'+idx+'">';
	for (var i=0;i<colsArr.length;i++) {
		if (!o[colsArr[i]])
			val = ' ';
		else
			val = o[colsArr[i]];
		if (val === true)
			outHTML += '<td align="center" valign="middle">X</td>';
		else
			outHTML += '<td align="left" valign="middle">' + getTrimmedValFor(val) + '</td>';
	}
	outHTML += '</tr>';
	return outHTML;
}

// trims long data
function getTrimmedValFor(html) {
	var MAX_STRING_LENGTH = 30;
	
	// remove html tags
	var val = html.replace(/(<([^>]+)>)/ig,"");
	
	// NOTE! Add Fix for HTML later! Right now we ONLY trim it if there's no HTML
	if (val.length > MAX_STRING_LENGTH && html.indexOf('select') == -1) {
		var tmpVal = val.substring(0, MAX_STRING_LENGTH);
		return tmpVal;
	}
	return html;
}
