<?php
require_once('../functions.php');
require_once('../functions_catalog.php');



$result = mysql_query('SELECT * FROM `UserInfo` WHERE is_newsletter_subscriber = 1 ORDER BY user_id ASC');
$out = '"Company","First","Last","Email","Business","Wholesale"';
$out .= "\n";

for ($i=0;$i<mysql_num_rows($result);$i++) {
	$row = mysql_fetch_array($result);
	if ($row['is_business'] == 1)
		$isBusiness = 'X';
	else
		$isBusiness = '';
		
	if ($row['is_wholesale'] == 1)
		$isWholesale = 'X';
	else
		$isWholesale = '';

	$out .= '"'.$row['company'] .
		'","'.ucwords($row['first_name']) .
		'","'.ucwords($row['last_name']) .
		'","'.$row['email'] .
		'","'.$isBusiness .
		'","'.$isWholesale . '"';
	$out .= "\n";
}


header('Content-Type: text/csv; charset=utf-8');
header('Content-Disposition: attachment; filename=Fiore Newsletter Emails '.date('Ymd').'.csv');
echo $out;
