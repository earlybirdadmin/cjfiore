<?php
require_once('../functions.php');
require_once('../functions_catalog.php');

if (isset($_POST['category_id']) && is_numeric($_POST['category_id']) && isset($_POST['item_id']) && is_numeric($_POST['item_id'])) {
	mysql_query('UPDATE `Items` SET category_id='.$_POST['category_id'].' WHERE item_id='.$_POST['item_id']);
	die('success');
}


// get plant category names

// get the list of plants
$keysArr = array('Plant', 'Genus', 'Species', 'Category');
$tableArr = array();

$result = mysql_query('SELECT * FROM `Plants`');
for ($i=0;$i<mysql_num_rows($result);$i++) {
	$row = mysql_fetch_array($result);
	$tmpArr = array();
	$tmpArr['item_id'] = $row['item_id'];
	$tmpArr['Genus'] = $row['genus'];
	$tmpArr['Species'] = $row['species'];
	
	$result2 = mysql_query('SELECT * FROM `Items` WHERE item_id='.$tmpArr['item_id']);
	if (mysql_num_rows($result2) == 0) {
		// die('WTF?'); // ignore plants that don't match an "Item"
		continue;
	}
	$row2 = mysql_fetch_array($result2);
	$tmpArr['category_id'] = $row2['category_id'];
	$tmpArr['Category'] = getCategoryDropDownForCategoryID($tmpArr['category_id'], $tmpArr['item_id']);
	$tmpArr['Plant'] = '<a href="item.php?i='.$tmpArr['item_id'].'">'.$row2['item_name'].'</a>';
	
	$tableArr[] = $tmpArr;
	
}

$out = base64_encode(json_encode(array('cols'=>$keysArr, 'table'=>$tableArr)));


function getCategoryDropDownForCategoryID($id, $item_id) {
	if (!isset($GLOBALS['categoryArr']))
		initCategoryArr();
	
	// add a hidden "sort" first
	$outHTML = '<span style="display: none;">'.str_pad($id, 2, '0', STR_PAD_LEFT).'</span>';
	// now add the drop-down
	$outHTML .= '<select onchange="updateCategoryFor(this, '.$item_id.');">';
	for ($i=0;$i<count($GLOBALS['categoryArr']);$i++) {
		$outHTML .= '<option value="'.$GLOBALS['categoryArr'][$i]['id'].'"';
		if ($GLOBALS['categoryArr'][$i]['id'] == $id)
			$outHTML .= ' SELECTED';
		$outHTML .= '>' . $GLOBALS['categoryArr'][$i]['name'] . '</option>';
	}
	$outHTML .= '</select>';
	
	return $outHTML;
}

function initCategoryArr() {
	$GLOBALS['categoryArr'] = array(array('id' => 0, 'name' => 'NONE'));

	$result = mysql_query('SELECT * FROM `Categories` WHERE category_id >= 7 AND category_id <= 14 ORDER BY sort_order ASC');
	for ($i=0;$i<mysql_num_rows($result);$i++) {
		$row = mysql_fetch_array($result);
		$GLOBALS['categoryArr'][] = array('id' => $row['category_id'], 'name' => $row['category_name']);
	}
}
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>fiore</title>
<?php extraCatalogHead(); ?>

<script src="CC_Table.js"></script>
<style>
.adminTable td {
	padding: 4px 6px;
	line-height: 1;
}
.adminTable {
	margin: 0px auto;
	min-width: 90%;
}
#dataTable {
	min-width: 900px;
	white-space: nowrap;
}
#searchBox {
	width: 50%;
	font-size: 1.2em;
	padding: 2px 4px;
}
a {
	text-decoration: none;
	color: #4e4244;
}
a:hover {
	text-decoration: underline;
}
</style>

</head>
<body>
<?php makeCatalogHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">

	<tr>
		<td valign="top" align="left" style="padding: 20px 10px 0px 20px;">
			<div style="width: 800px; margin: 0px auto 20px auto;">
				Search: <input type="search" id="searchBox" onkeyup="searchForText(this.value);" onsearch="searchForText(this.value);">
			</div>
			<div id="dataTable"></div>
			
			<div style="margin: 20px;">
				<a href="javascript:;" onclick="setMaxRows(999999);" id="showAll">Show All</a> | <a href="javascript:;" onclick="setMaxRows(50);" id="showFirst50">Show First 50</a>
			</div>
			
		</td>
	</tr>
</table>

<?php makeCatalogFooter(); ?>

<script language="javascript">
var d = addTableRowsToData("<?php echo $out; ?>");
var rowsArr = getTableRowsArr(d);
var html = getTableHTMLWithRowsArr(rowsArr);
//document.getElementById('dataTable').innerHTML = html;
// sort by category first
sortBy(3);

var ccc = false;
function updateCategoryFor(el, item_id) {
	var category_id = el.value;
	var tr = el.parentNode.parentNode;
	// load XML
	if (window.XMLHttpRequest) {
		req = new XMLHttpRequest();
	} else if (window.ActiveXObject) {
		req = new ActiveXObject("Microsoft.XMLHTTP");
	}
	if (req) {
		var params = "category_id="+category_id+"&item_id="+item_id;
		req.open("POST", "plants.php", true);
		
		req.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		
		req.onreadystatechange = function() {
			if (req.readyState == 4 && req.status == 200) {
				handleUpdateCategoryFor(req.responseText, tr);
			}
		};
		
		req.send(params);
	}
}

function handleUpdateCategoryFor(response, tr) {
	if (response == 'success') {
		// get row ID
		alert('success!');
	} else {
		alert('There was a problem updating the last plant! Refresh this page and try again, perhaps?');
	}
}
</script>


</body>
</html>
