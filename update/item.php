<?php
require_once('../functions.php');
require_once('../functions_catalog.php');

// set the MySQL structures
$itemIndexArr = array('item_id', 'category_id', 'item_name', 'description', 'image_file', 'is_available', 'is_featured', 'discount');
$plantIndexArr = array("item_id", "genus_common_name", "genus", "species", "mature_height", "mature_spread", "growth_habit", "growth_rate", "light_preference", "flower_color", "bears_fruit_in", "fruit", "foliage", "flowers_in", "fall_color", "growth_zone");
$sizesIndexArr = array("id", "item_id", "item_order", "item_size", "wholesale_price", "retail_price", "is_available");


// handle POST
if (isset($_POST['item_id'])) {
	hasValidSize();
	$itemsArr = getArrayFromIndexArray($itemIndexArr);
	if (categoryIsPlant($itemsArr['category_id'])) {
		$plantArr = getArrayFromIndexArray($plantIndexArr);
	}
	
	// sizes array is special
	$sizesArr = array();
	for ($i=0;isset($_POST['size'.$i.'-id']);$i++) {
		// checkbox for is_available
		if (isset($_POST['size'.$i.'-is_available']) && $_POST['size'.$i.'-is_available'] == 1)
			$isAvailable = 1;
		else
			$isAvailable = 0;
			
		$tmpArr = array('id' => $_POST['size'.$i.'-id'], 'item_id' => $_POST['item_id'], 'item_order' => $i, 'item_size' => $_POST['size'.$i.'-description'], 'wholesale_price' => $_POST['size'.$i.'-wholesale'], 'retail_price' => $_POST['size'.$i.'-retail'], 'is_available' => $isAvailable);
		if (is_numeric($tmpArr['wholesale_price']) && is_numeric($tmpArr['retail_price']))
			$sizesArr[] = $tmpArr;
	}
	
	
	
	// add image if we need to
	if (isset($_FILES['f0']['name']) && $_FILES['f0']['name'] != '') {
		$filename = '../assets/catalog/'.$_FILES['f0']['name'];
		if (file_exists($filename)) {
			// add numbers to the filename
			$pathParts = pathinfo($filename);
			for ($i=0;file_exists($filename);$i++) {
				$filename = $pathParts['dirname'].'/'.$pathParts['filename'].'-'.$i.'.'.$pathParts['extension'];
			}
		}
		
		move_uploaded_file($_FILES['f0']['tmp_name'], $filename);
		$itemsArr['image_file'] = basename($filename);
	}
	
	// set default image name
	if (!isset($itemsArr['image_file']) || trim($itemsArr['image_file']) == '') {
		$itemsArr['image_file'] = 'nophoto.jpg';
	}
	
	if (count($sizesArr) > 0) {
		if ($itemsArr['item_id'] <= 0) {
			// create a new blank item
			$itemsArr['item_id'] = createBlankRowAndGetIDFromTable('Items');
			$_GET['i'] = $itemsArr['item_id'];

			
			// set the sizesArr
			for ($i=0;$i<count($sizesArr);$i++) {
				$sizesArr[$i]['item_id'] = $itemsArr['item_id'];
			}
		}

		// update the item
		$query = 'UPDATE `Items` SET category_id='.$itemsArr['category_id'].', item_name="'.$itemsArr['item_name'].'", description="'.$itemsArr['description'].'", image_file="'.$itemsArr['image_file'].'", is_available='.$itemsArr['is_available'].', is_featured='.$itemsArr['is_featured'].', discount='.$itemsArr['discount'].' WHERE item_id='.$itemsArr['item_id'];
		$result = mysql_query($query);
		
		// update the plant if we need to
		if (isset($plantArr)) {
			// add blank plant info if we need to
			$result = mysql_query('SELECT * FROM `Plants` WHERE item_id='.$itemsArr['item_id']);
			if (mysql_num_rows($result) == 0) {
				$plantArr['item_id'] = $itemsArr['item_id'];
				mysql_query('INSERT INTO `Plants` (`item_id`) VALUES ('.$itemsArr['item_id'].')');
			}
			
			// update plant info
			$query = 'UPDATE `Plants` SET genus_common_name="'.$plantArr['genus_common_name'].'",
										genus="'.$plantArr['genus'].'",
										species="'.$plantArr['species'].'",
										mature_height="'.$plantArr['mature_height'].'",
										mature_spread="'.$plantArr['mature_spread'].'",
										growth_habit="'.$plantArr['growth_habit'].'",
										growth_rate="'.$plantArr['growth_rate'].'",
										light_preference="'.$plantArr['light_preference'].'",
										flower_color="'.$plantArr['flower_color'].'",
										bears_fruit_in="'.$plantArr['bears_fruit_in'].'",
										fruit="'.$plantArr['fruit'].'",
										foliage="'.$plantArr['foliage'].'",
										flowers_in="'.$plantArr['flowers_in'].'",
										fall_color="'.$plantArr['fall_color'].'",
										growth_zone="'.$plantArr['growth_zone'].'" 
									WHERE item_id='.$plantArr['item_id'];
			$result = mysql_query($query);
		}
		
		// delete all the sizes for this item
		mysql_query('DELETE FROM `Sizes` WHERE item_id='.$itemsArr['item_id']);
		
		// re-add all the sizes for this item (skipping blanks)
		for ($i=0;$i<count($sizesArr);$i++) {
			if (isset($sizesArr[$i]['item_size']) && isset($sizesArr[$i]['wholesale_price']) && isset($sizesArr[$i]['retail_price'])) {
				mysql_query('INSERT INTO `Sizes` VALUES (NULL, '.$sizesArr[$i]['item_id'].', '.$i.', "'.mysql_real_escape_string($sizesArr[$i]['item_size']).'", '.$sizesArr[$i]['wholesale_price'].', '.$sizesArr[$i]['retail_price'].', '.$sizesArr[$i]['is_available'].')');
			}
		}

	}
}








// get tht itemArr
if (isset($_GET['i']) && is_numeric($_GET['i'])) {
	$query = 'SELECT * FROM `Items` WHERE item_id='.$_GET['i'];
	$result = mysql_query($query);
	if (mysql_num_rows($result) > 0) {
		$itemArr = mysql_fetch_array($result);
	}
}

// set the itemArr if we don't have one
if (!isset($itemArr)) {
	$itemArr = array(	'item_id' => -1, 
						'category_id' => -1, 
						'item_name' => '', 
						'description' => '', 
						'image_file' => 'nophoto.jpg', 
						'is_available' => 1,
						'discount' => 0.00,
						'ITEM_NO' => 0);
}

if (isset($_GET['c'])){
	// category is set
	$cat = $_GET['c'];
} else if (isset($itemArr['category_id'])) {
	// item is set so get its parent category
	$result = mysql_query('SELECT * FROM `Categories` WHERE category_id='.$itemArr['category_id']);
	$row = mysql_fetch_array($result);
	$parentCat = $row['parent_id'];
	$cat = $itemArr['category_id'];
}

$_GET['c'] = $cat;
// get breadcrumbs array
$GLOBALS['breadcrumbsArr'] = getBreadcrumbsArr();

// get the sizesArr
$sizesArr = array();
$query = 'SELECT * FROM `Sizes` WHERE item_id = '.$itemArr['item_id'];
$result = mysql_query($query);
for ($i=0;$i<mysql_num_rows($result);$i++) {
	$sizesArr[] = mysql_fetch_array($result);
}



// get the plantArr
if (categoryIsPlant($itemArr['category_id'])) {
	$query = 'SELECT * FROM `Plants` WHERE item_id='.$itemArr['item_id'];
	$result = mysql_query($query);
	if (mysql_num_rows($result) > 0) {
		$plantArr = mysql_fetch_array($result);
	}
}

//set the plantArr if it's not set
if (!isset($plantArr)) {
	$plantArr = array(	'item_id' => $itemArr['item_id'],
						'genus_common_name' => '',
						'genus' => '',
						'species' => '',
						'mature_height' => '',
						'mature_spread' => '',
						'growth_habit' => '',
						'growth_rate' => '',
						'light_preference' => '',
						'flower_color' => '',
						'bears_fruit_in' => '',
						'fruit' => '',
						'foliage' => '',
						'flowers_in' => '',
						'fall_color' => '',
						'growth_zone' => '');
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>fiore</title>
<?php extraCatalogHead(); ?>
<script type="text/javascript" src="update.js"></script>
</head>
<body onload="addSize();">
<?php makeCatalogHeader(); ?>

<form method="POST" action="" enctype="multipart/form-data">
<input type="hidden" name="item_id" id="item_id" value="<?php echo $itemArr['item_id']; ?>">

<table cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td valign="top" align="left" style="padding-right: 20px; font-size: 16px;">
<h1>Update Items</h1>
<br>Category: 
<br><select name="parent_category" id="parent_category" onchange="updateSubcategories();">
<?php
$result = mysql_query('SELECT * FROM `Categories` WHERE parent_id=0 AND item_count != -1  ORDER BY category_id ASC');
$parentArr = array();
for ($i=0;$i<mysql_num_rows($result);$i++) {
	$row = mysql_fetch_array($result);
	echo '<option value="'.$row['category_id'].'"';
	if ($row['category_id'] == $parentCat)
		echo ' SELECTED';
	echo '>'.$row['category_name'].'</option>';
	
	$parentArr[] = $row['category_id'];
}
?>
</select>
<br>Subcategory: 
<div id="subcat0">
</div>


<br>Item Name:
<br><input type="text" name="item_name" id="item_name" value="<?php echo encodeForTextInput($itemArr['item_name']); ?>" style="width: 250px;">
<br>Description:
<br><textarea name="description" id="description" rows="5" style="width: 250px;"><?php echo $itemArr['description']; ?></textarea>
<br><input type="checkbox" name="is_available" value="1" id="is_available"<?php if ($itemArr['is_available']) { echo ' CHECKED'; } ?>> Available
<br><input type="checkbox" name="is_featured" value="1" id="is_featured"<?php if ($itemArr['is_featured']) { echo ' CHECKED'; } ?>> Featured
<br>Discount:
<br>$<input type="text" name="discount" id="discount" value="<?php echo encodeForTextInput($itemArr['discount']); ?>">
<br>
		</td>
		<td valign="top" align="left">
<div id="plantInfo">
<table cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td valign="top" align="left" style="padding-right: 20px;">
			<br>Genus:
			<br><input type="text" name="genus" id="genus" value="<?php echo encodeForTextInput($plantArr['genus']); ?>">
			<br>Mature Height:
			<br><input type="text" name="mature_height" id="mature_height" value="<?php echo encodeForTextInput($plantArr['mature_height']); ?>">
			<br>Growth Habit:
			<br><input type="text" name="growth_habit" id="growth_habit" value="<?php echo encodeForTextInput($plantArr['growth_habit']); ?>">
			<br>Light Preference:
			<br><input type="text" name="light_preference" id="light_preference" value="<?php echo encodeForTextInput($plantArr['light_preference']); ?>">

			<br>Bears Fruit In:
			<br><input type="text" name="bears_fruit_in" id="bears_fruit_in" value="<?php echo encodeForTextInput($plantArr['bears_fruit_in']); ?>">
			<br>Foliage:
			<br><input type="text" name="foliage" id="foliage" value="<?php echo encodeForTextInput($plantArr['foliage']); ?>">
			<br>Fall Color:
			<br><input type="text" name="fall_color" id="fall_color" value="<?php echo encodeForTextInput($plantArr['fall_color']); ?>">
		</td>
		<td valign="top" align="left">
			<br>Species:
			<br><input type="text" name="species" id="species" value="<?php echo encodeForTextInput($plantArr['species']); ?>">
			<br>Mature Spread:
			<br><input type="text" name="mature_spread" id="mature_spread" value="<?php echo encodeForTextInput($plantArr['mature_spread']); ?>">
			<br>Growth Rate:
			<br><input type="text" name="growth_rate" id="growth_rate" value="<?php echo encodeForTextInput($plantArr['growth_rate']); ?>">

			<br>Flower Color:
			<br><input type="text" name="flower_color" id="flower_color" value="<?php echo encodeForTextInput($plantArr['flower_color']); ?>">
			<br>Fruit / Cone:
			<br><input type="text" name="fruit" id="fruit" value="<?php echo encodeForTextInput($plantArr['fruit']); ?>">
			<br>Flowers In:
			<br><input type="text" name="flowers_in" id="flowers_in" value="<?php echo encodeForTextInput($plantArr['flowers_in']); ?>">
			<br>Growth Zone:
			<br><input type="text" name="growth_zone" id="growth_zone" value="<?php echo encodeForTextInput($plantArr['growth_zone']); ?>">
		</td>
	</tr>
</table>
</div>
		</td>
		<td valign="top" align="left" style="padding: 20px 0px 0px 20px;">
			<img src="../assets/catalog/<?php echo $itemArr['image_file']; ?>" width="151">
			<br>Upload New Image (600x679 pixels):
			<br><input type="file" name="f0">
			<br><input type="hidden" name="image_file" value="<?php echo encodeForTextInput($itemArr['image_file']); ?>">
			<br><input type="submit" value="submit">
		</td>
	</tr>
	<tr>
		<td colspan="3" valign="top" align="left">
<table cellspacing="4" cellpadding="0" border="0" id="sizesTable">
	<tr>
		<td vlaign="top" align="left"><b>Size</b></td>
		<td vlaign="top" align="left"><b>Wholesale Price</b></td>
		<td vlaign="top" align="left"><b>Retail Price</b></td>
		<td vlaign="top" align="left"><b>Available</b></td>
	</tr>
<?php
for ($i=0;$i<count($sizesArr);$i++) {
	echo '<tr><td valign="top" align="left">';
	echo '<input type="hidden" name="size'.$i.'-id"  id="size'.$i.'-id" value="'.$sizesArr[$i]['id'].'">';
	echo '<input type="text" name="size'.$i.'-description" id="size'.$i.'-description" value="'.encodeForTextInput($sizesArr[$i]['item_size']).'" style="width: 200px;"></td>';
	echo '<td valign="top" align="left">$<input type="text" name="size'.$i.'-wholesale" id="size'.$i.'-wholesale" value="'.encodeForTextInput($sizesArr[$i]['wholesale_price']).'" style="width: 80px;"></td>';
	echo '<td valign="top" align="left">$<input type="text" name="size'.$i.'-retail" id="size'.$i.'-retail" value="'.encodeForTextInput($sizesArr[$i]['retail_price']).'" style="width: 80px;"></td>';
	echo '<td valign="top" align="left"><input type="checkbox" name="size'.$i.'-is_available" value="1"';
	if ($sizesArr[$i]['is_available'] == 1)
		echo ' CHECKED';
	echo '> Available</td>';
	echo '</tr>';
}
?>
</table>
&nbsp;<a href="javascript:;" onclick="addSize();">[+] New Size</a>
<br>
<br><input type="submit" value="submit">
<!-- end sizes table -->

		</td>
	</tr>
</table>

</form>
<br>
<br>
<br>

<?php makeCatalogFooter(); ?>


<script language="javascript">
var subCatSelectArr = new Array();
<?php
for ($i=0;$i<count($parentArr);$i++) {
	$result = mysql_query('SELECT * FROM `Categories` WHERE parent_id='.$parentArr[$i].' ORDER BY category_id ASC');
	echo "\n";
	echo 'subCatSelectArr['.$parentArr[$i].'] = "<select name=\\"category_id\\" style=\\"width: 250px;\\">';
	for ($j=0;$j<mysql_num_rows($result);$j++) {
		$row = mysql_fetch_array($result);
		echo '<option value=\\"'.$row['category_id'].'\\"';
		if ($row['category_id'] == $cat)
			echo ' SELECTED';
		echo '>'.$row['category_name'].'</option>';
	}
	echo '</select>";';
}
?>



function updateSubcategories() {
	var o = document.getElementById('parent_category');
	var parentID = o.options[o.selectedIndex].value;
	document.getElementById('subcat0').innerHTML = subCatSelectArr[parentID];
	
	// hide/show plant div
	if (parentID == 1)
		document.getElementById('plantInfo').style.display = 'block';
	else
		document.getElementById('plantInfo').style.display = 'none';
}

updateSubcategories();
</script>

</body>
</html><?php
function hasValidSize() {
	$varsArr = array('size0-id', 'size0-description', 'size0-wholesale', 'size0-retail', 'size0-is_available');
	for ($i=0;$i<count($varsArr);$i++) {
		$idx = $varsArr[$i];
		// make sure all items are filled out
		if (!isset($_POST[$idx]) || trim($_POST[$idx]) == '') {
			return false;
		}
		
		return true;
	}
}


function getArrayFromIndexArray($idxArr) {
	$outArr = array();
	
	for ($i=0;$i<count($idxArr);$i++) {
		$idx = $idxArr[$i];
		if (isset($_POST[$idx]))
			$outArr[$idx] = mysql_real_escape_string($_POST[$idx]);
		else if (strpos($idx, 'is_') !== false) // booleans
			$outArr[$idx] = 0;
		else
			$outArr[$idx] = '';
	}
	
	return $outArr;
}


function createBlankRowAndGetIDFromTable($table) {
	$query = 'INSERT INTO `'.$table.'` (`item_id`) VALUES (NULL);';
	$result = mysql_query($query);
	// set the new item id
	return mysql_insert_id();
}