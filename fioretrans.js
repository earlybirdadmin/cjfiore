var translateLinkArr = new Array();
// menu items
translateLinkArr['ABOUT US'] = 'SOBRE NOSOTROS';
translateLinkArr['SERVICES'] = 'SERVICIOS';
translateLinkArr['PRODUCTS'] = 'PRODUCTOS';
translateLinkArr['PARTNERS'] = 'SOCIOS';
translateLinkArr['RESOURCES'] = 'RECURSOS';
translateLinkArr['CONTACT'] = 'CONTACTO';
// submenu items
translateLinkArr['History'] = 'Historia';
translateLinkArr['Staff'] = 'Personal';
translateLinkArr['Hours / Locations'] = 'Horas/Ubicaciones';
translateLinkArr['Careers'] = 'Carreras';
translateLinkArr['Sourcing'] = 'Abastecimiento';
translateLinkArr['Purchasing Solutions'] = 'Adquirir Soluciones';
translateLinkArr['Delivery'] = 'Entrega';
translateLinkArr['Planting / Fabrication'] = 'Plantar / Fabricación';
translateLinkArr['Plants'] = 'Plantas';
translateLinkArr['Landscape Supply'] = 'Suministros Paisaje';
translateLinkArr['Natural Stone'] = 'Piedra Natural';
translateLinkArr['Green Roof'] = 'Techo Verde';
translateLinkArr['Planters and Accessories'] = 'Macetas y Accesorios';
translateLinkArr['Specials'] = 'Especiales';

// top stuff
translateLinkArr['¿Habla español?'] = '&nbsp;English Version';
translateLinkArr['My Cart&nbsp;<img src="images/cartIcon.gif" width="20" height="14" border="0" style="position: relative; top: 3px;">'] = 'Mi Carrito&nbsp;<img src="images/cartIcon.gif" width="20" height="14" border="0" style="position: relative; top: 3px;">';
translateLinkArr['Sign Up'] = 'Alistarse';
translateLinkArr['Sign In'] = 'Registrarse';
translateLinkArr['PDF CATALOG'] = 'PDF CATÁLOGO';



var translateSpanArr = new Array();
//--------------- products.php ---------------
translateSpanArr['Fiore Products'] = 'Fiore Productos';
translateSpanArr['Fiore inventories the largest selection of above-ground, premium grade plant material in the Chicagoland area with over 1,100 varieties, including many that are unique or hard to find, selected from the finest nurseries in the country. We also offer a generous selection of natural stone products from the nation’s most trusted quarries, and a full range of landscape supply products including soil mixes, mulches and composts, assorted bulk materials, and drainage products.'] = 'Inventarios Fiore la mayor selección de encima de la tierra, material prima vegetal de grado en el área de Chicago, con más de 1.100 variedades, incluyendo muchos que son únicas o difíciles de encontrar, seleccionados de entre los mejores criaderos del país. También ofrecemos una amplia selección de productos de piedra natural de las canteras de mayor confianza de la nación, y una gama completa de productos de alimentación paisaje, incluidas las mezclas de suelo y compost, mantillo, materiales variados a granel, y productos de drenaje.';
translateSpanArr['SEARCH CATALOG'] = 'Buscar Catálogo';
// products page links
translateSpanArr['Plants'] = 'Plantas';
translateSpanArr['Landscape Supply'] = 'Suministros Paisaje';
translateSpanArr['Natural Stone'] = 'Piedra Natural';
translateSpanArr['Green Roof'] = 'Techo Verde';
translateSpanArr['Planters and Accessories'] = 'Macetas y Accesorios';
translateSpanArr['Specials'] = 'Especiales';


//--------------- about.php ---------------
translateSpanArr['Fiore Nursery and Landscape Supply has been a trusted partner and supplier to the Green Industry and home gardener for over 90 years,'] = 'Nursery Fiore y Landscape Supply ha sido un socio de confianza y proveedor de la industria verde y jardinero de la casa desde hace más de 90 años,';
translateSpanArr['providing the Chicagoland area with the area’s largest selection of premium grade plant material with over 1,100 varities. We now also offer natural stone and landscape supply products at both of our locations, sourced from some of the best suppliers in the midwest and available for immediate pick up or delivery.'] = 'proporcionar el área de Chicago con la selección más grande de la zona de material prima vegetal de grado con más de 1.100 variedades. Ahora también ofrecemos productos de piedra natural y del paisaje de abastecimiento, tanto de nuestras localidades, provenientes de algunos de los mejores proveedores en el medio oeste y disponibles para la inmediata recogida o entrega.';
translateSpanArr['At both our Prairie View and Chicago sales yard, you will find the same extensive selection of premium plants, natural stone, and landscape materials, combined with the superior customer service you’ve come to expect from Fiore. At Fiore, we remain committed to offering our customers the highest quality products available, along with our top-notch expert advice and consultation.'] = 'Tanto en nuestro Prairie View y el patio de ventas de Chicago, se encuentra la misma selección extensa de las plantas superiores, piedra natural y materiales del paisaje, junto con el servicio de atención al cliente que usted ha llegado a esperar de Fiore. Al Fiore, seguimos comprometidos a ofrecer a nuestros clientes productos de alta calidad disponibles, junto con nuestro asesoramiento de expertos de primer nivel y la consulta.';
translateSpanArr['Come visit us!'] = 'Ven a visitarnos!';


//--------------- services.php ---------------
translateSpanArr['Fiore Services'] = 'Fiore Servicios';
translateSpanArr['At Fiore, our number one goal is to provide our customers with<br>the highest level of customer satisfaction in our industry. We do<br>this by having one of the most qualified nursery teams in the<br>midwest, and by offering the best customer service available<br>from materials selection and sourcing to delivery and plant<br>consultation.'] = 'Al Fiore, nuestro objetivo número uno es proporcionar a nuestros clientes con<br>el mayor nivel de satisfacción del cliente en nuestra industria. hacemos<br>esto a través de uno de los equipos infantiles más calificados en el<br>midwest, y ofreciendo el mejor servicio al cliente disponible<br>desde la selección de materiales y de abastecimiento de la entrega y<br>de la planta consulta.';
// links
translateSpanArr['Sourcing'] = 'Abastecimiento';
translateSpanArr['Purchasing Solutions'] = 'Adquirir Soluciones';
translateSpanArr['Delivery'] = 'Entrega';
translateSpanArr['Planting/Fabrication'] = 'Plantar / Fabricación';

//--------------- partners.php ---------------
translateSpanArr['Fiore Partners'] = 'Fiore Socios';
translateSpanArr['Working with Fiore gains you access to the Fiore Network, our partner network with over 250 suppliers from across the country and around the globe. Our network allows us to supply you with unlimited, quality horticultural, stone, and landscape products and supplies. Our largest partners, Eden Stone Company, Valders Stone, and Midwest Trading, stock a full range of products on-site at both of our locations, eliminating wait time for the essentials you need.'] = 'Trabajar con ganancias Fiore tiene acceso a la Red de Fiore, nuestra red de socios con más de 250 proveedores de todo el país y alrededor del mundo. Nuestra red nos permite suministrarle ilimitado, hortícolas de calidad, piedra, paisaje y los productos e insumos. Nuestros principales socios de la compañía, Eden Valders Stone, Stone, y el comercio del Medio Oeste, las acciones de una amplia gama de productos en el sitio, tanto de nuestras instalaciones, eliminando el tiempo de espera para los elementos esenciales que necesita.';

//--------------- resources.php ---------------
translateSpanArr['Fiore Resources'] = 'Fiore Recursos';
translateSpanArr['At Fiore we continually strive to keep you up to date on the latest products and industry trends that can help you be more successful. Join us for an event or browse our resources to <br>help with research on your latest project.'] = 'Al Fiore nos esforzamos continuamente para mantenerse al día sobre los últimos productos y tendencias de la industria que pueden ayudarle a tener más éxito. Únase a nosotros para un evento o navegar por nuestros recursos para ayudar con la investigación en su último proyecto.';
translateSpanArr['News and Events'] = 'Noticias y Eventos';
translateSpanArr['Helpful Information'] = 'Información útil';
translateSpanArr['We have accumulated helpful and useful information over the years which is available here for your use.'] = 'Hemos acumulado una información útil y provechoso en los últimos años, que está disponible aquí para su uso.';
translateSpanArr['Stay up-to-date with the latest events and news at Fiore by singing up for our E-newsletter. And if you’ve missed something you can always find it in our archive.'] = 'Manténgase al día con los últimos acontecimientos y noticias en Fiore, cantando a nuestro boletín electrónico. Y si te has perdido algo que siempre se puede encontrar en nuestro archivo.';
translateSpanArr['Day-to-day thoughts and ideas from Fiore can help keep the ideas flowing. Follow us here.'] = 'Día a día los pensamientos e ideas de Fiore puede ayudar a mantener el flujo de ideas. Siga con nosotros aquí.';
translateSpanArr['Coming Soon'] = 'Muy Pronto';
translateSpanArr['No Upcoming Events or<br>News to Report'] = 'No hay próximos eventos o<br>Noticias al Informe';

translateLinkArr['Archived Fiore Newsletters'] = 'Fiore Boletines Archivados';

//--------------- contact.php ---------------
translateSpanArr['Sales Chicago'] = 'Ventas Chicago';
translateSpanArr['Sales Prairie View'] = 'Ventas Prairie View';
translateSpanArr['First Name:'] = 'Nombre:';
translateSpanArr['Last Name:'] = 'Apellido:';
translateSpanArr['Email&nbsp;Address:'] = 'Correo&nbsp;Electrónico:';
translateSpanArr['Message:'] = 'Mensaje:';





//--------------- about_history.php ---------------
translateSpanArr['Fiore, which means “flower” in Italian, has established a long legacy with its customers based on our commitment to offering quality products, trusted service, and broad selection. 90 years later, Fiore remains committed to delivering the same values.'] = 'Fiore, que significa "flor" en italiano, ha establecido un legado con sus clientes basado en nuestro compromiso de ofrecer productos de calidad, servicio confiable y la selección amplia. 90 años después, sigue siendo Fiore compromiso de ofrecer los mismos valores.';
translateSpanArr['For over four generations the Fiore family has been providing plant material to landscape architects, landscape contractors, and landscape design professionals in the Chicagoland area and across the country. The family’s roots run deep and have helped influence and shape the local and national green industry. Fiore Nursery has also helped elevate the industry’s standards and best practices which has earned Fiore the reputation as being the preferred and trusted supplier for plant material and natural stone in the area.'] = 'Durante más de cuatro generaciones de la familia Fiore ha estado proporcionando material vegetal a los arquitectos paisajistas, contratistas y profesionales del paisaje, diseño del paisaje en el área de Chicago y en todo el país. Las raíces de la familia son profundas y han ayudado a dar forma a la influencia y la industria ecológica local y nacional. Nursery Fiore también ha ayudado a elevar los estándares de la industria y las mejores prácticas que se ha ganado la reputación como Fiore ser el proveedor preferido y confiable para material vegetal y de piedra natural en la zona.';
translateSpanArr['Learn more about Fiore!'] = 'Más información acerca de Fiore!';
translateSpanArr['For more information about Fiore<br>Nursery and its history, download<br>these recent articles from some of<br>our industry trade publications.'] = 'Para obtener más información acerca de<br>Fiore Nursery MedlinePlus y su historia, <br>descargar estos artículos recientes de <br>algunos de nuestras publicaciones<br>comerciales de la industria.';

translateLinkArr['DOWNLOAD'] = 'DESCARGAR';



//--------------- about_staff.php ---------------
translateSpanArr['The Fiore Team'] = 'El equipo de Fiore';
translateSpanArr['<b>It is the people at Fiore, our greatest assets, that helps us stand apart</b>. Our highly regarded staff of horticulturists, certified arborists, landscape architects, and stone specialists average over 20 years of technical industry experience. These highly skilled and knowledgeable professionals are available as resources to help support you and your firm in achieving your goals. Going above and beyond. In order to provide the highest possible quality product, our buyers hand select approximately 85% of the plant material and stone products that are available in our sales yard.'] = '<b>Es la gente de Fiore, nuestros mayores activos, que nos ayuda a estar al margen</b>. Nuestro personal, altamente respetado de horticultores, arbolistas certificados, arquitectos paisajistas y especialistas en medios de piedra de más de 20 años de experiencia en el sector técnico. Estos profesionales altamente cualificados y conocedores están disponibles como recursos para ayudar a mantener a usted ya su empresa a alcanzar sus metas. Yendo más allá. Con el fin de proporcionar el producto de la mayor calidad posible, nuestra mano compradores seleccionar aproximadamente 85% del material vegetal y productos de piedra que están disponibles en el patio de ventas.';
translateSpanArr['Our team provides personal service from the first phone call through pickup or delivery, getting to know you and developing an understanding of your unique project requirements. Feel free to contact any of our staff with questions about materials or process. We look forward to hearing from you!'] = 'Nuestro equipo ofrece un servicio personalizado desde la primera llamada telefónica a través de recogida o entrega, conocer a usted y desarrollar una comprensión de los requisitos del proyecto únicas. No dude en ponerse en contacto con cualquiera de nuestro personal con preguntas acerca de los materiales o procesos. Esperamos con interés escuchar de usted!';
translateSpanArr['Careers at Fiore'] = 'Carreras en Fiore';
translateSpanArr['We are always on the lookout for great talent to add to our growing business. If you are interested in a career at Fiore, just send us an email at'] = 'Estamos siempre en la búsqueda de un gran talento para añadir a nuestra empresa en crecimiento. Si usted está interesado en una carrera en Fiore, envíenos un correo electrónico a';
translateSpanArr['and tell us why. In the meantime, check out the openings below to see if you might be a good fit.'] = 'y nos dicen por qué. Mientras tanto, echa un vistazo a las aberturas de abajo para ver si usted puede ser un buen ajuste.';

translateLinkArr['no current openings'] = 'no hay vacantes actuales';


//--------------- about_locations.php ---------------
translateSpanArr['Hours and Locations'] = 'Horas y Ubicaciones';
translateSpanArr['16606 West Highway 22, 4 miles<br>west of I-94, 1½ miles west of RT-21<br>and 2 miles east of RT-83.'] = '16606 West Highway 22, a 4 millas<br>oeste de la I-94, 1 ½ millas al oeste de la RT-21<br>y 2 millas al este de la RT-83.';
translateSpanArr['2901 West Ferdinand Street, 1<br>mile north of I-290 and 3<br>miles west of I-90. Just off<br>Sacramento Blvd., next to the<br>Center for Green Technology.'] = '2901 West Street Fernando, 1<br>millas al norte de la I-290 y 3<br>kilómetros al oeste de la I-90. Justo al lado<br>Sacramento Blvd., Junto a la<br>Centro para la Tecnología Verde.';
translateSpanArr['Hours 2012'] = 'Horas 2012';
translateSpanArr['<b>Fall</b><br>November 26 – December 21<br>Monday through Friday 8 – 4<br>Saturday/Sunday closed'] = '<b>Caer</b><br>Noviembre 26-diciembre 21<br>Lunes a viernes 8 a 4<br>Sábado / Domingo cerrado';
translateSpanArr['Closed for Holiday:<br>December 24 – January 1'] = 'Cerrado por vacaciones:<br>24 diciembre-1 enero';

translateSpanArr['Hours 2013'] = 'Horas 2013';
translateSpanArr['<b>Winter</b><br>January 2 – March 29<br>Monday through Friday 8 – 4<br>Saturday/Sunday closed<br>Loading by appointment only'] = '<b>Invierno</b><br>2 enero a 29 marzo<br>Lunes a viernes 8 a 4<br>Sábado / Domingo cerrado<br>Carga con cita previa';
translateSpanArr['<b>Spring/Summer</b><br>April 1 – July 3<br>Monday through Friday 7 – 5<br>Saturday Prairie View 7–1<br>Saturday Chicago 7–12<br>Sunday closed'] = '<b>Primavera / Verano</b><br>1 abril-3 julio<br>De lunes a viernes 7 a 5<br>Sábado Prairie View 1.7<br>Sábado Chicago 7-12<br>Domingo cerrado';
translateSpanArr['<b>Summer/Fall</b><br>July 5 – November 27<br>Monday through Friday 7 – 4<br>Saturday Prairie View 7–12<br>Saturday Chicago 7–12<br>Sunday closed'] = '<b>Verano / Otoño</b><br>5 julio-27 noviembre<br>De lunes a viernes 7 a 4<br>Sábado Prairie View 7.12<br>Sábado Chicago 7-12<br>Domingo cerrado';
translateSpanArr['<b>Winter</b><br>December 2 – December 23<br>Monday through Friday 7 – 4<br>Saturday/Sunday: closed<br>Loading by appointment only'] = '<b>Invierno</b><br>2 diciembre hasta 23 diciembre<br>De lunes a viernes 7 a 4<br>Sábado / domingo: cerrado<br>Carga con cita previa';
translateSpanArr['<b>Closed for Holidays:</b><br>May 27<br>July 4<br>November 28 – 30<br>December 24 – January 1'] = '<b>Cerrado por vacaciones:</b><br>27 de mayo<br>04 de julio<br>28 al 30 noviembre<br>24 diciembre-1 enero';

translateLinkArr['Driving Directions &gt;'] = 'Cómo llegar &gt;';


//--------------- services_sourcing.php ---------------
translateSpanArr['Sourcing'] = 'Abastecimiento';
translateSpanArr['<b>At Fiore we choose only the highest quality plant material and natural stone available in the market</b>. We do this by sourcing product coast to coast and buying from up to 250 nurseries and quarries a year. 85% of our specimen material is hand selected by our buyers who choose only the best available. Over the years we have come to understand what products are valued in the Chicagoland area, and we do our best to keep the those in stock. We understand, however, that your project may require more than what we currently have available in our yard. <b>Because of our extensive partner network, we are able to quickly replenish our inventories as well as obtain plant varieties and sizes and specialty stone pieces that may not be part of our normal inventory</b>. Sourcing hard to find specimens has become our specialty, so please do not hesitate to inquire about the rare or unusual. <br><b>We will do our best to track it down!</b>'] = '<b>En Fiore elegimos sólo la más alta calidad de material vegetal y piedra natural disponible en el mercado.</b> Esto lo hacemos a través del outsourcing costa a costa y producto de la compra de hasta 250 viveros y canteras al año. 85% de nuestro material de muestra es seleccionado a mano por nuestros compradores que eligen sólo los mejores disponibles. A través de los años hemos llegado a comprender qué productos se valoran en el área de Chicago, y hacemos todo lo posible para mantener el ésos en stock. Entendemos, sin embargo, que el proyecto puede requerir más de lo que actualmente tenemos disponible en nuestro patio.  <b>Debido a nuestra amplia red de socios, somos capaces de reponer rápidamente los inventarios, así como la obtención de variedades de plantas y tamaños y piezas especiales de piedra que no pueden formar parte de nuestro inventario normal.</b> Sourcing difícil encontrar ejemplares se ha convertido en nuestra especialidad, así que por favor no dude en preguntar acerca de la rara o poco común.<br><b>Haremos nuestro mejor esfuerzo para localizar a!</b>';
translateSpanArr['Purchasing Solutions Program'] = 'Compra Programa de Soluciones';
translateSpanArr['<b>Fiore offers an unique custom program designed to meet the purchasing needs of your firm</b>. Whether your business is large or small, this program is tailored to meet the specific needs of your operation relating to purchasing, product procurement, logistics, inventory management, and technical support. This program is designed to help optimize your company’s valuable resources. Outsource your purchasing to the purchasing professionals at Fiore and enjoy the following advantages:'] = '<b>Fiore ofrece un programa único diseñado para satisfacer las necesidades de compra de su empresa.</b> Tanto si su empresa es grande o pequeño, este programa está diseñado para satisfacer las necesidades específicas de su funcionamiento en relación a la compra, la adquisición de productos, logística, gestión de inventario y apoyo técnico. Este programa está diseñado para ayudar a optimizar los recursos valiosos de su empresa. Externalizar su compra a los profesionales de compras a Fiore y disfrute de las siguientes ventajas:';
translateSpanArr['more time to focus on core activities, i.e. design and selling'] = '';
translateSpanArr['realize cost and efficiency savings'] = 'más tiempo para concentrarse en sus actividades, es decir, el diseño y la venta';
translateSpanArr['reduce overhead.'] = 'reducir los gastos generales.';
translateSpanArr['These benefits will enhance your competitive advantage and lead to an increase in profits. To learn more details about this program or to find out of your firm qualifies please contact your sales associate or contact us at 847-913-1414,'] = 'Estos beneficios aumentarán su ventaja competitiva y conducir a un aumento de los beneficios. Para conocer más detalles sobre este programa o para saber de su empresa califica por favor póngase en contacto con su agente de ventas o póngase en contacto con nosotros en 847-913-1414,';



//--------------- services_delivery.php ---------------
translateSpanArr['Delivery'] = 'Entrega';
translateSpanArr['Fiore‘s convenient and cost saving delivery fleet includes large and small trucks, trailers, and enclosed trailers available daily to deliver material directly to your job site or yard. All trucks come with a lift gate to help in the unloading process. Please use the following charts to determine current rates.'] = 'Conveniente y económica de Fiore flota de reparto de ahorro incluye camiones grandes y pequeños, remolques y remolques cerrados disponibles todos los días para entregar el material directamente a su lugar de trabajo o en el patio. Todos los camiones que vienen con una puerta de ascensor para ayudar en el proceso de descarga. Por favor, utilice las siguientes tablas para determinar las tasas actuales.';
translateSpanArr['Rates from Prairie View Nursery Center'] = 'Tarifas desde Prairie View Vivero';
translateSpanArr['Rates from Chicago Nursery Center'] = 'Tarifas desde Chicago Vivero';

translateSpanArr['<b class="orangey">Did you know?</b> We can send additional labor and/or equipment (e.g. ball cart or tractor) to help you unload or move material at your job site. Please specify when you place your order. Additional fees may apply. <br><b>Forklift assistance</b> is available for stone or palletized orders for an additional $50.00. <br><b>On-site tractor service</b> (material placement) is $225 per hour; portal to portal.'] = '<b class="orangey">¿Sabías que?</b> Podemos enviar mano de obra adicional y / o equipo (carrito de pelota o tractor) para ayudarle a descargar o mover el material en su lugar de trabajo. Por favor, especifique al hacer su pedido. Cargos adicionales pueden aplicar.<br><b>Carretilla elevadora asistencia</b> está disponible para pedidos de piedra o paletizada para un adicional de $ 50.00.<br><b>Servicio in situ tractor</b> (colocación del material) es de $ 225 por hora, portal a portal.';
translateSpanArr['Delivery from Prairie View'] = 'Entrega de Prairie View';
translateSpanArr['Delivery from our Prairie View Nursery by a Fiore truck is available in Illinois with no minimum order required. A tag along trailer for larger deliveries is available for $50 extra. Delivery charges are based on mileage from our Prairie View Nursery to the delivery site. Please call for a quote for all out of state deliveries.'] = 'Entrega de nuestro Vivero Prairie View por un camión Fiore está disponible en Illinois, sin pedido mínimo requerido. Una etiqueta a lo largo de remolque para las grandes entregas está disponible por $ 50 extra. Los gastos de envío se basan en el kilometraje de nuestro Vivero Prairie View en el sitio de entrega. Por favor llame para una cita para todos los envíos de Estado.';
translateSpanArr['Delivery from Chicago'] = 'Entrega de Chicago';
translateSpanArr['Delivery from our Chicago Nursery by a Fiore truck is available in Illinois with no minimum order required. Delivery charges are based on mileage from our Chicago Nursery to the delivery site. The Chicago delivery truck is a one-ton truck with a 12-foot bed and a tailgate lift gate.'] = 'Entrega de nuestro cuarto de niños de Chicago por un camión Fiore está disponible en Illinois, sin pedido mínimo requerido. Los gastos de envío se basan en el kilometraje de nuestro cuarto de niños de Chicago para el sitio de entrega. El camión de reparto Chicago es un camión de una tonelada, con una cama de 12 pies y una puerta trasera ascensor.';
translateSpanArr['Delivery Policies'] = 'Políticas de Entrega';
translateSpanArr['Town'] = 'Ciudad';
translateSpanArr['Cost'] = 'Costo';
translateSpanArr['call for quote'] = 'llame para una cita';

translateLinkArr['See details'] = 'Ver detalles';

//--------------- services_delivery_policies.php ---------------
translateSpanArr['Delivery Policies'] = 'Políticas de Entrega';
translateSpanArr['Delivery charges are quoted for curbside drop off only. Delivery rates include services of truck and professional driver. Please request if additional labor or equipment is required. Additional charges may apply. If our truck is required to enter onto private property to unload plants, the customer will be responsible for any damage to the property. It is also the customer’s responsibility to provide adequate labor and equipment to unload the truck in a timely manner.'] = 'Los gastos de envío se cotizan en la acera para dejar fuera solamente. Tasas de ejecución incluyen los servicios de conductor de camión y profesional. Por favor, solicite si el trabajo o equipo adicional es requerido. Cargos adicionales pueden aplicar. Si el camión está obligado a entrar en la propiedad privada de descargar plantas, el cliente será responsable de cualquier daño a la propiedad. También es responsabilidad del cliente para proporcionar mano de obra y el equipo adecuados para descargar el camión en el momento oportuno.';
translateSpanArr['We will make every attempt to make deliveries as per customer’s request, however, all deliveries are subject to truck availability and prior schedules. All deliveries requiring a tractor are subject to truck and tractor availability and prior schedules. Fiore Nursery and Landscape Supply is not responsible for any damage done to property.'] = 'Haremos todo lo posible para hacer las entregas según indicaciones cliente, sin embargo, todas las entregas están sujetas a disponibilidad de camiones y horarios anteriores. Todas las entregas que requieren un tractor están sujetas a disponibilidad y camión tractor y los horarios anteriores. Nursery Landscape Supply Fiore y no es responsable de cualquier daño causado a la propiedad.';
translateSpanArr['<b>Delivery Cancellation Policy</b><br>All cancellations or changes to requested delivery date must be made at least 24 hours prior to scheduled time of delivery. A fee may be charged for orders cancelled or changed after a truck has been loaded. All deliveries depart as scheduled, rain or shine.'] = '<b>Entrega Condiciones de cancelación</b><br>Todas las cancelaciones o cambios de fecha de entrega solicitada debe hacerse por lo menos 24 horas antes de la hora programada para la entrega. Se puede cobrar un cargo por pedidos cancelados o cambiados después de un camión ha sido cargado. Todas las entregas salen como estaba previsto, la lluvia o con sol.';
translateSpanArr['<b>Pickups</b><br>No minimum order is necessary for material which is picked up at our yard. Same day pickup available'] = '<b>Pastillas</b><br>No hay pedido mínimo es necesario que el material que se recogió en nuestro patio. Pickup del mismo día disponible';

//--------------- services_other.php ---------------
translateSpanArr['Fabrication'] = 'Fabricación';
translateSpanArr['<b>High end stone fabrication is an art and craft</b>. We partner with local, highly skilled, and trusted stone fabricators who know that detail and quality are as important as timeliness and accuracy. We offer the following fabrication services:'] = '<b>Fabricación de alta piedra final es un arte y artesanía</b>. Nos asociamos con los locales, altamente calificados y fabricantes de piedra de confianza que conocen el detalle y la calidad son tan importantes como la puntualidad y la exactitud. Ofrecemos los servicios de fabricación siguientes:';
translateSpanArr['templating custom pieces for fabrication'] = 'plantillas personalizadas para la fabricación de piezas';
translateSpanArr['radius or straight cuts in bluestone, Indiana limestone, and quartzite materials'] = 'radio o cortes rectos en piedra azul, piedra caliza de Indiana y materiales de cuarcita';
translateSpanArr['edge finishing in thermal, rockface, and honed finishes with a bullnose or half bullnose edge'] = 'borde acabado en pared rocosa térmico, y acabados mates con un borde redondeado o medio bullnose';
translateSpanArr['thermal, polishing, brushing, and sandblasting of bluestone, limestone, and Eden product surfaces'] = 'térmicas, pulido, cepillado y pulido de piedra azul, piedra caliza, y Eden superficies del producto';
translateSpanArr['hole drilling in multiple diameters'] = 'agujero de perforación en múltiples diámetros';
translateSpanArr['sourcing of material that we do not stock for fabrication such as granite or various imports.'] = 'las fuentes de materias que no lo hacemos acciones para la fabricación como el granito o las importaciones distintas.';

translateSpanArr['Planting'] = 'Plantar';
translateSpanArr['For over 90 years Fiore has supported and partnered with Chicago’s landscape trade professionals'] = 'Por más de 90 años Fiore ha apoyado y colaborado con profesionales de Chicago del comercio paisaje';
translateSpanArr['We strongly recommend all homeowners or property owners, in need of a new landscape or a landscape renovation, to contact one of our industry trade associations for a local directory of accredited landscape architects, certified landscape contract professionals,  landscape design professionals, and stone masons as part of the selection process. It is also helpful to talk to neighbors and friends to see who they recommend. <br>Depending on the scope of your project, landscape design and construction can often require highly technical skills as well as highly creative talents. It is important that you find a professional who is qualified and whom you trust. The Chicagoland area is fortunate to have some of the most talented and skilled landscape professionals in the country. Please reference the following web sites to assist you in your selection process.'] = 'Recomendamos encarecidamente a todos los propietarios o dueños de propiedades, en la necesidad de un nuevo paisaje o una renovación del paisaje, ponerse en contacto con una de nuestras asociaciones comerciales de la industria para un directorio local de arquitectos paisajistas profesionales acreditados y certificados contrato paisaje, profesionales del diseño del paisaje y albañiles de piedra como parte del proceso de selección. También es útil hablar con vecinos y amigos para ver quién ellos recomiendan. MedlinePlus Dependiendo del alcance del proyecto, el diseño del paisaje y la construcción a menudo, requiere conocimientos muy técnicos, así como talentos creativos. Es importante que encuentre un profesional que está calificado y quién confiar. El área metropolitana de Chicago tiene la suerte de tener algunos de los paisajes más profesionales talentosos y calificados en el país. Por favor, consulte los siguientes sitios web para ayudarle en su proceso de selección.';
translateSpanArr['Although Fiore is primarily dedicated to serving the needs of our landscape professionals, the retail public is welcome to visit and shop from Fiore’s extensive product selection. For this reason, we do offer a planting service (<b>Fiore’s Home Gardener Planting Service</b>) to those customers who need assistance in planting the material they purchase from our nursery. We offer more extensive services to the landscape design professional (<b>Fiore’s Design Pro-Planting Service</b>) is in need of one or many of the following:'] = 'Aunque Fiore es principalmente dedicado a atender las necesidades de los profesionales de nuestro paisaje, el público al por menor son bienvenidos a visitar y comprar desde una amplia selección de productos de Fiore. Por esta razón, ofrecemos un servicio de siembra (<b>Gardener Fiore Inicio Servicio de plantación</b>) para aquellos clientes que necesitan asistencia en la siembra del material que compran a nuestro vivero. Ofrecemos servicios más amplios para el diseño del paisaje profesional (<b>Diseño Fiore Pro-Siembra Service</b>) está en la necesidad de uno o varios de los siguientes:';
translateSpanArr['landscape project management'] = 'panorama de la gestión del proyecto';
translateSpanArr['landscaping field labor'] = 'ajardinar trabajo de campo';
translateSpanArr['equipment rental and/or operation'] = 'alquiler de equipos y / o funcionamiento';
translateSpanArr['plant pruning'] = 'planta poda';
translateSpanArr['plant transplanting'] = 'planta trasplante';
translateSpanArr['specialty horticulture i.e. espalier construction'] = 'horticultura especialidad es decir espaldera construcción';
translateSpanArr['For questions regarding Fiore’s Home Gardener Planting Service or Fiore’s Design Pro-Planting Service (for the design professional), please contact'] = 'Si tienes preguntas sobre Gardener Fiore Inicio Servicio de plantación o Diseño de Fiore Pro-Plantación de servicio (para los profesionales del diseño), por favor póngase en contacto con';



//--------------- partners_detail.php ---------------
translateSpanArr['Located in Eden, Wisconsin, Eden Stone Company maintains year-round mining operations at eight quarry locations throughout Southeastern and Central Wisconsin. Through its relationship with Eden, Fiore is able to offer its customers the largest variety of landscape stone such as flagstone, outcropping, cut drywall, steps, natural stone pavers and architectural, landscape, full and thin building veneer, and dimensional cut stone in the region. With a wide array of colors and textures, Fiore is sure to have what you are looking for for your next landscape project.'] = 'Situado en el Edén, Wisconsin, Eden Stone Company mantiene durante todo el año las operaciones mineras en ocho localidades de canteras en todo el sureste de Wisconsin y Central. A través de su relación con Eden, Fiore es capaz de ofrecer a sus clientes la mayor variedad de paisaje de piedra como laja, afloramiento, drywall corte, pasos, adoquines de piedra natural y del paisaje, arquitectura, edificio completo y chapa fina y piedra corte dimensional en el región. Con una amplia gama de colores y texturas, Fiore está seguro de tener lo que usted está buscando para su proyecto de paisaje próximo.';

translateSpanArr['A subsidiary of Edens Stone Company, Valders Stone partners with Fiore to offer the highest quality limestone in the world, quarried in Valders, Wisonsin, and manufactured into architectural and dimensional cut stone. Fiore’s exclusive relationship with Valders’ staff of highly trained craftsman and artisans enables us to source and inventory the largest selection of Valders stone in the Chicagoland area. We are proud to represent and offer their superior product.'] = 'Una filial de Edens Stone Company, socios Valders Stone con Fiore para ofrecer la piedra caliza de alta calidad en el mundo, con canteras en Valders, Wisonsin, construidos en piedra corte arquitectónico y dimensiones. Fiore relación exclusiva con el personal de Valders\' artesano altamente capacitado y artesanos nos permite fuente y el inventario de la mayor selección de piedra Valders en el área de Chicago. Estamos orgullosos de representar y ofrecer su producto superior.';
translateSpanArr['Through our partnership with Midwest Trading Horticultural Supplies, Fiore is able to offer a wide range of hardgoods, such as premium hardwood and pine mulches, landscape, and rooftop growing media, CU Structural soil, and composts (mushroom, blended and organic). Midwest Trading has served the Green Industry in the Midwest for more than twenty years and takes great pride in the high quality products that it provides.'] = 'A través de nuestra asociación con Supplies Trading Midwest hortícolas, Fiore es capaz de ofrecer una amplia gama de hardgoods, tales como madera de pino de primera calidad y coberturas, el paisaje y los medios de comunicación en la azotea de cultivo, suelo estructural CU, y compost (setas, mezclado y orgánica). Trading Midwest ha servido a la industria ecológica en el Medio Oeste durante más de veinte años y se enorgullece de los productos de alta calidad que proporciona.';
translateSpanArr['Introducing GROW Modular. A furniture grade garden component system that incorporates modular design and construction, creating a turn key solution to contained environments. Sustainability is the central tenant of the design, focused on the triple bottom line of People, Planet, and Profit. <br><b>People</b> All components are proudly handmade in a controlled environment by local craftsmen, supporting our manufacturing base <br><b>Planet</b> Using sustainable materials and methods at every step to save energy and have minimal impact on the environment. <br><b>Profit</b> A competitive alternative solution to custom built exterior environments. Delivering value to those who have the highest level of commitment to quality and sustainable design. Great for urban and elevated environments!'] = 'Presentación de GROW Modular. Un grado muebles de jardín componente del sistema que incorpora un diseño modular y la construcción, la creación de una solución de llave en mano para entornos contenidas. La sostenibilidad es el inquilino central del diseño, se centró en la triple cuenta de personas, planeta y beneficios.<br><b>Gente</b> Todos los componentes son orgullosamente hechos a mano en un ambiente controlado por los artesanos locales, apoyando nuestra base de fabricación<br><b>Planeta</b> uso de materiales sostenibles y métodos en cada paso para ahorrar energía y un impacto mínimo en el medio ambiente.<br><b>Lucro</b> Una solución alternativa competitiva a la costumbre ambientes exteriores construidos. La entrega de valor a los que tienen el más alto nivel de compromiso con la calidad y el diseño sostenible. Ideal para entornos urbanos y elevado!';

translateSpanArr['Learn more about Grow Modular'] = 'Más información acerca de Grow Modular';





var translateOptionArr = new Array();
translateOptionArr['Common Name'] = 'Nombre común';
translateOptionArr['Botanic Name'] = 'Nombre Botánico';

var curLang = 'en';

function doTranslate() {
	// set lang
	if (curLang == 'en') {
		setCookie('lang', 'es', 30);
		curLang = 'es';
	} else {
		setCookie('lang', 'en', 30);
		curLang = 'en';
	}

	// do links
	var a = document.getElementsByTagName('a');
	for (var i=0;i<a.length;i++) {
		var idx = a[i].innerHTML;
		if (translateLinkArr[idx]) {
			a[i].innerHTML = translateLinkArr[idx];
			// add the english version back
			translateLinkArr[translateLinkArr[idx]] = idx;
		}
	}
	
	// do spans
	a = document.getElementsByTagName('span');
	for (var i=0;i<a.length;i++) {
		var idx = a[i].innerHTML;
		if (translateSpanArr[idx]) {
			a[i].innerHTML = translateSpanArr[idx];
			// add the english version back
			translateSpanArr[translateSpanArr[idx]] = idx;
		}
	}
	
	// do form selects
	a = document.getElementsByTagName('option');
	for (var i=0;i<a.length;i++) {
		var idx = a[i].innerHTML;
		if (translateOptionArr[idx]) {
			a[i].innerHTML = translateOptionArr[idx];
			// add the english version back
			translateOptionArr[translateOptionArr[idx]] = idx;
		}
	}
}


function setCookie(c_name,value,exdays) {
	var exdate=new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
	document.cookie=c_name + "=" + c_value;
}