<?php
require_once('functions.php');
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Charles J. Fiore Company, Inc- wholesale premium tree & shrub nursery and garden center near Chicago Illinois</title>

<?php extraHead(); ?>

<style type="text/css">
body {
	background: #4e4244;
}
#topMenu {
	background-image: URL('images/menuBG.png');
	background-repeat: repeat;
	*box-shadow: none;
}
#topBG {
	height: 120px;
}
#footerTable {
	top: 5px;
	border-top: none;
}
#contentBackground {
	height: 480px;
}

#contentTable td.small {
  font-size: 12px;
  line-height: 1.3;
  padding: 10px;
}

address {
    font-style: normal;
}

</style>
</head>
<body onload="nextIndexScroll();resizeIndexBG();" onresize="resizeIndexBG();">

<?php
/*
LEAVES OVERLAY
<table width="100%" height="500" cellspacing="0" cellpadding="0" border="0" style="position: absolute; top: 120px; left: 0px; z-index: 14;" id="indexLeafTable">
	<tr>
		<td valign="top" align="left"><img src="images/indexLeaf0.png" width="334" height="500" border="0"></td>
		<td valign="bottom" align="center"><img src="images/indexLeaf1.png" width="314" height="500" border="0"></td>
		<td valign="top" align="right"><img src="images/indexLeaf2.png" width="314" height="500" border="0"></td>
	</tr>
</table>
*/
?>

<?php makeHeader(); ?>


<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" style="padding: 0px;">
<img src="images/indexBox3.png" width="255" height="188" border="0" style="position: relative; top: -30px; left: 88px;" id="indexBox2">
		</td>
		<td valign="top" align="left" style="padding: 0px 0px 0px 240px;">
<!-- EMAIL SIGNUP -->
<div style="font: 28px fiore-bold, sans-serif; color: #ffffff; margin: 130px 0px 20px 0px; text-shadow: 1px 1px 3px #000000; margin-top: 20px; ">
    Our Availability List provides accurate pricing and stock availability updated every day.
</div>

<!-- <div id="emailSignup">Download Availability List&nbsp;&nbsp;<a id="downloadAvailabilityList"><img src="images/btnSubmit.png" width="75" height="22" border="0" alt="submit" style="position: relative; top: 6px;"></a></div> -->
<!-- END EMAIL SIGNUP -->

<br><br>

<!-- NEWS -->
<div style="position: absolute; bottom: 80px; left: 0px; width: 100%;">
<div id="newsHolder">
<table width="900" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td valign="top" align="left" style="border-right: dotted #4e4244 1px; padding-bottom: 10px;">
			<div class="newsItem" style="">
        <!-- <b>Fiore Bolingbrook is Open!</b> -->
        We provide the greater Chicago metropolitan area and Indianapolis region with the largest selection of premium-grade plant material, natural stone, and landscape supply products. All available from four convenient locations!
        <br />
        <br />
            <table width="100%">
                <tr>
                    <td>
                        <b>Fiore Prairie View</b>
                        <address>
                            16606 West Highway 22<br>
                            Prairie View, IL 60069
                        </address>
                    </td>
                    <td>
                        <b>Fiore Chicago</b>
                        <address>
                            2901 West Ferdinand Street <br>
                            Chicago, IL 60612
                        </address>
                    </td>
                </tr>
                <tr>
                    <td>
                        <b>Fiore Bolingbrook</b>
                        <address>
                            801 North Bolingbrook Drive <br>
                            Bolingbrook, IL 60440
                        </address>
                    </td>
                    <td>
                        <b>Fiore Indianapolis</b>
                        <address>
                            11460 Greenfield Avenue <br>
                            Noblesville, IN 46060
                        </address>
                    </td>
                </tr>
            </table>

			</div>
		</td>
		<td valign="top" align="left">
			<div class="newsItem" style="border-bottom: none; width: 200px;">
				<div class="blueLinks">

                    <span style="font-weight: bold; font-size: 1.1em;">
                        Aerial Tour of Our Nursery
                    </span>
                    <br><br>

					<a href="javascript:;" onclick="showVirtualTourVid();">
                        <div style="background-image: url('images/virtualNurseryVid.jpg'); background-size: cover; background-position: center center; background-repeat: no-repeat; width: 250px; height: 125px; display: block; margin-bottom: 10px; cursor: pointer;"></div>
					    Virtual Nursery Tour &gt;
                    </a>
				</div>
			</div>
		</td>
        <!-- <td valign="top" align="left">
            <div class="newsItem" style="border-bottom: none; width: 120px; font-size: 0.7em;">
                <b>Spring/Summer/Fall Hours:</b>
                <br>April to Thanksgiving
                <br><u>Monday thru Friday</u>
                <br>7:00AM – 4:00PM
                <br />
                <br />
                <b>Saturday Hours:</b>
                <br>Mid-April thru June
                <br><u>Saturday</u>
                <br>7:00AM – 12 Noon
                <br>
                <b>Winter Hours:</b>
                <br>Thanksgiving thru March
                <br><u>Monday thru Friday</u>
                <br>8:00AM - 4:00PM
                <br>
            </div>
        </td> -->
	</tr>
</table>
</div>
</div>
<!-- END NEWS -->
		</td>
	</tr>
</table>

</div><!-- close content -->

<?php makeFooter(); ?>


<!-- footer textrured strip -->
<div style="position: absolute; top: 620px; width: 100%; height: 100px; background: #ddd278 url('images/topBG.gif') repeat;">
&nbsp;
</div>
<!-- end footer textrured strip -->


<!-- index footer -->
<div style="position: absolute; top: 740px; left: 0px; width: 100%; height: 23px; z-index: 50;">
	<div class="footerIndex">
		<img src="images/socialsIndex2.gif" width="97" height="23" border="0" usemap="#socials" style="vertical-align: middle; margin-right: 15px; position: relative; top: -3px;">
			PRAIRIE VIEW: 847.913.1414 · CHICAGO: 773.533.1414 · BOLINGBROOK: 630.739.1414 · INDIANAPOLIS: 317.774.5266 · <a href="contact.php">Contact Us</a> · ©<?php echo date('Y'); ?>
	</div>
</div>
<!-- end index footer -->

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-39264121-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>



<style>
#virtualTourCover {
	display: none;
	position: fixed;
	top: 0px;
	left: 0px;
	z-index: 100;
	width: 100%;
	min-width: 500px;
	height: 100%;

	background-color: #000000;
	background-color: rgba(0, 0, 0, 0.8);
}
#virtualTourTableCell {
	display: table-cell;
	vertical-align: middle;
	text-align: center;
	padding: 40px;
}
#virtualTourVidHolder {
	display: inline-block;
	position: relative;
	border: solid #ffffff 20px;
	border-radius: 10px;
}
#virtualTourVid {
	position: relative;
	margin: 0px auto;
	max-width: 100%;

	background-color: #ffffff;
	box-shadow: 3px 3px 10px rgba(0, 0, 0, 0.5);
}
#virtualTourVid:before {
	content: "";
	display: block;
	width: 100%;
	padding-bottom: 56.25%; /* 720:1280 aspect ratio */
}

#virtualTourClose {
	position: absolute;
	top: -60px;
	right: -30px;
	width: 40px;
	height: 40px;

	cursor: pointer;
}
#virtualTourClose svg {
	stroke: #ffffff;
	fill: transparent;
	stroke-linecap: round;
	stroke-width: 2;
}
#virtualTourClose:hover svg {
	stroke: #db781b;
}


</style>


<script language="javascript">
function showVirtualTourVid() {
	document.getElementById('virtualTourCover').style.display = 'table';
	document.getElementById('virtualTourVid').play();
}
function hideVirtualTourVid() {
	document.getElementById('virtualTourVid').pause();
	document.getElementById('virtualTourCover').style.display = 'none';
}
function clickVirtualTourCover(e) {
	e = e || window.event;
	var el = e.srcElement || e.target;
	if (el.id == 'virtualTourTableCell') {
		hideVirtualTourVid();
	}
}
</script>

<div id="virtualTourCover"><div id="virtualTourTableCell" onclick="clickVirtualTourCover(event);">
	<div id="virtualTourVidHolder">
		<video controls id="virtualTourVid">
			<source src="//www.new-sites.net/cjfiore/vid/virtual_nursery_tour.mp4" type="video/mp4">
			<source src="vid/virtual_nursery_tour.webm" type="video/webm">
			Your browser does not support the video tag. Time to UPGRADE, SON!
		</video>
		<div onclick="hideVirtualTourVid();" id="virtualTourClose">
			<svg viewbox="0 0 40 40">
				<path class="close-x" d="M 10,10 L 30,30 M 30,10 L 10,30"></svg>
		</div>
	</div>
</div></div><!-- close cover & table-cell -->

<script>
function myFunction() {
    alert("Please use the Find us on Landscape Hub Button.");
}
</script>


</body>
</html>
