<?php
require_once('functions.php');

$isFilledOut = false;

// temp fix while php is NOT up
if (isset($_GET['q0']) && !isset($_POST['q0'])) {
	$_POST['q0'] = $_GET['q0'];
}

// send out email
if (isset($_POST['q0'])) {
	// use validation if possible
	if (function_exists('filter_var')) {
		if (!filter_var($_POST['q0'], FILTER_VALIDATE_EMAIL))
			$error = 'Please enter a valid email address.';
	} else if (strpos($_POST['q0'], '@') !== false || strpos($_POST['q0'], '.') !== false) {
		// simpler validation
		$error = 'Please enter a valid email address.';
	}
	
	/* email the email address, I guess... */
	if (!isset($error)) {
		$tmpArr = array('Email' => trim($_POST['q0']));
		/* email it! */
		emailAssociativeArrayWithTitleTo($tmpArr, 'Email Signup Request', 'matt@mattcourtright.com', 'Web Admin <noreply@cjfiore.com>');
		emailAssociativeArrayWithTitleTo($tmpArr, 'Email Signup Request', 'meighan@depkedesign.com', 'Web Admin <noreply@cjfiore.com>');
		emailAssociativeArrayWithTitleTo($tmpArr, 'Email Signup Request', 'Saleschicago@cjfiore.com', 'Web Admin <noreply@cjfiore.com>');
		$isFilledOut = true;
	}
}
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Sign Up for Fiore Emails - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraHead(); ?>
</head>
<body<?php if (!$isFilledOut) { echo ' onload="document.getElementById(\'q0\').focus();"'; } ?>>
<?php makeHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" style="padding: 0px 10px 0px 20px; border-right: solid #4e4244 1px;">
<h4>Sign Up for Fiore Emails</h4>
<br>
<?php
if (!$isFilledOut) {
?>
<form method="POST" action="email_signup.php">
<?php
if (isset($error)) {
 echo '<p style="color: #ff0000;">'.$error.'</p>';
}
?>

<p>Enter your email address:
<br><input type="text" name="q0" id="q0" style="width: 180px;">
<br>
<br><input type="image" src="images/footerSignup.png" width="68" height="20" border="0">
</form>
<?php
} else {
 	echo '<p>Thank you for signing up!</p>';
}
?>
		</td>
		<td valign="top" align="left" style="padding: 0px 20px 0px 20px;">
<h4>Fiore E-News Archive</h4>
<p>Stay up-to-date with the latest events and news
at Fiore by singing up for our E-newsletter. And if
you’ve missed something you can always find it in
our archive.
<br>
<br><b class="brownLinks" style="font-size: 13px;"><a href="http://archive.constantcontact.com/fs020/1101928436273/archive/1102200719057.html" target="_blank">Archived Fiore Newsletters &gt;</a></b>
</p>
		</td>
		<td></td>
	</tr>
	<tr>
		<td><img src="images/spacer.gif" width="225" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="305" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="390" height="1" border="0"></td>
	</tr>

</table>

<?php makeFooter(); ?>

</body>
</html>
