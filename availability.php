<?php
require_once('functions.php');

if (isset($_SESSION['user'])) {
	if (isWholesale()) {
		// output it as a file download
		$filename = '_avail/_fiorenurseryavailability2.xlsx';
		$c = file_get_contents($filename);
		header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="Fiore Nursery Availability ' . date('Ymd', filemtime($filename)) . '.xlsx"');
		echo $c;
		die();
	}
}

header('Location: http://www.cjfiore.com/');
exit();