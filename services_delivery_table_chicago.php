<table width="372" border="0" cellpadding="0" cellspacing="0" id="dlverytxt2">
	<tr>
		<td width="120" align="left" class="headtown"><b><span>Miles</span></b></td>
		<td width="111" align="left" class="headcosts"><b><span>Cost</span></b></td>
		<td width="139" class="chitdfarright" style="padding: 5px 5px 2px 15px; font-size:14px; font-weight:bold;">&nbsp;</td>
	</tr>
	<tr>
		<td bgcolor="#F3F3F3" class="chitdleft">1-2 &nbsp;miles</td>
		<td bgcolor="#F3F3F3" class="chitdright">70</td>
		<td bgcolor="#F3F3F3" class="chitdfarright">&nbsp;</td>
	</tr>
	<tr>
		<td class="chitdleft">3-4 &nbsp;miles</td>
		<td class="chitdright">85</td>
		<td class="chitdfarright">&nbsp;</td>
	</tr>
	<tr>
		<td bgcolor="#F3F3F3" class="chitdleft">5-7 &nbsp;miles</td>
		<td bgcolor="#F3F3F3" class="chitdright">110</td>
		<td bgcolor="#F3F3F3" class="chitdfarright">&nbsp;</td>
	</tr>
	<tr>
		<td class="chitdleft">8-9 &nbsp;miles</td>
		<td class="chitdright">125</td>
		<td class="chitdfarright">&nbsp;</td>
	</tr>
	<tr>
		<td bgcolor="#F3F3F3" class="chitdleft">10-12&nbsp; miles</td>
		<td bgcolor="#F3F3F3" class="chitdright">140</td>
		<td bgcolor="#F3F3F3" class="chitdfarright">&nbsp;</td>
	</tr>
	<tr>
		<td class="chitdleft">13-16&nbsp; miles</td>
		<td class="chitdright">155</td>
		<td class="chitdfarright">&nbsp;</td>
	</tr>
	<tr>
		<td bgcolor="#F3F3F3" class="chitdleft">17 + &nbsp;miles</td>
		<td bgcolor="#F3F3F3" class="chitdright"><span>call for quote</span></td>
		<td bgcolor="#F3F3F3" class="chitdfarright">&nbsp;</td>
	</tr>
	<tr>
		<td height="11" colspan="3" class="chitdbottom"><img src="img/shim.gif" width="1" height="1"></td>
		</tr>
</table>