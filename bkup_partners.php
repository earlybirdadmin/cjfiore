<?php
require_once('functions.php');
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Hardscape Partners - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraHead(); ?>
</head>
<body>
<?php makeHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" style="padding: 0px 10px 0px 20px;">

<!-- PARTNERS TABLE -->
<table width="10" cellspacing="0" cellpadding="0" border="0" style="margin-bottom: 6px;">
	<tr>
		<td valign="top" align="left">
			<div class="servicesOverviewBlock roundLeft10">
			<img src="images/partners0.jpg" width="225" height="160" border="0" class="roundLeft10" style="margin: 0px;">
			</div>
		</td>
		<td><img src="images/spacer.gif" width="5" height="1" border="0"></td>
		<td valign="top" align="left">
			<div class="servicesOverviewBlock">
			<img src="images/partners1.jpg" width="225" height="160" border="0" style="margin: 0px;">
			</div>
		</td>
		<td><img src="images/spacer.gif" width="5" height="1" border="0"></td>
		<td valign="top" align="left">
			<div class="servicesOverviewBlock">
			<img src="images/partners2.jpg" width="225" height="160" border="0" style="margin: 0px;">
			</div>
		</td>
		<td><img src="images/spacer.gif" width="5" height="1" border="0"></td>
		<td valign="top" align="left">
			<div class="servicesOverviewBlock roundRight10">
			<img src="images/partners3.jpg" width="225" height="160" border="0" class="roundRight10" style="margin: 0px;">
			</div>
		</td>
	</tr>
	<tr>
		<td><img src="images/spacer.gif" width="180" height="20" border="0"></td>
		<td><img src="images/spacer.gif" width="5" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="180" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="5" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="180" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="5" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="180" height="1" border="0"></td>
	</tr>
	<tr>
		<td valign="top" align="left" colspan="7">
			<h1><span>Fiore Hardscape Partners</span></h1>
			<p class="firstLevel" style="margin: 10px 30px 0px 0px;"><span>Working with Fiore allows you access to the Fiore Network, our partner network with over 250 suppliers from across the country and around the globe. Our network allows us to supply you with unlimited, quality horticultural, stone, and landscape products and supplies. Our largest partners, Eden Stone Company, Valders Stone, and Midwest Trading, stock a full range of products on-site at both of our locations, eliminating wait time for the essentials you need.</span></p>
		</td>
<!--
		<td></td>
		<td valign="top" align="left">
			<h1>&nbsp;</h1>
			<div class="bigContentMenu">
			<a href="partners_detail.php#eden" style="border-top: none;">Eden Stone Company &gt;</a>
			<a href="partners_detail.php#valders">Valders Stone &gt;</a>
			<a href="partners_detail.php#midwest">Midwest Trading &gt;</a>
			<a href="partners_detail.php#pavers">Belgard Hardscapes &amp;<br>Glen-Gery Clay Pavers &gt;</a>
			<a href="partners_detail.php#kellygreen">Kellygreen Design, Inc. &gt;</a>
			</div>
		</td>
		<td valign="top" align="left" colspan="2">
			<div class="leafItOnTop" style="top: -70px; left: -20px;"><img src="images/treeGreen.png" width="205" height="499" border="0"></div>
		</td>
-->
	</tr>
</table>
<!-- END PARTNERS TABLE -->


		</td>
	</tr>
	<tr>
		<td valign="top" align="left" style="padding: 20px 10px 80px 20px; font-size: 16px; line-height: 1.8">
            <a name="techo"></a>
            <p>
                <a href="https://www.techo-bloc.com" target="_blank"><img src="images/partners-detail-techo.png" width="120" height="70" border="0" alt="Techo-Bloc" class="partner-image" ></a>
                <span class="h4">Techo - Bloc </span>
         			<a href="https://www.techo-bloc.com/" target="_blank">Site</a>
            </p>
            <div style="width: 100%; height: 1px; border-bottom: solid #4e4244 1px; clear: both; margin: 20px 0px;"></div>

            <a name="eden"></a>
            <p>
                <a href="http://www.edenstone.net/" target="_blank"><img src="images/partners_detail1.gif" width="120" height="70" border="0" alt="Techo-Bloc" class="partner-image" ></a>
                <span class="h4">Eden Stone Company</span>
         			<a href="http://www.edenstone.net/" target="_blank">Site</a>
            </p>
            <div style="width: 100%; height: 1px; border-bottom: solid #4e4244 1px; clear: both; margin: 20px 0px;"></div>


            <!-- <h4>Eden Stone Company</h4>
            <br><img src="images/partners_detail0.jpg" width="315" height="206" border="0" style="float: right; margin: 0px 64px 0px 32px;">
            <span>Located in Eden, Wisconsin, Eden Stone Company maintains year-round mining operations at eight quarry locations throughout Southeastern and Central Wisconsin. Through its relationship with Eden, Fiore is able to offer its customers the largest variety of landscape stone such as flagstone, outcropping, cut drywall, steps, natural stone pavers and architectural, landscape, full and thin building veneer, and dimensional cut stone in the region. With a wide array of colors and textures, Fiore is sure to have what you are looking for your next landscape project.</span>
            <br><a href="http://www.edenstone.net" target="_blank"><img src="images/partners_detail0.gif" width="120" height="70" border="0" alt="Eden Stone Company" style="margin-top: 4px;"></a>
            <div style="width: 100%; height: 1px; border-bottom: solid #4e4244 1px; clear: both; margin: 20px 0px;"></div>

            <img src="images/partners_detail1.jpg" width="330" height="205" border="0" style="float: left; margin: 0px 15px 0px 0px;">
            <a name="valders"></a>
            <h4>Valders Stone</h4>
            <br><span>A subsidiary of Eden Stone Company, Valders Stone partners with Fiore to offer the highest quality limestone in the world, quarried in Valders, Wisconsin, and manufactured into architectural and dimensional cut stone. Fiore’s exclusive relationship with Valders’ staff of highly trained craftsman and artisans enables us to source and inventory the largest selection of Valders stone in the Chicagoland area. We are proud to represent and offer their superior product.</span>
            <br><a href="http://www.valdersstone.com" target="_blank"><img src="images/partners_detail1.gif" width="115" height="75" border="0" alt="Valders Stone & Marble, Inc." style="margin-top: 4px;"></a>
            <div style="width: 100%; height: 1px; border-bottom: solid #4e4244 1px; clear: both; margin: 20px 0px;"></div> -->

            <a name="rochester"></a>
            <p>
                <a href="http://rochestercp.com" target="_blank"><img src="images/rochester-concrete-products.svg" width="120" height="70" border="0" alt="Techo-Bloc" class="partner-image" ></a>
                <span class="h4">Rochester Concrete Products</span>
         			<a href="http://rochestercp.com" target="_blank">Site</a>
            </p>
            <div style="width: 100%; height: 1px; border-bottom: solid #4e4244 1px; clear: both; margin: 20px 0px;"></div>

            <!-- <h4>Rochester Concrete Products</h4>
 			<br><a href="http://rochestercp.com/" target="_blank">Site</a>
            <div style="width: 100%; height: 1px; border-bottom: solid #4e4244 1px; clear: both; margin: 20px 0px;"></div>
            -->

            <a name="mirage"></a>
            <p>
                <a href="https://mirageusa.net" target="_blank"><img src="images/mirage-usa.png" width="120" height="70" border="0" alt="Techo-Bloc" class="partner-image" ></a>
                <span class="h4">Mirage/Evo</span>
         			<a href="https://mirageusa.net" target="_blank">Site</a>
            </p>
            <div style="width: 100%; height: 1px; border-bottom: solid #4e4244 1px; clear: both; margin: 20px 0px;"></div>

            <!-- <h4>Mirage/Evo</h4>
 			<br><a href="https://mirageusa.net/" target="_blank">Site</a>
            <div style="width: 100%; height: 1px; border-bottom: solid #4e4244 1px; clear: both; margin: 20px 0px;"></div> -->

            <a name="midwest"></a>
            <p>
                <a href="http://www.midwest-trading.com" target="_blank"><img src="images/partners_detail2.gif" width="120" height="70" border="0" alt="Techo-Bloc" class="partner-image" ></a>
                <span class="h4">Midwest Trading</span>
         			<a href="http://www.midwest-trading.com" target="_blank">Site</a>
            </p>
            <div style="width: 100%; height: 1px; border-bottom: solid #4e4244 1px; clear: both; margin: 20px 0px;"></div>


            <!-- <h4>Midwest Trading</h4> -->

            <!-- <br><span>Through our partnership with Midwest Trading Horticultural Supplies, Fiore is able to offer a wide range of hardgoods, such as premium hardwood and pine mulches, landscape, and rooftop growing media, CU Structural soil, and composts (mushroom, blended and organic). Midwest Trading has served the Green Industry in the Midwest for more than twenty years and takes great pride in the high quality products that it provides.</span>
            <br><a href="http://www.midwest-trading.com" target="_blank"><img src="images/partners_detail2.gif" width="110" height="75" border="0" alt="Midwest Trading Hortiultural Supplies" style="margin-top: 4px;"></a> -->

            <!-- <div style="width: 100%; height: 1px; border-bottom: solid #4e4244 1px; clear: both; margin: 20px 0px;"></div> -->
            <!-- <img src="images/partners_detail4.jpg" width="332" height="202" border="0" style="float: right; margin: 0px 64px 20px 32px;"> -->
            <a name="pavers"></a>

            <p>
                <a href="https://www.glengery.com" target="_blank"><img src="images/partners_detail4.jpg" width="120" height="70" border="0" alt="Techo-Bloc" class="partner-image" ></a>
                <span class="h4">Glen-Gery Clay Pavers</span>
         			<a href="https://www.glengery.com" target="_blank">Site</a>
            </p>
            <div style="width: 100%; height: 1px; border-bottom: solid #4e4244 1px; clear: both; margin: 20px 0px;"></div>

            <!--
            <h4>Glen-Gery Clay Pavers</h4>
            <br>
            <span> Founded in 1890, Glen-Gery Corporation is the largest molded brick and fifth largest brick manufacturer in the U.S. Glen-Gery holds the distinction of an honored and trusted name in brickmaking throughout the building industry. Its diverse product line of over 300 products includes extruded, machine molded and glazed face brick, brick pavers, and a complete assortment of brick shapes and custom shape units. </span>
            <br><a href="https://www.glengery.com/" target="_blank">Site</a>


            <div style="width: 100%; height: 1px; border-bottom: solid #4e4244 1px; clear: both; margin: 20px 0px;"></div>
            <img src="images/partners_detail4.jpg" width="332" height="202" border="0" style="float: left; margin: 0px 64px 20px 32px;"> -->
            <a name="pavers"></a>
            <p>
                <a href="http://www.belgard.com/" target="_blank"><img src="images/partners_detail4.gif" width="120" height="70" border="0" alt="Techo-Bloc" class="partner-image" ></a>
                <span class="h4">Belgard Hardscapes</span>
         			<a href="http://www.belgard.com/" target="_blank">Site</a>
            </p>
            <div style="width: 100%; height: 1px; border-bottom: solid #4e4244 1px; clear: both; margin: 20px 0px;"></div>
            <!-- <h4>Belgard Hardscapes</h4>
            <br><span>One of Fiore‘s newest partners, Belgard Hardscapes has been in business since 1995, manufacturing interlocking pavers, paving stone and garden wall products. These products include the traditional and antiqued paver and wall series. All Belgard products have a long history of successful applications on thousands of residential and commercial projects throughout the United States and Canada.
            </span>
            <br><a href="http://www.belgard.com/" target="_blank"><img src="images/partners_detail4.gif" width="135" height="52" border="0" alt="BELGARD" style="margin-top: 14px;"></a>
 -->

            <div style="clear: both; height: 1px;"></div>
            <div class="leafItOnTop" style="top: -26px; left: 540px;"><img src="images/leafGreen2.png" width="164" height="168" border="0"></div>

		</td>
	</tr>

</table>

<?php makeFooter(); ?>

</body>
</html>
