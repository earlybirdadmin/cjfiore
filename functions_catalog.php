<?php 
require_once('functions.php');

define('LONG_LIST', 0);
define('SPECIAL_PAGE', 1);
define('LARGE_IMAGES', 2);
define('CATEGORY_TYPE_PLANT', 3);


// get the sort index
if (isset($_COOKIE['primary_name_type']) && $_COOKIE['primary_name_type'] == 'botanical') {
	define('PRIMARY_NAME_TYPE', 'botanical_name');
	define('SECONDARY_NAME_TYPE', 'common_name');
} else {
	define('PRIMARY_NAME_TYPE', 'common_name');
	define('SECONDARY_NAME_TYPE', 'botanical_name');
}

// set the "last" page viewed in the catalog
if (PAGE != 'cart.php' && PAGE != 'addToCart.php' && PAGE != 'checkout.php' && PAGE != 'confirm.php')
	$_SESSION['lastPage'] = $_SERVER['REQUEST_URI'];



// get breadcrumbs array
$GLOBALS['breadcrumbsArr'] = getBreadcrumbsArr();

// set superparent
$GLOBALS['superparent'] = 1;


function extraCatalogHead() {
	extraHead();
?>
<link rel=stylesheet type="text/css" href="fiore_catalog.css">

<?php
}


function makeCatalogHeader($showBreadCrumbs = true) {
	makeHeader();
?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" style="padding: 0px 10px 0px 20px; border-right: solid #d2ced0 1px;">
<!-- LH MENU -->
<div class="catalogLHMenu">

<?php
$menuItems = getCategoriesWithParentID(0);
for ($i=0;$i<count($menuItems);$i++) {
	$urlArr = array('Plants' => 'products_plants.php',
					'Landscape Supply' => 'products_landscape_supply.php',
					'Natural Stone' => 'products_natural_stone.php',
					'Green Roof' => 'products_green_roof.php',
					'Planters and Accessories' => 'products_planters_accessories.php',
					'Specials' => 'products_specials.php',
					'Concrete and Clay Pavers' => 'products_concrete_clay_pavers.php');
	$menuIdx = $menuItems[$i]['category_name'];

	if ($GLOBALS['breadcrumbsArr']['category']['txt'] == $menuItems[$i]['category_name']) {
		// menu IS active
		echo '<a href="'.$urlArr[$menuIdx].'" style="color: #da771b;';
		
		// if there ARE submenus, there is no bottom border
		if (count($menuItems[$i]['subCatArr']) > 0)
			echo ' border-bottom: none;';
			
		// close the link
		echo '">'.$menuItems[$i]['category_name'].'</a>';
		
		// lh submenus
		if (count($menuItems[$i]['subCatArr']) > 0) {
			echo '<div class="catalogLHSubMenu" id="lhSubmenu'.$i.'">';
			for ($j=0;$j<count($menuItems[$i]['subCatArr']);$j++) {
				$curItem = $menuItems[$i]['subCatArr'][$j];
				// open link
				echo '<a href="browse.php?c='.$curItem['category_id'].'&category='.convertToGetVar($curItem['category_name']).'"';
				// make it orange if it's the active one
				if (isset($GLOBALS['breadcrumbsArr']['subcategory']['txt']) && $GLOBALS['breadcrumbsArr']['subcategory']['txt'] == $curItem['category_name'])
					echo ' style="color: #da771b;"';
				echo '>';
				// category name
				echo $curItem['category_name'];
				// item count
				//echo '&nbsp;('.$curItem['item_count'].')';
				// close link
				echo '</a>';
			} // end for $j
			
			/* ADD FEATURED IF WE'RE ON PLANTS */
			if ($menuItems[$i]['category_id'] == 1) {
				echo '<a href="plants_featured.php"';
				if (PAGE == 'plants_featured.php')
					echo ' style="color: #da771b;"';
				echo '>Featured</a>';
			}
			
			echo '</div>';
		} // end if there IS a submenu
	} else {
		// menus is NOT active
		echo '<a href="'.$urlArr[$menuIdx].'">'.$menuItems[$i]['category_name'].'</a>';
	}
}

?>
<!-- END LH MENU -->
		</td>
		<td valign="top" align="left" style="padding: 0px 10px 0px 20px;">
<?php
	if ($showBreadCrumbs) {
		showBreadCrumbs();
	}
}




function showBreadCrumbs() {
	echo '<div class="breadCrumbs">';
	showCommonBotanicalRadios();
	
	
	// start with category (ASSUMES WE ALWAYS HAVE A CATEGORY DEFINED)
	echo '<a href="'.$GLOBALS['breadcrumbsArr']['category']['url'].'"';
	if (isset($GLOBALS['breadcrumbsArr']['lastItem']) && $GLOBALS['breadcrumbsArr']['lastItem'] == 'category')
		echo ' style="color: #da771b;"';
	echo '>'.$GLOBALS['breadcrumbsArr']['category']['txt'].'</a>';
	
	// next we do a subcategory, if there is one
	if (isset($GLOBALS['breadcrumbsArr']['subcategory'])) {
		echo ' / <a href="'.$GLOBALS['breadcrumbsArr']['subcategory']['url'].'"';
		if ($GLOBALS['breadcrumbsArr']['lastItem'] == 'subcategory')
			echo ' style="color: #da771b;"';
		echo '>'.$GLOBALS['breadcrumbsArr']['subcategory']['txt'].'</a>';
	}
	
	// next we do a genus, if there is one
	if (isset($GLOBALS['breadcrumbsArr']['genus'])) {
		echo ' / <a href="'.$GLOBALS['breadcrumbsArr']['genus']['url'].'"';
		if ($GLOBALS['breadcrumbsArr']['lastItem'] == 'genus')
			echo ' style="color: #da771b;"';
		echo '>'.$GLOBALS['breadcrumbsArr']['genus']['txt'].'</a>';
	}
	
	// next we do a item, if there is one
	if (isset($GLOBALS['breadcrumbsArr']['item'])) {
		echo ' / <a href="'.$GLOBALS['breadcrumbsArr']['item']['url'].'"';
		if ($GLOBALS['breadcrumbsArr']['lastItem'] == 'subcategory')
			echo ' style="color: #da771b;"';
		echo '>'.$GLOBALS['breadcrumbsArr']['item']['txt'].'</a>';
	}
	

	echo '</div>';
}

function showCommonBotanicalRadios() {
	echo '<div class="commonBotanicalRadios">';
	if (PRIMARY_NAME_TYPE == 'botanical_name') {
		echo '<a href="javascript:;"><img src="images/spacer.gif" width="15" height="15" border="0" style="background: url(\'images/commonBotanicalRadios.gif\') 0px -15px no-repeat;"></a>&nbsp;View By Botanical Name';
		echo '&nbsp;&nbsp;';
		echo '<a href="javascript:;" onclick="setBotanicalView(false);"><img src="images/spacer.gif" width="15" height="15" border="0" style="background: url(\'images/commonBotanicalRadios.gif\') 0px 0px no-repeat;"></a>&nbsp;View By Common Name';
	} else {
		echo '<a href="javascript:;" onclick="setBotanicalView(true);"><img src="images/spacer.gif" width="15" height="15" border="0" style="background: url(\'images/commonBotanicalRadios.gif\') 0px 0px no-repeat;"></a>&nbsp;View By Botanical Name';
		echo '&nbsp;&nbsp;';
		echo '<a href="javascript:;""><img src="images/spacer.gif" width="15" height="15" border="0" style="background: url(\'images/commonBotanicalRadios.gif\') 0px -15px no-repeat;"></a>&nbsp;View By 
		Common Name';
	}
	echo '</div>';
}



function makeCatalogFooter() {
?>
		</td>
	</tr>
	<tr>
		<td><img src="images/spacer.gif" width="150" height="1" border="0" style="border-right: solid #808080 1px;"></td>
		<td><img src="images/spacer.gif" width="801" height="1" border="0"></td>
	</tr>
</table>
<!-- end content table -->

<?php 
	makeFooter();
}






function getCategoriesWithParentID($id) {
	// init $out
	$outArr = array();
	
	// validate $id
	if (!is_numeric($id))
		return $outArr;
	
	// get all items with the parent_id (maybe later add is_active to the table?)
	$result = mysql_query('SELECT * FROM `Categories` WHERE parent_id='.$id.' ORDER BY sort_order ASC');
	for ($i=0;$i<mysql_num_rows($result);$i++) {
		// get base array
		$row = mysql_fetch_array($result);
		
		// add subcategories array
		$row['subCatArr'] = getCategoriesWithParentID($row['category_id']);
		
		// add subcategories item count
		for ($j=0;$j<count($row['subCatArr']);$j++) {
			$row['item_count'] += $row['subCatArr'][$j]['item_count'];
		}
		
		// finally, add to $out
		$outArr[] = $row;
	}
	
	return $outArr;
}




function convertToGetVar($in) {
	$out = str_replace(' / ', '_', $in);
	$out = str_replace(' ', '_', $out);
	$out = str_replace(',', '', $out);
	$out = str_replace('/', '', $out);
	$out = strtolower($out);
	return $out;
}
function convertFromGetVar($in) {
	$out = str_replace('_', ' ', $in);
	$out = ucwords($out);
	return $out;
}


function hasSizesAvailable($item_id) {
	if (!is_numeric($item_id))
		return false;
		
	$result = mysql_query('SELECT * FROM `Sizes` WHERE item_id='.$item_id.' AND is_available=1 LIMIT 1');
	if (!$result || mysql_num_rows($result) == 0)
		return false;
	else
		return true;
}


function categoryIsPlant($categoryID) {
	if ($categoryID >= 7 AND $categoryID <= 14) // LAZY!!!
		return true;
}

function getBreadcrumbsArr() {
	$arr = array();
	
	// set the item if it exists
	if (isset($_GET['i']) && is_numeric($_GET['i'])) {
		$result = mysql_query('SELECT * FROM `Items` WHERE item_id='.$_GET['i']);
		if (mysql_num_rows($result) > 0) {
			$row = mysql_fetch_array($result);
			$txt = $row['item_name'];
			
			// set this for later
			$_GET['c'] = $row['category_id'];
			
			// use botanical name if we need to
			if (PRIMARY_NAME_TYPE == 'botanical_name' && categoryIsPlant($row['category_id'])) {
				$result = mysql_query('SELECT * FROM `Plants` WHERE item_id='.$row['item_id']);
				$row2 = mysql_fetch_array($result);
				$txt = $row2['genus'].' '.$row2['species'];
			}
		
			$arr['item'] = array('url' => 'browse.php?i='.$_GET['i'].'&item='.convertToGetVar($txt),
								'txt' => $txt);
								
			if (!isset($arr['lastItem']))
				$arr['lastItem'] = 'item';
		} // end if mysql_num_rows > 0
	}
	
	// set the genus if it exists
	if (isset($_GET['genus'])) {
		$arr['genus'] = array('url' => 'browse.php?genus='.$_GET['genus'],
								'txt' => $_GET['genus']);
								
		$result = mysql_query('SELECT * FROM `Plants`, `Items` WHERE Plants.genus="'.mysql_real_escape_string($_GET['genus']).'" AND Plants.item_id = Items.item_id');
		if (mysql_num_rows($result) > 0) {
			$row = mysql_fetch_array($result);
			$_GET['c'] = $row['category_id'];
		}
		
		// fix for extracat
		if (isset($_GET['extracat']) && is_numeric($_GET['extracat'])) {
			$_GET['c'] = $_GET['extracat'];
		}
		
		if (!isset($arr['lastItem']))
				$arr['lastItem'] = 'genus';
	}
	
	// set the category if it exists
	if (isset($_GET['c']) && is_numeric($_GET['c'])) {
		$result = mysql_query('SELECT * FROM `Categories` WHERE category_id='.$_GET['c']);
		if (mysql_num_rows($result) > 0) {
			$row = mysql_fetch_array($result);
			if ($row['parent_id'] == 0) {
				$arr['category'] = array('url' => 'browse.php?c='.$row['category_id'].'&category='.convertToGetVar($row['category_name']),
									'txt' => $row['category_name']);
				if (!isset($arr['lastItem']))
					$arr['lastItem'] = 'category';
			} else {
				$arr['subcategory'] = array('url' => 'browse.php?c='.$row['category_id'].'&category='.convertToGetVar($row['category_name']),
									'txt' => $row['category_name']);
				
				// add parent category
				$result = mysql_query('SELECT * FROM `Categories` WHERE category_id='.$row['parent_id']);
				$row = mysql_fetch_array($result);
				$arr['category'] = array('url' => 'browse.php?c='.$row['category_id'].'&category='.convertToGetVar($row['category_name']),
									'txt' => $row['category_name']);
									
				if (!isset($arr['lastItem']))
					$arr['lastItem'] = 'subcategory';
			}
		}
	}
	
	if (PAGE == 'search.php') {
		$arr['category'] = array('url' => 'search.php?q='.$_GET['q'], 'txt' => 'Search: '.$_GET['q']);
	} else if (PAGE == 'products_specials.php') {
		$arr['category'] = array('url' => 'products_specials.php', 'txt' => 'Specials');
	}
	
	
	// just set it to plants if we don't have a category defined (as a catch)
	if (!isset($arr['category']) || $arr['category']['txt'] == 'Plants') {
		$arr['category'] = array('url' => 'products_plants.php', 'txt' => 'Plants');
	}
	
	return $arr;
}



function getBreadCrumbsForItemWithID($id) {
	$out = '';
	if (!is_numeric($id))
		return $out;
		
	$query = 'SELECT * FROM `Items` WHERE item_id='.$id;
	$result = mysql_query($query);
	if (!$result || mysql_num_rows($result) == 0)
		return $out;

	$row = mysql_fetch_array($result);
	$cat = $row['category_id'];
	
	$query = 'SELECT * from `Categories` WHERE category_id='.$cat;
	$result = mysql_query($query);
	$row = mysql_fetch_array($result);
	$out = '<a href="browse.php?c='.$row['category_id'].'">'.$row['category_name'].'</a>';
	
	if ($row['parent_id'] != 0) {
		$query = 'SELECT * from `Categories` WHERE category_id='.$row['parent_id'];
		$result = mysql_query($query);
		$row = mysql_fetch_array($result);
		$out = '<a href="browse.php?c='.$row['category_id'].'">'.$row['category_name'].'</a> / ' . $out;
	}
	
	return $out;
}
function getAbsoluteBreadCrumbsForItemWithID($id) {
	$out = '';
	if (!is_numeric($id))
		return $out;
		
	$query = 'SELECT * FROM `Items` WHERE item_id='.$id;
	$result = mysql_query($query);
	if (!$result || mysql_num_rows($result) == 0)
		return $out;

	$row = mysql_fetch_array($result);
	$cat = $row['category_id'];
	
	$query = 'SELECT * from `Categories` WHERE category_id='.$cat;
	$result = mysql_query($query);
	$row = mysql_fetch_array($result);
	$out = '<a href="http://www.cjfiore.com/browse.php?c='.$row['category_id'].'">'.$row['category_name'].'</a>';
	
	if ($row['parent_id'] != 0) {
		$query = 'SELECT * from `Categories` WHERE category_id='.$row['parent_id'];
		$result = mysql_query($query);
		$row = mysql_fetch_array($result);
		$out = '<a href="http://www.cjfiore.com/browse.php?c='.$row['category_id'].'">'.$row['category_name'].'</a> / ' . $out;
	}
	
	return $out;
}


function getItemArrFromSizeID($id) {
	$outArr = array();
	if (!is_numeric($id))
		return $outArr;
		
	$query = 'SELECT * FROM `Sizes`, `Items` WHERE Sizes.id='.$id.' AND Sizes.item_id = Items.item_id AND Sizes.is_available=1';
	$result = mysql_query($query);
	
	if (mysql_num_rows($result) == 0)
		return $outArr;
	
	$outArr = mysql_fetch_array($result);
	if (categoryIsPlant($outArr['category_id'])) {
		$result2 = mysql_query('SELECT * FROM `Plants` WHERE item_id='.$outArr['item_id']);
		$row2 = mysql_fetch_array($result2);
		$outArr['botanical_name'] = $row2['genus'].' '.$row2['species'];
		$outArr['common_name'] = $outArr['item_name'];
		$outArr['item_name'] = $outArr[PRIMARY_NAME_TYPE];
		$outArr['item_name2'] = $outArr[SECONDARY_NAME_TYPE];
	}
	
	return $outArr;
}




function encodeForTextInput($in) {
	$out = str_replace('"', '&quot;', $in);
	
	return $out;
}