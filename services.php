<?php
require_once('functions.php');
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Our Services - CJ Fiore, Nursery and Landscape Supply</title>
<style type="text/css">
.servicesOverviewBlock a {
	letter-spacing: 1px;
}
</style>

<?php extraHead(); ?>
</head>
<body>
<?php makeHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" style="padding: 0px 10px 0px 20px;">

<!-- SERVICES TABLE -->
<table width="920" cellspacing="0" cellpadding="0" border="0" style="margin-bottom: 6px;">
	<tr>
		<td valign="top" align="left">
<div class="servicesOverviewBlock roundLeft10">
<a href="services_sourcing.php"><img src="images/services_overview0.jpg" width="227" height="159" border="0" class="roundTopLeft10"><br>&nbsp;&nbsp;<span>Sourcing</span> &gt;</a>
</div>
		</td>
		<td><img src="images/spacer.gif" width="5" height="1" border="0"></td>
		<td valign="top" align="left">
<div class="servicesOverviewBlock">
<a href="services_sourcing.php#purchasing_solutions"><img src="images/services_overview1.jpg" width="230" height="159" border="0"><br>&nbsp;&nbsp;<span>Purchasing Solutions</span> &gt;</a>
</div>
		</td>
		<td><img src="images/spacer.gif" width="5" height="1" border="0"></td>
		<td valign="top" align="left">
<div class="servicesOverviewBlock">
<a href="services_delivery.php"><img src="images/services_overview2.jpg" width="226" height="159" border="0"><br>&nbsp;&nbsp;<span>Delivery</span> &gt;</a>
</div>
		</td>
		<td><img src="images/spacer.gif" width="5" height="1" border="0"></td>
		<td valign="top" align="left">
<div class="servicesOverviewBlock roundRight10">
<a href="services_other.php"><img src="images/services_overview3.jpg" width="226" height="159" border="0" class="roundTopRight10"><br>&nbsp;&nbsp;<span>Planting/Fabrication</span> &gt;</a>
</div>
		</td>
	</tr>
	<tr>
		<td><img src="images/spacer.gif" width="180" height="15" border="0"></td>
		<td><img src="images/spacer.gif" width="5" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="180" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="5" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="180" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="5" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="180" height="1" border="0"></td>
	</tr>
</table>
<!-- END SERVICES TABLE -->

<h1><span>Fiore Services</span></h1>
<p class="firstLevel" style="margin: 10px 0px 250px 0px;"><span>At Fiore, our number one goal is to provide our customers with<br>the highest level of customer satisfaction in our industry. We do<br>this by having one of the most qualified nursery teams in the<br>midwest, and by offering the best customer service available<br>from materials selection and sourcing to delivery and plant<br>consultation.</span></p>
		</td>
	</tr>
</table>

<?php makeFooter(); ?>

</body>
</html>
