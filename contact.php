<?php
require_once('functions.php');
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Contact Us - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraHead(); ?>
</head>
<body>
<?php makeHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" style="padding: 10px 10px 0px 20px;">
<h1 style="line-height: 1;">Contact Fiore</h1>

<table cellspacing="0" cellpadding="0" border="0" style="margin: 20px 0px 100px 0px;">
	<tr>
		<td valign="top" align="left">
<div class="beigeBlock">
<b>Fiore Prairie View</b>
<br>16606 W. Highway 22
<br>Prairie View, IL 60069
<br>T 847-913-1414
<br>F 847-913-9690
<br><a href="mailto:sales@cjfiore.com">sales@cjfiore.com</a>
<br>
<br><b>Fiore Chicago</b>
<br>2901 W. Ferdinand St.
<br>Chicago, IL 60612
<br>T 773-533-1414
<br>F 773-533-9690
<br><a href="mailto:saleschicago.cjfiore.com">saleschicago@cjfiore.com</a>
<br>
<br><b>Fiore Bolingbrook</b>
<br>801 N Bolingbrook Dr
<br>Bolingbrook, IL 60440
<br>T 630-739-1414
<br><a href="mailto:billhope@cjfiore.com">billhope@cjfiore.com</a>
<br>
<br><b>Fiore Indianapolis</b>
<br>T 317-774-5266
<br>11460 Greenfield Avenue
<br>Noblesville, IN 46060
<br><a href="mailto:michaelmckernin@cjfiore.com.cjfiore.com">MichaelMcKernin@cjfiore.com</a>
</div>
		</td>
		<td valign="top" align="left" style="padding: 0px 20px; font-size: 13px;">
<!-- FORM TABLE -->
<form method="POST" action="contact.php">
<table width="340" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td valign="middle" align="left" style="font-size: 13px;"><span>First Name:</span></td>
		<td valign="middle" align="left"><input type="text" name="q0" id="q0" class="contactInput"></td>
	</tr>
	<tr>
		<td valign="middle" align="left" style="font-size: 13px;"><span>Last Name:</span></td>
		<td valign="middle" align="left"><input type="text" name="q1" id="q1" class="contactInput"></td>
	</tr>
	<tr>
		<td valign="middle" align="left" style="font-size: 13px;"><span>Email&nbsp;Address:</span></td>
		<td valign="middle" align="left"><input type="text" name="q2" id="q2" class="contactInput"></td>
	</tr>
	<tr>
		<td valign="top" align="left" style="font-size: 13px;"><span>Message:</span></td>
		<td valign="top" align="left"><textarea rows="8" name="q3" id="q3" class="contactInput"></textarea><br><input type="image" src="images/btnSubmit.png" width="75" height="22" border="0" alt="Submit" style="margin-left: 10px;">
</td>
	</tr>
</table>
</form>
<div class="leafItOnTop" style="top: 50px; left: 100px;"><img src="images/leafGreen.png" width="164" height="168" border="0">
<!-- END FORM TABLE -->
		</td>
	</tr>
	<tr>
		<td><img src="images/spacer.gif" width="180" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="340" height="1" border="0"></td>
	</tr>
</table>

		</td>
	</tr>
</table>

<?php makeFooter(); ?>

</body>
</html>
