<?php
require_once('functions.php');

$sent = false;
$err = '';
$highlightID = 0;
if (isset($_POST['filledOut']) && $_POST['filledOut'] == 'yup') {
	// require these IDs
	$requiredArr = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
	// Keys used to build the form
	$keyArr = array('Name', 'Address', 'City', 'State', 'Zip', 'Phone Number', 'Driver\'s License', 'CDL License', 
	
	'Position Applying For', 'Pay Desired', 'Date Available to Start', 'Full Time', 'Part Time', 'Temporary', 'Sesonal', 

	'Past Employer', 'Address', 'City', 'State', 'Zip', 'Phone', 'Supervisor', 'Position/Duties', 'Start Date', 'End Date', 'Reason For Leaving',
	'Past Employer', 'Address', 'City', 'State', 'Zip', 'Phone', 'Supervisor', 'Position/Duties', 'Start Date', 'End Date', 'Reason For Leaving',
	'Past Employer', 'Address', 'City', 'State', 'Zip', 'Phone', 'Supervisor', 'Position/Duties', 'Start Date', 'End Date', 'Reason For Leaving',

	'School Name', 'City/State', 'Graduated', 'Subjects Studied',
	'School Name', 'City/State', 'Graduated', 'Subjects Studied',
	'School Name', 'City/State', 'Graduated', 'Subjects Studied',
	
	'Other Relevant Experience');
	
	// headers ids
	$headersArr = array();
	$headersArr[0] = 'About You';
	$headersArr[8] = 'Employment Desired';
	$headersArr[15] = 'Employment History';
	$headersArr[48] = 'Educational Background';
	$headersArr[60] = 'Other Relevant Experience';
	
	// make sure the required IDs are filled out
	for ($i=0;$i<count($requiredArr);$i++) {
		$id = $requiredArr[$i];
		
		if (!isset($_POST['q'.$id]) || trim($_POST['q'.$id]) == '') {
			if ($err == '') {
				$err = 'Please fill out the following:<br>';
				$highlightID = $id;	
			}
			$err .= $keyArr[$id].'<br>';
		}
	}
	
	// make sure "authorization" is set
	if (!isset($_POST['authorize']) || $_POST['authorize'] != 'yup') {
		$err .= 'You must click authorize at the bottom of this form.<br>';
	}
	
	if ($err == '') {
		// build associative array
		$emailArr = array();
		for ($i=0;$i<count($keyArr);$i++) {
			$key = $keyArr[$i];
			if (isset($_POST['q'.$i]))
				$val = trim($_POST['q'.$i]);
			else
				$val = '';
			
			if ($val != '')
				$emailArr[$key] = $val;
		}
		
		//emailAssociativeArrayWithTitleTo($emailArr, 'Fiore Job Application', 'matt@mattcourtright.com');
		
		
		$title = 'Application for Employment';
		/* try #2, build html/text separately */
		$html = '<table cellspacing="5" cellpadding="0" border="0" style="font-family: Tahoma, Verdana, sans-serif; font-size: 10pt;"><tr><td valign="top" align="left" colspan="2"><h2>' . $title . '</h2>&nbsp;<br><h3>ABOUT YOU</h3></td></tr>';
		$html .= '<tr><td>'.getEmailHTMLForID(0).'</td><td align="right">Date of Application: '.date('m/j/Y').'</td></tr>';
		$html .= '<tr><td colspan="2">'.getEmailHTMLForID(1).getEmailHTMLForID(2).getEmailHTMLForID(3).getEmailHTMLForID(4).'<br>'.getEmailHTMLForID(5).'</td></tr>';
		$html .= '<tr><td>'.getEmailHTMLForID(6).'</td><td align="right">'.getEmailHTMLForID(7).'</td></tr>';
		$html .= '<tr><td colspan="2">&nbsp;<br><h3>EMPLOYMENT DESIRED</h3></td></tr>';
		
		$html .= '<tr><td valign="top">'.getEmailHTMLForID(8).'<br><br>'.getEmailHTMLForID(9).'</td><td valign="top">Type of Employment Desired:<br>';
			if (isset($_POST['q11']) && $_POST['q11'] != '')
				$html .= $keyArr[11].' ';
			if (isset($_POST['q12']) && $_POST['q12'] != '')
				$html .= $keyArr[12].' ';
			if (isset($_POST['q13']) && $_POST['q13'] != '')
				$html .= $keyArr[13];
			if (isset($_POST['q14']) && $_POST['q14'] != '')
				$html .= $keyArr[14].' ';
		$html .= '<br>'.getEmailHTMLForID(10).'</td></tr>';

		$html .= '<tr><td colspan="2">&nbsp;<br><h3>EMPLOYMENT HISTORY</h3></td></tr>';
		$html .= getEmploymentHistoryStartingAtID(15);
		$html .= getEmploymentHistoryStartingAtID(26);
		$html .= getEmploymentHistoryStartingAtID(37);

		$html .= '<tr><td colspan="2">&nbsp;<br><h3>EDUCATIONAL BACKGROUND</h3></td></tr>';
		$html .= getEducationStartingAtID(48);
		$html .= getEducationStartingAtID(51);
		$html .= getEducationStartingAtID(54);

		$html .= '<tr><td colspan="2">&nbsp;<br><h3>OTHER RELEVANT EXPERIENCES</h3></td></tr>';
		$html .= '<tr><td colspan="2">'.nl2br($_POST['q60']).'</td></tr>';
		
		$html .= '</table>';
		
		//emaiHTMLWithTitleTo($html, 'Fiore Job Application', 'matt@mattcourtright.com');
		//emaiHTMLWithTitleTo($html, 'Fiore Job Application', 'meighan@depkedesign.com');
		emaiHTMLWithTitleTo($html, 'Fiore Job Application', 'Management@cjfiore.com');
		
		$sent = true;
	}
}
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Employment Application - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraHead(); ?>
<style>
#contentTable td {
	font: 12px fiore-light, Helvetica-Light, sans-serif;
	line-height: 1.5;
}
h3 {
	margin-top: 20px;
}

.formTA {
	width: 100%;
	height: 61px;
	margin-bottom: 10px;
}
.formText {
	width: 100%;
	margin: 0px 0px 10px 0px;
}

</style>
</head>
<body onload="document.getElementById('q<?php echo $highlightID; ?>').focus();">
<?php makeHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" style="padding: 10px 10px 0px 20px;">
			<h1><span>Employment Application</span></h1>
			
			<?php
			if ($sent) {
				echo '<p style="width: 450px; margin-top: 10px;"><span>Thank you for submitting your application. We will review your application shortly.</span></p>';
			} else {
				echo '<p style="width: 450px; margin-top: 10px;"><span>Please fill out the form below and we will review.</span></p>';
				if ($err != '')
					echo '<p style="width: 450px; margin-top: 10px; color: #ff0000;"><span>'.$err.'</span></p>';
			?>
			
			

			<form method="POST">
				
				<table width="100%" cellspacing="0" cellpadding="0" border="0" class="innerTable">
					<tr>
						<td valign="top" align="left" style="width: 50%;">
							<h3>About You</h3>
							<br>Name: <?php formTextForID(0); ?>
							<br>Address: <?php formTextForID(1, 130); ?>
								City: <?php formTextForID(2, 70); ?>
								State: <?php formStateDropDownForID(3); ?>
								Zip: <?php formTextForID(4, 50); ?>
					
							<br>Phone: <?php formTextForID(5); ?>
							<br>Do you have a valid Driver's License <?php formRadioForIDWithVals(6, array('Yes', 'No')); ?>
							<br>Do you have a valid CDL (truck) License <?php formRadioForIDWithVals(7, array('Yes', 'No')); ?>
						</td>
						<td valign="top" align="left" style="padding-left: 20px;">
							<h3>Employment Desired</h3>
							<br>Position(s) Applied for: <?php formTextForID(8); ?>
							<br>Pay Desired: <?php formTextForID(9); ?>
							<br>Date Available to Start: <?php formTextForID(10); ?>
							<br>Type of Employment Desired: 
								<br><?php formCheckboxForIDWithVal(11, 'Full Time'); ?>
								<?php formCheckboxForIDWithVal(12, 'Part Time'); ?>
								<br><?php formCheckboxForIDWithVal(13, 'Temporary'); ?>
								<?php formCheckboxForIDWithVal(14, 'Seasonal'); ?>
						</td>
					</tr>
				</table>
			
				<h3>Employment History</h3>
				<br>Please list your relevant work history over the last three years.
				<br>
				<br>
				<table width="100%" cellspacing="0" cellpadding="0" border="0" class="innerTable">
					<tr>
						<td valign="top" align="left" style="width: 50%;">
							<b>1.</b> Employer's Name: <?php formTextForID(15); ?>
							<br>Address: <?php formTextForID(16, 130); ?>
								City: <?php formTextForID(17, 70); ?>
								State: <?php formStateDropDownForID(18); ?>
								Zip: <?php formTextForID(19, 50); ?>
								
							<br>Phone: <?php formTextForID(20); ?>
							<br>Supervisor's Name: <?php formTextForID(21); ?>
						</td>
						<td valign="top" align="left" style="padding-left: 20px;">
							Position/Duties:
							<br><?php formTextAreaForID(22); ?>
							<br>Date Employed From: <?php formTextForID(23, 100); ?> To: <?php formTextForID(24, 100); ?>
							<br>Reason for Leaving: <?php formTextForID(25); ?>
						</td>
					</tr>
					<tr>
						<td colspan="2"><br>&nbsp;</td>
					</tr>
					<tr>
						<td valign="top" align="left" style="width: 50%;">
							<b>2.</b> Employer's Name: <?php formTextForID(26); ?>
							<br>Address: <?php formTextForID(27, 130); ?>
								City: <?php formTextForID(28, 70); ?>
								State: <?php formStateDropDownForID(29); ?>
								Zip: <?php formTextForID(30, 50); ?>
								
							<br>Phone: <?php formTextForID(31); ?>
							<br>Supervisor's Name: <?php formTextForID(32); ?>
						</td>
						<td valign="top" align="left" style="padding-left: 20px;">
							Position/Duties:
							<br><?php formTextAreaForID(33); ?>
							<br>Date Employed From: <?php formTextForID(34, 100); ?> To: <?php formTextForID(35, 100); ?>
							<br>Reason for Leaving: <?php formTextForID(36); ?>
						</td>
					</tr>
					<tr>
						<td colspan="2"><br>&nbsp;</td>
					</tr>
					<tr>
						<td valign="top" align="left" style="width: 50%;">
							<b>3.</b> Employer's Name: <?php formTextForID(37); ?>
							<br>Address: <?php formTextForID(38, 130); ?>
								City: <?php formTextForID(39, 70); ?>
								State: <?php formStateDropDownForID(40); ?>
								Zip: <?php formTextForID(41, 50); ?>
								
							<br>Phone: <?php formTextForID(42); ?>
							<br>Supervisor's Name: <?php formTextForID(43); ?>
						</td>
						<td valign="top" align="left" style="padding-left: 20px;">
							Position/Duties:
							<br><?php formTextAreaForID(44); ?>
							<br>Date Employed From: <?php formTextForID(45, 100); ?> To: <?php formTextForID(46, 100); ?>
							<br>Reason for Leaving: <?php formTextForID(47); ?>
						</td>
					</tr>
				</table>
				
				<h3>Educational Background</h3>
				<br>School Name: <?php formTextForID(48, 150); ?> City/State: <?php formTextForID(49, 100); ?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Did you Graduate? <?php formRadioForIDWithVals(50, array('Yes', 'No')); ?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Subjects Studied: <?php formTextForID(51, 150); ?>

				<br>School Name: <?php formTextForID(52, 150); ?> City/State: <?php formTextForID(53, 100); ?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Did you Graduate? <?php formRadioForIDWithVals(54, array('Yes', 'No')); ?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Subjects Studied: <?php formTextForID(55, 150); ?>

				<br>School Name: <?php formTextForID(56, 150); ?> City/State: <?php formTextForID(57, 100); ?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Did you Graduate? <?php formRadioForIDWithVals(58, array('Yes', 'No')); ?>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Subjects Studied: <?php formTextForID(59, 150); ?>
				
				<h3>Other Relevant Experience</h3>
				<br>Please give us any additional relevant information that would be helpful to us in considering your application
				<br><?php formTextAreaForID(60); ?>
				
				<h3>Authorization</h3>
				<br><label><input type="checkbox" name="authorize" id="authorize" value="yup">
				I certify that the information I have provided in this application is true and complete.  Any misrepresentations or falsifications are grounds for the cancellation of this application or, if I have been hired, termination of my employment.

				<br>I authorize investigation of all statements contained in this application to Charles J. Fiore Company, Inc. about my background and release all parties from liability for any damage that may result from the release or use of such information.

				<br>If I am hired, I understand that my employment can be terminated, with or without cause, at any time by either 
				Charles J. Fiore Company, Inc. or myself.</label>
				
				
				<script language="javascript">
					var tKey = 'filledOut';
					var tVal = 'yup';
					document.write('<input type="hidden" name="'+tKey+'" value="'+tVal+'">');
				</script>
				
				<br>
				<br>
				<input type="submit" value="submit">
			</form>
			
			<?php
			} // end if NOT sent
			?>
			
			<br><br><br>
		</td>
	</tr>
</table>

<?php makeFooter(); ?>

</body>
</html>
<?php
function formTextForID($id = 0, $width = 0) {
	if (isset($_POST['q'.$id]))
		$tmpVal = str_replace('"', '&quot;', $_POST['q'.$id]);
	else
		$tmpVal = '';
		
	if ($width == 0)
		echo '<input type="text" class="formText" name="q'.$id.'" id="q'.$id.'" value="'.$tmpVal.'">';
	else
		echo '<input type="text" class="formText" name="q'.$id.'" id="q'.$id.'" value="'.$tmpVal.'" style="width: '.$width.'px;">';
}


function formTextAreaForID($id = 0) {
	if (isset($_POST['q'.$id]))
		$tmpVal = $_POST['q'.$id];
	else
		$tmpVal = '';
		
		
	echo '<textarea name="q'.$id.'" id="q'.$id.'" class="formTA">'.$tmpVal.'</textarea>';
}



function formRadioForIDWithVals($id = 0, $valArr = array()) {
	if (count($valArr) < 2)
		return;
	
	if (isset($_POST['q'.$id]))
		$tmpVal = str_replace('"', '&quot;', $_POST['q'.$id]);
	else
		$tmpVal = '';
		
	for ($i=0;$i<count($valArr);$i++) {
		echo '<label><input type="radio" name="q'.$id.'" id="q'.$id.'" value="'.$valArr[$i].'"';
		if ($valArr[$i] == $tmpVal)
			echo ' CHECKED';
			
		echo '>'.$valArr[$i].'</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	}
}


function formCheckboxForIDWithVal($id = 0, $label = '') {
	if (isset($_POST['q'.$id]) && $_POST['q'.$id] == 'yup')
		echo '<label><input type="checkbox" name="q'.$id.'" id="q'.$id.'" value="yup" CHECKED>'.$label.'</label>';
	else
		echo '<label><input type="checkbox" name="q'.$id.'" id="q'.$id.'" value="yup">'.$label.'</label>';
}


function formStateDropDownForID($id = 0) {
	$stateArr = array('AL', 'AK', 'AS', 'AZ', 'AR', 'CA', 'CO', 'CT', 'DE', 'DC', 'FM', 'FL', 'GA', 'GU', 'HI', 'ID', 'IL', 'IN', 'IA', 'KS', 'KY', 'LA', 'ME', 'MH', 'MD', 'MA', 'MI', 'MN', 'MS', 'MO', 'MT', 'NE', 'NV', 'NH', 'NJ', 'NM', 'NY', 'NC', 'ND', 'MP', 'OH', 'OK', 'OR', 'PW', 'PA', 'PR', 'RI', 'SC', 'SD', 'TN', 'TX', 'UT', 'VT', 'VI', 'VA', 'WA', 'WV', 'WI', 'WY', 'AE', 'AA', 'AP');
	
	if (isset($_POST['q'.$id]))
		$tmpVal = $_POST['q'.$id];
	else
		$tmpVal = 'IL';
		
	echo '<select name="q'.$id.'" id="q'.$id.'">';
		for ($i=0;$i<count($stateArr);$i++) {
			if ($stateArr[$i] == $tmpVal)
				echo '<option value="'.$stateArr[$i].'" SELECTED>'.$stateArr[$i].'</option>';
			else
				echo '<option value="'.$stateArr[$i].'">'.$stateArr[$i].'</option>';
		}
	echo '</select>';
}



function getEmailHTMLForID($id) {
	$keyArr = array('Name', 'Address', 'City', 'State', 'Zip', 'Phone Number', 'Driver\'s License', 'CDL License', 
	
	'Position Applying For', 'Pay Desired', 'Date Available to Start', 'Full Time', 'Part Time', 'Temporary', 'Sesonal', 

	'Past Employer', 'Address', 'City', 'State', 'Zip', 'Phone', 'Supervisor', 'Position/Duties', 'Start Date', 'End Date', 'Reason For Leaving',
	'Past Employer', 'Address', 'City', 'State', 'Zip', 'Phone', 'Supervisor', 'Position/Duties', 'Start Date', 'End Date', 'Reason For Leaving',
	'Past Employer', 'Address', 'City', 'State', 'Zip', 'Phone', 'Supervisor', 'Position/Duties', 'Start Date', 'End Date', 'Reason For Leaving',

	'School Name', 'City/State', 'Graduated', 'Subjects Studied',
	'School Name', 'City/State', 'Graduated', 'Subjects Studied',
	'School Name', 'City/State', 'Graduated', 'Subjects Studied',
	
	'Other Relevant Experience');
	
	$out = $keyArr[$id] . ': <span style="font-weight: bold; padding: 0px 5px;">'.$_POST['q'.$id].'</span>&nbsp;&nbsp;';
	return $out;
}

function getEmploymentHistoryStartingAtID($startingID) {
	// make sure something is filled out
	$filledOut = false;
	for ($i=0;$i<11;$i++) {
		$id = $i + $startingID;
		if (isset($_POST['q'.$id]) && trim($_POST['q'.$id]) != '')
			if ($_POST['q'.$id] != 'IL') // ALSO ignore default state (IL)
				$filledOut = true;
	}
	
	if (!$filledOut)
		return '';
	
	// build html
	
	// name/phone
	$out = '<tr><td valign="top">'.getEmailHTMLForID($startingID).'</td><td valign="top" align="right">'.getEmailHTMLForID($startingID + 5).'</td></tr>';
	
	// address
	$out .= '<tr><td colspan="2">'.getEmailHTMLForID($startingID + 1).getEmailHTMLForID($startingID + 2).getEmailHTMLForID($startingID + 3).getEmailHTMLForID($startingID + 4).'</td></tr>';
	
	// supervisor / position // employed from-to / reson for leaving
	$out .= '<tr><td colspan="2">'.getEmailHTMLForID($startingID + 6).'<br>'.getEmailHTMLForID($startingID + 7).'<br>'.getEmailHTMLForID($startingID + 8).'&nbsp;&nbsp;&nbsp;'.getEmailHTMLForID($startingID + 9).'<br>'.getEmailHTMLForID($startingID + 10).'</td></tr>';
	
	// extra space
	$out .= '<tr><td colspan="2">&nbsp;</td></tr>';
	
	return $out;
}

function getEducationStartingAtID($startingID) {
	// make sure something is filled out
	$filledOut = false;
	for ($i=0;$i<4;$i++) {
		$id = $i + $startingID;
		if (isset($_POST['q'.$id]) && trim($_POST['q'.$id]) != '')
			if ($_POST['q'.$id] != 'IL') // ALSO ignore default state (IL)
				$filledOut = true;
	}
	
	if (!$filledOut)
		return '';
		
	$out = '<tr><td colspan="2">'.getEmailHTMLForID($startingID).'&nbsp;&nbsp;&nbsp;'.getEmailHTMLForID($startingID + 1).'&nbsp;&nbsp;&nbsp;'.getEmailHTMLForID($startingID + 2).'</td></tr>';
	
	// extra space
	$out .= '<tr><td colspan="2">&nbsp;</td></tr>';

	return $out;
}