<?php
// set category = natural stone
$_GET['c'] = 3;


require_once('functions_catalog.php');
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Natural Stone - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraCatalogHead(); ?>
</head>
<body>
<?php makeCatalogHeader(false); ?>

<table width="801" cellspacing="0" cellpadding="0" border="0" style="margin-bottom: 5px;">
	<tr>
		<td valign="top" align="left">
<div class="productPlantBlock roundTopLeft10">
<a href="browse.php?c=26&category=flagstone"><img src="images/products_natural_stone0.jpg" width="434" height="186" border="0" class="roundTopLeft10"><div>Flagstone &gt;</div></a>
</div>
		</td>
		<td valign="top" align="left">
<div class="productPlantBlock">
<a href="browse.php?c=27&category=pavers"><img src="images/products_natural_stone1.jpg" width="361" height="186" border="0"><div>Pavers  &gt;</div></a>
</div>
		</td>
	</tr>
</table>

<table width="801" cellspacing="0" cellpadding="0" border="0" style="margin-bottom: 5px;">
	<tr>
		<td valign="top" align="left">
<div class="productPlantBlock" style="margin-bottom: 5px;">
<a href="browse.php?c=28&category=dry_wall"><img src="images/products_natural_stone2.jpg" width="232" height="187" border="0"><div>Dry Wall  &gt;</div></a>
</div>

<div class="productPlantBlock">
<a href="browse.php?c=32&category=steps"><img src="images/products_natural_stone5.jpg" width="232" height="342" border="0"><div>Steps  &gt;</div></a>
</div>
		</td>
		<td valign="top" align="left">
<!-- inner table 1-->
<table width="563" cellspacing="0" cellpadding="0" border="0" style="margin-bottom: 5px;">
	<tr>
		<td valign="top" align="left" rowspan="2">
<div class="productPlantBlock">
<a href="browse.php?c=30&category=boulders"><img src="images/products_natural_stone3.jpg" width="266" height="368" border="0"><div>Boulders  &gt;</div></a>
</div>
		</td>
		<td valign="top" align="left">
<div class="productPlantBlock" style="margin-bottom: 5px;">
<a href="browse.php?c=29&category=outcropping"><img src="images/products_natural_stone4.jpg" width="292" height="189" border="0"><div>Outcropping  &gt;</div></a>
</div>
		</td>
	</tr>
	<tr>
		<td valign="top" align="left">
<div class="productPlantBlock">
<a href="browse.php?c=31&category=coping"><img src="images/products_natural_stone6.jpg" width="292" height="174" border="0"><div>Coping  &gt;</div></a>
</div>
		</td>
	</tr>
</table>
<!-- end inner table 1 -->

<!-- inner table 2 -->
<table width="563" cellspacing="0" cellpadding="0" border="0" style="margin-bottom: 5px;">
	<tr>
		<td valign="top" align="left">
<div class="productPlantBlock">
<a href="browse.php?c=33&category=lawn_edging"><img src="images/products_natural_stone7.jpg" width="326" height="161" border="0"><div>Lawn Edging  &gt;</div></a>
</div>
		</td>
		<td valign="top" align="left">
<div class="productPlantBlock">
<a href="browse.php?c=34&category=veneer"><img src="images/products_natural_stone8.jpg" width="232" height="161" border="0"><div>Veneer  &gt;</div></a>
</div>
		</td>
	</tr>
</table>
<!-- end inner table 2 -->
		</td>
	</tr>
</table>






<?php makeCatalogFooter(); ?>

</body>
</html>
