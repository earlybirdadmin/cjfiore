<?php
require_once('functions_catalog.php');

// single item
if (isset($_GET['i'])) {
	require('inc/plants_single.php');
	exit();
}

// genus
if (isset($_GET['genus'])) {
	require('inc/plants_few.php');
	exit();
}


// category
$specialCategories = array();
$specialCategories[1] = 'products_plants.php';
$specialCategories[2] = 'products_landscape_supply.php';
$specialCategories[3] = 'products_natural_stone.php';
$specialCategories[4] = 'products_green_roof.php';
$specialCategories[5] = 'products_planters_accessories.php';
$specialCategories[6] = 'products_specials.php';
$specialCategories[24] = 'products_grow_modular.php';
$specialCategories[35] = 'products_concrete_clay_pavers.php';

if (isset($_GET['c']) && is_numeric($_GET['c'])) {
	$query = 'SELECT * FROM `Items` WHERE is_available=1 AND category_id='.$_GET['c'];
	$result = mysql_query($query);
	$totalItems = mysql_num_rows($result);
	
	$cat = $_GET['c'];
	
	if (isset($specialCategories[$cat]) && $specialCategories[$cat] != '') {
		require($specialCategories[$cat]);
		exit();
	} else if (categoryIsPlant($_GET['c'])) {
		if ($totalItems > 20 || $_GET['c'] == 14) {
			require('inc/plants_many.php');
			exit();
		}
		require('inc/plants_few.php');
		exit();
	} else if ($_GET['c'] == 15 || $_GET['c'] == 23){
		// large images
		require('inc/items_few.php');
		exit();
	} else if ($_GET['c'] == 20) {
		// misc is labels only
		require('inc/labels_only.php');
		exit();
	} else {
		// regular long list
		require('inc/items_many.php');
		exit();
	}
}

