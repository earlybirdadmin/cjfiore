<?php
require_once('functions.php');

if (!isset($_SESSION['user']['user_id']) || !is_numeric($_SESSION['user']['user_id'])) {
	header('Location: login.php');
	exit();
}

$user_id = $_SESSION['user']['user_id']
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Order History - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraHead(); ?>
</head>
<body>
<?php makeHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" style="padding: 20px 10px 0px 20px;">
<?php
if (isset($_GET['order']) && is_numeric($_GET['order'])) {
	$result = mysql_query('SELECT * FROM `SavedOrders` WHERE user_id='.$user_id.' AND id='.$_GET['order']);
	if (mysql_num_rows($result) > 0) {
		$row = mysql_fetch_array($result);
		$row['order_html'] = addBluePayTable($row['order_html'], $_GET['order']);

		$html = '<h1>Order History</h1><br>Your order placed on '.date('F j, Y', $row['order_date']).'.';

		$html .= $row['order_html'].'<br><br>';
	}
}

if (isset($html)) {
	echo $html;
} else {
	echo '<h1>Order History</h1><br>Here you can view your past order history.<br><br>';
	$result = mysql_query('SELECT * FROM `SavedOrders` WHERE user_id='.$user_id.' ORDER BY order_date DESC');
	for ($i=0;$i<mysql_num_rows($result);$i++) {
		$row = mysql_fetch_array($result);
		echo '- <a href="saved_orders.php?order='.$row['id'].'">View Order</a> placed on '.date('F j, Y', $row['order_date']).'<br><br>';
	}
	
	if ($i == 0)
		echo 'Sorry, there are no previous orders on file for you.';
		
}
?>
		</td>
	</tr>
</table>

<?php makeFooter(); ?>

</body>
</html>
<?php
function addBluePayTable($html = '', $invoiceID = 'N/A') {
	// init subtotal
	if (isset($GLOBALS['subtotal']))
		$subtotal = $GLOBALS['subtotal'];
	else
		$subtotal = '';

	// outer-table (for bluePay link)
	$o = '<table width="970" cellspacing="0" cellpadding="0" border="0"><tr><td valign="top" align="left">';
	// add original html
	$o .= $html;
	$o .= '<td><div style="width: 20px;">&nbsp;</div></td><td valign="bottom" align="left">';
		$o .= '<small><b>Make Payment:</b></small><br>';
		// begin bluePay link
		$o .= '<form action="https://secure.bluepay.com/interfaces/shpf" method="POST" target="_blank">';
		$o .= '<input type="hidden" name="SHPF_FORM_ID" value="CJF01"><input type="hidden" name="SHPF_ACCOUNT_ID" value="100033809953"><input type="hidden" name="SHPF_TPS_DEF" value="SHPF_FORM_ID SHPF_ACCOUNT_ID DBA TAMPER_PROOF_SEAL AMEX_IMAGE DISCOVER_IMAGE TPS_DEF SHPF_TPS_DEF CUSTOM_HTML REBILLING REB_CYCLES REB_AMOUNT REB_EXPR REB_FIRST_DATE"><input type="hidden" name="SHPF_TPS" value="20082bd28ccc0adc94b0c5389040b21b"><input type="hidden" name="MODE" value="LIVE"><input type="hidden" name="TRANSACTION_TYPE" value="SALE"><input type="hidden" name="DBA" value="Charles J. Fiore Company Inc. "><input type="hidden" name="TAMPER_PROOF_SEAL" value="f219b89be6cf7b846d360215f8a5a591"><input type="hidden" name="REBILLING" value="0"><input type="hidden" name="REB_CYCLES" value=""><input type="hidden" name="REB_AMOUNT" value=""><input type="hidden" name="REB_EXPR" value=""><input type="hidden" name="REB_FIRST_DATE" value=""><input type="hidden" name="AMEX_IMAGE" value="amex.gif"><input type="hidden" name="DISCOVER_IMAGE" value="discvr.gif"><input type="hidden" name="REDIRECT_URL" value="https://secure.bluepay.com/interfaces/shpf?SHPF_FORM_ID=CJF02&amp;SHPF_ACCOUNT_ID=100033809953&amp;SHPF_TPS_DEF=SHPF_ACCOUNT_ID SHPF_FORM_ID RETURN_URL DBA AMEX_IMAGE DISCOVER_IMAGE SHPF_TPS_DEF&amp;SHPF_TPS=4bce39f14011411bd7838a0f6d19823e&amp;RETURN_URL=javascript%3Ahistory%2Ego%28%2D2%29&amp;DBA=Charles%20J%2E%20Fiore%20Company%20Inc%2E%20&amp;AMEX_IMAGE=amex%2Egif&amp;DISCOVER_IMAGE=discvr%2Egif"><input type="hidden" name="TPS_DEF" value="MERCHANT APPROVED_URL DECLINED_URL MISSING_URL MODE TRANSACTION_TYPE TPS_DEF REBILLING REB_CYCLES REB_AMOUNT REB_EXPR REB_FIRST_DATE"><input type="hidden" name="CUSTOM_HTML" value="">';
		$o .= '<input type="hidden" name="AMOUNT" value="'.getSubtotalFromHTML($html).'">';
		$o .= '<input type="hidden" name="CUSTOM_ID" value="'.getUserVal(13).'">';
		$o .= '<input type="hidden" name="INVOICE_ID" value="'.$invoiceID.'">';
		$o .= '<input type="hidden" name="NAME" value="'.getUserVal(5).'">';
		$o .= '<input type="hidden" name="COMPANY_NAME" value="'.getUserVal(7).'">';
		$o .= '<input type="hidden" name="ADDR1" value="'.getUserVal(8).'">';
		$o .= '<input type="hidden" name="CITY" value="'.getUserVal(11).'">';
		$o .= '<input type="hidden" name="STATE" value="'.getUserVal(12).'">';
		$o .= '<input type="hidden" name="ZIPCODE" value="'.getUserVal(10).'">';
		$o .= '<input type="hidden" name="EMAIL" value="'.getUserVal(3).'">';
		$o .= '<input type="hidden" name="PHONE" value="'.getUserVal(9).'">';
		
		$o .= '<input type="image" src="images/bluePayLogo.gif" width="140" height="45" border="0" value="Pay Now">';
		$o .= '</form><br>&nbsp;';
		// end bluePay link
		
	// close outer-table
	$o .= '</td></tr></table>';
	
	return $o;
}

function getSubtotalFromHTML($html) {
	$arr = explode('<b style="font: 18px fiore-bold, Arial, sans-serif; font-weight: bold;">$', $html);
	if (count($arr) != 2)
		return '';
	
	$arr2 = explode('<br><br>', $arr[1]);
	if (!is_numeric($arr2[0]))
		return '';
		
	return $arr2[0];
}

function getUserVal($id) {
	$idxArr = array('user', 'password0', 'password1', 'email', 'is_business', 'first_name', 'last_name', 'company', 'address', 'phone', 'zip', 'city', 'state', 'user_id');
	
	$idx = $idxArr[$id];
	if (isset($_SESSION['user'][$idx]))
		return $_SESSION['user'][$idx];
	else
		return '';
}