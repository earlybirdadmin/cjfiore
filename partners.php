<?php
require_once('functions.php');
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Hardscape Partners - CJ Fiore, Nursery and Landscape Supply</title>
<?php
    extraHead();
?>
</head>
<body>
<?php
    makeHeader();
?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" style="padding: 0px 10px 0px 20px;">

<!-- PARTNERS TABLE -->
<table width="10" cellspacing="0" cellpadding="0" border="0" style="margin-bottom: 6px;">
	<tr>
		<td valign="top" align="left">
			<div class="servicesOverviewBlock roundLeft10">
			<img src="images/partners0.jpg" width="225" height="160" border="0" class="roundLeft10" style="margin: 0px;">
			</div>
		</td>
		<td><img src="images/spacer.gif" width="5" height="1" border="0"></td>
		<td valign="top" align="left">
			<div class="servicesOverviewBlock">
			<img src="images/partners1.jpg" width="225" height="160" border="0" style="margin: 0px;">
			</div>
		</td>
		<td><img src="images/spacer.gif" width="5" height="1" border="0"></td>
		<td valign="top" align="left">
			<div class="servicesOverviewBlock">
			<img src="images/partners2.jpg" width="225" height="160" border="0" style="margin: 0px;">
			</div>
		</td>
		<td><img src="images/spacer.gif" width="5" height="1" border="0"></td>
		<td valign="top" align="left">
			<div class="servicesOverviewBlock roundRight10">
			<img src="images/partners3.jpg" width="225" height="160" border="0" class="roundRight10" style="margin: 0px;">
			</div>
		</td>
	</tr>
	<tr>
		<td><img src="images/spacer.gif" width="180" height="20" border="0"></td>
		<td><img src="images/spacer.gif" width="5" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="180" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="5" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="180" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="5" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="180" height="1" border="0"></td>
	</tr>
	<tr>
		<td valign="top" align="left" colspan="7">
			<h1><span>Fiore Hardscape Partners</span></h1>
			<p class="firstLevel" style="margin: 10px 30px 0px 0px;"><span>Working with Fiore allows you access to the Fiore Network, our partner network with over 250 suppliers from across the country and around the globe. Our network allows us to supply you with unlimited, quality horticultural, stone, and landscape products and supplies. Our largest partners, Eden Stone Company, Valders Stone, and Midwest Trading, stock a full range of products on-site at both of our locations, eliminating wait time for the essentials you need.</span></p>
		</td>
	</tr>
</table>
<!-- END PARTNERS TABLE -->


		</td>
	</tr>
	<tr>
		<td valign="top" align="left" style="padding: 20px 10px 80px 20px; font-size: 16px; line-height: 1.8">
            <a name="techo"></a>
            <p>
                <a href="https://www.techo-bloc.com" target="_blank"><img src="images/partners-detail-techo.png" width="120" height="70" border="0" alt="Techo-Bloc" class="partner-image" ></a>
                <span class="h4">Techo - Bloc </span>
         			<a href="https://www.techo-bloc.com/" target="_blank">Site</a>
            </p>
            <p>With life at home always in mind, Techo-Bloc’s focus on landscape product design has pushed the boundaries; creating materials for notable curb appeal and stunning, as well as functional, backyard spaces.</p>
            <div style="width: 100%; height: 1px; border-bottom: solid #4e4244 1px; clear: both; margin: 20px 0px;"></div>

            <a name="eden"></a>
            <p>
                <a href="http://www.edenstone.net/" target="_blank"><img src="images/partners_edenstone.png" width="120" height="70" border="0" alt="Techo-Bloc" class="partner-image" ></a>
                <span class="h4">Eden Stone Company</span>
         			<a href="http://www.edenstone.net/" target="_blank">Site</a>
            </p>
            <p>Eden Stone is fortunate to have access to material that is time tested as well as aesthetically driven.  The mineral composition as well as the tight compaction of these layers resulted in an extremely dense, high strength Limestone.</p>
            <div style="width: 100%; height: 1px; border-bottom: solid #4e4244 1px; clear: both; margin: 20px 0px;"></div>

            <a name="valders"></a>
            <p>
                <a href="http://www.valdersstone.com/" target="_blank"><img src="images/partners_detail1.gif" width="120" height="70" border="0" alt="Techo-Bloc" class="partner-image" ></a>
                <span class="h4">Valders</span>
                    <a href="http://www.valdersstone.com/" target="_blank">Site</a>
            </p>
            <p>Our skilled craftsmen use decades of experience to bring your ideas to fruition. Today applications for our limestone are only limited to the imagination of the design professional.</p>
            <div style="width: 100%; height: 1px; border-bottom: solid #4e4244 1px; clear: both; margin: 20px 0px;"></div>


            <a name="rochester"></a>
            <p>
                <a href="http://rochestercp.com" target="_blank"><img src="images/rochester-concrete-products.svg" width="120" height="70" border="0" alt="Techo-Bloc" class="partner-image" ></a>
                <span class="h4">Rochester Concrete Products</span>
         			<a href="http://rochestercp.com" target="_blank">Site</a>
            </p>
            <p>For nearly a century, Rochester Concrete Products™ and its sister companies have been refining the art and science of interlocking concrete products. RCP is recognized throughout the Midwest for creating landscape products rich in texture, full of character and manufactured with the highest quality at an affordable price. For these reasons, Rochester Concrete Products is the professional’s first choice.</p>
            <div style="width: 100%; height: 1px; border-bottom: solid #4e4244 1px; clear: both; margin: 20px 0px;"></div>


            <a name="mirage"></a>
            <p>
                <a href="https://mirageusa.net" target="_blank"><img src="images/mirage-usa.png" width="120" height="70" border="0" alt="Techo-Bloc" class="partner-image" ></a>
                <span class="h4">Mirage/Evo</span>
         			<a href="https://mirageusa.net" target="_blank">Site</a>
            </p>
            <p>Mirage USA is the latest step in the on-going evolution of Mirage Granito Ceramico SpA, seeking to deliver an innovative, skilled response to the demands of the North American market. An expression of constant technological development applied to the manufacture of porcelain products, Mirage USA offers a perfect combination of the most sophisticated, exclusive Italian-crafted design and the advantages of manufacturing in the USA.</p>
            <div style="width: 100%; height: 1px; border-bottom: solid #4e4244 1px; clear: both; margin: 20px 0px;"></div>

            <a name="pavers"></a>

            <p>
                <a href="https://www.glengery.com" target="_blank"><img src="images/partners_detail4.jpg" width="120" height="70" border="0" alt="Techo-Bloc" class="partner-image" ></a>
                <span class="h4">Glen-Gery Clay Pavers</span>
         			<a href="https://www.glengery.com" target="_blank">Site</a>
            </p>
            <p>Glen-Gery is the superior choice among architects, builders and homeowners who require high quality building products that meet both innovative design challenges and demanding construction specifications. Glen-Gery is an industry leader for its’ diversified product line of more than 600 brick products, which are available in a wide-array of sizes and textures.</p>
            <div style="width: 100%; height: 1px; border-bottom: solid #4e4244 1px; clear: both; margin: 20px 0px;"></div>

            <a name="belgard"></a>
            <p>
                <a href="http://www.belgard.com/" target="_blank"><img src="images/partners_detail4.gif" width="120" height="70" border="0" alt="Techo-Bloc" class="partner-image" ></a>
                <span class="h4">Belgard Hardscapes</span>
         			<a href="http://www.belgard.com/" target="_blank">Site</a>
            </p>
            <p>We exist for one purpose: to make outside your kind of beautiful. This goes beyond turning creativity and craftsmanship into lasting outdoor spaces. It means bringing your custom hardscape designs to life, together. Because with Belgard, you get more than just beautiful customized paver products, you get a partner to help you every step of the way.</p>
            <div style="width: 100%; height: 1px; border-bottom: solid #4e4244 1px; clear: both; margin: 20px 0px;"></div>

            <div style="clear: both; height: 1px;"></div>
            <div class="leafItOnTop" style="top: -26px; left: 540px;"><img src="images/leafGreen2.png" width="164" height="168" border="0"></div>

		</td>
	</tr>

</table>

<?php
    makeFooter();
?>

</body>
</html>
