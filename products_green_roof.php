<?php
$_GET['c'] = 4; // green roof

require_once('functions_catalog.php');



$query = 'SELECT * FROM `Items`, `Sizes` WHERE Items.discount != 0 AND Items.is_available=1 AND Items.item_id=Sizes.item_id';
$result = mysql_query($query);
$itemArr = array();
for ($i=0;$i<mysql_num_rows($result);$i++) {
	$itemArr[] = mysql_fetch_array($result);
}

// create the itemArr
$singleItem = array('name' => 'Item ', 'desc' =>'Size and weight here', 'price' => '000');

?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Green Roof - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraCatalogHead(); ?>
</head>
<body>
<?php makeCatalogHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td valign="top" align="left" style="padding-right: 20px; font-size: 16px;">
<h1>When Size Matters!</h1>

<br>Access is often the limiting factor when installing a roof top, balcony or courtyard garden.
With this in mind, Fiore can provide a wide range of roof top reliable specimen trees and
ornamentals in various size containers. Our Roof Ready Plants™ have been selected with
durability, uniqueness, space, and mobility in mind. If you don‘t see the plant you are
looking for on this list, please inquire, as we have a wide range of resources.
<br>&nbsp;
		</td>
		<td valign="top" align="left" style="border-left: solid #d2ced0 1px;">
<img src="images/products_green_roof0.gif" width="220" height="155" border="0" alt="roof ready plants">
		</td>
	</tr>
	<tr>
		<td valign="top" align="left" colspan="2">
<!-- size table -->
<div class="greenTable">
<table width="375" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td valign="middle" align="center"><b>POT SIZE</b></td>
		<td valign="middle" align="center"><b>WIDTH</b></td>
		<td valign="middle" align="center"><b>HEIGHT</b></td>
		<td valign="middle" align="center"><b>AVERAGE WEIGHT</b></td>
	</tr>
	<tr>
		<td valign="middle" align="center">#5</td>
		<td valign="middle" align="center">10.5"</td>
		<td valign="middle" align="center">11"</td>
		<td valign="middle" align="center">15lb</td>
	</tr>
	<tr>
		<td valign="middle" align="center">#7 squat</td>
		<td valign="middle" align="center">14"</td>
		<td valign="middle" align="center">10"</td>
		<td valign="middle" align="center">25lb</td>
	</tr>
	<tr>
		<td valign="middle" align="center">#7</td>
		<td valign="middle" align="center">14"</td>
		<td valign="middle" align="center">13.5"</td>
		<td valign="middle" align="center">35lb</td>
	</tr>
	<tr>
		<td valign="middle" align="center">#10</td>
		<td valign="middle" align="center">17"</td>
		<td valign="middle" align="center">11"</td>
		<td valign="middle" align="center">50lb</td>
	</tr>
	<tr>
		<td valign="middle" align="center">#15</td>
		<td valign="middle" align="center">17"</td>
		<td valign="middle" align="center">16"</td>
		<td valign="middle" align="center">75lb</td>
	</tr>
	<tr>
		<td valign="middle" align="center">#20</td>
		<td valign="middle" align="center">23.5"</td>
		<td valign="middle" align="center">13"</td>
		<td valign="middle" align="center">125lb</td>
	</tr>
	<tr>
		<td valign="middle" align="center">#25</td>
		<td valign="middle" align="center">23.5"</td>
		<td valign="middle" align="center">18"</td>
		<td valign="middle" align="center">150lb</td>
	</tr>
</table>
</div>

<img src="images/products_green_roof1.jpg" width="299" height="200" border="0">
<!-- end size table -->
		</td>
	</tr>
	<tr>
		<td valign="top" align="left">
<div class="brownLinks" style="font-family: fiore-book; margin-top: 20px;">
<a href="pdf/green_roof.pdf" target="_blank">SEE OUR COMPLETE LISTING OF ROOF READY PLANTS <img src="images/lightRoundedArrow.gif" width="13" height="12" border="0" style="position: relative; top: 2px;"></a>
</div>
		</td>
		<td valign="top" align="left" style="border-left: solid #d2ced0 1px;">
			<img src="images/spacer.gif" width="1" height="400" border="0">
		</td>
	</tr>
	<tr>
		<td><img src="images/spacer.gif" width="532" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="266" height="1" border="0"></td>
	</tr>
</table>

<?php makeCatalogFooter(); ?>

</body>
</html>
