<?php
require_once('functions.php');
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Our Partners - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraHead(); ?>
</head>
<body>
<?php makeHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" style="padding: 20px 10px 80px 20px; font-size: 13px;">
<a name="eden"></a>
<h4>Eden Stone Company</h4>
<br><img src="images/partners_detail0.jpg" width="315" height="206" border="0" style="float: right; margin: 0px 64px 0px 32px;">
<span>Located in Eden, Wisconsin, Eden Stone Company maintains year-round mining operations at eight quarry locations throughout Southeastern and Central Wisconsin. Through its relationship with Eden, Fiore is able to offer its customers the largest variety of landscape stone such as flagstone, outcropping, cut drywall, steps, natural stone pavers and architectural, landscape, full and thin building veneer, and dimensional cut stone in the region. With a wide array of colors and textures, Fiore is sure to have what you are looking for your next landscape project.</span>
<br><a href="http://www.edenstone.net" target="_blank"><img src="images/partners_detail0.gif" width="120" height="70" border="0" alt="Eden Stone Company" style="margin-top: 4px;"></a>

<div style="width: 100%; height: 1px; border-bottom: solid #4e4244 1px; clear: both; margin: 20px 0px;"></div>

<img src="images/partners_detail1.jpg" width="330" height="205" border="0" style="float: left; margin: 0px 15px 0px 0px;">
<a name="valders"></a>
<h4>Valders Stone</h4>
<br><span>A subsidiary of Eden Stone Company, Valders Stone partners with Fiore to offer the highest quality limestone in the world, quarried in Valders, Wisconsin, and manufactured into architectural and dimensional cut stone. Fiore’s exclusive relationship with Valders’ staff of highly trained craftsman and artisans enables us to source and inventory the largest selection of Valders stone in the Chicagoland area. We are proud to represent and offer their superior product.</span>
<br><a href="http://www.valdersstone.com" target="_blank"><img src="images/partners_detail1.gif" width="115" height="75" border="0" alt="Valders Stone & Marble, Inc." style="margin-top: 4px;"></a>

<div style="width: 100%; height: 1px; border-bottom: solid #4e4244 1px; clear: both; margin: 20px 0px;"></div>

<img src="images/partners_detail2.jpg" width="320" height="200" border="0" style="float: right; margin: 0px 64px 0px 32px;">
<a name="midwest"></a>
<h4>Midwest Trading</h4>
<br><span>Through our partnership with Midwest Trading Horticultural Supplies, Fiore is able to offer a wide range of hardgoods, such as premium hardwood and pine mulches, landscape, and rooftop growing media, CU Structural soil, and composts (mushroom, blended and organic). Midwest Trading has served the Green Industry in the Midwest for more than twenty years and takes great pride in the high quality products that it provides.</span>
<br><a href="http://www.midwest-trading.com" target="_blank"><img src="images/partners_detail2.gif" width="110" height="75" border="0" alt="Midwest Trading Hortiultural Supplies" style="margin-top: 4px;"></a>


<div style="width: 100%; height: 1px; border-bottom: solid #4e4244 1px; clear: both; margin: 20px 0px;"></div>
<img src="images/partners_detail4.jpg" width="332" height="202" border="0" style="float: right; margin: 0px 64px 20px 32px;">
<a name="pavers"></a>
<h4>Belgard Hardscapes &amp; Glen-Gery Clay Pavers</h4>
<br><span>One of Fiore‘s newest partners, Belgard Hardscapes has been in business since 1995, manufacturing interlocking pavers, paving stone and garden wall products. These products include the traditional and antiqued paver and wall series. All Belgard products have a long history of successful applications on thousands of residential and commercial projects throughout the United States and Canada. 
<br>
<br>Founded in 1890, Glen-Gery Corporation is the largest molded brick and fifth largest brick manufacturer in the U.S. Glen-Gery holds the distinction of an honored and trusted name in brickmaking throughout the building industry. Its diverse product line of over 300 products includes extruded, machine molded and glazed face brick, brick pavers, and a complete assortment of brick shapes and custom shape units. 
</span>
<br><a href="http://belgard.biz/index.htm" target="_blank"><img src="images/partners_detail4.gif" width="135" height="52" border="0" alt="BELGARD" style="margin-top: 14px;"></a>


<div style="width: 100%; height: 1px; border-bottom: solid #4e4244 1px; clear: both; margin: 20px 0px;"></div>
<img src="images/partners_detail5.jpg" width="316" height="202" border="0" style="float: left; margin: 0px 15px 0px 0px;">
<a name="kellygreen"></a>
<h4>Kellygreen Design, Inc.</h4>
<br><span>Fiore is proud to partner with local artist and craftsman Leo Kelly of Kellygreen Design, a local firm that specializes
in custom woodwork and construction. Fiore and Kellygreen Design have collaborated to bring to
market an affordable series of custom made trellises. These trellises are locally produced and use sustainably
grown and harvested western red cedar wood.</span>
<br>
<br><b class="brownLinks"><a href="http://www.kellygreendesigninc.com" target="_blank"><span>Learn More</span> &gt;</a></b>
<br>


<div style="clear: both; height: 1px;"></div>
<div class="leafItOnTop" style="top: -26px; left: 540px;"><img src="images/leafGreen2.png" width="164" height="168" border="0"></div>

		</td>
	</tr>
</table>

<?php makeFooter(); ?>

</body>
</html>
