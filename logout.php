<?php
require_once('functions.php');

session_destroy();
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Log Out - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraHead(); ?>
</head>
<body>
<?php makeHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" style="padding: 20px 10px 0px 20px;">
<h1>Log Out</h1>
<br>You have been successfully logged out.
<br>
<br><a href="login.php">click here</a> to log back in.
		</td>
	</tr>
</table>

<?php makeFooter(); ?>

</body>
</html>
