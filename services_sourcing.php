<?php
require_once('functions.php');
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Sourcing - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraHead(); ?>
</head>
<body>
<?php makeHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td><img src="images/spacer.gif" width="595" height="20" border="0"></td>
		<td><img src="images/spacer.gif" width="15" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="355" height="1" border="0"></td>
	</tr>
	<tr>
		<td valign="top" align="left" style="font-size: 13px;">
<img src="images/services_sourcing0.jpg" width="587" height="275" border="0">
<br><h4><span>Sourcing</span></h4>
<br><span><b>At Fiore we choose only the highest quality plant material and natural stone available in the market</b>. We do this by sourcing product coast to coast and buying from up to 250 nurseries and quarries a year. 85% of our specimen material is hand selected by our buyers who choose only the best available. Over the years we have come to understand what products are valued in the Chicagoland area, and we do our best to keep those in stock. We understand, however, that your project may require more than what we currently have available in our yard. <b>Because of our extensive partner network, we are able to quickly replenish our inventories as well as obtain plant varieties and sizes and specialty stone pieces that may not be part of our normal inventory</b>. Sourcing hard to find specimens has become our specialty, so please do not hesitate to inquire about the rare or unusual. <br><b>We will do our best to track it down!</b></span>
		</td>
		<td></td>
		<td valign="top" align="left" style="font-size: 13px;"><img src="images/services_sourcing2.jpg" width="354" height="504" border="0"></td>
	</tr>
	<tr>
		<td><img src="images/spacer.gif" width="595" height="20" border="0"></td>
		<td><img src="images/spacer.gif" width="15" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="355" height="1" border="0"></td>
	</tr>
	<tr>
		<td valign="top" align="left" style="font-size: 13px;">
<img src="images/services_sourcing1.jpg" width="595" height="204" border="0" style="margin-bottom: 8px;">
<a name="purchasing_solutions"></a>
<br>
<h4><span>Purchasing Solutions Program</span></h4>
<br><span><b>Fiore offers a unique custom program designed to meet the purchasing needs of your firm</b>. Whether your business is large or small, this program is tailored to meet the specific needs of your operation relating to purchasing, product procurement, logistics, inventory management, and technical support. This program is designed to help optimize your company’s valuable resources. Outsource your purchasing to the purchasing professionals at Fiore and enjoy the following advantages:</span>
<ul>
	<li><b><span>more time to focus on core activities, i.e. design and selling</span></b></li>
	<li><b><span>realize cost and efficiency savings</span></b></li>
	<li><b><span>reduce overhead.</span></b></li>
</ul>

<br><span>These benefits will enhance your competitive advantage and lead to an increase in profits. To learn more details about this program or to find out if your firm qualifies please contact your sales associate or contact us at 847-913-1414,</span> <b class="brownLinks"><script language="javascript">
var tWho = 'sales';
var tAt = '@';
var tWhere = 'cjfiore.com';
document.write('<a href="mailto:'+tWho+tAt+tWhere+'">'+tWho+tAt+tWhere+'</a>');
</script>.
<br>&nbsp;
		</td>
		<td></td>
		<td valign="top" align="left" style="font-size: 13px;"><img src="images/services_sourcing3.jpg" width="354" height="204" border="0"></td>
	</tr>
</table>

<?php makeFooter(); ?>

</body>
</html>
