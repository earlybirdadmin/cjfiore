<?php
require_once('functions.php');
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Our History - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraHead(); ?>
</head>
<body>
<?php makeHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" style="padding: 20px 10px 20px 20px; font-size: 13px;">
<h1 style="color: #4e4244">History</h1>

<div class="darkGreyBlock" style="float: right; width: 280px; margin-left: 100px;">
<span>Fiore, which means “flower” in Italian, has established a long legacy with its customers based on our commitment to offering quality products, trusted service, and broad selection. 90 years later, Fiore remains committed to delivering the same values.</span>
</div>
<div class="leafItOnTop" style="top: 155px; left: 730px;"><img src="images/lilyOrange.png" width="159" height="146" border="0"></div>


<p style="color: #da771b; margin-top: 0px; font-size: 16px;"><span>For over four generations the Fiore family has been providing plant material to landscape architects, landscape contractors, and landscape design professionals in the Chicagoland area and across the country. The family’s roots run deep and have helped influence and shape the local and national green industry. Fiore Nursery has also helped elevate the industry’s standards and best practices which has earned Fiore the reputation as being the preferred and trusted supplier for plant material and natural stone in the area.</span></p>

<div style="clear: both; margin-bottom: 0px;">&nbsp;</div>

<div style="width: 949px; height: 537px; overflow: visible;">
<img src="images/about_history0.jpg" width="949" height="637" border="0">
</div>

<br>

<!-- bumped up 100px by the div above -->
<table cellspacing="10" cellpadding="0" border="0">
	<tr>
		<td valign="top" align="left">
<a href="pdf/fiore_family_feature.pdf" target="_blank"><img src="images/about_history_mag1.jpg" width="100" height="142" border="0"></a>
<br><a href="pdf/fiore_family_feature.pdf" target="_blank" class="rounded darkBG smallerText">DOWNLOAD</a>
		</td>
		<td valign="top" align="left">
<a href="pdf/nursery_feature.pdf" target="_blank"><img src="images/about_history_mag2.jpg" width="100" height="142" border="0"></a>
<br><a href="pdf/nursery_feature.pdf" target="_blank" class="rounded darkBG smallerText">DOWNLOAD</a>
		</td>
		<td valign="top" align="left" style="font-size: 13px;">
<b style="color: #da771b;"><span>Learn more about Fiore!</span></b>
<br><span>For more information about Fiore<br>Nursery and its history, download<br>these recent articles from some of<br>our industry trade publications.</span>
		</td>
	</tr>
</table>
		</td>
	</tr>
</table>

<?php makeFooter(); ?>

</body>
</html>
