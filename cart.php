<?php
require_once('functions_catalog.php');

if (!isset($_SESSION['lastPage'])) {
	$_SESSION['lastPage'] = 'products.php';
}

if (!isset($_SESSION['cart']))
	$_SESSION['cart'] = array();
	
// check for cart post
if (isset($_POST['doCartPost']) && $_POST['doCartPost'] == 'yup') {
	$newCartArr = array();
	for ($i=0;isset($_POST['cartItem'.$i]);$i++) {
		if (is_numeric($_POST['cartItem'.$i]) && is_numeric($_POST['item'.$i.'Quantity']) && $_POST['item'.$i.'Quantity'] > 0) {
			$newCartArr[] = array('item_id' => $_POST['cartItem'.$i], 'quantity' => round($_POST['item'.$i.'Quantity']));
		}
	}
	
	$_SESSION['cart'] = $newCartArr;
}

// combine cart items that match
for ($i=0;$i<count($_SESSION['cart']) - 1;$i++) {
	for ($j=$i+1;$j<count($_SESSION['cart']);$j++) {
		if ($_SESSION['cart'][$i]['item_id'] == $_SESSION['cart'][$j]['item_id']) {
			$_SESSION['cart'][$i]['quantity'] += $_SESSION['cart'][$j]['quantity'];
			unset($_SESSION['cart'][$j]);
			$_SESSION['cart'] = array_values($_SESSION['cart']);
		}
	}
}



// handle post changes to the cart


// finally, save the cart if we're a logged in user
saveCartToDB();
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Your Cart - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraHead(); ?>
</head>
<body>
<?php makeHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" style="padding: 20px 10px 0px 20px;">
<a href="<?php echo $_SESSION['lastPage']; ?>"><img src="images/btnContinueShopping.gif" width="142" height="24" border="0" style="float: right;"></a>
<h1>My Cart</h1>

<!-- cart table -->
<form method="POST" action="checkout.php" id="checkoutForm">
<input type="hidden" name="doCartPost" value="yup">
<table width="735" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td><img src="images/spacer.gif" width="15" height="20" border="0"></td>
		<td><img src="images/spacer.gif" width="330" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="180" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="100" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="80" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="45" height="1" border="0"></td>
	</tr>
	<tr>
		<td class="roundLeft4 darkBG">&nbsp;</td>
		<td class="darkBG"><b style="font: 11px fiore-bold, Arial, sans-serif; font-weight: bold;">ITEM</b></td>
		<td class="darkBG"><b style="font: 11px fiore-bold, Arial, sans-serif; font-weight: bold; padding-left: 50px;">DETAILS</b></td>
		<td class="darkBG"><b style="font: 11px fiore-bold, Arial, sans-serif; font-weight: bold;">QUANTITY</b></td>
		<td class="darkBG"><b style="font: 11px fiore-bold, Arial, sans-serif; font-weight: bold;">COST</b></td>
		<td class="roundRight4 darkBG"></td>
	</tr>
<?php
if (isWholesale())
	$priceIdx = 'wholesale_price';
else
	$priceIdx = 'retail_price';

for ($i=0;$i<count($_SESSION['cart']);$i++) {
	// validate data
	if (!is_numeric($_SESSION['cart'][$i]['item_id']) || !is_numeric($_SESSION['cart'][$i]['quantity']))
		continue;
	
	$item = getItemArrFromSizeID($_SESSION['cart'][$i]['item_id']);
	if (count($item) > 0) {
		echo '<tr>';
			echo '<td></td>';
			echo '<td valign="middle" align="left" style="border-bottom: solid #d9d7d7 1px; padding: 15px 0px 20px 0px;"><div class="brownLinks" style="font-size: 11px;">'.getBreadCrumbsForItemWithID($item['item_id']).'</div><h4>'.$item['item_name'].'</h4>';
			if (isset($item['item_name2']))
				echo '<br>'.$item['item_name2'];
			echo '</td>';
			
			
			echo '<td valign="middle" align="left" style="border-bottom: solid #d9d7d7 1px;">';
				echo '<table cellspacing="0" cellpadding="3" border="0"><tr><td><img src="images/spacer.gif" width="45" height="1" border="0"></td><td><img src="images/spacer.gif" width="135" height="1" border="0"></td></tr><tr>';
				echo '<td valign="middle" align="right" style="padding-right: 10px;"><span style="font-size: 12px;">Size</span></td>';
				echo '<td valign="middle" align="left" style="padding-bottom: 8px;"><span style="font: 12px fiore-book, sans-serif;">'.$item['item_size'].'</span></td></tr>';
				echo '<tr><td valign="middle" align="right" style="padding-right: 10px;"><span style="font-size: 12px;">Price</span></td>';
				echo '<td valign="middle" align="left"><span style="font: 12px fiore-book, sans-serif;">$'.$item[$priceIdx].'</span></td></tr></table>';
			echo '</td>'; 
			
			echo '<td valign="middle" align="left" style="border-bottom: solid #d9d7d7 1px; padding-left: 10px;"><input type="text" name="item'.$i.'Quantity" id="item'.$i.'Quantity" size="2" maxlength="3" value="'.$_SESSION['cart'][$i]['quantity'].'" style="padding: 6px; text-align: center;"></td>';
			
			echo '<td valign="middle" align="left" style="border-bottom: solid #d9d7d7 1px;"><span style="font: 12px fiore-book, Arial;">$'.number_format($item[$priceIdx] * $_SESSION['cart'][$i]['quantity'],2, '.', ',').'</span></td>';
			
			echo '<td valign="middle" align="right" style="border-bottom: solid #d9d7d7 1px;"><span class="blueLinks" style="font-size: 12px;"><a href="javascript:;" onclick="updateCart();">Update</a><br><a href="javascript:;" onclick="deleteCartItem('.$i.');">Delete</a></span><input type="hidden" name="cartItem'.$i.'" id="cartItem'.$i.'" value="'.$item['id'].'"></td>';
		echo '</tr>';
	}
}
?>
	<tr>
		<?php
if ($i > 0) {
	echo '<td valign="top" align="right" colspan="6"><input type="image" src="images/btnCheckout.gif" width="89" height="41 border="0" style="position: relative; left: 8px; top: 10px;"></td>';
} else {
	echo '<td valign="top" align="left" colspan="6">&nbsp;<br>No items in your cart.</td>';
} ?>
	</tr>
</table>
</form>
<!-- end cart table -->

		</td>
		<td valign="top" align="left" style="padding: 120px 0px 0px 10px;">
			<div class="beigeBlock">
Plant orders are subject to
availability at time of order.
Prices are for pick up at
the Prairie View or Chicago
location. For delivery
infomation, please contact
us.
			</div>
		</td>
	</tr>
	<tr>
		<td valign="top" align="right">
			<div class="beigeBlock" style="width: 380px; margin: 20px 10px 20px 0px; text-align: left;">
			<b>Please note:</b> Order requests submitted online are not processed as a firm order until a Fiore Customer Service Representative acknowledges receipt of the request confirms availability of product. <b style="line-height: 1.4;">Fiore will email a order acknowledgment and confirmation to you.</b> Please call if you have any questions. <br>Thank you.</div>

		</td>
		<td></td>
	</tr>
	<tr>
		<td><img src="images/spacer.gif" width="755" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="200" height="1" border="0"></td>
	</tr>
</table>


<?php makeFooter(); ?>

</body>
</html>
