<?php
require_once('functions.php');
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Charles J. Fiore Company, Inc- wholesale premium tree & shrub nursery and garden center near Chicago Illinois</title>

<?php extraHead(); ?>

<style type="text/css">
body {
	background: #4e4244;
}
#topMenu {
	background-image: URL('images/menuBG.png');
	background-repeat: repeat;
	*box-shadow: none;
}
#topBG {
	height: 120px;
}
#footerTable {
	top: 5px;
	border-top: none;
}
#contentBackground {
	height: 480px;
}
</style>
</head>
<body onload="nextIndexScroll();resizeIndexBG();" onresize="resizeIndexBG();">

<table width="100%" height="500" cellspacing="0" cellpadding="0" border="0" style="position: absolute; top: 120px; left: 0px; z-index: 14;" id="indexLeafTable">
	<tr>
		<td valign="top" align="left"><img src="images/indexLeaf0.png" width="334" height="500" border="0"></td>
		<td valign="bottom" align="center"><img src="images/indexLeaf1.png" width="314" height="500" border="0"></td>
		<td valign="top" align="right"><img src="images/indexLeaf2.png" width="314" height="500" border="0"></td>
	</tr>
</table>

<?php makeHeader(); ?>


<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" style="padding: 0px;">
<img src="images/indexBox3.png" width="255" height="188" border="0" style="position: relative; top: -30px; left: 88px;" id="indexBox2">
		</td>
		<td valign="top" align="left" style="padding: 0px 0px 0px 290px;">
<!-- EMAIL SIGNUP -->
<div style="font: 28px fiore-bold, sans-serif; color: #ffffff; margin: 130px 0px 20px 0px; text-shadow: 1px 1px 3px #000000;">
Subscribe to Fiore<br>News for special offers<br>and tips!
</div>

<div id="emailSignup">Sign Up for Fiore Emails&nbsp;&nbsp;<a href="login.php"><img src="images/btnSubmit.png" width="75" height="22" border="0" alt="submit" style="position: relative; top: 6px;"></a></div>
<!-- END EMAIL SIGNUP -->

<br><br>

<!-- NEWS -->
<div style="position: absolute; bottom: 80px; left: 0px; width: 100%;">
<div id="newsHolder">
<table width="950" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td valign="top" align="left" style="border-right: dotted #4e4244 1px;">
			<div class="newsItem">
			<b>The holiday season is fast approaching!</b>
			<br>Fiore is featuring an expanded and competitively priced selection of Holiday Greens and Accessories for all of your winter projects! 
			
			<br>
			<br><span class="blueLinks"><a href="pdf/CJF_2013_HolidayGreens.pdf" target="_blank">Learn More &gt;</a></span>
			</div>
		</td>
		<td valign="top" align="left" style="border-right: dotted #4e4244 1px;">
			<div class="newsItem" style="width: 200px;">
			<b>Catch Fiore at the ILCA 2013 Annual Party and Member Meeting</b>
			<br>Thursday, November 7, 2013 
			<br>
			<br>
			<br><span class="blueLinks"><a href="https://www.ilca.net/annual_meeting/annual_meeting.aspx" target="_blank">Learn more &gt;</a></span>
			</div>
		</td>
		<td valign="top" align="left">
			<div class="newsItem" style="border-bottom: none; width: 350px;">
			<b>Fiore Winter Supplies</b>
			<br>Both Fiore locations will have Rock Salt, Ice Melt and Calcium Chloride in stock and available for pick up or advance order! 
			<br>Stock up and SAVE when you purchase a full pallet!
			<br>
			<br><span class="blueLinks"><a href="pdf/Fiore_Winter_Supply.pdf" target="_blank">Learn more &gt;</a></span>
			</div>
		</td>
	</tr>
</table>
</div>
</div>
<!-- END NEWS -->
		</td>
	</tr>
</table>

</div><!-- close content -->

<?php makeFooter(); ?>


<!-- footer textrured strip -->
<div style="position: absolute; top: 620px; width: 100%; height: 100px; background: #ddd278 url('images/topBG.gif') repeat;">
&nbsp;
</div>
<!-- end footer textrured strip -->


<!-- index footer -->
<div style="position: absolute; top: 740px; left: 0px; width: 100%; height: 23px; z-index: 50;">
	<div class="footerIndex" style="">
		<img src="images/socialsIndex.gif" width="73" height="23" border="0" usemap="#socials" style="vertical-align: middle; margin-right: 15px; position: relative; top: -3px;">
PRAIRIE VIEW: 16606 W HIGHWAY 22 | PRAIRIE VIEW, IL 60069 | 847.913.1414 | CHICAGO: 2901 W FERDINAND ST | CHICAGO, IL 60612 | 773.533.1414 | <a href="contact.php">CONTACT US</a> | ©<?php echo date('Y'); ?>
	</div>
</div>
<!-- end index footer -->
<?php



	if ($_SERVER['REMOTE_ADDR'] == '::1' || $_SERVER['SERVER_ADDR'] == '10.0.1.2' || $_SERVER['SERVER_ADDR'] == '68.168.98.213') { } else {
?>
<script type="text/javascript">
 
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-39264121-1']);
  _gaq.push(['_trackPageview']);
 
  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
 
</script>
<?php
	} // end if we're not on a staging site
?>

<div style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; background: url('images/bgBlack80.png') repeat; z-index: 900;" id="popUp" onclick="closePopUp();">

	<div style="background: #ffffff; position: absolute; top: 36px; left: 50%; width: 800px; margin-left: -400px; border-radius: 10px;">
		<img src="images/popup.jpg" width="700" height="525" border="0" style="width: 700px; height: 525px; margin: 40px auto; display: block;">
		<a href="javascript:;" onclick="closePopUp();"><img src="images/x.png" width="42" height="42" border="0" style="position: absolute; top: -20px; right: -20px;"></a>
	</div>

</div>
<script language="javascript">
function closePopUp() {
	document.getElementById('popUp').style.display = 'none';
}
</script>
</body>
</html>
