<?php
if (isset($_GET['extracat']) && is_numeric($_GET['extracat'])) {
	$_GET['c'] = $_GET['extracat'];
}
require_once('functions_catalog.php');

if (isset($_GET['genus'])) {
	if (isset($_GET['extracat']) && is_numeric($_GET['extracat'])) {
		$result = mysql_query('SELECT * FROM `Items`, `Plants` WHERE Plants.genus="'.mysql_real_escape_string($_GET['genus']).'" AND Items.is_available=1 AND Items.category_id='.$_GET['extracat'].' AND Plants.item_id=Items.item_id');
	} else {
		$result = mysql_query('SELECT * FROM `Items`, `Plants` WHERE Plants.genus="'.mysql_real_escape_string($_GET['genus']).'" AND Items.is_available=1 AND Plants.item_id=Items.item_id');
	}
} else if (isset($_GET['featured'])) {
	// get the category name
	$result = mysql_query('SELECT * FROM `Categories` WHERE category_id=100');
	$row = mysql_fetch_array($result);
	$categoryText = nl2br($row['category_text']);


	$result = mysql_query('SELECT * FROM `Items`, `Plants` WHERE Items.is_featured=1 AND Items.is_available=1 AND Plants.item_id=Items.item_id');

} else if (isset($_GET['c']) && is_numeric($_GET['c'])) {
	$result = mysql_query('SELECT * FROM `Items`, `Plants` WHERE Items.category_id='.$_GET['c'].' AND Items.is_available=1 AND Plants.item_id=Items.item_id');
} else {
	die();
}

$plantsArr = array();
$k = 0;
for ($i=0;$i<mysql_num_rows($result);$i++) {
	$tmpArr = mysql_fetch_array($result);
	$tmpArr['botanical_name'] = $tmpArr['genus'].' '.$tmpArr['species'];
	$tmpArr['common_name'] = $tmpArr['item_name'];
	
	if (hasSizesAvailable($tmpArr['item_id'])) {
		$plantsArr[] = $tmpArr;
	}
	
}

$title = $plantsArr[0]['genus'];
if (isset($categoryText)) {
	$title = 'Featured Plants: '.$categoryText;
}	
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title><?php echo $title; ?> - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraCatalogHead(); ?>
</head>
<body>
<?php 
makeCatalogHeader(); 
if (isset($categoryText))
	echo '<div style="border-bottom: solid #d2ced0 1px; margin-bottom: 20px; margin-top: -20px;"><h1>Featured Plants:<br>' . $categoryText . '</h1></div><style type="text/css">.breadCrumbs { border: none; }</style>';

?>

<?php
for ($i=0;$i<count($plantsArr);$i++) {
?>
<div style="padding: 0px 0px 15px 0px; border-bottom: solid #d2ced0 1px;">
<table width="800" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td valign="top" align="left" style="border-right: solid #d2ced0 1px; padding: 0px 15px 20px 0px;">
<!-- PLANT NAME -->
<br><h1><?php echo $plantsArr[$i][PRIMARY_NAME_TYPE]; ?></h1>
<br><span style="font: 12px fiore-light;"><i><?php echo $plantsArr[$i][SECONDARY_NAME_TYPE]; ?></i></span>
<br>
<br>
<!-- PLANT IMAGE -->
<?php
$imgSrc = 'assets/catalog/'.$plantsArr[$i]['image_file'];
// get width and height
if (!file_exists($imgSrc)) {
	$imgHTML = '<img src="images/nophoto.jpg" width="153" height="173" border="0">';
} else {
	list($ow, $oh, $type, $attr) = getimagesize($imgSrc, $info);
	
	$scale = max(153/$ow, 173/$oh); // 153 is the width (+2 for the border), 155 is the height (+2 for border)
	$w = $scale * $ow;
	$h = $scale * $oh;
	
	$x = (153 - $w)/2;
	$y = (173 - $h)/2;
	
	$imgHTML = '<img src="'.$imgSrc.'" width="'.$w.'" height="'.$h.'" border="0" style="position: relative; top: '.$y.'px; left: '.$x.'px;">';
}
?>
<div class="catalogFloatImage">
<?php echo $imgHTML; ?>
</div>
<!-- PLANT SPECS -->
<?php
$infoArr = array('mature_height', 'mature_spread', 'growth_habit', 'growth_rate', 'light_preference', 'flower_color', 'bears_fruit_in', 'fruit', 'foliage', 'flowers_in', 'fall_color', 'growth_zone');

for ($j=0;$j<count($infoArr);$j++) {
	$idx = $infoArr[$j];
	if (isset($plantsArr[$i][$idx]) && $plantsArr[$i][$idx] != '')
		echo convertFromGetVar($idx).': <b>'.$plantsArr[$i][$idx].'</b><br>';
}
?>

<!-- PLANT DESCRIPTION -->
<div style="clear: both; margin-top: 8px;">
<?php echo $plantsArr[$i]['description']; ?>
</div>
		</td>
		<td valign="top" align="left" style="padding: 0px 15px;">
<!-- PLANT PRICES/SIZES -->
<?php
// get pricing table
$result = mysql_query('SELECT * FROM `Sizes` WHERE item_id='.$plantsArr[$i]['item_id'].' AND is_available=1 ORDER BY sort_order ASC, id ASC');
if (mysql_num_rows($result) > 0) {
?>
<table width="350" cellspacing="0" cellpadding="0" border="0" class="catalogPricingTable">
<?php
if (isWholesale())
	$priceIdx = 'wholesale_price';
else
	$priceIdx = 'retail_price';
	
/* headers */
echo '<tr>';
	echo '<td valign="middle" align="left"><b>Size</b></td>';	
	if (isWholesale()) {
		echo '<td valign="middle" align="left"><b>Price</b></td>';	
		echo '<td valign="middle" align="left"><b>Quantity</b></td>';	
		echo '<td valign="middle" align="left"><b>Total</b></td>';	
	}
echo '</tr>';	

/* item sizes */
for ($j=0;$j<mysql_num_rows($result);$j++,$k++) {
	$row = mysql_fetch_array($result);
	echo '<tr>';
	echo '<td valign="middle" align="left" style="padding-right: 10px;">'.$row['item_size'].'</td>';
	if (isWholesale()) {
		echo '<td valign="middle" align="left">$<span id="item'.$i.'Price">'.number_format($row[$priceIdx], 2).'</span></td>';
		echo '<td valign="middle" align="left"><input type="text" name="item'.$i.'Quantity" id="item'.$i.'Quantity" size="2" onkeyup="return updateItemTotal('.$i.');" onchange="updateItemTotal('.$i.');" maxlength="3"></td>';
		echo '<td valign="middle" align="left"><span id="item'.$i.'Total"></span><input type="hidden" name="size'.$i.'" id="size'.$i.'" value="'.$row['id'].'"></td>';
	}
	echo '</tr>';
}

/* footer/add to cart */
if (isWholesale()) {
	echo '<tr>';
		echo '<td valign="middle" align="right" colspan="4" style="border: none;">';	
			echo '<a href="javascript:;" onclick="addItemsToCart();"><img src="images/btnAddToCart.gif" width="103" height="37" border="0" alt="Add to Cart">';	
		echo '</td>';	
	echo '</tr>';	
	
	echo '<tr>';
		echo '<td style="border: none;"><img src="images/spacer.gif" width="120" height="1" border="0"></td>';	
		echo '<td style="border: none;"><img src="images/spacer.gif" width="90" height="1" border="0"></td>';	
		echo '<td style="border: none;"><img src="images/spacer.gif" width="80" height="1" border="0"></td>';	
		echo '<td style="border: none;"><img src="images/spacer.gif" width="60" height="1" border="0"></td>';	
	echo '</tr>';	
}
?>
</table>
<?php
}
?>
<!-- END PLANT PRICES/SIZES -->

		</td>
	</tr>
	<tr>
		<td><img src="images/spacer.gif" width="420" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="380" height="1" border="0"></td>
	</tr>
</table>
</div>
<?php
} // end for $i

if ($i == 0) {
	echo '<b>No Plants in Stock.</b><br><br>Check back soon!';
}
?>


<?php makeCatalogFooter(); ?>

</body>
</html>
