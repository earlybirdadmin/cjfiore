<?php
require_once('functions_catalog.php');

// make sure we have a category set
if (!isset($_GET['c']) || !is_numeric($_GET['c'])) {
	$_GET['c'] = 1;
	$extraLink = '';
} else {
	$extraLink = '&extracat='.$_GET['c'];
}

// get the category name
$result = mysql_query('SELECT * FROM `Categories` WHERE category_id='.$_GET['c']);
$row = mysql_fetch_array($result);
$categoryName = $row['category_name'];
$categoryText = $row['category_text'];

$query = 'SELECT DISTINCT Plants.genus, Plants.genus_common_name
			FROM `Plants`, `Items` 
			WHERE Items.is_available = 1 AND Items.category_id='.$_GET['c'].' AND Plants.item_id = Items.item_id';

if (PRIMARY_NAME_TYPE == 'botanical_name')
	$query .= ' ORDER BY Plants.genus ASC';
else
	$query .= ' ORDER BY Plants.genus_common_name ASC';
$result = mysql_query($query);
//die($query);

//build array of all plants
$genuses = array();
$allPlants = array();
for ($i=0;$i<mysql_num_rows($result);$i++) {
	$row = mysql_fetch_array($result);
	if ($row['genus_common_name'] == '')
		$row['genus_common_name'] = $row['genus'];
		
	$tmpArr = array('botanical_name' => $row['genus'], 'common_name' => $row['genus_common_name']);
	if ($tmpArr['botanical_name'] == '' && $tmpArr['common_name'] == '') {
	
	} else {
		$allPlants[] = $tmpArr;
	}
}


// create the plantArr (an array of 4 rows)
$plantArr = array(array(), array(), array(), array());
for ($i=0;$i<count($allPlants);$i++) {
	$idx = $i % 4; // 4 rows
	$plantArr[$idx][] = $allPlants[$i];
}
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title><?php echo $categoryName; ?> - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraCatalogHead(); ?>
</head>
<body>
<?php makeCatalogHeader(); ?>

<h1><?php echo $categoryName; ?></h1>
<br><?php echo $categoryText; ?>

<?php
if (count($plantArr[0]) == 0) {
	if ($categoryText == '')
		echo '<br><br><b>There are no '.$categoryName.' in stock.</b><br><br>Check back soon!<br>';
} else {
?>
<table width="800" cellspacing="0" cellpadding="0" border="0">
	<tr>
		<td valign="top" align="left" style="border-right: solid #d2ced0 1px;">
<?php
// create the plant html
for ($i=0;$i<count($plantArr[0]);$i++) {
	echo '<a class="plantIndex" href="browse.php?genus='.$plantArr[0][$i]['botanical_name'].$extraLink.'">';
	echo '<b>'.$plantArr[0][$i][PRIMARY_NAME_TYPE].'</b>';
	echo '<br><span style="font-family: fiore-light;">'.$plantArr[0][$i][SECONDARY_NAME_TYPE].'</span>';
	echo '</a>';
}
?>
		</td>
		<td valign="top" align="left" style="border-right: solid #d2ced0 1px;">
<?php
// create the plant html
for ($i=0;$i<count($plantArr[1]);$i++) {
	echo '<a class="plantIndex" href="browse.php?genus='.$plantArr[1][$i]['botanical_name'].$extraLink.'">';
	echo '<b>'.$plantArr[1][$i][PRIMARY_NAME_TYPE].'</b>';
	echo '<br><span style="font-family: fiore-light;">'.$plantArr[1][$i][SECONDARY_NAME_TYPE].'</span>';
	echo '</a>';
}
?>		</td>
		<td valign="top" align="left" style="border-right: solid #d2ced0 1px;">
<?php
// create the plant html
for ($i=0;$i<count($plantArr[2]);$i++) {
	echo '<a class="plantIndex" href="browse.php?genus='.$plantArr[2][$i]['botanical_name'].$extraLink.'">';
	echo '<b>'.$plantArr[2][$i][PRIMARY_NAME_TYPE].'</b>';
	echo '<br><span style="font-family: fiore-light;">'.$plantArr[2][$i][SECONDARY_NAME_TYPE].'</span>';
	echo '</a>';
}
?>		</td>
		<td valign="top" align="left" style="border-right: solid #d2ced0 1px;">
<?php
// create the plant html
for ($i=0;$i<count($plantArr[3]);$i++) {
	echo '<a class="plantIndex" href="browse.php?genus='.$plantArr[3][$i]['botanical_name'].$extraLink.'">';
	echo '<b>'.$plantArr[3][$i][PRIMARY_NAME_TYPE].'</b>';
	echo '<br><span style="font-family: fiore-light;">'.$plantArr[3][$i][SECONDARY_NAME_TYPE].'</span>';
	echo '</a>';
}
?>		</td>
	</tr>
	<tr>
		<td><img src="images/spacer.gif" width="199" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="199" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="199" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="199" height="1" border="0"></td>
	</tr>
</table>
<?php
} // end if we HAVE plants
?>

<?php makeCatalogFooter(); ?>

</body>
</html><?php
// compares all words to find the first difference
function getCommonNameFromGenusesArray($genArr) {
	$word = '';
	for ($i=0;$i<count($genArr) - 1;$i++) {
		for ($j=$i+1;$j<count($genArr);$j++) {
			$positionOfFirstDifference = strspn($genArr[$i] ^ $genArr[$j], "\0");
			if ($positionOfFirstDifference > strlen($word))
				$word = substr($genArr[$i], 0, $positionOfFirstDifference);
		}
	}
	
	if (strlen($word) > 0)
		return $word;
	else
		return '';
}
