<?php
require_once('functions_catalog.php');

// get the category name
if (!isset($_GET['c']) || !is_numeric($_GET['c']))
	$_GET['c'] = 7;
	
$result = mysql_query('SELECT * FROM `Categories` WHERE category_id='.$_GET['c']);
$row = mysql_fetch_array($result);
$categoryName = $row['category_name'];
$categoryText = $row['category_text'];

// get the list of products
$productArr = getProductArrayFromCatID($row['category_id']);
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title><?php echo $categoryName; ?> - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraCatalogHead(); ?>
</head>
<body>
<?php makeCatalogHeader(); ?>

<h1><?php echo $categoryName; ?></h1>
<br><?php echo $categoryText; ?>

<?php
if (count($productArr) == 0) {
	echo '<b>No Items in Stock.</b><br><br>Check back soon!';
} else {
?>
<table width="800" cellspacing="0" cellpadding="5" border="0" id="productTable">
<?php
if (isWholesale())
	$priceIdx = 'wholesale_price';
else
	$priceIdx = 'retail_price';
	
/* headers */
echo '<tr>';
	echo '<td valign="middle" align="left" style="border-bottom: solid #878787 1px;"><b>Item Name</b></td>';
	if (isWholesale()) {
		echo '<td valign="middle" align="center" style="border-bottom: solid #878787 1px;"><b>Size</b></td>';	
		echo '<td valign="middle" align="center" style="border-bottom: solid #878787 1px;"><b>Price</b></td>';	
		echo '<td valign="middle" align="center" style="border-bottom: solid #878787 1px;"><b>Quantity</b></td>';	
		echo '<td valign="middle" align="center" style="border-bottom: solid #878787 1px;"><b>Total</b></td>';	
	} else {
		echo '<td valign="middle" align="center" style="border-bottom: solid #878787 1px;" colspan="4"><b>Size</b></td>';	
	}
echo '</tr>';	

	
for ($i=0,$k=0;$i<count($productArr);$i++) {
	if ($productArr[$i]['itemArr']['is_available'] == 0)
		continue;
		
	echo '<tr>';
	if (count($productArr[$i]['sizesArr']) == 0) {
		// it's a label
		echo '<td valign="top" align="left" colspan="5" style="border-bottom: solid #878787 1px;">';
		echo '<b>'.$productArr[$i]['itemArr']['item_name'].'</b>&nbsp;';
		
		if (trim($productArr[$i]['itemArr']['description']) != '')
			echo '<br><i>'.nl2br($productArr[$i]['itemArr']['description']).'</i>';
		
		echo '</td>';
	} else {
		// item name
		echo '<td valign="top" align="left" style="border-bottom: solid #878787 1px;">';
		echo $productArr[$i]['itemArr']['item_name'];
		if (trim($productArr[$i]['itemArr']['description']) != '')
			echo '<br><i>'.nl2br($productArr[$i]['itemArr']['description']).'</i>';
		echo '</td>';
		
		echo '<td valign="top" align="left" style="border-bottom: solid #878787 1px;" colspan="4">';
			echo '<table width="380" cellspacing="0" cellpadding="0" border="0">';
		// go through each SIZE
		for ($j=0;$j<count($productArr[$i]['sizesArr']);$j++) {
			echo '<tr>';
			if (isWholesale()) {
				// item size
				echo '<td valign="middle" align="center">';
				echo $productArr[$i]['sizesArr'][$j]['item_size'];
				echo '</td>';
				// item price
				echo '<td valign="middle" align="center">';
				echo '<span id="item'.$k.'Price">'.number_format($productArr[$i]['sizesArr'][$j][$priceIdx], 2).'</span>';
				echo '</td>';
				// item quantity
				echo '<td valign="middle" align="center">';
				echo '<input type="text" name="item'.$k.'Quantity" id="item'.$k.'Quantity" size="10" onkeyup="return updateItemTotal('.$k.');" onchange="updateItemTotal('.$k.');" maxlength="10">';
				echo '</td>';
				// item total
				echo '<td valign="middle" align="center">';
				echo '<span id="item'.$k.'Total"></span><input type="hidden" name="size'.$k.'" id="size'.$k.'" value="'.$productArr[$i]['sizesArr'][$j]['id'].'">';
				echo '</td>';
			} else {
				// not-wholesale
				// item size
				echo '<td valign="middle" align="center" colspan="4">';
				echo $productArr[$i]['sizesArr'][$j]['item_size'];
				echo '</td>';
			}
			echo '</tr>';
			//increment $k
			$k++;
		}
		echo '<tr><td><img src="images/spacer.gif" width="110" height="1" border="0"></td>';
		echo '<td><img src="images/spacer.gif" width="110" height="1" border="0"></td>';
		echo '<td><img src="images/spacer.gif" width="70" height="1" border="0"></td>';
		echo '<td><img src="images/spacer.gif" width="130" height="1" border="0"></td></tr></table></td>';
	}
	echo '</tr>';
}

/* footer/add to cart */
if (isWholesale()) {
	echo '<tr>';
		echo '<td colspan="2"></td>';
		echo '<td valign="middle" align="right" colspan="3">';
			echo '<a href="javascript:;" onclick="addItemsToCart();"><img src="images/btnAddToCart.gif" width="103" height="37" border="0" alt="Add to Cart"></a>';	
		echo '</td>';	
	echo '</tr>';	
	
	echo '<tr>';
		echo '<td style="border: none;"><img src="images/spacer.gif" width="120" height="1" border="0"></td>';	
		echo '<td style="border: none;"><img src="images/spacer.gif" width="90" height="1" border="0"></td>';	
		echo '<td style="border: none;"><img src="images/spacer.gif" width="80" height="1" border="0"></td>';	
		echo '<td style="border: none;"><img src="images/spacer.gif" width="60" height="1" border="0"></td>';	
	echo '</tr>';	
}

?>
	<tr>
		<td><img src="images/spacer.gif" width="340" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="100" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="100" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="60" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="120" height="1" border="0"></td>
	</tr>
</table>
<?php
} // end if there ARE items
?>

<?php makeCatalogFooter(); ?>

</body>
</html>
