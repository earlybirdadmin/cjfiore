<?php
require_once('functions_catalog.php');

if (!isset($_GET['i']) || !is_numeric($_GET['i'])) {
	$_GET['i'] = 1;
}

$result = mysql_query('SELECT * FROM `Items`, `Plants` WHERE Items.item_id='.$_GET['i'].' AND Plants.item_id='.$_GET['i']);
$plantArr = mysql_fetch_array($result);

$plantArr['botanical_name'] = $plantArr['genus'].' '.$plantArr['species'];
$plantArr['common_name'] = $plantArr['item_name'];


?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title><?php echo $plantArr['botanical_name'].' ('.$plantArr['common_name'].')'; ?> - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraCatalogHead(); ?>
</head>
<body>
<?php makeCatalogHeader(); ?>

<h1><?php echo $plantArr[PRIMARY_NAME_TYPE]; ?></h1>
<br><span style="font: 12px fiore-light;"><i><?php echo $plantArr[SECONDARY_NAME_TYPE]; ?></i></span>

<?php
if (isset($plantArr['description']) && $plantArr['description'] != '') {
?>
<table width="700" cellspacing="0" cellpadding="0" border="0" style="margin-top: 15px; border-bottom: solid #d2ced0 1px;">
	<tr>
		<td valign="top" align="left" style="border-right: solid #d2ced0 1px; padding: 0px 15px 20px 0px;">
<!-- PLANT DESCRIPTION -->
<?php echo $plantArr['description']; ?>
		</td>
		<td valign="top" align="left" style="padding: 0px 15px;">
<!-- PLANT IMAGE -->
<?php
if (isset($plantArr['image_file']) && $plantArr['image_file'] != '') {
	echo '<img src="assets/catalog/'.$plantArr['image_file'].'" width="200" border="0"><br>&nbsp;';
}
?>
		</td>
	</tr>
	<tr>
		<td><img src="images/spacer.gif" width="400" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="260" height="1" border="0"></td>
	</tr>
</table>
<?php
} else if (isset($plantArr['image_file']) && $plantArr['image_file'] != '') {
	echo '<br><img src="assets/catalog/'.$plantArr['image_file'].'" border="0">';
}
?>

<table width="615" cellspacing="0" cellpadding="0" border="0" style="margin: 20px 0px;">
	<tr>
		<td valign="top" align="left" style="border-right: solid #d2ced0 1px;">
<!-- PLANT SPECS -->
<?php
$infoArr = array('mature_height', 'mature_spread', 'growth_habit', 'growth_rate', 'light_preference', 'flower_color', 'bears_fruit_in', 'fruit', 'foliage', 'flowers_in', 'fall_color', 'growth_zone');

for ($i=0;$i<count($infoArr);$i++) {
	$idx = $infoArr[$i];
	if (isset($plantArr[$idx]) && $plantArr[$idx] != '')
		echo convertFromGetVar($idx).': <b>'.$plantArr[$idx].'</b><br>';
}
?>
		</td>
		<td valign="top" align="left">
<!-- PLANT PRICES/SIZES -->
<?php
// get pricing table
$result = mysql_query('SELECT * FROM `Sizes` WHERE item_id='.$plantArr['item_id'].' AND is_available=1 ORDER BY sort_order ASC, id ASC');
if (mysql_num_rows($result) > 0) {
?>
<table width="350" cellspacing="0" cellpadding="0" border="0" class="catalogPricingTable">
<?php
if (isWholesale())
	$priceIdx = 'wholesale_price';
else
	$priceIdx = 'retail_price';
	
/* headers */
echo '<tr>';
	echo '<td valign="middle" align="left"><b>Size</b></td>';	
	if (isWholesale()) {
		echo '<td valign="middle" align="left"><b>Price</b></td>';	
		echo '<td valign="middle" align="left"><b>Quantity</b></td>';	
		echo '<td valign="middle" align="left"><b>Total</b></td>';	
	}
echo '</tr>';	

/* item sizes */
for ($i=0;$i<mysql_num_rows($result);$i++) {
	$row = mysql_fetch_array($result);
	echo '<tr>';
	echo '<td valign="middle" align="left" style="padding-right: 10px;">'.$row['item_size'].'</td>';
	if (isWholesale()) {
		echo '<td valign="middle" align="left">$<span id="item'.$i.'Price">'.number_format($row[$priceIdx], 2).'</span></td>';
		echo '<td valign="middle" align="left"><input type="text" name="item'.$i.'Quantity" id="item'.$i.'Quantity" size="2" onkeyup="return updateItemTotal('.$i.');" onchange="updateItemTotal('.$i.');" maxlength="3"></td>';
		echo '<td valign="middle" align="left"><span id="item'.$i.'Total"></span><input type="hidden" name="size'.$i.'" id="size'.$i.'" value="'.$row['id'].'"></td>';
	}
	echo '</tr>';
}

/* footer/add to cart */
if (isWholesale()) {
	echo '<tr>';
		echo '<td valign="middle" align="right" colspan="4" style="border: none;">';	
			echo '<a href="javascript:;" onclick="addItemsToCart();"><img src="images/btnAddToCart.gif" width="103" height="37" border="0" alt="Add to Cart">';	
		echo '</td>';	
	echo '</tr>';	
	
	echo '<tr>';
		echo '<td style="border: none;"><img src="images/spacer.gif" width="120" height="1" border="0"></td>';	
		echo '<td style="border: none;"><img src="images/spacer.gif" width="90" height="1" border="0"></td>';	
		echo '<td style="border: none;"><img src="images/spacer.gif" width="80" height="1" border="0"></td>';	
		echo '<td style="border: none;"><img src="images/spacer.gif" width="60" height="1" border="0"></td>';	
	echo '</tr>';	
}
?>
</table>
<?php
}
?>
<!-- END PLANT PRICES/SIZES -->

		</td>
	</tr>
	<tr>
		<td><img src="images/spacer.gif" width="240" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="380" height="1" border="0"></td>
	</tr>
</table>


<?php makeCatalogFooter(); ?>

</body>
</html>
