<?php
require_once('functions_catalog.php');

// get the category name
if (!isset($_GET['c']) || !is_numeric($_GET['c']))
	$_GET['c'] = 7;

// get the category name
$result = mysql_query('SELECT * FROM `Categories` WHERE category_id='.$_GET['c']);
$row = mysql_fetch_array($result);
$categoryName = $row['category_name'];
$categoryText = $row['category_text'];

// check all items with the same category id
$query = 'SELECT * FROM `Items`, `Sizes` WHERE Items.category_id='.$_GET['c'].' AND Items.is_available=1 AND Items.item_id=Sizes.item_id ORDER BY Items.sort_order ASC';
$result = mysql_query($query);
$itemArr = array();
for ($i=0;$i<mysql_num_rows($result);$i++) {
	$itemArr[] = mysql_fetch_array($result);
}

// check all items with the parent category id
$result = mysql_query('SELECT * FROM `Categories` WHERE parent_id='.$_GET['c']);
if (mysql_num_rows($result) > 0) {
	$additionalCatArr = array();
	for ($i=0;$i<mysql_num_rows($result);$i++) {
		$row = mysql_fetch_array($result);
		$additionalCatArr[] = $row['category_id'];
	}
	$cats = implode(' OR Items.category_id=', $additionalCatArr);
	$query = 'SELECT * FROM `Items`, `Sizes` WHERE (Items.category_id='.$cats.') AND Items.is_available=1 AND Items.item_id=Sizes.item_id';
	$result = mysql_query($query);
	for ($i=0;$i<mysql_num_rows($result);$i++) {
		$itemArr[] = mysql_fetch_array($result);
	}
}
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title><?php echo $categoryName; ?> - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraCatalogHead(); ?>
</head>
<body>
<?php makeCatalogHeader(); ?>

<h1><?php echo $categoryName; ?></h1>
<br>
<?php
if (trim($categoryText) != '') 
	echo '<p style="width: 600px;">'.$categoryText.'</p>';

if (count($itemArr) == 0) {
	echo '<br><h3>New products coming soon!</h3><br>Check back shortly';
}
?>

<table cellspacing="0" cellpadding="0" border="0" style="margin-top: 20px;">
<?php
if (isWholesale())
	$priceIdx = 'wholesale_price';
else
	$priceIdx = 'retail_price';
	
for ($i=0;$i<count($itemArr);) {
	for ($j=0;$j<3;$j++,$i++) {
	if ($i % 3 == 0)
		echo '<tr>';
	
	echo '<td valign="top" align="left" style="padding: 0px 15px 15px 0px;">';
	
	if ($i < count($itemArr)) {
		echo '<img src="assets/catalog/'.$itemArr[$i]['image_file'].'" height="200" border="0" style="margin-bottom: 14px;">';
		if (isWholesale()) {
			echo '<a href="javascript:;" onclick="addSingleItemToCart('.$i.');"><img src="images/btnAddToCart3.gif" width="78" height="31" border="0" alt="Add to Cart" class="addToCartItem"></a>';
		}
		echo '<br><h1>'.$itemArr[$i]['item_name'].'</h1>';
		echo '<br>'.$itemArr[$i]['item_size'];
		if (isWholesale()) {
			echo '<br><b>$'.$itemArr[$i][$priceIdx].'</b>';
			echo '<input type="hidden" name="size'.$i.'" id="size'.$i.'" value="'.$itemArr[$i]['id'].'"><input type="hidden" name="item'.$i.'Quantity" id="item'.$i.'Quantity" value="0">';
		}
	}
	
	echo '</td>';
	
	if ($i % 3 == 2)
		echo '</tr>';
	}
}
?>
	<tr>
		<td><img src="images/spacer.gif" width="266" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="266" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="266" height="1" border="0"></td>
	</tr>
</table>

<?php makeCatalogFooter(); ?>

</body>
</html>
