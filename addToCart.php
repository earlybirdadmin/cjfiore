<?php
header ("Pragma: no-cache");
header ("Cache-Control: no-cache, must-revalidate, max_age=0");
header ("Expires: 0");
header('Content-Type: text/javascript');

require_once('functions_catalog.php');

for ($i=0;isset($_GET['size'.$i]);$i++) {
	if (is_numeric($_GET['size'.$i]) && is_numeric($_GET['quantity'.$i]) && $_GET['quantity'.$i] > 0) {
		$newItem = array('item_id' => $_GET['size'.$i], 
						'quantity' => round($_GET['quantity'.$i]));
	
		if (!isset($_SESSION['cart'])) {
			$_SESSION['cart'] = array($newItem);
		} else {
			$_SESSION['cart'][] = $newItem;
		}
	}
}
?>
window.location = 'cart.php';