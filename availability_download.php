<?php
require_once('functions.php');

if (isset($_SESSION['user'])) {
	if (isWholesale()) {
		// output it as a file download
		$filename = '_avail/_fiorenurseryavailability2.xlsx';
		$c = file_get_contents($filename);
		header('Content-type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="Fiore Nursery Availability ' . date('Ymd', filemtime($filename)) . '.xlsx"');
		echo $c;
		die();
	}
}

if (!isset($_SESSION['user'])) {
	// log in user 
	if (isset($_POST['user']) && isset($_POST['pass']) && trim($_POST['user']) != '' && trim($_POST['pass']) != '') {
		$pass = md5($_POST['pass'].'cjf');
		$user = strtolower($_POST['user']);
	
		$result = mysql_query('SELECT * FROM `UserLogin` WHERE pass="'.mysql_real_escape_string($pass).'"');
		if (mysql_num_rows($result) == 0){
			$err0 = 'Incorrect username or password.  Please try again or <a href="password_reset.php">reset your password</a> to get access.';
		} else {
			for ($i=0;$i<mysql_num_rows($result);$i++) {
				$row = mysql_fetch_array($result);
				if ($user == strtolower($row['user']) || $user == strtolower($row['email'])) {
					// add user info to session
					$userArr = $row;
				
					logInUserWithID($userArr['user_id']);
				
			
					// bread the for loop
					break;
				} // end if user or email match
			} // end for $i
		
			if (!isset($_SESSION['user']))
				$err0 = 'Incorrect username or password.  Please try again or <a href="password_reset.php">reset your password</a> to get access.';
		} // end if mysql_num_rows > 0
	} 
	// end log in user
}
?><!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
	"http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8">
<title>Download Weekly Availability Spreadsheet - CJ Fiore, Nursery and Landscape Supply</title>
<?php extraHead(); ?>
</head>
<body>
<?php makeHeader(); ?>

<table cellspacing="0" cellpadding="0" border="0" id="contentTable">
	<tr>
		<td valign="top" align="left" style="padding: 10px 10px 0px 20px;">
<h1 style="line-height: 1;">Download Weekly Availability Spreadsheet</h1>

<table cellspacing="0" cellpadding="0" border="0" style="margin: 20px 0px 100px 0px;">
	<tr>
		<td valign="top" align="left" style="width: 600px;">
<div class="beigeBlock">
<?php
if (isset($_SESSION['user'])) {
	echo '<br>Click the button below to download our weekly availability spreadsheet.<br><br><br><a href="availability.php" class="rounded darkBG smallerText nowrap" style="color: #ffffff;">Download Weekly Availability Spreadsheet</a><br><br>&nbsp;';
} else {
	if (isset($err0)) {
		echo '<span style="color: #ff0000; font-size: 13px;">'.$err0.'</span>';
	}
?>
<form method="POST" action="">
	<b>You must sign in to download our weekly availability spreadsheet.</b>
	<br><sup style="color: #da771b;">*</sup> User name or email address:
	<br><input type="text" name="user" id="user">
	<br><sup style="color: #da771b;">*</sup> Password:
	<br><input type="password" name="pass" id="pass">
	</div>
	<input type="hidden" name="Submit" value="Enter">
	<input type="image" src="images/sign_in.png" width="68" height="20" border="0" style="margin: 10px;">
</form>

<script language="javascript">
document.getElementById('user').focus();
</script>

<?php
}
?>
</div>
		</td>
	</tr>
</table>
</form>
<div class="leafItOnTop" style="top: 50px; left: 100px;"><img src="images/leafGreen.png" width="164" height="168" border="0">
<!-- END FORM TABLE -->
		</td>
	</tr>
	<tr>
		<td><img src="images/spacer.gif" width="180" height="1" border="0"></td>
		<td><img src="images/spacer.gif" width="340" height="1" border="0"></td>
	</tr>
</table>

		</td>
	</tr>
</table>

<?php makeFooter(); ?>

</body>
</html>
